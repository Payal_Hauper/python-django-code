from django.conf import settings
from django.contrib.auth.models import User
from django.test import TransactionTestCase, TestCase
from django.test.client import Client

from django_cron.models import Cron
from django_banklink.models import Transaction
from money.models import MoneyAccount, Currency
from money import Money
from shop.models import Shop, ShopItem, ShopItemData, ShopCategory, PurchasedItem, Purchase
from venue.models import Venue, Event, PromoterEmployee, Promoter, VenuePermission
from venueadmin.forms import EventTicketForm

from datetime import datetime, timedelta, date
from decimal import Decimal
from itertools import groupby
import logging

from utils.utils import merge_moneys


class ShopItemCreationTestCase(TestCase):
    def setUp(self):
        self.owner = User.objects.create_user("testowner", "kala@example.com", "testowner")
        self.promoter = Promoter.objects.create(name='test promoter', created_by=self.owner)
        self.venue = Venue.objects.create(name="Test venue", active=True, owner=self.promoter, created_by=self.owner)
        self.venue_money_account = MoneyAccount.objects.create(balance_currency="EUR")
        self.shop = Shop.objects.create(venue=self.venue,
            margin=Decimal('0.05'),
            money_account=self.venue_money_account,
            default_currency=Currency.objects.get(code='EUR'),
        )
        # Create a shopcategory for the "ticket" items
        ShopCategory.objects.create(shop=self.venue.shop,
           name="Ticket shopcat",
           category_type=ShopCategory.TYPE_TICKET,
           created_by=self.owner,
       )

    def create_request(self, user):
        kws = {'user': user}
        request = type("FauxDjangoRequest", (), kws)
        return request

    def create_event(self, venue, owner):
        return Event.objects.create(name="Test event",
            venue=venue,
            created_by=self.owner,
            owner=self.promoter,
            start_time=datetime.now(),
            end_time=datetime.now() + timedelta(days=2),
            baseTicketPrice=01234,
        )

    def test_init_empty_form(self):
        # Test empty form - used in clean render
        empty = EventTicketForm(None, shop=self.shop)
        self.assertFalse(empty.is_valid())
        # Assert currency was rendered in form
        self.assertIn('EUR', empty.as_p())
        # Try to create incorrect empty form (shop kwarg missing)
        with self.assertRaises(KeyError):
            EventTicketForm()

    def _submit_ticket_form(self, event, instance=None, **kwargs):
        form_data = {
            'name': kwargs['name'],
            'description': kwargs['description'],
            'price': kwargs['price'],
            'in_stock': kwargs['in_stock'],
            'sell_until_0': kwargs['sell_until_0'],  # (date.today() + timedelta(days=1)).strftime("%Y-%m-%d"),
            'sell_until_1': kwargs['sell_until_1'],  # "13:33",
        }
        if 'visible' in kwargs:
            form_data['visible'] = kwargs['visible']
        if 'id' in kwargs:
            form_data['id'] = kwargs['id']
        # Create faux request
        _request = self.create_request(self.owner)
        setattr(_request, 'POST', form_data)

        # Add faux POST attr to request
        submitted_form = EventTicketForm(_request.POST, shop=self.shop, event=event, instance=instance, request=_request)
        self.assertTrue(submitted_form.is_valid())
        shopitem = submitted_form.save()

        return shopitem

    def test_submit_new_form(self):
        """
            test if *single* new shopitem and shopitem data can be saved to DB
            with ticket form
        """

        event = self.create_event(self.venue, self.promoter)

        form_data = {
            'visible': "on",
            'name': 'Cool Ticket',
            'description': 'description',
            'price': 4,
            'in_stock': 333,
            'sell_until_0': (date.today() + timedelta(days=1)).strftime("%Y-%m-%d"),
            'sell_until_1': "13:33",
        }

        self.assertEquals(ShopItem.objects.count(), 0)
        self.assertEquals(ShopItemData.objects.count(), 0)

        _shopitem = self._submit_ticket_form(event, **form_data)

        self.assertEquals(ShopItem.objects.count(), 1)
        self.assertEquals(ShopItemData.objects.count(), 1)

        #
        # The objects should be in the DB, start checking values
        #

        shopitem = ShopItem.objects.all()[0]
        shopitem_data = ShopItemData.objects.all()[0]

        self.assertEqual(shopitem, _shopitem)
        self.assertEqual(shopitem.data, _shopitem.data)

        self.assertEquals(shopitem.data, shopitem_data)
        self.assertEquals(shopitem_data.item, shopitem)

        self.assertEquals(shopitem.shop, self.shop)
        self.assertEquals(shopitem.data, shopitem_data)
        self.assertEquals(shopitem.visible, True)
        self.assertEquals(shopitem.in_stock, 333)

        self.assertEquals(shopitem_data.item, shopitem)
        # FIXME: We assert event.name here, but it might be
        # something else if name field is enabled in ticket form
        self.assertEquals(shopitem_data.name, event.name)
        # FIXME: We assert an empty string here, fix this test
        # if the description field is added to the ticket form
        self.assertEquals(shopitem_data.description, "")
        self.assertEquals(shopitem_data.price, Money(4, 'EUR'))
        self.assertEquals(shopitem_data.obsolete, False)
        self.assertEquals(shopitem_data.event, event)
        self.assertEquals(shopitem_data.sellable_until, datetime.now().replace(hour=13, minute=33, second=0, microsecond=0)+timedelta(days=1))

    def test_submit_shopitem_change_form(self):
        """
            Test if editing a *single* shopitem+data works in ticket form
        """
        event = self.create_event(self.venue, self.owner)

        form_data = {
            'visible': "on",
            'name': event.name,
            'description': 'description',
            'price': 4,
            'in_stock': 333,
            'sell_until_0': (date.today() + timedelta(days=1)).strftime("%Y-%m-%d"),
            'sell_until_1': "13:33",
        }

        self.assertEquals(ShopItem.objects.count(), 0)
        self.assertEquals(ShopItemData.objects.count(), 0)

        self._submit_ticket_form(event, **form_data)

        self.assertEquals(ShopItem.objects.count(), 1)
        self.assertEquals(ShopItemData.objects.count(), 1)

        #
        # We now have a stable ShopItem+data, let's try to edit it
        #

        shopitem = ShopItem.objects.all()[0]
        shopitem_data = ShopItemData.objects.all()[0]
        # Save the value states of the data object
        _shopitem_data_dict = shopitem_data.__dict__
        del _shopitem_data_dict['_state']

        # Add the ID field(to make the form bindable)
        form_data['id'] = shopitem.id
        # Try to change some fields ONLY in shopitem
        # This should just edit the shopitem and not create a new data
        del form_data['visible']
        form_data['in_stock'] = 222
        self._submit_ticket_form(event, instance=shopitem, **form_data)

        # Assert no new objects were created
        self.assertEquals(ShopItem.objects.count(), 1)
        self.assertEquals(ShopItemData.objects.count(), 1)

        # Refetch fresh items
        shopitem = ShopItem.objects.all()[0]
        shopitem_data = ShopItemData.objects.all()[0]

        # Assert the changes in shopitem
        ## These changed
        self.assertEqual(shopitem.in_stock, form_data['in_stock'])
        self.assertEqual(shopitem.visible, False)
        # Assert that the items still PK to each other
        self.assertEqual(shopitem.data, shopitem_data)
        self.assertEqual(shopitem, shopitem_data.item)

        # Assert the changes in shopitemdata - NONE!
        _x = shopitem_data.__dict__
        del _x['_state']
        del _x['_item_cache']
        self.assertEqual(_shopitem_data_dict, _x)


    def test_submit_shopitem_data_change_form(self):
        event = self.create_event(self.venue, self.owner)

        form_data = {
            'visible': "on",
            'name': 'Cool Ticket',
            'description': 'description',
            'price': 4,
            'in_stock': 333,
            'sell_until_0': (date.today() + timedelta(days=1)).strftime("%Y-%m-%d"),
            'sell_until_1': "13:33",
        }

        self.assertEquals(ShopItem.objects.count(), 0)
        self.assertEquals(ShopItemData.objects.count(), 0)

        _shopitem = self._submit_ticket_form(event, **form_data)

        self.assertEquals(ShopItem.objects.count(), 1)
        self.assertEquals(ShopItemData.objects.count(), 1)

        #
        # Change sell_until in form and observe a new shopitemdata being created
        #

        form_data['id'] = _shopitem.id
        form_data['sell_until_0'] = (date.today() + timedelta(days=7)).strftime("%Y-%m-%d")
        form_data['sell_until_1'] = "01:01"

        self._submit_ticket_form(event, instance=_shopitem, **form_data)

        self.assertEquals(ShopItem.objects.count(), 1)
        self.assertEquals(ShopItemData.objects.count(), 2)

        shopitem = ShopItem.objects.all()[0]
        old_shopitem_data = ShopItemData.objects.all()[0]
        new_shopitem_data = ShopItemData.objects.all()[1]

        self.assertEquals(shopitem.data, new_shopitem_data)
        self.assertEquals(old_shopitem_data.obsolete, True)
        self.assertEquals(new_shopitem_data.obsolete, False)

        self.assertEquals(new_shopitem_data.item, old_shopitem_data.item)
        self.assertEquals(new_shopitem_data.name, old_shopitem_data.name)
        self.assertEquals(new_shopitem_data.description, old_shopitem_data.description)
        self.assertEquals(new_shopitem_data.price, old_shopitem_data.price)
        self.assertNotEquals(new_shopitem_data.obsolete, old_shopitem_data.obsolete)

        # Check that categories(a m2m relationship) are copied
        self.assertTrue(old_shopitem_data.categories.all())
        self.assertItemsEqual(old_shopitem_data.categories.all(), new_shopitem_data.categories.all())

class BillingViewTestCase(TestCase):

    def setUp(self):

        ### Set up initial data

        # Full bruto sum
        self.initial_total_sum = Money(Decimal('1323.99'), 'EUR')
        # All bankfees summed
        self.initial_fees = [Money(Decimal('-50.60'), 'EUR')]
        # Total declined item amount
        self.initial_credit = Money(Decimal('-31.31'), 'EUR')
        # Prices for shopitems
        self.correct_prices = set([Decimal('5'), Decimal('7'), Decimal('11'), Decimal('13'), Decimal('17.31')])
        # Purchase fees for all Purchases (all in eur)
        self.purchase_fees = ['4.4', '0.5', '0.275', '8.53', '0.42', '19.73', '5.075', '4.6737', '4.09475', '0.3462', '2.55416']
        # Purchase neto w bankfees
        self.bruto_sum_minus_bank_fees = [Money(Decimal('1273.39'), 'EUR')]
        self.bruto_sum_minus_bank_fees_and_margin = [Money(Decimal('1140.99'), 'EUR')]
        self.bruto_sum_minus_bank_fees_and_margin_tax = [Money(Decimal('1114.51'), 'EUR')]
        # Final neto minus declines for the month
        self.final_neto_with_bankfees = [Money(Decimal('1242.08'), 'EUR')]
        self.final_neto_with_bankfees_and_margin = [Money(Decimal('1109.68'), 'EUR')]
        self.final_neto_with_bankfees_and_margin_tax = [Money(Decimal('1083.20'), 'EUR')]
        # Total amount of every item we are going to buy
        self.buy_items = {
                1: 17,
                2: 19-2, # -2 for decliners
                3: 23,
                4: 27,
                5: 29-1, # -1 for decliner
                }

        ###
        ### Set up conf
        ###

        self.old_settings = settings
        settings.BANK_FEES = {
            'events': { 'MARGIN': Decimal('0.02'),
                'MIN_FEE': Money(Decimal('0.00'), "EUR"),
                'METHOD': 'margin',
                },
            'USABANK': {'MIN_FEE': Money(Decimal('0.31'), 'USD'),
                'MARGIN': Decimal('0.07'),
                'METHOD': 'both-conditional-currency',
                },
            'UKBANK': {'MIN_FEE': Money(Decimal('0.19'), 'GBP'),
                'MARGIN': Decimal('0.015'),
                'METHOD': 'both-conditional-currency',
                },
            'EURBANK': {'MIN_FEE': Money(Decimal('0.3'), 'EUR'),
                'MARGIN': Decimal('0.03'),
                'METHOD': 'both-conditional-currency',
                },
            'paypal': {'MIN_FEE': Money(Decimal('0.20'), 'EUR'),
                'MARGIN': Decimal('0.034'),
                'METHOD': 'both-fixed-currency',
                },
            }
        try:
            settings.INSTALLED_APPS.remove('django_cron')
        except Exception as e:
            pass

        logging.disable(logging.WARNING)

        ###
        ### Set up venue side
        ###

        # Testowner
        self.owner = User.objects.create_user("testowner", "kala@example.com", "testowner")
        # Test promoter
        self.promoter = Promoter.objects.create(name='test promoter', created_by=self.owner)
        # The single testvenue
        self.venue = Venue.objects.create(name="Test venue",
                active=True,
                created_by=self.owner,
                owner=self.promoter)

        # Grant permissions to owner and promoter
        PromoterEmployee.objects.create(user=self.owner, promoter=self.promoter, created_by=self.owner,
                permission_billing=True)
        VenuePermission.objects.create(
            venue=self.venue,
            promoter=self.promoter,
            created_by=self.owner,
            permission_billing=True,
        )

        # Create money account for shop
        self.venue_money_account = MoneyAccount.objects.create(balance_currency="EUR")

        # Create shop for venue with only bankfees enabled
        self.shop = Shop.objects.create(venue=self.venue,
                margin=Decimal('0.05'),
                money_account=self.venue_money_account,
                default_currency=Currency.objects.get(code='EUR'),
                enable_vat=False,
                enable_margin=False,
                )

        # Create a shopcategory for the "ticket" items
        self._shop_cat_ticket = ShopCategory.objects.create(shop=self.venue.shop,
               name="Ticket shopcat",
               category_type=ShopCategory.TYPE_TICKET,
               created_by=self.owner,
               )

        # Testdata - NOTE: Changes to this will break assertions later
        events = (("first", Money(Decimal('5'), 'EUR')),
                ("second", Money(Decimal('7'), 'EUR')),
                ("third", Money(Decimal('11'), 'EUR')),
                ("fourth", Money(Decimal('13'), 'EUR')),
                ("fifth", Money(Decimal('17.31'), 'EUR')),
                )
        self.events = []
        # Create defined events
        for (event_name, event_ticket_price) in events:
            event = Event.objects.create(
                name=event_name,
                venue=self.venue,
                owner=self.promoter,
                created_by=self.owner,
                start_time=datetime.now(),
                end_time=datetime.now()+timedelta(days=2),
                baseTicketPrice=01234,
            )
            self.events.append(event)
            # Create shopitem for this event
            shopitem = ShopItem(shop=self.venue.shop)
            shopitem.init_data(
                name="Ticket for %s" % event_name,
                description="Descriptive description",
                price=Money(event_ticket_price.amount, event_ticket_price.currency),
                event=event,
                sellable_until=event.end_time,
                created_by=self.owner,
            )
            shopitem.save()

        ###
        ### Set up buyer side
        ###

        # Create testbuyer
        self.buyer = User.objects.get_or_create(username="testbuyer")[0]
        # Create money account for buyer
        self.buyer.money_account = MoneyAccount.objects.create(balance_currency="EUR")

        ###
        ### Buy all the things!!!
        ###

        self.acquire_items({1:12}, 'USABANK')
        self.acquire_items({1:4}, 'UKBANK')
        self.acquire_items({1:1}, 'UKBANK')
        self.acquire_items({2:17}, 'USABANK')
        self.acquire_items({2:2}, 'EURBANK')
        self.acquire_items({3:23, 4:2}, 'USABANK')
        self.acquire_items({4:25}, 'UKBANK')
        self.acquire_items({5:9}, 'EURBANK')
        self.acquire_items({5:15}, 'UKBANK')
        self.acquire_items({5:1}, 'events')
        self.acquire_items({5:4}, 'paypal')

        first_decline = list(PurchasedItem.objects.filter(item_data__item__id=2).all()[:2])
        second_decline = PurchasedItem.objects.filter(item_data__item__id=5).all()[:1]
        first_decline.extend(second_decline)
        map(lambda x: x.declined(self.owner), first_decline)

    def acquire_items(self, _dict, bank):
        purchase = self.shop.book_items(self.buyer, _dict)
        purchase.bank_transaction = Transaction.objects.create(user=self.buyer,
            bank_name=bank,
            status="C",
            amount=0xdeadbeef,
            currency='EUR',
        )
        purchase.save()
        purchase.complete_purchase()


    def test_event_prices_correct(self):
        """ NOTE: This method checks if *initial data* is consistent.
        Check that Event ticket prices are consistent with other test data values
        """
        all_event_prices = set(map(lambda x: x.data.price.amount, self.venue.shop.items.all()))

        self.assertEqual(self.correct_prices, all_event_prices)

    def test_bought_all_the_items(self):
        """ NOTE: This method checks if *initial data* is consistent.
        Check that purchased ticket amounts are consistent with rest of the test data
        """
        purchased_items = PurchasedItem.objects.filter(owner=self.buyer, status=PurchasedItem.STATUS_BILLED)
        keyfunc = lambda x: x.item_data.item.id
        bought_items = dict([(key, len(list(group))) for key, group in groupby(purchased_items, keyfunc)])

        self.assertEqual(self.buy_items, bought_items)

    def test_purchase_sums(self):
        purchases = Purchase.objects.filter(owner=self.buyer, status=PurchasedItem.STATUS_BILLED)
        actual_total_sum = sum(map(lambda x: x.price, purchases))
        self.assertEqual(self.initial_total_sum, actual_total_sum)

    def test_billing_context_w_bankfees(self):
        """
            Only bankfees enabled
        """
        c = Client()
        c.login(username="testowner", password="testowner")
        resp = c.get('/pro/venue/1/billing/')
        self.assertEqual(resp.status_code, 200)
        context = resp.context['data'][0]

        self.assertEqual(context['bankfees'], self.initial_fees)
        self.assertEqual(context['total_credit'], self.initial_credit)
        self.assertEqual(context['total_purchase_netos'], self.bruto_sum_minus_bank_fees)
        self.assertEqual(context['total'], self.final_neto_with_bankfees)

    def test_billing_context_w_bankfees_and_margin(self):
        """
            Bankfees and margin enabled
        """
        venue_shop = Venue.objects.all()[0].shop
        venue_shop.enable_margin = True
        venue_shop.margin = Decimal('0.10')
        venue_shop.save()

        c = Client()
        c.login(username="testowner", password="testowner")
        resp = c.get('/pro/venue/1/billing/')
        self.assertEqual(resp.status_code, 200)
        context = resp.context['data'][0]

        self.assertEqual(context['bankfees'], self.initial_fees)
        self.assertEqual(context['total_credit'], self.initial_credit)
        self.assertEqual(context['total_purchase_netos'], self.bruto_sum_minus_bank_fees_and_margin)
        self.assertEqual(context['total'], self.final_neto_with_bankfees_and_margin)

    def test_billing_context_with_bankfees_and_margin_and_tax(self):
        """
            Bankfees and margin enabled with tax
        """
        venue_shop = Venue.objects.all()[0].shop
        venue_shop.enable_margin = True
        venue_shop.margin = Decimal('0.10')
        venue_shop.enable_vat = True
        venue_shop.vat = Decimal('0.20')
        venue_shop.save()

        c = Client()
        c.login(username="testowner", password="testowner")
        resp = c.get('/pro/venue/1/billing/')
        self.assertEqual(resp.status_code, 200)
        context = resp.context['data'][0]

        self.assertEqual(context['bankfees'], self.initial_fees)
        self.assertEqual(context['total_credit'], self.initial_credit)
        self.assertEqual(context['total_purchase_netos'], self.bruto_sum_minus_bank_fees_and_margin_tax)
        self.assertEqual(context['total'], self.final_neto_with_bankfees_and_margin_tax)

    def test_item_fees(self):
        def eur(amount):
            return Money(Decimal(amount), 'EUR')

        purchase_fees = map(eur, self.purchase_fees)
        purchases = Purchase.objects.filter(owner=self.buyer, status=PurchasedItem.STATUS_BILLED)
        fees = [item for sublist in map(lambda x: x.get_banking_fees(), purchases) for item in sublist]

        self.assertItemsEqual(purchase_fees, fees)

