# -*- coding: utf-8 -*-

from api.v1.handlers import _ShopItemCategorie

from django import forms
from django.conf import settings
from django.contrib.auth.models import User
from django.core.cache import cache
from django.core.exceptions import PermissionDenied, ValidationError
from django.core.files import File
from django.core.urlresolvers import reverse
from django.core.validators import validate_email
from django.db import transaction
from django.db.models import Sum, F
from django.forms.forms import NON_FIELD_ERRORS
from django.forms.models import BaseModelFormSet, ModelMultipleChoiceField
from django.forms.models import modelformset_factory
from django.forms.util import ErrorList, flatatt
from django.forms.widgets import HiddenInput, RadioFieldRenderer, TextInput
from django.template import Context
from django.template.loader import render_to_string
from django.utils import formats, timezone
from django.utils.encoding import force_unicode
from django.utils.html import escape, conditional_escape
from django.utils.functional import curry
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext, ugettext_lazy as _
from django.utils.translation import ungettext

import urllib
import hashlib
import logging
import pytz
import re
import os
from datetime import date, datetime, time, timedelta
from notification.notifications import NotificationMessage
from crispy_forms.bootstrap import FormActions, AppendedText
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, MultiField, Field, Div, HTML
from crispy_forms.utils import render_field
from django_google_maps_widget.widgets import GoogleMapsWidget
from smtplib import SMTPRecipientsRefused

from account.email import send_venueadmin_invite_email, send_venueadmin_list_invite_email
from avatar.forms import AvatarField
from money import Money
from invite_member.models import InvitationKey
from money.models import Currency
from people_database.models import Person
from utils import facebook_utils
from venue.cron import FetchFacebookEvents
from venue.models import (Event, Venue, Promoter, VenueList, VenueFacebookList, VenueFreeList, VenuePromoterList,
                          VenueListMember, VenueFacebookListTag, EventAvatar, EventMobileAvatar, VenueAvatar,
                          Organization,
                          UserPermission, VisitorCategory, VisitorCategoryGroup, VenueVisitor, EventNote,
                          EventPromotionEmail, EventSeries, Withdrawal, TemporaryFileWrapper, EventCoverAvatar,
                          VenueArea, EventTag, FreeTicket)
from shop.models import ShopItem, ShopCategory, PurchasedItem, gen_item_number
from ui.forms import (PopupDateTimeWidget, BootstrapDateTimeWidget, SupplementedInputWidget,
                      ToggleCheckboxInput, ImageWithPreviewWidget)
from utils.avatars import avatar_image_path, mobile_avatar, cover_avatar
from utils.facebook_utils import extract_id_token_from_str, FacebookObjectByIdFetcher
from utils.reserved import is_slug_reserved
from utils.statsd import statsd
from utils.utils import get_obj_and_locks, send_email, metrics, yearsago


class EventTagMultipleChoiceField(ModelMultipleChoiceField):
    def label_from_instance(self, obj):
        return obj.name


class CharFieldStatusWidget(forms.Widget):
    """
        This is used to generate a charfield, with three different 'status'
        elemnts besides it.
        This should be managed with JS.
    """
    def render(self, name, value, attrs):
        loading_string = _("Loading")
        fail_string = _("Failed")
        success_string = _("Success")

        final_attrs = self.build_attrs(attrs, name=name)
        if hasattr(self, 'initial') and self.initial:
            value = self.initial
        return mark_safe(u'''<input%s type="text" name="%s" value="%s" />
                <span style="display: none;" class="field-status status-loading">%s</span>
                <span style="display: none;" class="field-status status-fail">%s</span>
                <span style="display: none;" class="field-status status-success">%s</span>''' %
                (flatatt(final_attrs), name, value, loading_string, fail_string, success_string)
        )


class AppendedPlainText(AppendedText):
    """ Appends plain text to crispy-forms input field

    This is similar to crispy's AppendedText except that the addon won't be joined with the input field.
    """
    template = "bootstrap/layout/appended_plain_text.html"


class SuggestionSearchWidget(forms.Widget):
    """
        This is used to generate a charfield that is compatible with
        the jQuery doubleSuggest.
    """

    def render(self, name, value, attrs):
        final_attrs = self.build_attrs(attrs, name=name)
        if hasattr(self, 'initial') and self.initial:
            value = self.initial
        if not value:
            value = ''
        return mark_safe(u'''<input%s type="text" autocomplete="off" value="%s" />''' %
                (flatatt(final_attrs), value))


class SuggestionSearchCreateEventForm(forms.Form):
    venue_selection = forms.CharField(label=_("Where is it?"), required=False, placeholder=_("Search venues..."), widget=SuggestionSearchWidget())
    def __init__(self, *args, **kwargs):
        super(SuggestionSearchCreateEventForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.layout = Layout(
                Field('venue_selection', css_class='input-2xlarge'))

class VenueListLineParser(object):
    """ Helper class for parsing user-input lines of a list.
    """
    # Valid list line formats are:
    #  <line> = <name_and_count>
    #  <line> = <name_and_count>,<comment>
    #  <line> = <name_and_count>(<comment>)
    #  <line> = <name_and_count> (<comment>)
    #  <line> = <name_with_count> <comment>
    #  <line> = <name_and_count> <hashtags>
    #
    #  <name_and_count> = <name>
    #  <name_and_count> = <name_with_count>
    #  <name_with_count> = <name>+<count>
    #  <name_with_count> = <name> +<count>

    LINE_REGEX = re.compile(
        r"^(?P<name>[\w \.\-`'&\/\"]+)"     # name
        r'( *\+\s?(?P<plus>\d+))?'      # additional guests
        r'('
            r' *\((?P<comment_1>.+)\) *'    # comment in brackets, possibly followed by spaces
            r'|'                            # ... or ...
                r' *(?(plus),?|,)'          # comma, mandatory if plus wasn't given
                r'(?P<comment_2>.+)'        # and comment, matching anything
        r')?'
        r'$'
        , re.UNICODE)
    # This regex is for backwards compatibility, used when the main one does not match:
    #  <line> = <name> (<comment>) +<count>
    COMPAT_LINE_REGEX = re.compile(
        r'^(?P<name>[\w \.\-]+)'            # name
        r' *\((?P<comment_1>.+)\) *'        # comment in brackets, possibly followed by spaces
        r'( *\+(?P<plus>\d+))?'             # additional guests
    , re.UNICODE)

    def parse_line(self, line, compat=False):
        """ Tries to parse the given line

        Returns None if the line could not be parsed.
        Otherwise, returns (name, plus, comment) tuple. If plus or comment are not present in the line, they are set to
        0 or '' respectively.
        """
        m = self.LINE_REGEX.match(line)
        if not m and compat:
            m = self.COMPAT_LINE_REGEX.match(line)
        if not m:
            return None, None, None

        name = m.group('name').strip()
        plus = int(m.group('plus') or 0)
        comment = (m.group('comment_1') or m.group('comment_2') or '').strip()
        if len(comment) >= 2 and comment[0] == '(' and comment[-1] == ')':
            comment = comment[1:-1]

        return name, plus, comment

    def parse_email_from_comment(self, comment):
        if not comment:
            return None

        comment_tokens = comment.split()
        email = comment_tokens[0] if comment_tokens else None
        try:
            validate_email(email)
            return email
        except ValidationError:
            return None


class BaseVenueListForm(forms.ModelForm):

    def format_members_text(self, members):
        members_lines = [ self.format_member_line(member) for member in members.itervalues() ]
        members_lines = sorted(members_lines, key=lambda m: m.lower())
        return u'\n'.join(members_lines)

    def format_member_line(self, member):
        """ Formats a member into textual line form. Does not append newline.
        """
        line = member.name
        if member.count > 1:
            line += u" +%d" % (member.count - 1)
        if member.comment:
            line += u", %s" % member.comment

        self.member_count += member.count
        return line

    def clean_members(self):
        data = self.cleaned_data['members']
        try:
            self.members = self.parse_members(data)
        except forms.ValidationError:
            # We might want to modify values of form fields (in case there are conflicts) and we need data to be mutable for
            #  that.
            self.data = self.data.copy()
            self.data['members'] = data = "\n".join(sorted(data.split("\n")))
            raise
        return data

    def clean_members_orig(self):
        data = self.cleaned_data['members_orig']
        self.members_orig = self.parse_members(data, compat=True)
        return data

    def parse_members(self, data, compat=False):
        # Maps member keys (lowercase (name, comment) tuples) to MemberData objects
        members = {}

        invalid_lines = []
        # Maps member keys to (name, comment) tuples
        duplicate_lines = dict()

        # Parse all the input lines
        line_parser = VenueListLineParser()
        for line in data.split('\n'):
            line = line.replace('\t', ' ').strip()
            # Skip empty lines
            if not len(line):
                continue

            # Try to match the line against the regex
            name, additional_guests, comment = line_parser.parse_line(line, compat)
            # We can detect invalid lines by checking if the name is None
            if not name:
                # Instead of raising an error immediately, save the problem and continue. We will later show a summary
                #  of all the problems found to the user, making it easier to fix them all at once.
                invalid_lines.append(line)
                # Also log the invalid lines so that we can analyse them later if necessary.
                logging.info("VenueListForm.parse_members(): invalid line: '%s'", line)
                statsd.incr('pro.lists.errors.line_invalid')
                continue
            # Our database has 80-char limit for the name and 4096-char limit for comment.
            if len(name) > 80 or len(comment) > 4096:
                logging.info("VenueListForm.parse_members(): too long line: '%s'", line)
                statsd.incr('pro.lists.errors.line_too_long')
                raise forms.ValidationError(mark_safe(_("A line is too long:<br>") + escape(line)))

            member_data = VenueList.MemberData(name, 1+additional_guests, comment, line=line)
            member_key = member_data.key
            # Check for duplicate names
            if member_key in members:
                duplicate_lines[member_key] = (name, comment)
                statsd.incr('pro.lists.errors.member_duplicate')
                continue

            members[member_key] = member_data

        if invalid_lines:
            # Print at most 3 lines
            error = _("Input is incorrectly formatted. Check the correct line format above. ")
            self.raise_members_format_error(invalid_lines, error, max_lines_count=3)

        if duplicate_lines:
            # Print at most 15 lines
            MAX_DUPLICATE_NAMES = 15
            error = _("No name is allowed more than once. If you wish, you can add more info after the name, e.g. phone number or birth year.<br />\n")
            duplicate_fragments = []
            for member_id in sorted(duplicate_lines.keys())[:MAX_DUPLICATE_NAMES]:
                name, comment = duplicate_lines[member_id]
                if comment:
                    duplicate_fragments.append('%s (%s)' % (name, comment))
                else:
                    duplicate_fragments.append('%s' % name)

            self.raise_members_format_error(duplicate_fragments, error, submessage='', max_lines_count=MAX_DUPLICATE_NAMES)

        return members

    def raise_members_format_error(self, lines, message, submessage=None, max_lines_count=5):
        """ Raises ValidationError() about formatting of members field.
        """

        error = message
        error += '<br />'
        if submessage is None:
            error += ungettext("The problematic line is: ", "The problematic lines are: ", len(lines))
            error += '<br />'
        else:
            error += submessage

        error += '<br />'.join(['<em>%s</em>' % escape(line) for line in lines[:max_lines_count]])
        error += '<br />'

        if len(lines) > max_lines_count:
            error += _("and %(more_count)d more.") % {'more_count': len(lines) - max_lines_count}
            error += '<br />'

        raise forms.ValidationError(mark_safe(error))

    def do_merge_transaction(self):
        # Commit everything so far
        transaction.commit()

        # Try to merge, commit if successful. If not successful then roll back only the merge-related changes.
        merged = False
        try:
            merged = self.try_merge_list()
        except VenueList.MergeConflict, conflict:
            # Compile conflict error message
            msg = force_unicode(_('Conflicting changes detected!<br />\n'))
            msg += force_unicode(_('Someone has changed the list at the same time as you did. We could not merge all the changes automatically :-(<br />\n'))
            msg += force_unicode(_('Please re-apply or ignore the following changes, then try to save again:<br />\n'))
            conflicting_changes = []
            for key in conflict.added:
                conflicting_changes.append(_('Added: %s') % self.format_member_line(self.members[key]))
            for key in conflict.removed:
                conflicting_changes.append(_('Removed: %s') % self.format_member_line(self.members_orig[key]))
            for key in conflict.modified:
                conflicting_changes.append(_('Modified: %(orig_name)s -&gt; %(new_name)s') % {
                    'orig_name': self.format_member_line(self.members_orig[key]),
                    'new_name': self.format_member_line(self.members[key]),
                })
            conflicting_changes = [ '<li>%s</li>' % change for change in conflicting_changes ]
            msg += '<ul class="merge-conflicts">\n' + '\n'.join(conflicting_changes) + '</ul>\n'
            self._errors.setdefault(NON_FIELD_ERRORS, self.error_class()).append(mark_safe(msg))

            # We also have to update the values of the form fields to reflect the _current_ state of the list.
            self.data[self.prefix + '-members'] = members_text = self.format_members_text(conflict.merged_list)
            self.data[self.prefix + '-members_orig'] = members_text = self.format_members_text(self.instance.current_members_dict())
            self.data[self.prefix + '-modified_timestamp'] = self.instance.modified_timestamp.isoformat()

            # Rollback the transaction, mark the form as having conflicts and return
            transaction.rollback()
            return merged, False

        except:
            logging.exception('Unknown exception in BaseVenueListForm.save() while merging')
            transaction.rollback()
            raise
        else:
            transaction.commit()
            return merged, True

    def try_merge_list(self):
        # Lock the VenueList instance. It will stay locked until the transaction ends
        locked_inst = get_obj_and_locks(VenueList, [self.instance.pk])[0]
        self.instance.merge_members(self.members_orig, self.members, self.user)
        return True

    @staticmethod
    def venue_active_event_lists(organization):
        """ Returns all event lists of given venue which are attached to events which haven't ended.
        """
        return VenueList.objects.filter(list_type=VenueList.TYPE_NORMAL, owner=organization, event__end_time__gt=timezone.now())

    def propagate_permanent_list_changes(self):
        """ Tries to push changes made in a permanent list to all the non-global lists with the same name.
        """
        if self.members_orig == self.members:
            return

        logging.info("Pushing list %s (%d) changes to copies...", str(self.instance), self.instance.id)
        for list in VenueListForm.venue_active_event_lists(self.instance.owner).filter(sync_from=self.instance):
            logging.info("    Pushing list changes to %s (%d)", str(list), list.id)
            statsd.incr('pro.lists.propagate_permanent.attempts')
            try:
                list.merge_members(self.members_orig, self.members, self.user)
                statsd.incr('pro.lists.propagate_permanent.successes')
            except VenueList.MergeConflict, conflict:
                # Try to do a manual merge
                logging.warning("        Conflicts while trying to update copy; trying to apply forcefully")
                # We need to check if removed or modified entries are still present in the current list. If not, we have
                #  to remove them from the diff, to avoid getting errors.
                current_list_members = list.current_members_dict()
                current_list_members_set = set(current_list_members.keys())
                removed = conflict.removed & current_list_members_set
                modified = conflict.modified & current_list_members_set
                list.apply_diff(conflict.added, removed, modified, current_list_members, self.members, self.user)
                statsd.incr('pro.lists.propagate_permanent.forced')

    def enforce_members_permissions(self):
        """ Ensures that the user editing the form has edited only her own guests.

        Used when the user has explicit permissions that must be enforced. I.e. she can only edit her own guests.
        """
        added, removed, modified = self.instance.members_diff(self.members_orig, self.members)
        # Check which guests were added by this user and which ones by others. Also count user's guests
        is_created_by_user = {}
        user_guests = 0
        for member in self.instance.members.all():
            member_created_by_user = member.created_by_id == self.user.id
            is_created_by_user[(member.full_name.lower(), member.comment.lower())] = member_created_by_user
            if member_created_by_user:
                user_guests += member.ticket_count
        forbidden_changes = []
        for key in removed:
            if not is_created_by_user[key]:
                forbidden_changes.append(_('Removed: %s') % self.format_member_line(self.members_orig[key]))
        for key in modified:
            if not is_created_by_user[key]:
                forbidden_changes.append(_('Modified: %(orig_name)s -&gt; %(new_name)s') % {
                    'orig_name': self.format_member_line(self.members_orig[key]),
                    'new_name': self.format_member_line(self.members[key]),
                })
            else:
                user_guests -= self.members_orig[key].count
                user_guests += self.members[key].count
        if forbidden_changes:
            msg = force_unicode(_("You cannot change or remove guests added by other users.<br />"))
            msg += force_unicode(_("The following changes are forbidden:"))
            forbidden_changes = ['<li>%s</li>' % change for change in forbidden_changes]
            msg += '<ul class="merge-conflicts">\n' + '\n'.join(forbidden_changes) + '</ul>\n'
            self._errors.setdefault(NON_FIELD_ERRORS, self.error_class()).append(mark_safe(msg))
            statsd.incr('pro.lists.errors.forbidden_changes')

        user_guests += sum([self.members[key].count for key in added])
        user_guests -= sum([self.members_orig[key].count for key in removed])
        if self.permission.list_limit and self.permission.list_limit < user_guests:
            msg = _("You can add only %(allowed)d guests to the list but you currently have %(current)d guests.") % {
                'allowed': self.permission.list_limit,
                'current': user_guests,
            }
            self._errors.setdefault(NON_FIELD_ERRORS, self.error_class()).append(mark_safe(msg))
            statsd.incr('pro.lists.errors.forbidden_too_many_guests')

# This is currently only used to merge members in a VenueList, so an instance has to be given.
class VenueListForm(BaseVenueListForm):
    class Meta:
        model = VenueList
        fields = []

    members = forms.CharField(required=False)
    members_orig = forms.CharField(required=False)

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        self.user = self.request.user
        self.organization = kwargs.pop('organization')
        self.save_conflicts = False
        self.member_count = 0

        self.members = kwargs['instance'].current_members_dict()
        self.members_orig = {}

        self.permission = None
        object = kwargs['instance'].event if kwargs['instance'].event else kwargs['instance'].owner
        if not object.has_user_permission(self.request.user, 'list_edit'):
            # Does the user have explicit permissions to the list?
            permissions = UserPermission.objects.filter(list=kwargs['instance'], user=self.request.user, permission_list_edit=True)
            if not permissions:
                raise PermissionDenied("%s.list_edit" % self.request.user)
            assert len(permissions) == 1
            self.permission = permissions[0]

        # Format initial list
        members_text = self.format_members_text(self.members)
        initial = kwargs.pop('initial', {})
        initial['members'] =  members_text
        initial['members_orig'] =  members_text

        super(VenueListForm, self).__init__(*args, initial=initial, **kwargs)

    def parse_members(self, data, compat=False):
        members = super(VenueListForm, self).parse_members(data, compat)

        if self.instance.list_type == VenueList.TYPE_FREE_TICKET:
            # Ensure all members have valid emails in comment
            parser = VenueListLineParser()
            invalid_members = []
            too_many_pluses = []
            for member in members.itervalues():
                if not parser.parse_email_from_comment(member.comment):
                    invalid_members.append(member)
                elif member.count > 6:
                    too_many_pluses.append(member)

            if invalid_members:
                error = _("Some lines are missing email addresses.")
                self.raise_members_format_error([member.line for member in invalid_members], error)

            if too_many_pluses:
                error = _("Free ticket list members are allowed to have up to 5 extra guests. Some have too many.")
                self.raise_members_format_error([member.line for member in too_many_pluses], error)

        return members

    def clean(self):
        if self.permission:
            # Check explicit permission's constraints
            self.enforce_members_permissions()

        return self.cleaned_data

    def save(self, commit=True):
        self.save_conflicts = False

        # Check if the list has been modified in the meantime
        merged = False
        with transaction.commit_manually():
            is_permalist = not bool(self.instance.event)
            merged, success = self.do_merge_transaction()
            if not success:
                self.save_conflicts = True
                return None
            if success and is_permalist:
                # Push changes to copies of this list
                try:
                    self.propagate_permanent_list_changes()
                except Exception as e:
                    logging.exception("Exception while trying to propagate changes of permanent list '%s' (%d)",
                                      self.instance.name, self.instance.id)
                    transaction.rollback()
                    raise

                transaction.commit()

        if self.instance.event:
            self.instance.event.lists.add(self.instance)
        else:
            # Add to all active events if a new permanent list was added
            for event in Event.objects.filter(owner_org=self.organization, end_time__gt=timezone.now()):

                # Check if this permalist isn't added to this event
                if VenueList.objects.filter(event=event, owner=self.organization, sync_from=self.instance).exists():
                    continue

                # Make a copy of the permanent list and attach it to the event
                logging.info("    %s: copying new permanent list '%s' (%d) to event %s (%d)",
                             self.organization.display_name(), self.instance.name, self.instance.pk, event.name, event.pk)
                self.instance.copy({'created_by': self.user, 'event': event, 'sync_from': self.instance})

        return self.instance


class VenueFacebookListTagForm(forms.ModelForm):
    class Meta:
        model = VenueFacebookListTag
        fields = ('tag', 'first_n')

    delete = forms.BooleanField(initial=False, required=False, widget=forms.HiddenInput)
    first_n = forms.IntegerField(min_value=1, initial=100)

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        self.min_n = kwargs.pop('min_n', 1)

        super(VenueFacebookListTagForm, self).__init__(*args, **kwargs)

        self.fields['first_n'].min_value = self.min_n
        self.fields['first_n'].widget.attrs['class'] = 'input-mini'
        self.fields['tag'].widget.attrs['class'] = 'input-small'

    def has_changed(self):
        # Always validate the form
        # When a fb tag is added in the front end, it always gets validated in the back end as well
        return True

    def clean(self):
        super(VenueFacebookListTagForm, self).clean()
        data = self.cleaned_data

        # Delete required field errors if 'delete' is checked
        if 'delete' in data and data['delete']:
            field_names = ['tag', 'first_n']
            for field_name in field_names:
                self.remove_field_error(field_name)
            return data

        return data

    def _post_clean(self):
        # _post_clean() validates fields according to the model
        # Now we can easily have a template fb tag, that is not validated against the model (it's always deleted)
        data = self.cleaned_data

        # Delete required field errors if 'delete' is checked
        if 'delete' not in data or not data['delete']:
            super(VenueFacebookListTagForm, self)._post_clean()

    def clean_tag(self):
        data = self.cleaned_data['tag']
        if ' ' in data:
            raise forms.ValidationError(ugettext("The tag must not contain spaces"))

        return data

    def clean_first_n(self):
        first_n = self.cleaned_data['first_n']
        if first_n < self.min_n and first_n != self.instance.first_n:
            raise forms.ValidationError(ugettext("Tag must be applied to at least %d attendees (current size of the Facebook list)") % self.min_n)

        return first_n

    def save(self, list_instance, commit=True):
        if self.cleaned_data['delete']:
            if self.instance.pk:
                # Delete the list
                logging.info("Deleting venue facebook list tag %d of list %d ('%s')", self.instance.pk, self.instance.list_id, self.instance.tag)
                # Can't use VenueFacebookListTag.objects.delete() here since we need to call VenueFacebookListTag.delete()
                #  to do some special processing and remove the tags from members
                self.instance.delete()

            return None

        instance = super(VenueFacebookListTagForm, self).save(False)
        instance.list = list_instance
        instance.created_by = self.user
        instance.save()
        return instance

    def remove_field_error(self, field):
        try:
            del self._errors[field]
        except:
            pass


class BaseVenueFacebookListTagFormSet(BaseModelFormSet):
    """ Formset used for editing the VenueList objects

    This is used for editing global lists on venue lists page and for editing non-global lists on event lists page.
    """
    model = VenueList

    def __init__(self, *args, **kwargs):
        self.event = kwargs.pop('event')
        self.user = kwargs.pop('user')
        self.min_n = kwargs.pop('min_n', 1)
        kwargs['queryset'] = VenueFacebookListTag.objects.filter(list__in=self.event.lists.all())

        super(BaseVenueFacebookListTagFormSet, self).__init__(*args, **kwargs)

    def _construct_form(self, i, **kwargs):
        kwargs['user'] = self.user
        kwargs['min_n'] = self.min_n

        return super(BaseVenueFacebookListTagFormSet, self)._construct_form(i, **kwargs)

    def save(self, list_instance, commit=True):
        self.list_instance = list_instance
        super(BaseVenueFacebookListTagFormSet, self).save(commit)

    def save_new(self, form, commit=True):
        """Overridden to give list_instance parameter to the form."""
        return form.save(self.list_instance, commit=commit)

    def save_existing(self, form, instance, commit=True):
        """Overridden to give list_instance parameter to the form."""
        return form.save(self.list_instance, commit=commit)

class VenueFacebookListForm(forms.ModelForm):
    """ Used for editing event's Facebook list's properties

    It has a hidden checkbox for adding/removing the FB list as well as the date/time widget for choosing until what
    time the list should be updated.
    """
    class Meta:
        model = VenueFacebookList
        fields = ('update_until',)
        widgets = {
            'update_until': BootstrapDateTimeWidget(),
        }

    use_facebook_list = forms.BooleanField(initial=False, required=False, widget=ToggleCheckboxInput)

    def __init__(self, *args, **kwargs):
        self.event = kwargs.pop('event')
        self.user = kwargs.pop('user')
        self.venue = self.event.venue
        self.member_count = 0

        event_fb_list = self.event.lists.filter(list_type=VenueList.TYPE_FACEBOOK)
        assert len(event_fb_list) <= 1

        if not 'instance' in kwargs:
            if event_fb_list:
                kwargs['instance'] = event_fb_list[0].venuefacebooklist
            else:
                kwargs['instance'] = VenueFacebookList(list_type=VenueList.TYPE_FACEBOOK, owner=self.event.owner_org,
                        created_by=self.user, name='Facebook', update_until=self.event.start_time, event=self.event)
        if not 'initial' in kwargs:
            kwargs['initial'] = {}
        kwargs['initial']['use_facebook_list'] = len(event_fb_list) > 0

        update_until = kwargs['instance'].update_until
        kwargs['initial']['update_until'] = self.event.localize_dt(update_until)

        self.member_count = VenueListMember.objects.filter(list__id=kwargs['instance'].id).aggregate(Sum('ticket_count'))['ticket_count__sum'] or 0

        super(VenueFacebookListForm, self).__init__(*args, **kwargs)

        # Create a nested formset for tags
        min_n = 1
        if self.instance.pk:
            min_n = self.instance.members.count()
        self.tags_formset = VenueFacebookListTagFormSet(data=self.data or None, files=self.files or None,
                prefix=self.prefix + '-formset', event=self.event, user=self.user, min_n=min_n)

    def clean(self):
        if 'use_facebook_list' in self.cleaned_data and not self.cleaned_data['use_facebook_list']:
            # If FB list is not checked, remove django's update_until error
            try:
                del self._errors['update_until']
            except:
                pass

        if 'use_facebook_list' not in self.cleaned_data or 'update_until' not in self.cleaned_data:
            # There are already errors in some fields; return immediately
            return self.cleaned_data

        if self.cleaned_data['use_facebook_list'] and self.cleaned_data['update_until'] < timezone.now() and 'update_until' in self.changed_data:
            self._errors['update_until'] = self._errors.get('update_until', ErrorList())
            self._errors['update_until'].append(ugettext("Facebook list registration end time can not be in the past"))

        # Validate tags formset
        if not self.tags_formset.is_valid():
            raise forms.ValidationError('')

        return self.cleaned_data

    def save(self, commit=True):
        if self.cleaned_data['use_facebook_list']:
            # Save the model
            instance = super(VenueFacebookListForm, self).save(commit)

            # Save the tags formset
            self.tags_formset.save(list_instance=instance)
            return instance
        else:
            # Remove the list, if it exists
            if self.instance and self.instance.pk:
                logging.info("VenueFacebookListForm: delete FB list %d (%d members) for event (%d) %s", self.instance.pk, self.instance.members.count(), self.instance.event.id, self.instance.event)
                VenueFacebookList.objects.filter(pk=self.instance.pk).delete()
            return None


class VenueFreeListForm(forms.ModelForm):
    """ Used for editing event's Free guestlist's properties

    It has a hidden checkbox for adding/removing the FB list, the date/time widget for choosing until what
    time the list should be open and the limit of extra guests for every guest
    """
    EXTRA_GUESTS_NONE = 0
    EXTRA_GUESTS_CHOICES = (
        (EXTRA_GUESTS_NONE, _('none')),
    )
    for i in range(1,10):
        EXTRA_GUESTS_CHOICES += ((i, '+%d' % i),)

    class Meta:
        model = VenueFreeList
        fields = ('name', 'open_until', 'description', 'extra_guests')
        widgets = {
            'open_until': PopupDateTimeWidget(),
        }

    name = forms.CharField(label=_('List name'), placeholder=_('RSVP list'))
    open_until = forms.DateTimeField(label=_("Registration ends"), widget=BootstrapDateTimeWidget)
    use_free_list = forms.BooleanField(initial=False, required=False, widget=ToggleCheckboxInput)
    description = forms.CharField(placeholder=_("What do registered users get? Free entrance? Welcoming drink?"),
                                  widget=forms.Textarea(attrs={
                                      'class': 'input-block-level',
                                      'rows': '6',
                                  }))
    extra_guests = forms.ChoiceField(choices=EXTRA_GUESTS_CHOICES, initial=3)

    def __init__(self, *args, **kwargs):
        self.event = kwargs.pop('event')
        self.user = kwargs.pop('user')
        self.venue = self.event.venue
        self.member_count = 0

        event_free_list = self.event.lists.filter(list_type=VenueList.TYPE_FREEGUESTLIST)
        assert len(event_free_list) <= 1

        if not 'instance' in kwargs:
            if event_free_list:
                kwargs['instance'] = event_free_list[0].venuefreelist
            else:
                kwargs['instance'] = VenueFreeList(list_type=VenueList.TYPE_FREEGUESTLIST, owner=self.event.owner_org,
                        created_by=self.user, name=_("RSVP list"), open_until=self.event.start_time, extra_guests=3, event=self.event)
        if not 'initial' in kwargs:
            kwargs['initial'] = {}
        kwargs['initial']['use_free_list'] = len(event_free_list) > 0

        kwargs['initial']['name'] = kwargs['instance'].name
        open_until = kwargs['instance'].open_until
        kwargs['initial']['open_until'] = self.event.localize_dt(open_until)
        kwargs['initial']['description'] = kwargs['instance'].description
        kwargs['initial']['extra_guests'] = kwargs['instance'].extra_guests

        super(VenueFreeListForm, self).__init__(*args, **kwargs)

        self.member_count = VenueListMember.objects.filter(list__id=kwargs['instance'].id).aggregate(Sum('ticket_count'))['ticket_count__sum'] or 0

        self.fields['extra_guests'].widget.attrs['class'] = 'input-small select-gray'

    def clean(self):
        if 'use_free_list' in self.cleaned_data and not self.cleaned_data['use_free_list']:
            # If free guestlist is not checked, remove django's open_until error
            try:
                del self._errors['open_until']
            except:
                pass

        if 'use_free_list' not in self.cleaned_data or 'name' not in self.cleaned_data or \
                'open_until' not in self.cleaned_data or 'description' not in self.cleaned_data or 'extra_guests' not in self.cleaned_data:
            # There are already errors in some fields; return immediately
            return self.cleaned_data


        if self.cleaned_data['use_free_list'] and self.cleaned_data['open_until'] < timezone.now() and 'open_until' in self.changed_data:
            self._errors['open_until'] = self._errors.get('open_until', ErrorList())
            self._errors['open_until'].append(ugettext("RSVP list registration end time can not be in the past"))

        return self.cleaned_data

    def save(self, commit=True):
        if self.cleaned_data['use_free_list']:
            # Save the model
            instance = super(VenueFreeListForm, self).save(commit)

            return instance
        else:
            # Remove the list, if it exists
            if self.instance and self.instance.pk:
                logging.info("VenueFreeListForm: delete Free guestlist %d (%d members) for event (%d) %s", self.instance.pk, self.instance.members.count(), self.instance.event.id, self.instance.event)
                VenueFreeList.objects.filter(pk=self.instance.pk).delete()
            return None


# Create formsets. Each one contains one extra form, used as template for dynamic list adding. Also they get custom prefixes.
VenueFacebookListTagFormSet = modelformset_factory(VenueFacebookListTag, form=VenueFacebookListTagForm, formset=BaseVenueFacebookListTagFormSet, extra=1)


class OrganizationField(forms.ModelChoiceField):
    """ Used for organization selection
    """
    def label_from_instance(self, obj):
        return obj.display_name()

    def to_python(self, value):
        if value == 'add_new':
            return value
        else:
            return super(OrganizationField, self).to_python(value)


class VenueForm(forms.ModelForm):
    """ Venue form used when creating or editing venues
    """

    class Meta:
        model = Venue
        fields = (
            'name', 'phone',
            'addr_country_code', 'timezone', 'addr_city', 'addr_street',
            'location_lat', 'location_lon',
            'website', 'email',
            'purchase_terms',
            'facebookUrl',
            'slug',
        )

    avatar = AvatarField(label=_('Logo'), model=VenueAvatar, required=False)
    delete_avatar = forms.BooleanField(initial=False, required=False, widget=forms.HiddenInput())

    i_own_this = forms.BooleanField(label=_("Claim the venue"), initial=False, required=False,
        help_text=_("Check this box if you would like to claim the venue.\n"
            "We will verify your ownership and mark you as the owner of the venue."))
    location_lat = forms.CharField(label=_("Location"),required=(not settings.DEBUG), widget = GoogleMapsWidget(
        attrs={'width': '100%', 'height': '320px', 'longitude_id':'id_location_lon', 'base_point': '59.439,24.754'}),
        error_messages={'required': _('Please select point from the map.')},
        help_text=_("Drag the pin to update the venue's location on the map."))
    location_lon = forms.CharField(required=(not settings.DEBUG), widget = forms.HiddenInput())

    def __init__(self, *args, **kwargs):
        super(VenueForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['class'] = 'span12'
        self.fields['name'].placeholder = force_unicode(_('Please type a correct name'))
        self.fields['phone'].widget.attrs['class'] = 'span12'
        self.fields['phone'].placeholder = force_unicode(_('(044) 3234 3455'))
        self.fields['website'].widget.attrs['class'] = 'span12'
        self.fields['website'].placeholder = force_unicode(_('Please type a correct name'))
        self.fields['email'].widget.attrs['class'] = 'span12'
        self.fields['email'].placeholder = force_unicode(_('info@email.com'))
        self.fields['addr_country_code'].widget.attrs['class'] = 'span12 select-gray'
        self.fields['addr_city'].widget.attrs['class'] = 'span12'
        self.fields['addr_city'].placeholder = force_unicode(_('Please type a city'))
        self.fields['addr_street'].widget.attrs['class'] = 'span12'
        self.fields['addr_street'].placeholder = force_unicode(_('Please type a street'))
        self.fields['timezone'].widget.attrs['class'] = 'span12 select-gray'
        self.fields['facebookUrl'].widget.attrs['class'] = 'span12'
        self.fields['facebookUrl'].placeholder = force_unicode(_('facebook.com/page-name'))
        self.fields['slug'].widget.attrs['class'] = 'span12'
        self.fields['slug'].label = force_unicode(_('Choose short link'))
        self.fields['slug'].help_text_below = force_unicode(_('This short URL can be used to promote all upcoming events. Guests can get on the list, book tables and buy tickets using the short link.'))
        self.fields['slug'].placeholder = force_unicode(_('venue-name'))
        self.fields['purchase_terms'].widget.attrs['class'] = 'span12'
        self.fields['purchase_terms'].widget.attrs['rows'] = '5'
        self.fields['purchase_terms'].placeholder = force_unicode(_('Write your own terms'))
        self.fields['i_own_this'].help_text_below = force_unicode(_('Check this box if you would like to claim the venue. We will verify your ownership and mark you as the owner of the venue.'))
        self.helper = FormHelper()
        self.helper.form_action = reverse('venue_add_simple')

        if self.instance.pk:
            # Edit venue
            del self.fields['i_own_this']
            if self.instance.slug:
                del self.fields['slug']

    def clean_slug(self):
        slug = self.cleaned_data['slug']

        if not slug:
            return slug

        if Venue.objects.filter(slug__iexact=slug).exists() or is_slug_reserved(slug.lower()):
            raise forms.ValidationError(_('This web address is already taken'))

        return slug

    def clean_facebookUrl(self):
        url = self.cleaned_data['facebookUrl']

        self.facebook_page_id = ''
        try:
            clean_fb_id = extract_id_token_from_str(url)
            self.facebook_page_id = FacebookObjectByIdFetcher(clean_fb_id).get()['id']
        except:
            pass

        fb_queryset = Venue.objects.filter(facebook_page_id=self.facebook_page_id)
        if self.instance.pk:
            fb_queryset = fb_queryset.exclude(pk=self.instance.id)

        fb_org_queryset = Organization.objects.filter(facebook_page_id=self.facebook_page_id)

        if self.facebook_page_id and (fb_queryset.exists() or fb_org_queryset.exists()):
            raise forms.ValidationError(_('This facebook ID is already used'))

        return url

    def save(self, commit=True):
        venue = super(VenueForm, self).save(False)
        if (self.cleaned_data["location_lat"], self.cleaned_data["location_lon"]) == (u'', u''):
            venue.location_lat = None
            venue.location_lon = None
        venue.facebook_page_id = self.facebook_page_id
        if commit:
            venue.save()
        return venue


class CurrencySelectionForm(forms.Form):
    """ A very simple form for selecting currency.

    Used to select organization's currency, currently on venue menu page.
    """
    currency = forms.ModelChoiceField(Currency.objects.order_by('id'), label=_("Currency"), empty_label='---')

    helper = FormHelper()
    helper.form_tag = False
    helper.layout = Layout(
        Field('currency'),
    )


class OrganizationLegalInfoForm(forms.ModelForm):
    required_form_fields = ('official_name', 'currency', 'registry_number', 'addr_country_code', 'addr_city', 'addr_street')

    class Meta:
        model = Organization
        fields = (
            'official_name', 'registry_number', 'phone', 'vat_code', 'iban_number',
            'addr_country_code', 'addr_city', 'addr_street', 'facebookUrl',
        )

    currency_choices = [(x.id, x.code) for x in Currency.objects.order_by('id').all()]
    currency_choices.insert(0, ('', '---'))

    currency = forms.ChoiceField(label=_("Currency"), choices=currency_choices)
    location_lat = forms.CharField(label=_("Location"), required=False, widget = GoogleMapsWidget(
        attrs={'width': '100%', 'height': '320px', 'longitude_id':'id_location_lon', 'base_point': '59.439,24.754', 'draggable': 'false'}))
    location_lon = forms.CharField(required=False, widget = forms.HiddenInput())

    def __init__(self, *args, **kwargs):
        instance = kwargs.get('instance', None)
        venue = instance.owned_venue

        # If organization doesn't have a address default to venue's address
        # We only check city here, because all the fields are required and
        # if this doesn't exists, none of the address fields has a value
        if instance and not instance.addr_city and venue:
            kwargs['initial'] = kwargs.get('initial', dict())
            kwargs['initial']['addr_country_code'] = venue.addr_country_code
            kwargs['initial']['addr_city'] = venue.addr_city
            kwargs['initial']['addr_street'] = venue.addr_street

        super(OrganizationLegalInfoForm, self).__init__(*args, **kwargs)

        self.fields['facebookUrl'].placeholder = force_unicode(_('facebook.com/page-name'))
        self.fields['facebookUrl'].widget.attrs['class'] = 'span7'

        self.fields['official_name'].widget.attrs['class'] = 'span12'
        self.fields['registry_number'].widget.attrs['class'] = 'span12'
        self.fields['phone'].widget.attrs['class'] = 'span12'
        self.fields['addr_country_code'].widget.attrs['class'] = 'span12 select-gray'
        self.fields['addr_city'].widget.attrs['class'] = 'span12'
        self.fields['addr_city'].placeholder = force_unicode(_('Please type a city'))
        self.fields['addr_street'].widget.attrs['class'] = 'span12'
        self.fields['addr_street'].placeholder = force_unicode(_('Please type a street'))

        self.fields['currency'].widget.attrs['class'] = 'span6 select-gray'
        self.fields['currency'].help_text_below = ugettext("The chosen currency will be bound to the organization and cannot be changed later.")
        if instance and instance.has_shop():
            self.fields['currency'].initial = instance.currency.id
            self.fields['currency'].widget.attrs['disabled'] = True

        # Require only official_name and currency fields
        for name, field in self.fields.iteritems():
            if name in self.required_form_fields:
                field.required = True
            else:
                field.required = False

    def clean_facebookUrl(self):
        url = self.cleaned_data['facebookUrl']

        self.facebook_page_id = ''
        try:
            clean_fb_id = extract_id_token_from_str(url)
            self.facebook_page_id = FacebookObjectByIdFetcher(clean_fb_id).get()['id']
        except:
            if url:
                raise forms.ValidationError(_('Access to the Facebook page was denied'))

        fb_queryset = Organization.objects.filter(facebook_page_id=self.facebook_page_id)
        if self.instance.pk:
            fb_queryset = fb_queryset.exclude(pk=self.instance.pk)

        fb_extra_queryset = Venue.objects.filter(facebook_page_id=self.facebook_page_id)
        if self.facebook_page_id and (fb_queryset.exists() or fb_extra_queryset.exists()):
            raise forms.ValidationError(_('This facebook ID is already used'))

        return url


    def save(self, commit=True):
        org = super(OrganizationLegalInfoForm, self).save(False)

        org.facebook_page_id = self.facebook_page_id
        if commit:
            org.save()
        return org


class OrganizationSubscriptionForm(forms.Form):
    ACTION_CREATE = 'create'
    ACTION_CREATE_CONFIRM = 'confirm'
    ACTION_CANCEL = 'cancel'
    ACTION_CHOICES = [(c, c) for c in (ACTION_CREATE, ACTION_CANCEL, ACTION_CREATE_CONFIRM)]

    action = forms.ChoiceField(widget=HiddenInput, choices=ACTION_CHOICES)
    stripe_token = forms.CharField(widget=HiddenInput, required=False)

    def clean(self):
        super(OrganizationSubscriptionForm, self).clean()

        data = self.cleaned_data
        if data.get('action') == self.ACTION_CREATE and not data.get('stripe_token'):
            self.append_field_error('stripe_token', "stripe_token is required when creating subscription")

        return data

    def append_field_error(self, field, error):
        self._errors[field] = self._errors.get(field, ErrorList())
        self._errors[field].append(error)


class VenueFacebookShopForm(forms.ModelForm):
    class Meta:
        model = Venue
        # facebookUrl is not in fields since we don't want to use django url validation which requires schema in front of url.
        # FIXME: Proper sync for facebook_page_id and facebookUrl
        fields = ('facebook_page_id',)

    facebookUrl = forms.CharField(label=_("Facebook page URL"), required=False,
                                  placeholder=_("e.g. https://www.facebook.com/events"))
    facebook_page_id = forms.CharField(required=True, widget=HiddenInput)

    def __init__(self, *args, **kwargs):
        super(VenueFacebookShopForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            'facebook_page_id',
            Field('facebookUrl', css_class='input-block-level'),
        )

    def clean_facebook_page_id(self):
        """
            We must require unique values for all Venues, otherwise we don't know
            which Venues shopitems to show in FB shop app.
        """
        data = self.cleaned_data['facebook_page_id']

        try:
            assert not Venue.objects.filter(facebook_page_id=data).exclude(id=self.instance.pk).exists()
            assert not Organization.objects.filter(facebook_page_id=data).exclude(id=self.instance.pk).exists()
        except AssertionError:
            raise forms.ValidationError(_("This Facebook page is already taken by another venue or organization. If you think this is incorrect, please contact us."))
        return data


class OrganizationFacebookShopForm(VenueFacebookShopForm):
    class Meta:
        model = Organization
        # facebookUrl is not in fields since we don't want to use django url validation which requires schema in front of url.
        # FIXME: Proper sync for facebook_page_id and facebookUrl
        fields = ('facebook_page_id', )


class BaseEventPromoterFormSet(BaseModelFormSet):

    model = VenuePromoterList

    def __init__(self, *args, **kwargs):
        self.extra = kwargs.pop('extra')
        self.request = kwargs.pop('request', None)
        self.promoters = kwargs.pop('promoters')
        # self.show_members = kwargs.pop('show_members', False)
        # self.currency = kwargs.pop('currency', None)
        super(BaseEventPromoterFormSet, self).__init__(*args, **kwargs)

    # Methods for late binding variables
    def set_event(self, event):
        for form in self.forms:
            form._event = event

    def _construct_form(self, i, **kwargs):
        kwargs['request'] = self.request
        # kwargs['show_members'] = self.show_members
        # kwargs['currency'] = self.currency
        kwargs['promoters'] = self.promoters
        form = super(BaseEventPromoterFormSet, self)._construct_form(i, **kwargs)

        return form


class TimePeriodMultiField(MultiField):
    """
        Change the MultiField so that it wouldn't get an "error" class every time a random form field has an error
    """
    def render(self, form, form_style, context):
        for error in form.errors:
            if error in self.fields:
                self.css_class += " error"
                break

        # We need to render fields using django-uni-form render_field so that MultiField can
        # hold other Layout objects inside itself
        fields_output = u''
        self.bound_fields = []
        for field in self.fields:
            fields_output += render_field(field, form, form_style, context, 'uni_form/multifield.html', self.label_class, layout_object=self)

        return render_to_string(self.template, Context({'multifield': self, 'fields_output': fields_output}))


class EventTicketForm(forms.ModelForm):
    """
        Displays a ticketform for an event

        <shop> can be given to __init__, or provided before .save() as '._shop'
            Shop has to be the event->owner_organizer->shop, (eg. not event->venue->shop)

        <user> is mandatory

        <event> is mandatory on ticket creation.
    """
    class Meta:
        model = ShopItem
        fields = ('name', 'description', 'price', 'sell_from', 'sell_until', 'total')

    name = forms.CharField(label=_("Ticket name"), placeholder=_('e.g. VIP ticket'), required=True)
    description = forms.CharField(label=_("Description"),
            placeholder=_("If you have multiple tickets for the event, describe what's the difference between them. Special entry? VIP table? Free drinks?"),
            required=False, widget=forms.Textarea(attrs={'rows': 3}))
    price = forms.DecimalField(label=_("Price"), decimal_places=2, min_value=1, max_digits=6, required=True)
    total = forms.IntegerField(label=_("Total amount"), initial=500, required=True, min_value=1,
            help_text=_("Specify the maximum number of tickets to be sold."))
    sell_from = forms.DateTimeField(label=_("Sale starts"), widget=BootstrapDateTimeWidget, required=False)
    sell_until = forms.DateTimeField(label=_("Sale ends"), widget=BootstrapDateTimeWidget, required=True)
    delete = forms.BooleanField(initial=False, widget=HiddenInput, required=False)

    pending_tickets_count = 0
    sold_tickets_count = 0
    taken_tickets_count = 0

    def __init__(self, *args, **kwargs):
        self._user = kwargs.pop('user')
        self.event = kwargs.pop('event')
        sell_until = kwargs.pop('sell_until', None)
        self.end_time = self.event.end_time if self.event else None

        self.shop = self.event.owner_org.shop if self.event else kwargs.pop('shop', None)
        self.currency = self.shop.default_currency if self.shop else '-'
        instance = kwargs.get('instance', None)
        prefix = kwargs.get('prefix', None)

        # Get the instance if it comes from form_data with ajax
        match = re.search('^ticket-(\d+)$', prefix) if prefix else None
        if match and not instance:
            instance_id = match.group(1)
            try:
                instance = ShopItem.objects.get(id=instance_id, data__event=self.event)
            except ShopItem.DoesNotExist:
                pass
            else:
                kwargs['instance'] = instance

        pending_tickets_count = 0
        sold_tickets_count = 0

        # If editing a ticket, fill some initial data
        if instance:
            data = instance.data

            kwargs['initial'] = kwargs.get('initial', dict())
            kwargs['initial']['name'] = data.name
            kwargs['initial']['description'] = data.description
            kwargs['initial']['price'] = data.price.amount
            kwargs['initial']['sell_from'] = data.event.localize_dt(data.sellable_from) if data.sellable_from else None
            kwargs['initial']['sell_until'] = data.event.localize_dt(data.sellable_until)

            # Get the amount of tickets that are already sold or pending
            tickets_query = PurchasedItem.objects.exclude(status=PurchasedItem.STATUS_CANCELLED).filter(item_data__item=instance)
            pending_tickets_count = tickets_query.filter(status=PurchasedItem.STATUS_PENDING).count()
            sold_tickets_count = tickets_query.exclude(status=PurchasedItem.STATUS_PENDING).count()

        super(EventTicketForm, self).__init__(*args, **kwargs)

        self.fields['sell_until'].initial = self.event.end_time_local if self.event else sell_until
        self.fields['sell_from'].help_text_below = _("Leave blank to start the sale immediately")

        sold_text = ''
        if sold_tickets_count or pending_tickets_count:
            sold_text = ungettext("%(ticket_count)d ticket sold",
                                  "%(ticket_count)d tickets sold",
                                  sold_tickets_count) % {'ticket_count': sold_tickets_count}
            if pending_tickets_count:
                pending_text = _('and %(pending_count)d pending') % {'pending_count': pending_tickets_count}
                sold_text = "%s %s" % (sold_text, pending_text)

        if instance:
            setattr(instance, 'left_ticket_count', instance.total - sold_tickets_count)

        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            'name',
            Div(
                Div('sell_from', css_class='pull-left'),
                Div('sell_until', css_class='pull-left'),
                css_class='clearfix fields-collapsing'
            ),
            Div(
                Div(AppendedText('price', self.currency, css_class="input-price"), css_class='pull-left'),
                Div(
                    Div(Field('total', css_class='input-mini'), css_class='pull-left'),
                    Div(HTML(sold_text), css_class='pull-left tickets-sold'),
                css_class='pull-left'),
                css_class='clearfix fields-collapsing'
            ),
            Field('description', css_class='input-block-level'),
            'delete',
        )

    def clean(self):
        super(EventTicketForm, self).clean()
        data = self.cleaned_data
        # Delete required field errors if 'delete' is checked
        if 'delete' in data and data['delete']:
            field_names = ['name', 'price', 'sell_until', 'total']
            for field_name in field_names:
                self.remove_field_error(field_name)

            return data

        sell_from = data.get("sell_from", None)
        sell_until = data.get("sell_until", None)
        if sell_from and sell_until and sell_from >= sell_until:
            self.append_field_error('sell_from', ugettext("Sale ending time cannot be before the start time"))
        if sell_until and sell_until > self.end_time:
            self.append_field_error('sell_until', ugettext("Sale ending time cannot be after event's ending time"))

        return data

    def clean_total(self):
        cleaned_total = self.cleaned_data['total']
        if self.instance.pk:
            sold_tickets = PurchasedItem.objects.exclude(purchase__status=PurchasedItem.STATUS_CANCELLED)\
                .filter(item_data__item_id=self.instance.id).count()
            if sold_tickets > cleaned_total:
                raise forms.ValidationError(_("Total amount can't be smaller than the amount of tickets sold."))

        return cleaned_total

    def save(self, commit=True):
        if not self.instance.pk and self.cleaned_data.get('delete'):
            return None

        m = super(EventTicketForm, self).save(commit=False)
        m.visible = not self.cleaned_data.get('delete')
        if not m.visible:
            if commit:
                m.save()
            return m
        m.shop = self.shop

        # This isn't saved yet
        m.init_data(
            name=self.cleaned_data.get('name'),
            created_by=self._user,
            event=self.event,
            description=self.cleaned_data.get('description'),
            price=Money(self.cleaned_data['price'], self.currency),
            sellable_from=self.cleaned_data['sell_from'],
            sellable_until=self.cleaned_data['sell_until']
        )
        with transaction.commit_on_success():
            if self.instance.pk:
                # Lock the ShopItem so that we will get the correct in_stock count and
                # to ensure that the total is not changed
                instance = ShopItem.objects.select_for_update().get(id=self.instance.id)
                stock_change = self.cleaned_data['total'] - instance.total
                m.in_stock = F('in_stock') + stock_change
            else:
                m.in_stock = self.cleaned_data['total']

            if commit:
                is_new_instance = not self.instance.pk
                # Which also calls self.data.save()
                m.save()
                if is_new_instance:
                    # Add ticket category
                    ticket_categories = ShopCategory.objects.filter(shop=m.shop, category_type=ShopCategory.TYPE_TICKET)
                    if ticket_categories:
                        m.data.categories.add(ticket_categories[0])
                    else:
                        logging.error("No ticket categories found for shop %d (event %d: %s)",
                            m.shop.id, self.event.id, self.event.name)
                else:
                    # Ensure that in_stock count is not negative
                    ShopItem.objects.filter(id=m.id, in_stock__lt=0).update(in_stock=0)

        return m


    def append_field_error(self, field, error):
        self._errors[field] = self._errors.get(field, ErrorList())
        self._errors[field].append(error)

    def remove_field_error(self, field):
        try:
            del self._errors[field]
        except:
            pass


class EventTicketForm2(EventTicketForm):
    def __init__(self, *args, **kwargs):
        super(EventTicketForm2, self).__init__(*args, **kwargs)

        self.fields['sell_from'].help_text_below = ""
        self.fields['sell_until'].label = ""

        self.helper.layout = Layout(
            'name',
            Div(
                Div(
                    Field('sell_from'),
                    css_class="pull-left"
                ),
                Div(
                    Div(
                        HTML("-"),
                        css_class="pull-left from_to"
                    ),
                    Field('sell_until')
                ),

                css_class='clearfix fields-collapsing'
            ),
            Div(
                Div(AppendedText('price', self.currency, css_class="input-price"), css_class='pull-left'),
                Div(Field('total', css_class='input-mini'), css_class='pull-left'),
                css_class='clearfix fields-collapsing'
            ),
            Field('description', css_class='input-block-level'),
            'delete',
        )

class EventPromoterForm(forms.ModelForm):
    """ Displays a promoter form for an event

    From here, the user can activate/deactivate promoter for given event and change settings of active promoters.
    This form is not for editing guests of a list.
    """
    class Meta:
        model = VenuePromoterList
        fields = ('venuelist_ptr', 'promoter', 'commission_male', 'commission_female')

    # Use venuelist_ptr instead of id since inherited models, such as VenuePromoterList, have FK to parent as id.
    venuelist_ptr = forms.IntegerField(widget=forms.HiddenInput, required=False)
    active = forms.BooleanField(widget=ToggleCheckboxInput, required=False)
    promoter = forms.ModelChoiceField(queryset=Promoter.objects.none(), widget=forms.HiddenInput)
    list_limit = forms.IntegerField(min_value=0, required=False)
    commission_male = forms.DecimalField(label=_("Male commission"),
        min_value=0, max_digits=5, decimal_places=2, required=False)
    commission_female = forms.DecimalField(label=_("Female commission"),
        min_value=0, max_digits=5, decimal_places=2, required=False)

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        self.promoters = kwargs.pop('promoters')
        # self.currency = kwargs.pop('currency')
        self.user = self.request.user
        # self._event will usually be set to proper value by the formset (see BaseEventPromoterFormSet.set_event())
        self._event = kwargs.pop('event', None)

        self.member_count = 0

        super(EventPromoterForm, self).__init__(*args, **kwargs)
        self.fields['promoter'].queryset = self.promoters

        self.promoter_obj = None

        if self.instance.pk:
            self.fields['active'].initial = True
            self.fields['promoter'].initial = self.instance.promoter.id
            self.promoter_obj = self.instance.promoter
            if self.promoter_obj.email:
                promoter_permission = UserPermission.objects.filter(list__id=self.instance.venuelist_ptr.id, permission_list_edit=True, user__email__iexact=self.promoter_obj.email)
                # O means that promoter doesn't have permission to edit the Guest List.
                # A new UserPermission will be created, if there's no existing oneand this is blank or bigger than one.
                # After this is changed back to 0, UserPermission will be deleted.
                self.fields['list_limit'].initial = 0
                if promoter_permission.exists():
                    self.fields['list_limit'].initial = promoter_permission[0].list_limit
                    # self.fields['permission_id'].initial = promoter_permission[0].id
        elif 'promoter' in kwargs.get('initial', {}):
            self.promoter_obj = kwargs['initial']['promoter']

        list_limit_div = Div(AppendedPlainText('list_limit', _('guests'), css_class='input-mini'), css_class='pull-left')
        # Delete the list_limit field if promoter_obj is known and doesn't have email. When we're initially displaying
        #  the page, we always know the promoter object and when the form is used from ajax code, we don't care about
        #  whether the field is shown or not since it doesn't affect logic in any way.
        if self.promoter_obj and not self.promoter_obj.email:
            del self.fields['list_limit']
            list_limit_div = HTML('')

        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            'venuelist_ptr', 'promoter',  # Hidden fields
            Div(
                list_limit_div,
                Div(AppendedPlainText('commission_male', _('per guest'), css_class='input-mini'), css_class='pull-left'),
                Div(AppendedPlainText('commission_female', _('per guest'), css_class='input-mini'), css_class='pull-left'),
                css_class='fields-collapsing clearfix'
            ),
        )
        # TODO: crispy-forms 1.1.x automatically appends fields that are not in the layout, which results in duplicate
        #  active field. 1.2.x doesn't do that. We'll need to either migrate to 1.2.x or somehow fix it here.

    def has_changed(self):
        # This is needed to force BaseModelFormSet.save_new_objects() to save forms that are not changed wrt their
        #  initial data. We have such forms on the Add Event page and want the user to be able to create promoter lists
        #  without toggling anything.
        return True

    def clean(self):
        super(EventPromoterForm, self).clean()

        _cleaned = self.cleaned_data
        if 'active' in _cleaned and not _cleaned['active']:
            self.remove_field_error('commission_male')
            self.remove_field_error('commission_female')

        return _cleaned

    def save(self, commit=True):
        # Case when there's no VenuePromoterList created and the promoter is not added this time either.
        if not self.instance.pk and not self.cleaned_data.get('active', False):
            return None

        # If we have a list for promoter and it is being removed
        if self.instance.pk and not self.cleaned_data.get('active', False):
            logging.info("Deleting VenuePromoterList %d ('%s') for event %d (%s)",
                         self.instance.pk, self.instance.name, self.instance.event.id, self.instance.event.name)
            VenueList.objects.filter(pk=self.instance.pk).delete()
            return None

        # If we're creating the list, ensure it doesn't exist yet
        if not self.instance.pk:
            try:
                existing_list = VenuePromoterList.objects.get(event=self._event, promoter=self.cleaned_data['promoter'])
                # If the list exists, we want to apply the changes to the existing list. Copy all the other attributes
                #  from the existing list to form instance.
                self.instance.pk = existing_list.pk
                self.instance.list_type = existing_list.list_type
                self.instance.owner = existing_list.owner
                self.instance.event = existing_list.event
                self.instance.name = existing_list.name
                self.instance.created_by = existing_list.created_by
                self.instance.created_timestamp = existing_list.created_timestamp

                logging.warning("EventPromoterForm.save(): list already exists for event %d, promoter %d",
                                self._event.id, self.cleaned_data['promoter'].id)
                statsd.incr('pro.lists.promoters.already_exists')
            except VenuePromoterList.DoesNotExist:
                pass

        promoter_list = super(EventPromoterForm, self).save(commit=False)

        promoter_list.modified_timestamp = timezone.now()

        if not self.instance.pk:
            promoter_list.list_type = VenueList.TYPE_PROMOTER
            promoter_list.owner = self._event.owner_org
            promoter_list.event = self._event
            promoter_list.name = promoter_list.promoter.name
            promoter_list.created_by = self.request.user

        if commit:
            promoter_list.save()

        # We need to check for permissions only if the promoter has an email.
        if promoter_list.promoter.email:
            # Create list_edit permission for promoter
            form_data = {
                'email': promoter_list.promoter.email,
                'list_limit': self.cleaned_data.get('list_limit'),
            }
            if self.cleaned_data.get('list_limit') is 0:
                # List limit has to be > 0, we will delete it anyway
                form_data['list_limit'] = 1
                form_data['delete'] = True

            # Check if we have existing permission
            promoter_permissions = list(UserPermission.objects.filter(list=promoter_list, permission_list_edit=True, user__email__iexact=promoter_list.promoter.email)[:1])
            if promoter_permissions:
                permission_form = ListUserForm(form_data, list=promoter_list, request=self.request, instance=promoter_permissions[0])
            else:
                permission_form = ListUserForm(form_data, list=promoter_list, request=self.request)

            def save_permission_form(form):
                try:
                    # Sending emails is always done after the model has been saved in the form.
                    # It's basically the last thing done before form.save() returns.
                    form.save()
                except SMTPRecipientsRefused:
                    # Sends an notification to the organization
                    msg = _("The invitation to the promoter (%(promoter_name)s) with the e-mail %(email)s could not be sent. \n"
                            "Please check if the the e-mail is correct!") % {
                            'promoter_name': promoter_list.name,
                            'email': promoter_list.promoter.email,
                        }
                    NotificationMessage.create(msg, self.user, from_staff=True, to_organization=promoter_list.owner)

            permission_user = permission_form.get_user(promoter_list.promoter.email)
            # Give explicit list permission only if the user has no permission for the event
            if not promoter_list.event.has_user_permission(permission_user, 'list_edit'):
                if promoter_list.has_user_permission(permission_user, 'list_edit'):
                    permission = UserPermission.objects.get(user=permission_user, list=promoter_list, permission_list_edit=True)
                    if permission.list_limit != self.cleaned_data['list_limit']:
                        permission.closed_by = self.user
                        permission.closed_timestamp = timezone.now()
                        permission.save()
                        save_permission_form(permission_form)
                else:
                    save_permission_form(permission_form)

        return promoter_list

    def list_limit_label(self):
        # Figure out the current list_limit
        if hasattr(self, 'cleaned_data') and 'list_limit' in self.cleaned_data:
            list_limit = self.cleaned_data['list_limit']
        elif 'list_limit' in self.fields:
            list_limit = self['list_limit'].value()
        else:
            return ''

        # Format it nicely and return
        if list_limit > 0:
            return '+%s' % list_limit
        elif list_limit is None and self.promoter_obj and self.promoter_obj.email:
            return ugettext("unlimited")
        else:
            return ''

    def remove_field_error(self, field):
        try:
            del self._errors[field]
        except:
            pass


# Construct the promoter formset
EventPromoterFormSet = curry(modelformset_factory(VenuePromoterList, form=EventPromoterForm, formset=BaseEventPromoterFormSet), prefix='promoter')


class TemporaryImageForm(forms.ModelForm):
    class Meta:
        model = TemporaryFileWrapper

    def clean_uploaded_file(self):
        uploaded_file = self.cleaned_data.get('uploaded_file', False)
        if uploaded_file:
            if uploaded_file._size > 2*1024*1024:
                raise forms.ValidationError("Image file too large ( > 2mb )")
            return uploaded_file
        else:
            raise forms.ValidationError("Couldn't read uploaded image")


class ImageWithPreviewHelper(object):
    """ Helper class to use with avatar fields, this has logic for
        avatar saving, deletion, changing. This also initializes the field
        widgets. So be sure to change only the parameters after you have called init.

        NOTE: This should always be initialized AFTER form base init so the
        form.fields, form.data etc are already populated.
    """

    def __init__(self, fields, form_instance, user):
        self.field_names = fields
        self.user = user
        self.form = form_instance
        self.data = form_instance.data
        self.files = form_instance.files

        for field in self.field_names:
            file_field = self.form.fields[field["name"]]
            temp_field_key = '%s_auto_temp' % field["name"]
            del_field_key = '%s_auto_delete' % field["name"]

            if temp_field_key not in self.form.fields:
                self.form.fields[temp_field_key] = forms.ModelChoiceField(queryset=TemporaryFileWrapper.objects.all(), widget=forms.HiddenInput(), required=False)
            if del_field_key not in self.form.fields:
                self.form.fields[del_field_key] = forms.BooleanField(required=False, widget=forms.HiddenInput())

            cAvatar = self.get_avatar(field)
            file_field.widget = ImageWithPreviewWidget(current_image=cAvatar, allow_url=field.get('allow_url', False))

    def get_avatar(self, field, object_only=False):
        temp_field_key = '%s_auto_temp' % field["name"]

        cAvatar = field["has_avatar"]
        if not cAvatar:
            temp_file = None
            temp_pk = None

            if self.files and field["name"] in self.files:
                temp_file = self.files[field["name"]]

            if self.data and temp_field_key in self.data:
                temp_pk = self.data[temp_field_key]

            tempAvatar = self.save_temporary_image(temp_file, temp_pk)
            if object_only:
                return tempAvatar

            if tempAvatar:
                cAvatar = tempAvatar.uploaded_file.url
                self.form.fields[temp_field_key].initial = tempAvatar.id
                self.form.data[temp_field_key] = tempAvatar.id
        return cAvatar

    def post_save(self, instance):
        for field in self.field_names:
            if not field["has_avatar"]:
                cAvatar = self.get_avatar(field, object_only=True)
            else:
                cAvatar = None

            delete_field_key = '%s_auto_delete' % field["name"]
            self.form.fields[field["name"]].save_avatar(self.form,
                                                        owner=instance,
                                                        field_name=field['name'],
                                                        delete=self.form.cleaned_data[delete_field_key],
                                                        filewrapper=cAvatar)

    def save_temporary_image(self, the_file, the_pk=None):
        if the_file:
            # Provide early exit for files with the same md5sum. We don't want multiple uploads. :)
            # All though, this should also check for broken temporary files, which may cause issues if
            # used as a early exit. Not really sure how they might happen in real world.
            md5 = hashlib.md5()
            for chunk in the_file.chunks():
                md5.update(chunk)
            md5sum = md5.hexdigest()
            try:
                tmp =  TemporaryFileWrapper.objects.filter(md5sum=md5sum)[0]
                if tmp.uploaded_file:
                    if os.path.isfile(tmp.uploaded_file.path):
                       return tmp
            except (TemporaryFileWrapper.DoesNotExist, IndexError):
                pass
        else:
            md5sum = ""

        if the_pk:
            try:
                return TemporaryFileWrapper.objects.get(pk=the_pk)
            except TemporaryFileWrapper.DoesNotExist:
                return None
        else:
            temp_form = TemporaryImageForm({
                'created_by': self.user.id,
                'timestamp': timezone.now(),
                'md5sum': md5sum
            }, {
                'uploaded_file': the_file
            })
            if temp_form.is_valid():
                return temp_form.save()
            else:
                return None


class EventForm(forms.ModelForm):

    class Meta:
        model = Event
        fields = (
            'name', 'active',
            'start_time', 'end_time',
            'baseTicketPrice',
            'description',
            'facebook_id',
            'purchase_confirmation_message',
            'tags',
        )

    active = forms.BooleanField(label=_("Visible"), widget=ToggleCheckboxInput, initial=True, required=False)
    start_time = forms.DateTimeField(label=_("Start time"), widget=BootstrapDateTimeWidget)
    end_time = forms.DateTimeField(label=_("End time"), widget=BootstrapDateTimeWidget)
    venue = forms.IntegerField(widget=HiddenInput())
    tags = EventTagMultipleChoiceField(label=_("Tags"), queryset=EventTag.objects.filter(visible=True))
    baseTicketPrice = forms.DecimalField(label=_("Price at door"), required=False, decimal_places=2, max_digits=5,
                                         help_text=_("Enter the highest price that is charged at the door during the event. We use it to show the discount."),
                                         widget=SupplementedInputWidget(attrs={
                                             'class': ' span2'
                                             }))
    currency = forms.ModelChoiceField(Currency.objects.order_by('id'), label=_("Currency"), empty_label='---')
    facebook_id = forms.CharField(label='', required=False, placeholder="https://www.facebook.com/events/1234567890/",
                                  widget=TextInput(attrs={'class': 'input-block-level'}))

    avatar = AvatarField(label=_('Flyer'), model=EventAvatar, required=False)
    mobile_avatar = AvatarField(label=_('Mobile Avatar'), model=EventMobileAvatar, required=False)
    cover_avatar = AvatarField(label=_('Cover Image'), model=EventCoverAvatar, required=False)

    image_url = forms.URLField(widget=forms.HiddenInput, required=False)

    def __init__(self, *args, **kwargs):
        venue = kwargs.pop('venue')
        self.organization = kwargs.pop('organization')
        self.user = kwargs.pop('user')

        super(EventForm, self).__init__(*args, **kwargs)

        helper_fields = [
            {
                'name': 'avatar',
                'has_avatar': self.instance.has_avatar() and avatar_image_path(self.instance, 140, 200),
                'allow_url': True,
            }
        ]

        if self.user.is_staff:
            helper_fields.append({
                'name': 'mobile_avatar',
                'has_avatar': mobile_avatar(self.instance, 640, 600) if self.user.is_staff else None
            })
        else:
            del self.fields['mobile_avatar']

        helper_fields.append({
            'name': 'cover_avatar',
            'has_avatar': cover_avatar(self.instance, 1000, 255),
        })

        self.image_helper = ImageWithPreviewHelper(helper_fields, form_instance=self, user=self.user)

        # Set default start/end times, these depend on the venue timezone.
        if venue:
            self.fields['venue'].initial = venue.id
            self.fields['start_time'].initial = self.default_start_time(venue)
            self.fields['end_time'].initial = self.default_end_time(venue)

        self.fields['active'].help_text_below = _("Inactive events will not be visible to users")
        self.fields['description'].widget.attrs['rows'] = '6'
        self.fields['description'].widget.attrs['class'] = 'input-block-level'
        self.fields['currency'].help_text_below = ugettext("The chosen currency will be bound to the organization and cannot be changed later.")
        self.fields['currency'].widget.attrs['class'] = 'input-small'
        self.fields['name'].widget.attrs['class'] = 'input-block-level'
        self.fields['tags'].help_text_below = _("Choose at least one tag to describe the event.")

        if self.organization.feature_customizable_purchase_message:
            self.fields['purchase_confirmation_message'].widget.attrs['rows'] = '6'
            self.fields['purchase_confirmation_message'].widget.attrs['class'] = 'input-block-level'
        else:
            del self.fields['purchase_confirmation_message']

        if self.organization.has_shop():
            del self.fields['currency']

        if not self.organization.feature_public:
            self.fields['active'].initial = False
            self.fields['active'].widget = HiddenInput()

    @staticmethod
    def default_start_time(venue):
        # 23:00 today
        tz = pytz.timezone(venue.timezone)
        return datetime.combine(datetime.now(tz=tz).date(), time(23, 00))

    @staticmethod
    def default_end_time(venue):
        # 5:00 tomorrow
        tz = pytz.timezone(venue.timezone)
        return datetime.combine(datetime.now(tz=tz).date(), time(5, 00)) + timedelta(days=1)

    def clean(self):
        super(EventForm, self).clean()

        _cleaned = self.cleaned_data

        if 'end_time' in _cleaned and _cleaned['end_time'] < timezone.now():
            _cleaned['start_time'] = self.instance.start_time

        # Assert end_time and start_time in _cleaned AND end_time >= start_time
        if set(['start_time', 'end_time']).issubset(set(_cleaned)) and _cleaned['end_time'] < _cleaned['start_time']:
            self.append_field_error('end_time', _("End time can not be before start time"))

        # Raise an error when the org can't create visible events and the event was not active previously.
        if not self.organization.feature_public and (not self.instance.pk or not self.instance.active) and _cleaned['active']:
            raise forms.ValidationError(_("You can't create public events"))

        return _cleaned

    def clean_end_time(self):
        end_time = self.cleaned_data['end_time']
        now = timezone.now()

        if (self.instance.pk and self.instance.end_time > now) or not self.instance.pk:
            # If new event or event that's end_time is in the future
            if end_time < now:
                raise forms.ValidationError(_("End time can not be in the past"))
        elif self.instance.pk:
            return self.instance.end_time

        return self.cleaned_data['end_time']

    def clean_facebook_id(self):
        """ Given an url containing "eid=<digits>" or just a number, we return the captured number,
        stripping everything else
        """
        data = self.cleaned_data['facebook_id']
        try:
            fb_eid = extract_id_token_from_str(data)
        except Exception:
            raise forms.ValidationError(_("This doesn't look like a valid Facebook event ID"))
        return fb_eid

    def clean_venue(self):
        venue_id = self.cleaned_data['venue']
        if Venue.objects.filter(id=venue_id).exists():
            return venue_id

        raise forms.ValidationError(_("This venue does not exist"))

    def append_field_error(self, field, error):
        self._errors[field] = self._errors.get(field, ErrorList())
        self._errors[field].append(error)

    def pull_fb_image(self, event):
        if not self.cleaned_data['avatar'] and self.cleaned_data['image_url'] and not event.get_avatar():
            event_poster = urllib.urlretrieve(self.cleaned_data['image_url'])[0]
            save_to_filename = "%d_%s.jpg" % (event.id, event.facebook_id)
            if not FetchFacebookEvents.is_valid_image(open(event_poster, 'rb')):
                logging.warning("EventForm::pull_fb_image: got invalid image for %s (%s) in %s (%s)",
                        event.name, event.facebook_id)
                return False

            if not facebook_utils.is_default_FB_poster(event_poster):
                avatar = EventAvatar()
                avatar.owner = event
                avatar.primary = True
                avatar.avatar.save(save_to_filename, File(open(event_poster, 'rb')), save=False)
                avatar.save()
                os.remove(event_poster)


class EventForm2(EventForm):

    # TODO: Adding this doesn't seem reasonable atm, need a modal or something to configure some of the info required for a push... etc.
    # push_to_facebook = forms.BooleanField(label=_("Push event to your Facebook page"), widget=ToggleCheckboxInput, initial=True, required=False)

    def __init__(self, *args, **kwargs):
        super(EventForm2, self).__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_tag = False

        self.fields['start_time'].label = _("Date & Time")
        self.fields['end_time'].label = ' '

        self.fields['avatar'].widget.extra_classes += "imagefield_v2"
        self.fields['avatar'].widget.help_text = _("Your image must be JPG, GIF, or "
                                                   "PNG format and not exceed 2MB.")

        if 'mobile_avatar' in self.fields:
            self.fields['mobile_avatar'].widget.extra_classes += "imagefield_v2 mobile_avatar"
            self.fields['mobile_avatar'].widget.help_text = _("Your image must be JPG, GIF, or "
                                                              "PNG format and not exceed 2MB. It "
                                                              "will be resized to make it's width 600px.")
        if 'cover_avatar' in self.fields:
            self.fields['cover_avatar'].widget.extra_classes += "imagefield_v2 cover_avatar"
            self.fields['cover_avatar'].widget.help_text = _("Shown on the frontpage for featured events. "
                                                             "Your image must be JPG, GIF, or "
                                                              "PNG format and not exceed 2MB. It "
                                                              "will be resized to make it's width 1000px.")

        self.fields['active'].widget = forms.Select(choices=((True, _('Public, visible on events.com')), (False, _('Private, not visible on events.com'))))
        self.fields['active'].label = _('Public or Private?')
        self.fields['active'].help_text_below = ''


class EventSeriesForm(forms.ModelForm):

    class Meta:
        model = EventSeries
        fields = ('recur_0', 'recur_1', 'recur_2', 'recur_3', 'recur_4', 'recur_5', 'recur_6')

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')

        super(EventSeriesForm, self).__init__(*args, **kwargs)

        for field in self.fields:
            self.fields[field].widget = HiddenInput()
            self.fields[field].initial = False

        self.helper = FormHelper()
        self.helper.form_tag = False

    def save(self, commit=True):
        series = super(EventSeriesForm, self).save(commit=False)
        remains_active = any([self.cleaned_data[field] for field in self.fields])
        new_instance = not bool(self.instance.pk)

        if new_instance:
            series.created_by = self.request.user
        elif not remains_active:
            # Close the series
            series.closed_by = self.request.user
            series.closed_timestamp = timezone.now()
        elif series.closed_by:
            # Since it is still the same series, lets open it again
            series.closed_by = None
            series.closed_timestamp = None

        # Save the instance only when it is an existing one or a new series that will be active
        if commit and (not new_instance or remains_active):
            series.save()

        return series


class EventPushToFacebookInfoForm(forms.Form):
    """ Used for _displaying_ the push-event-to-facebook form on event promoting page.
    """
    name = forms.CharField(_("Event name"),
                           widget=forms.TextInput(attrs={'class': 'input-block-level'}))
    description = forms.CharField(_("Event description"),
                                  widget=forms.Textarea(attrs={'rows': 5, 'class': 'input-block-level'}))
    facebook_page_url = forms.CharField(label=_("Facebook page URL"), required=False,
                                        widget=forms.TextInput(attrs={'class': 'input-block-level'}),
                                        placeholder=_("e.g. https://www.facebook.com/events"))

    def __init__(self, event, *args, **kwargs):
        kwargs.setdefault('initial', {})
        kwargs['initial'].setdefault('name', event.name)
        kwargs['initial'].setdefault('description', event.description)

        super(EventPushToFacebookInfoForm, self).__init__(*args, **kwargs)

        if event.venue.facebook_page_id:
            del self.fields['facebook_page_url']


class EventPushToFacebookForm(forms.Form):
    """ Used for _validating_ data when an event is pushed to facebook from the event promoting page.
    """
    venue_facebook_id = forms.CharField(widget=forms.HiddenInput)
    facebook_id = forms.CharField(widget=forms.HiddenInput)
    access_token = forms.CharField(widget=forms.HiddenInput)
    cover_image = forms.ImageField(required=False)


class EventTableSellingForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = (
            'table_booking_enabled',
            'table_booking_until',
            'table_booking_help_text',
            'table_booking_areas',
        )

    table_booking_enabled = forms.BooleanField(label="", initial=True, required=False, widget=ToggleCheckboxInput)
    table_booking_until = forms.DateTimeField(label=_("Sell tables until"), widget=BootstrapDateTimeWidget)
    table_booking_help_text = forms.CharField(label=_("Table booking description"),
                                              widget=forms.Textarea(attrs={'rows': 4, 'class': 'input-block-level'}),
                                              placeholder=_("Click on the button to browse floorplans and to select a table"))
    table_booking_areas = forms.models.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple,
                                                                queryset=VenueArea.objects.none())

    def __init__(self, *args, **kwargs):
        print('EventTableSellingForm')

        super(EventTableSellingForm, self).__init__(*args, **kwargs)
        self.fields['table_booking_areas'].queryset = VenueArea.objects.filter(venue=self.instance.venue)

        # Ensure that we have an existing Event
        assert self.instance.id
        if not self.initial['table_booking_until']:
            self.initial['table_booking_until'] = self.instance.start_time_local

        org = self.instance.owner_org
        if org.id != self.instance.venue.owner_org_id:
            # Can't use tables because organizer isn't venue's owner
            self.status = 'disabled'
            del self.fields['table_booking_enabled']
            del self.fields['table_booking_until']
            del self.fields['table_booking_help_text']

        elif not org.user_table_booking_enabled():
            self.status = 'no_floorplans'
            del self.fields['table_booking_enabled']
            del self.fields['table_booking_until']
            del self.fields['table_booking_help_text']

        else:
            self.status = 'enabled'

        if not org.feature_booking_area_specific:
            self.area_bookings_enabled = False
            del self.fields['table_booking_areas']
        else:
            self.area_bookings_enabled = True

    def clean(self):
        super(EventTableSellingForm, self).clean()

        _cleaned = self.cleaned_data
        if 'table_booking_enabled' in _cleaned and not _cleaned['table_booking_enabled']:
            self.remove_field_error('table_booking_until')
            self.remove_field_error('table_booking_help_text')
        if _cleaned.get('table_booking_enabled', False) and _cleaned['table_booking_until'] > self.instance.end_time:
            self.append_field_error('table_booking_until', force_unicode(_("Table booking cannot end after event's end time")))

        return _cleaned

    def save(self, commit=True):
        event = super(EventTableSellingForm, self).save(False)

        event.table_booking_enabled = self.cleaned_data['table_booking_enabled']

        # Activate all/none of the menu items for this event
        if event.table_booking_enabled:
            if not event.table_booking_menu_items.exists():
                event.table_booking_menu_items.add(*list(ShopItem.objects.menu_items(event.owner_org.shop)))
        else:
            event.table_booking_menu_items.clear()

        if 'table_booking_until' in self.cleaned_data:
            event.table_booking_until = self.cleaned_data['table_booking_until']
        if 'table_booking_help_text' in self.cleaned_data:
            event.table_booking_help_text = self.cleaned_data['table_booking_help_text']
        if 'table_booking_areas' in self.cleaned_data:
            event.table_booking_areas = self.cleaned_data['table_booking_areas']

        if commit:
            event.save()

        return event

    def append_field_error(self, field, error):
        self._errors[field] = self._errors.get(field, ErrorList())
        self._errors[field].append(error)

    def remove_field_error(self, field):
        try:
            del self._errors[field]
        except:
            pass


class EventAddNoteForm(forms.ModelForm):
    """
        Displays a promoter form for an event
    """
    class Meta:
        model = EventNote
        fields = ('text',)

    text = forms.CharField(label=_('Add note <i class="icon-pencil"></i>'), required=False, widget=forms.Textarea(attrs={
            # Change rows attribute, so that the autosize would work correctly
            'rows': '2',
        }))

    def __init__(self, *args, **kwargs):
        self.event = kwargs.pop('event', None)
        self.user = kwargs.pop('user', None)

        self.helper = FormHelper()
        self.helper.layout = Layout(
            Field('text', css_class='hide-field-background'),
            Div(
                HTML('<button type="reset" id="reset-id-cancel" class="btn" name="cancel">%s</button>' % ugettext('Cancel')),
                HTML('<button type="submit" id="submit-id-save" class="btn btn-primary" name="save">%s</button>' % ugettext('Save')),
                css_class='form-actions hide',
            ),
        )

        super(EventAddNoteForm, self).__init__(*args, **kwargs)

        self.fields['text'].placeholder = mark_safe(_("Click to add notes&hellip;"))

    def save(self, commit=True):
        assert self.event
        assert self.user

        note = super(EventAddNoteForm, self).save(False)
        note.event = self.event
        note.created_by = self.user

        if commit:
            note.save()

        return note


class TableBookingMobileForm(forms.Form):
    """ Simple form for rendering table booking form on mobile devices.
    """
    name = forms.CharField(placeholder=_("John Smith"), max_length=80)
    email = forms.EmailField(label=_("E-mail"), max_length=128, placeholder=_("john@events.com"), required=False)
    party_m = forms.ChoiceField(label=_("Males"), choices=[(i,i) for i in range(0,41)], required=False)
    party_f = forms.ChoiceField(label=_("Females"), choices=[(i,i) for i in range(0,41)], required=False)
    phone_number = forms.CharField(label=_("Phone"), max_length=32, required=False)
    arrival_time = forms.CharField(label=_("Expected Arrival Time"), max_length=64, required=False)
    comment = forms.CharField(label=_("Notes"), placeholder=_("Notes to manager"), max_length=4096, required=False, widget=forms.Textarea(attrs={
        'class': 'input-block-level',
        'rows': '3',
    }))

    helper = FormHelper()
    helper.form_tag = False
    helper.layout = Layout(
        Field('name', css_class='input-block-level'),
        Field('email', css_class='input-block-level'),
        Field('party_m', css_class='input-block-level'),
        Field('party_f', css_class='input-block-level'),
        Field('phone_number', css_class='input-block-level'),
        Field('arrival_time', css_class='input-block-level'),
        Field('comment'),
    )

class UserManagementForm(forms.ModelForm):
    """ Base form for user/permissions management.

    It handles some common parts of user management, including handling of unregistered users, sending invites and
    deleting permissions.
    """
    class Meta:
        model = UserPermission
        fields = ('id',)

    id = forms.IntegerField(widget=forms.HiddenInput, required=False)
    email = forms.EmailField(widget=forms.HiddenInput)
    delete = forms.BooleanField(widget=forms.HiddenInput, initial=False, required=False)

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')

        if 'initial' not in kwargs:
            kwargs['initial'] = {}
        if 'instance' in kwargs and kwargs['instance']:
            kwargs['initial']['email'] = kwargs['instance'].user.email

        super(UserManagementForm, self).__init__(*args, **kwargs)

    def get_user(self, email, create=True):
        # If we're working on an existing permission, we already have the user
        if self.instance.pk:
            # Ensure that the email address is valid (we don't allow changing it)
            assert self.instance.user.email.lower() == email.lower()
            return self.instance.user

        # Try to find user(s) matching the entered email from the DB
        matching_users = User.objects.filter(email__iexact=email).order_by('-is_active')
        # TODO: what if multiple users match? Currently we use first one of them (i.e. random matching user)
        if len(matching_users) >= 1:
            # Auto-accept for the matching user
            return matching_users[0]
        elif create:
            # Max username length is 30chars, so use a prefix plus 24chars of sha1 of email
            username = '__inv:' + hashlib.sha1(email).hexdigest()[:24]
            # Create a new inactive user
            user = User.objects.create_user(username, email)
            user.is_active = False
            user.save()

            return user
        else:
            # Create temporary user object, don't save into DB
            return User(email=email, is_active=False)

    def update_instance(self, permission):
        """ Called when UserPermission instance's data should be updated.

        This is called for both new and existing permissions from save() method.
        """
        raise NotImplementedError

    def new_instance(self, to_user, from_user, permission, request):
        """ Called after a new permission has been created and saved.
        """
        pass

    def save(self, commit=True):
        permission = super(UserManagementForm, self).save(False)

        # Are we deleting the permission?
        if self.cleaned_data['delete']:
            if self.instance.pk:
                # Mark the permission as inactive (closed)
                permission.closed_by = self.request.user
                permission.closed_timestamp = timezone.now()

                if commit:
                    permission.save()

            return permission

        # Are we creating a new instance?
        if not self.instance.pk:
            permission.user = self.get_user(self.cleaned_data['email'])
            permission.created_by = self.request.user

        self.update_instance(permission)

        is_new_instance = not self.instance.pk
        if commit:
            permission.save()

        if is_new_instance:
            self.new_instance(permission.user, self.request.user, permission, request=self.request)

        return permission


class OrganizationUserForm(UserManagementForm):
    class RoleFieldRenderer(RadioFieldRenderer):
        """ Fancier radiobutton rendering for organization's user role selection.
        """

        TEMPLATE = u"""
        <label for="%(id)s" class="well needsclick no-border no-shadow">
            <span class="role-name pop-font smb-font">%(choice_label)s</span>
            <span class="role-description">
                %(description)s
            </span>
            <span class="role-input">
                <input type="radio" id="%(id)s" value="%(choice_value)s" name="%(name)s"
                    class="pull-right"%(checked_attr)s>
            </span>
        </label>
        """

        def render(self):
            choices_html = []
            for choice in self:
                if 'custom' != choice.choice_value:
                    choices_html.append(self.TEMPLATE % {
                        'id': '%s_%s' % (choice.attrs['id'], choice.index),
                        'name': choice.name,
                        'description': UserPermission.EMPLOYEE_ROLE_DATA[choice.choice_value]['description'],
                        'choice_value': choice.choice_value,
                        'choice_label': choice.choice_label,
                        'checked_attr': ' checked' if choice.choice_value == choice.value else '',
                    })

            return mark_safe('\n'.join(choices_html))

    role = forms.ChoiceField(choices=UserPermission.EMPLOYEE_ROLE_CHOICES,
                             widget=forms.RadioSelect(renderer=RoleFieldRenderer))

    def __init__(self, *args, **kwargs):
        self.organization = kwargs.pop('organization')

        super(OrganizationUserForm, self).__init__(*args, **kwargs)

        if self.instance.pk:
            self.initial['role'] = self.instance.get_role_id()
            # Add 'custom' to roles combo if that's the current role.
            if self.initial['role'] == 'custom':
                self.fields['role'].choices = self.fields['role'].choices + [('custom', _('Custom'))]

    def update_instance(self, permission):
        # Update the user's role
        permission.set_role(self.cleaned_data['role'])
        permission.organization = self.organization

    def new_instance(self, to_user, from_user, permission, request):
        if to_user.is_active:
            # Don't send invites to registered users
            return

        key_type = 'organization-user-%d' % self.organization.id
        data = {
            'organization': self.organization.id,
            'userpermission': permission.id,
        }
        key = InvitationKey.objects.create_invitation(from_user, to_user.email, key_type, data)
        accept_url = self.request.build_absolute_uri(reverse('invite_member-accept', args=(key.key,)))
        #print "    Send invite email to", invitee_email, "; url is", accept_url
        send_venueadmin_invite_email(to_user.email, key, self.organization, from_user, accept_url)

        metrics.record("PRO", "Invitation Sent", {'user_type': permission.get_role_id()}, user=from_user, request=request)
        metrics.record("PRO", "Invitation Recieved", {'user_type': permission.get_role_id()}, user=to_user, request=request)


class OrganizationAddUserForm(OrganizationUserForm):
    email = forms.EmailField()
    role = forms.ChoiceField(choices=UserPermission.EMPLOYEE_ROLE_CHOICES, widget=forms.Select)

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Field('email', css_class='input-block-level'),
            Field('role', css_class='input-block-level'),
        )
        super(OrganizationAddUserForm, self).__init__(*args, **kwargs)


class VisitorCategoryForm(forms.ModelForm):

    class Meta:
        model = VisitorCategory
        fields = ('name',)


    name = forms.CharField(label="", placeholder=_('Category name'), max_length=20, required=True)

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        self.group = kwargs.pop('group', None)

        super(VisitorCategoryForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Field('name', css_class='input-block-level'),
        )

    def clean_name(self):
        name = self.cleaned_data['name']

        if VisitorCategory.objects.filter(group=self.group, name__iexact=name).exists():
            raise forms.ValidationError(_('Category %(name)s already exists') % {'name': name})

        return name

    def save(self, commit=True):
        category = super(VisitorCategoryForm, self).save(commit=False)
        category.created_by = self.user
        category.group = self.group
        category.position =  VisitorCategory.objects.filter(group=self.group).count()

        if commit:
            category.save()

        return category


class VisitorCategoryGroupForm(forms.ModelForm):

    class Meta:
        model = VisitorCategoryGroup
        fields = ('name',)

    name = forms.CharField(label="", placeholder=_('Group name'), max_length=20, required=True)

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        self.organization = kwargs.pop('organization', None)

        super(VisitorCategoryGroupForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Field('name', css_class='input-block-level'),
            )

    def clean_name(self):
        name = self.cleaned_data['name']

        if VisitorCategoryGroup.objects.filter(organization=self.organization, name__iexact=name).exists():
            raise forms.ValidationError(_('Group %(name)s already exists') % {'name': name})

        return name

    def save(self, commit=True):
        group = super(VisitorCategoryGroupForm, self).save(commit=False)
        group.created_by = self.user
        group.organization = self.organization

        if commit:
            group.save()

        return group


class ListUserForm(UserManagementForm):
    """ Form for giving users access to individual lists.
    """
    class Meta:
        model = UserPermission
        fields = ('id', 'list_limit')

    email = forms.EmailField()
    # list_limit's max value is 1 billion because we have some Promoters with insanely high list_limits and we can't
    #  set it to a lower value here since ListUserForm is used to activate these promoters when a new event is created.
    #  We can lower this value only when no active Promoter has a bigger value.
    list_limit = forms.IntegerField(min_value=0, max_value=1000000000, required=False, placeholder=_("unlimited"))

    def __init__(self, *args, **kwargs):
        self.list = kwargs.pop('list', None)

        if 'initial' not in kwargs:
            kwargs['initial'] = {}

        super(ListUserForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Field('email', css_class='input-block-level'),
            Field('list_limit', css_class='input-block-level'),
            'delete',
            )

    def clean_email(self):
        email = self.cleaned_data['email']

        if not self.instance.pk:
            if UserPermission.objects.filter(list=self.list, user__email__iexact=email).exists():
                raise forms.ValidationError(ugettext("Manager with the same e-mail already exists"))

        return email

    def update_instance(self, permission):
        permission.list = self.list
        if not permission.pk:
            permission.set_permissions(UserPermission.ALL_PERMISSIONS)

        if not permission.pk and not self.list.event:
            # Add the same permission to all event lists synced from this permanent list
            synced_event_lists = VenueList.objects.filter(list_type=VenueList.TYPE_NORMAL, owner=self.list.owner,
                    event__end_time__gt=timezone.now(), sync_from=self.list)
            already_have_managers = UserPermission.objects.filter(list__id__in=[list.id for list in synced_event_lists],
                                                                  user__email__iexact=permission.user.email).values_list('list__id', flat=True)
            for synced_list in synced_event_lists:
                if synced_list.id not in already_have_managers:
                    synced_perm = UserPermission()
                    synced_perm.user = permission.user
                    synced_perm.list = synced_list
                    synced_perm.created_by = permission.created_by
                    synced_perm.list_limit = permission.list_limit
                    synced_perm.set_permissions(UserPermission.ALL_PERMISSIONS)
                    synced_perm.save()

    def new_instance(self, to_user, from_user, permission, request):
        # Figure out subject for the email/notification
        if permission.list.event:
            start_date = formats.date_format(permission.list.event.start_time_local.date(), 'events_GRAPH_DATE')
            subject = _("You were asked to promote the event %(event_name)s @ %(venue_name)s on %(date)s") % {
                'event_name': permission.list.event.name,
                'venue_name': permission.list.event.venue.name,
                'date': start_date,
            }
        else:
            # It's a permanent list
            subject = _("You were asked to promote events of %(org_name)s") % {
                'org_name': permission.list.owner.display_name(),
            }

        if to_user.is_active:
            cache_key = "listuserform_sent_%s_%s" % \
                        (permission.list.event.id if permission.list.event else permission.list.owner_id, to_user.email)
            if not cache.get(cache_key):
                cache.set(cache_key, True, 60*60) # 1hour cache

                # Create a notification for the user
                NotificationMessage.create(subject, from_user, from_organization=permission.list.owner, to_user=to_user)

                # And send email as well
                if permission.list.event:
                    list_url = reverse('event_edit_lists', kwargs={ 'eventid': permission.list.event.id })
                else:
                    list_url = reverse('org_lists', kwargs={ 'organizationid': permission.list.owner_id })
                send_email(email=to_user.email, subject=subject,
                    template="emails/venueadmin_list_email.html",
                    permission=permission, list=permission.list, inviting_user=from_user,
                    list_url= self.request.build_absolute_uri(list_url))
            return

        key_type = 'list-user-%d' % self.list.id
        data = {
            'list': self.list.id,
            'userpermission': permission.id,
        }

        key = InvitationKey.objects.create_invitation(from_user, to_user.email, key_type, data)
        accept_url = self.request.build_absolute_uri(reverse('invite_member-accept', args=(key.key,)))
        #print "    Send invite email to", invitee_email, "; url is", accept_url
        send_venueadmin_list_invite_email(to_user.email, subject, key, permission, from_user, accept_url)

        km_type = "list manager"
        if permission.list.list_type == VenueList.TYPE_PROMOTER:
            km_type = "promoter"
        metrics.record("PRO", "Invitation Sent", {'user_type': km_type}, user=from_user, request=request)
        metrics.record("PRO", "Invitation Recieved", {'user_type': km_type}, user=to_user, request=request)


class OrganizationPromoterForm(forms.ModelForm):

    class Meta:
        model = Promoter
        fields = ('name', 'list_limit', 'commission_male', 'commission_female')

    id = forms.IntegerField(widget=forms.HiddenInput, required=False)
    email = forms.EmailField(widget=forms.HiddenInput, required=False)
    delete = forms.BooleanField(widget=forms.HiddenInput, initial=False, required=False)
    list_limit = forms.IntegerField(required=False, max_value=10000, help_text=_("Leave empty for unlimited"))
    commission_male = forms.DecimalField(label=_("Male commission"),
                                         min_value=0, max_digits=5, decimal_places=2, required=False)
    commission_female = forms.DecimalField(label=_("Female comm."),
                                           min_value=0, max_digits=5, decimal_places=2, required=False)

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        self.organization = kwargs.pop('organization')

        if 'initial' not in kwargs:
            kwargs['initial'] = {}
        if 'instance' in kwargs:
            kwargs['initial']['id'] = kwargs['instance'].id
            kwargs['initial']['email'] = kwargs['instance'].email
            kwargs['initial']['list_limit'] = kwargs['instance'].list_limit

        super(OrganizationPromoterForm, self).__init__(*args, **kwargs)

        # The first part makes sure we are not in ajax where it would otherwise be always deleted.
        if 'id' in kwargs['initial'] and not kwargs['initial'].get('email', None):
            del self.fields['list_limit']
            email_div = HTML('')
            list_limit_div = HTML('')
        else:
            email_div = Div(
                HTML('<label>' + self['email'].label + '</label>'),
                HTML('<span class="label promoter-email-label">' + self.instance.email + '</span>'),
                css_class='span6'
            )
            list_limit_div = Div(AppendedPlainText('list_limit', _('guests'), css_class='input-mini'), css_class='pull-left')

        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            'id', 'email', 'delete',  # Hidden fields
            Div(
                Div(
                    Div(Field('name', css_class='input-block-level'), css_class='span6'),
                    email_div,
                    css_class='row-fluid'
                ),
                Div(
                    list_limit_div,
                    Div(AppendedPlainText('commission_male', _('per guest'), css_class='input-mini'), css_class='pull-left'),
                    Div(AppendedPlainText('commission_female', _('per guest'), css_class='input-mini'), css_class='pull-left'),
                    css_class='fields-collapsing clearfix'
                ),
                css_class='fields-container',
            )
        )

    def clean_name(self):
        data = self.cleaned_data['name']
        same_name = Promoter.objects.filter(organization=self.organization, name__iexact=data)
        if self.instance.pk:
            same_name = same_name.exclude(id=self.instance.id)
        if same_name.exists():
            raise forms.ValidationError(_("Promoter with the same name already exists"))

        return data

    def clean_email(self):
        data = self.cleaned_data['email']
        same_email = Promoter.objects.filter(organization=self.organization, email__iexact=data)
        if self.instance.pk:
            same_email = same_email.exclude(id=self.instance.id)
        if data and same_email.exists():
            raise forms.ValidationError(_("Promoter with the same e-mail already exists"))

        return data

    def clean(self):
        cleaned = self.cleaned_data

        if not 'email' in cleaned:
            cleaned['list_limit'] = 0

        return cleaned

    def save(self, commit=True):
        promoter = super(OrganizationPromoterForm, self).save(commit=False)

        if self.cleaned_data['delete']:
            if self.instance.pk:
                # Delete the promoter (mark closed)
                promoter.closed_by = self.request.user
                promoter.closed_timestamp = timezone.now()
                VenuePromoterList.objects.filter(event__start_time__gt=timezone.now(), promoter=promoter).delete()

                if commit:
                    promoter.save()

            return promoter

        # Are we creating a new instance?
        if not self.instance.pk:
            promoter.email = self.cleaned_data['email']
            promoter.list_limit = self.cleaned_data['list_limit'] if self.cleaned_data['email'] else None

            promoter.organization = self.organization
            promoter.created_by = self.request.user

        if commit:
            promoter.save()

        return promoter


class OrganizationAddPromoterForm(OrganizationPromoterForm):
    email = forms.EmailField(required=False)

    def __init__(self, *args, **kwargs):
        super(OrganizationAddPromoterForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Field('name', css_class='input-block-level'),
            Field('email', css_class='input-block-level'),
            Field('list_limit', css_class='input-block-level'),
            Div(
                Div(AppendedPlainText('commission_male', _('per guest'), css_class='input-mini'), css_class='span6'),
                Div(AppendedPlainText('commission_female', _('per guest'), css_class='input-mini'), css_class='span6'),
                css_class='row-fluid'
            ),
        )


class VenueClaimForm(forms.Form):
    email_address = forms.EmailField(label=_("Email"), required=True)

    new_org_name = forms.CharField(label=_("New organizer name"), required=False,
        help_text=_("Enter the name of your company, e.g. 'Awesome Organizer, Inc.'"))

    organization = OrganizationField(label=_("Organization"), required=False, queryset=Organization.objects.all(),
        help_text=_("Choose the organization (company) that will be the owner of this venue and will manage it."))

    def __init__(self, *args, **kwargs):
        selected_org = kwargs.pop('selected_org', None)
        user = kwargs.pop('user')
        super(VenueClaimForm, self).__init__(*args, **kwargs)

        # Set organization choices
        self.fields['organization'].queryset = UserPermission.get_user_organizations(user, with_permission='org_set_venue').filter(venue=None)
        # Append add_new to the choices
        organization_choices = list(self.fields['organization'].choices)
        organization_choices.append(("add_new", _('Add new organization...')))
        self.fields['organization'].choices = organization_choices
        if selected_org:
            self.fields['organization'].initial = selected_org.id

        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.layout = Layout(
            Field('email_address', css_class='input-2xlarge'),
            Field('organization', css_class='input-2xlarge'),
            Field('new_org_name', css_class='input-2xlarge'),
            FormActions(
                Submit('save_changes', _('Send request'), css_class="btn-primary"),
            )
        )


class VenueMenuItemForm(forms.Form):
    """ Simple form to validate incoming menu item values.

    Meant only for validation.
    """
    name = forms.CharField(max_length=50)
    price = forms.DecimalField(decimal_places=2, min_value=1, max_digits=6)


class VenueListMemberForm(forms.ModelForm):

    class Meta:
        model = VenueListMember
        fields = ('full_name', 'comment')

    uuid = forms.CharField(max_length=32, required=False, widget=HiddenInput)
    list = forms.IntegerField(min_value=1, widget=HiddenInput)
    full_name = forms.CharField(label=_("Guest name"), placeholder=_('Ashton Kutcher +2'), max_length=80)

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        self.event = kwargs.pop('event', None)
        self.permissions = None
        self.user = kwargs.pop('user', self.request.user)

        self.max_plus_count = kwargs.pop('max_plus_count', 1000)
        self.allow_comment = kwargs.pop('allow_comment', True)

        # These come from clean()
        self.list = None
        self.ticket_count = None
        self.error_list_full = False # Is the list full?
        self.will_be_full = False # Will the list be full after this guest

        self.is_modify = False
        self.old_name = ""

        super(VenueListMemberForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            'uuid',
            'list',
            Field('full_name', css_class='input-block-level'),
            Field('comment', css_class='input-block-level'),
            )

    def clean(self):
        data = self.cleaned_data
        if 'full_name' not in data or 'comment' not in data or 'list' not in data:
            # We already have errors
            return data

        if self.instance.pk:
            self.old_name = self.instance.full_name
            self.is_modify = True

        try:
            self.list = VenueList.objects.get(id=data['list'])
        except:
            self.append_field_error('list', ugettext("Please select a list"))
        else:
            # For permalists the object is an Organization, otherwise Event
            if self.user:
                object = self.list.event if self.list.event else self.list.owner
                if not object.has_user_permission(self.user, 'list_edit'):
                    # Does the user have explicit permissions to some of the lists?
                    self.permissions = UserPermission.objects.filter(list=self.list, user=self.user, permission_list_edit=True)
                    if not self.permissions:
                        raise PermissionDenied("%s.list_edit" % self.user)

            if self.list.list_type == VenueList.TYPE_FREE_TICKET:
                # Allow no more than 5 pluses in free ticket list
                self.max_plus_count = min(5, self.max_plus_count)

        self.is_permalist = self.list.event is None

        raw_name = self.cleaned_data['full_name'].strip()

        parser = VenueListLineParser()
        name, plus, comment = parser.parse_line(raw_name)
        if not name:
            self.append_field_error('full_name', ugettext("Wrong name format"))
        elif plus > self.max_plus_count:
            self.append_field_error('full_name', ugettext("Max plus count is %d") % self.max_plus_count)
        else:
            data['full_name'] = name
            self.ticket_count = plus+1 if plus else 1
            if self.allow_comment and comment:
                data['comment'] = comment + ', ' + data['comment']
            comment = data['comment']

            list_member_exists = VenueListMember.objects.filter(full_name__iexact=data['full_name'], comment__iexact=comment, list=self.list)
            if self.instance:
                list_member_exists = list_member_exists.exclude(uuid=self.instance.uuid)
            if list_member_exists.exists():
                self.append_field_error('full_name', ugettext("Guest with the same name and comment already exists in the list"))

            if self.list and self.list.list_type == VenueList.TYPE_FREE_TICKET:
                if not VenueListLineParser().parse_email_from_comment(comment):
                    self.append_field_error('comment', ugettext("Comment must begin with email address"))

            # Ensure the list limit is not exceeded
            if self.permissions:
                # Skip the limit check if list member already in DB and ticket count is smaller than previously
                if not self.instance.pk or not self.ticket_count <= self.instance.ticket_count:
                    permission = filter(lambda p: p.list_id == self.list.id, self.permissions)
                    assert len(permission) == 1
                    permission = permission[0]

                    if self.instance:
                        users_guests = self.list.list_members_count(exclude_uuid=self.instance.uuid, created_by=self.user)
                    else:
                        users_guests = self.list.list_members_count(created_by=self.user)

                    users_guests += self.ticket_count
                    self.will_be_full = permission.list_limit and permission.list_limit <= users_guests

                    if permission.list_limit and permission.list_limit < users_guests:
                        self.error_list_full = True
                        self.append_field_error('full_name', ugettext("You have exceeded the limit of guests you are allowed to add to this list"))

        # Ensure that manager is editing his own guests
        if self.permissions and self.instance.pk:
            if self.instance.created_by != self.user:
                self.append_field_error('full_name', ugettext("You can't edit someone else's guests"))

        return data

    def save(self, commit=True, email=''):

        data = self.cleaned_data

        list_member = super(VenueListMemberForm, self).save(commit=False)
        is_new = not self.instance.pk

        if is_new:
            visitor = VenueVisitor.get_visitor(data['full_name'], email)

            list_member.list = self.list
            list_member.visitor = visitor
            list_member.created_by = self.user

            if commit and self.permissions:
                metrics.record("PRO", "Guest Added By Promoter", {'is_permalist': self.is_permalist}, request=self.request)

        list_member.ticket_count = self.ticket_count
        list_member.is_email_sent = False

        if commit:
            # propagate changes to the copies of this list when needed
            if self.is_permalist:
                member_data = VenueList.MemberData(
                    list_member.full_name,
                    list_member.ticket_count,
                    list_member.comment,
                    list_member.id)
                old_data = VenueList.MemberData(
                    self.old_name,
                    list_member.ticket_count,
                    list_member.comment,
                    list_member.id)
                item_data = {
                    member_data.key: member_data,
                }
                for list in VenueListForm.venue_active_event_lists(self.list.owner).filter(sync_from=self.list):
                    if self.is_modify:
                        if old_data.key in list.current_members_dict():
                            list.apply_diff([member_data.key],
                                            [old_data.key],
                                            [],
                                            list.current_members_dict(),
                                            item_data,
                                            self.user)
                    else:
                        list.apply_diff([member_data.key], [], [], [], item_data, self.user)

            list_member.save() # Saves for the current list.

            # Free ticket list magic
            if self.list.list_type == VenueList.TYPE_FREE_TICKET:
                existing_ticket_ids = FreeTicket.objects.filter(list_member=list_member).values_list('id', flat=True)
                if len(existing_ticket_ids) != list_member.ticket_count:
                    logging.info(
                        "VenueListMemberForm.save(): %s (id %d, list %d) has %d tickets, should have %d, adjusting...",
                        list_member, list_member.id, self.list.id, len(existing_ticket_ids), list_member.ticket_count)

                if len(existing_ticket_ids) > list_member.ticket_count:
                    FreeTicket.objects.filter(id__in=existing_ticket_ids[list_member.ticket_count:]).delete()
                elif len(existing_ticket_ids) < list_member.ticket_count:
                    for i in range(list_member.ticket_count - len(existing_ticket_ids)):
                        FreeTicket.objects.create(list_member=list_member, item_number=gen_item_number(self.list.event))
                    # TODO: should we send out the email again?

        if is_new:
            statsd.incr('pro.lists.regular.added')
        else:
            statsd.incr('pro.lists.regular.modified')

        return list_member

    def append_field_error(self, field, error):
        self._errors[field] = self._errors.get(field, ErrorList())
        self._errors[field].append(error)


class PromoteVenueListMemberForm(VenueListMemberForm):

    full_name = forms.CharField(label='', placeholder=_('Your Name'), min_length=6)
    email = forms.EmailField(label='', placeholder=_("Email Address"), max_length=128)

    def __init__(self, *args, **kwargs):
        # Call base.__init so we can override some fields
        super(PromoteVenueListMemberForm, self).__init__(*args, **kwargs)

        # Hide Comment Field
        self.fields['comment'].widget = forms.HiddenInput()

        # Change the layout object
        self.helper.layout = Layout(
            'list',
            Field('full_name'),
            Field('email'),
        )

    def clean_email(self):
        data = self.cleaned_data['email']
        if VenueListMember.objects.filter(visitor__email__iexact=data, list__id=self.cleaned_data['list']).exists():
            raise forms.ValidationError(_('You are already in this list!'))
        return data

    def save(self, commit=True):

        email = unicode(self.cleaned_data['email'])
        list_member = super(PromoteVenueListMemberForm, self).save(False, email=email)
        list_member.comment = email

        if commit:
            list_member.save()

        return list_member


class WysiwygArea(forms.Widget):
    """
        Renders a WYSIWYG "form field".

        It includes
         - hidden field that can be used as form element (NOTE: JS logic for including the value has to be added separately)
         - toolbar (custom toolbar html can be given to the widget with an attribute)
         - div element where the text can be edited (needs to be initialized separately)
    """
    media = forms.Media(js=[ 'js/jquery.hotkeys.js', 'js/bootstrap-wysiwyg.js' ])

    def __init__(self, *args, **kwargs):
        self.toolbar = kwargs.pop('toolbar', None)
        super(WysiwygArea, self).__init__(*args, **kwargs)


    def render(self, name, value, attrs=None, buttons_html=None):
        if value is None: value = ''
        final_attrs = self.build_attrs(attrs)
        toolbar = self.toolbar or """
            <div data-target="#editor" data-role="editor-toolbar" class="btn-toolbar">
                <div class="btn-group">
                    <a title="" data-toggle="dropdown" class="btn dropdown-toggle"><i class="awesome-text-height"></i>&nbsp;<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a data-edit="fontSize 5"><font size="5">%(huge)s</font></a></li>
                        <li><a data-edit="fontSize 3"><font size="3">%(normal)s</font></a></li>
                        <li><a data-edit="fontSize 1"><font size="1">%(small)s</font></a></li>
                    </ul>
                </div>
                <div class="btn-group">
                    <a title="" data-edit="bold" class="btn"><i class="awesome-bold"></i></a>
                    <a title="" data-edit="italic" class="btn"><i class="awesome-italic"></i></a>
                    <a title="" data-edit="underline" class="btn"><i class="awesome-underline"></i></a>
                </div>
                <div class="btn-group">
                    <a title="" data-edit="insertunorderedlist" class="btn"><i class="awesome-list-ul"></i></a>
                    <a title="" data-edit="insertorderedlist" class="btn"><i class="awesome-list-ol"></i></a>
                </div>
                <div class="btn-group">
                    <a title="" data-edit="justifyleft" class="btn"><i class="awesome-align-left"></i></a>
                    <a title="" data-edit="justifycenter" class="btn"><i class="awesome-align-center"></i></a>
                    <a title="" data-edit="justifyright" class="btn"><i class="awesome-align-right"></i></a>
                </div>
                <div class="btn-group">
                    <a data-target="#add-link-modal" class="btn" data-toggle="popup-modal">
                        <i class="awesome-link"></i>
                    </a>
                    <a data-edit="unlink" class="btn" data-original-title="Remove Hyperlink">
                        <i class="awesome-cut"></i>
                    </a>
                </div>
                <div id="add-link-modal" class="modal popup-modal hide" tabindex="-1">
                    <div class="modal-body input-append">
                        <input type="text" data-edit="createLink" placeholder="%(url)s">
                        <button type="button" class="btn" data-target="#add-link-modal" data-toggle="popup-modal">%(add)s</button>
                    </div>
                </div>
            </div>
        """ % {
            'huge': ugettext('Huge'),
            'normal': ugettext('Normal'),
            'small': ugettext('Small'),
            'url': ugettext('Web address'),
            'add': ugettext('Add'),
        }
        return mark_safe(toolbar + u'<input type="hidden" name="%s" value="%s">' % (name, value) + u'<div%s>%s</div>' % (flatatt(final_attrs),
                conditional_escape(force_unicode(value))))


class EventPromotionRecipientsForm(forms.ModelForm):

    class Meta:
        model = EventPromotionEmail
        fields = ()


    AGE_NONE = 0
    AGE_CHOICES = (
        (AGE_NONE, _('All')),
    )
    for i in range(16, 100):
        AGE_CHOICES += ((i, i),)

    ADDED_CHOICES = (
        (0, _('Everyone')),
        (3, _('Last 3 days')),
        (7, _('Last 7 days')),
        (30, _('Last 30 days')),
        (90, _('Last 90 days')),
    )

    tags = forms.CharField(label=_('Tags'), required=False, placeholder=_("Click here to select"))
    age_from = forms.ChoiceField(choices=AGE_CHOICES, initial=0, required=False)
    age_to = forms.ChoiceField(choices=AGE_CHOICES, initial=0, required=False)
    added_to_database = forms.ChoiceField(label=_('Added to database'), choices=ADDED_CHOICES, initial=0, required=False)


    def __init__(self, *args, **kwargs):
        self.event = kwargs.pop('event')
        super(EventPromotionRecipientsForm, self).__init__(*args, **kwargs)

    def clean_age_from(self):
        try:
            return int(self.cleaned_data['age_from'])
        except:
            return self.cleaned_data['age_from']

    def clean_age_to(self):
        try:
            return int(self.cleaned_data['age_to'])
        except:
            return self.cleaned_data['age_to']

    def clean_added_to_database(self):
        try:
            return int(self.cleaned_data['added_to_database'])
        except:
            return self.cleaned_data['added_to_database']

    def get_recipients(self):
        recipients = Person.objects.exclude(email='').filter(organization=self.event.owner_org)

        if self.cleaned_data['tags']:
            for tag in self.cleaned_data['tags'].split(','):
                recipients = recipients.filter(tags__id=tag)

        if self.cleaned_data['age_from'] or self.cleaned_data['age_to']:
            recipients = recipients.filter(birthday_date__isnull=False)
            today = timezone.now()
            if self.cleaned_data['age_from']:
                recipients = recipients.filter(birthday_date__lte=yearsago(self.cleaned_data['age_from']).date())
            if self.cleaned_data['age_to']:
                recipients = recipients.filter(birthday_date__gt=yearsago(self.cleaned_data['age_to'] + 1).date())

        if self.cleaned_data['added_to_database']:
            recipients = recipients.filter(created_timestamp__gt=timezone.now() - timedelta(days=self.cleaned_data['added_to_database']))

        return recipients.values_list('name', 'email')


class EventPromotionForm(EventPromotionRecipientsForm):

    class Meta:
        model = EventPromotionEmail
        fields = ('title', 'sender', 'content')

    def __init__(self, *args, **kwargs):
        self.event = kwargs['event']
        self.created_by = kwargs.pop('created_by')
        super(EventPromotionForm, self).__init__(*args, **kwargs)

        self.fields['title'].initial = self.event.name
        self.fields['title'].widget.attrs['class'] = 'input-block-level'
        self.fields['sender'].initial = self.created_by.email
        self.fields['sender'].widget.attrs['class'] = 'input-block-level'
        self.fields['content'].widget = WysiwygArea()

        self.fields['tags'].widget.attrs['class'] = 'input-block-level'
        self.fields['age_from'].widget.attrs['class'] = 'input-small'
        self.fields['age_to'].widget.attrs['class'] = 'input-small'
        self.fields['added_to_database'].widget.attrs['class'] = 'input-block-level'

    def save(self, commit=True):
        if not self.instance.pk:
            self.instance.event = self.event
            self.instance.created_by = self.created_by

        return super(EventPromotionForm, self).save(commit=commit)


class ChatAdminForm(forms.Form):
    body = forms.CharField(label=_("Message"), max_length=140)

    def __init__(self, *args, **kwargs):
        super(ChatAdminForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Field('body', css_class='input-2xlarge'),
            Submit('save_changes', _('Post'), css_class="btn-primary"),
        )


class WithdrawForm(forms.ModelForm):

    class Meta:
        model = Withdrawal
        fields = ('name', 'iban')

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        self.organization = kwargs.pop('organization')
        self.requested_amount = kwargs.pop('requested_amount')
        super(WithdrawForm, self).__init__(*args, **kwargs)

        self.fields['name'].initial = self.organization.official_name
        self.fields['iban'].initial = self.organization.iban_number
        self.fields['iban'].help_text = _("""International Bank Account Number (IBAN) is an internationally agreed means of identifying bank accounts across national borders.""")
        self.fields['iban'].help_text_below = _("""Don't know your IBAN? <a href="http://www.ibancalculator.com/bic_und_iban.html" target="_blank">Use this calculator.</a>""")

        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Field('name', css_class='input-block-level'),
            Field('iban', css_class='input-block-level'),
        )

    def save(self, commit=True):
        if not self.instance.pk:
            self.instance.created_by = self.user
            self.instance.organization = self.organization
            self.instance.requested_amount = self.requested_amount

        return super(WithdrawForm, self).save(commit=commit)


class WithdrawalPayForm(forms.ModelForm):

    class Meta:
        model = Withdrawal
        fields = ('paid_by',)

    def __init__(self, *args, **kwargs):
        super(WithdrawalPayForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.layout = Layout(
            'paid_by',
            FormActions(
                Submit('confirm_payment', _('Confirm payment'), css_class="btn-primary"),
            )
        )

    def save(self, commit=True):
        withdrawal = super(WithdrawalPayForm, self).save(commit=False)

        withdrawal.paid_timestamp = timezone.now()
        if commit:
            withdrawal.save()

        return withdrawal

