# -*- coding: utf-8 -*-

from django.core.paginator import Paginator
from django.db import transaction
from django.db.models import Sum
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.serializers.json import DjangoJSONEncoder
from django.core.cache import cache
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.core.validators import validate_email
from django.db.models.query_utils import Q
from django.http import Http404, HttpResponse, HttpResponseBadRequest, HttpResponseRedirect, HttpResponseForbidden
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect, render_to_response
from django.template import RequestContext
from django.utils import formats, timezone
from django.utils.dates import WEEKDAYS_ABBR
from django.utils.dateformat import DateFormat
from django.utils.decorators import method_decorator
from django.utils.encoding import smart_str
from django.utils.formats import get_format, date_format
from django.utils.html import escape
from django.utils.translation import ngettext, pgettext, ugettext, ungettext, ugettext_lazy as _, string_concat
from django.views.decorators.cache import cache_control
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import last_modified, require_POST
from django.views.generic import TemplateView, View

from account.models import UserProfile, DistributorRegion
from account.forms import EditUserForm, OrganizationForm, ProEditProfileForm
from forms import WithdrawForm, OrganizationSubscriptionForm
from location.utils import get_timezone_by_ip, get_location_info_by_ip
from money import Money
from money.models import MoneyAccount, Currency
from notification.models import Notification
from notification.notifications import NotificationMessage
from people_database.forms import PersonForm, PersonPeriodFilterForm
from people_database.models import Person, PersonTag
from shop.models import (RedeemPackage, Shop, ShopItem, ShopItemData, PurchasedItem, ShopCategory, DeclinedItem,
                         ShopData, SpecialTicket)
from ui.templatetags.events_avatars import avatar_css_url
from utils.statsd import statsd, statsd_record_http_response_code
from venue.data import AREA_OBJECT_CHOICES
from venue.models import (Event, VisitorCategoryGroup, Venue, Promoter, VenueList, VenueListMember, VenueVisit,
                          VenuePromoterList, VenueVisitor, UserPermission,
                          Organization, PurchasesOverview, VisitorCategory, VenueArea, VenueTableBooking, EventAction,
                          VenueTicketBuyersList, VenueAreaTable, EventNote, EventPromotionEmail, VenueFacebookList,
                          Withdrawal)

from venueadmin.forms import (EventForm, EventPromoterFormSet, EventAddNoteForm, EventSeriesForm,
                              VenueFacebookListForm, VenueFreeListForm, VenueForm,
                              VenueClaimForm, OrganizationAddUserForm, OrganizationAddPromoterForm,
                              EventTableSellingForm, EventTicketForm, ListUserForm, VenueListMemberForm,
                              VenueListLineParser, CurrencySelectionForm, TableBookingMobileForm, WithdrawalPayForm,
                              EventPushToFacebookForm, EventPushToFacebookInfoForm, EventPromotionForm, ChatAdminForm,
                              EventForm2, EventTicketForm2, OrganizationFacebookShopForm)
from venue.notifications import VenueClaimRequest, EventCreationRequest
from venueadmin.tutorial import TUTORIAL_STEPS
from venueadmin.email import send_event_creation_email, send_org_greeting_email
from people_database.models import sync_person
from utils.avatars import avatar_image_path, mobile_avatar
from utils.utils import (get_pdf_data_from_html, merge_moneys, send_email, send_mandrill_email, slugify, natural_sort,
                         metrics, get_site_url)

# use drop-in replacement for csv which supports unicode
import unicodecsv as csv

import calendar
from collections import defaultdict
from datetime import date, datetime, timedelta, time as dtime
from decimal import Decimal, ROUND_HALF_UP
import facebook
from functools import wraps
from itertools import groupby, izip_longest
from hashlib import md5
import json
import logging
import pytz
import re
import time
import types
import math

from django_xhtml2pdf.utils import generate_pdf
from utils.facebook_utils import extract_id_token_from_str, FacebookObjectByIdFetcher
from utils import utils
from venueadmin.forms import OrganizationUserForm, OrganizationPromoterForm, OrganizationLegalInfoForm, VisitorCategoryForm, VisitorCategoryGroupForm
from social_auth.models import UserSocialAuth

import django_tables2 as tables
import stripe
from stripe.error import StripeError
import pipedrive


ADMIN_LOGIN_URL = "/app/login/"
# Increased when the format of guestlist.json changes. Forces page reload on client side.
GUESTLIST_FORMAT_VERSION = 3

stripe.api_key = settings.STRIPE_SECRET_KEY


def shop_should_ask_vat(organiser_shop, venue_event_is_held_at=None):
    """
        Organiser_shop is the newly created shop
        If called as by-product of event creation venue_event_is_held_at is the event location.

        Given a shop(entity bound to organisation) this function will evaluate if we should ask for VAT.
        if the organisation (or its venue) has an official address and country that will be the basis of the decision,
          else(if this function is called from creating a new event)
        the address of the venue the event is organised at is used.
    """
    # only country that we need to ask VAT for.
    COUNTRY_CODE_ESTONIA = 'EE'

    # where is the event organiser/ new shop's owner located at?
    if organiser_shop.owner.addr_country_code:
        return organiser_shop.owner.addr_country_code == COUNTRY_CODE_ESTONIA

    # if we don't know organiser's country code.. but he has a venue we can look at
    elif organiser_shop.owner.venue and organiser_shop.owner.venue.addr_country_code:
        return organiser_shop.owner.venue.addr_country_code == COUNTRY_CODE_ESTONIA

    # is event, look at events venue
    elif venue_event_is_held_at and venue_event_is_held_at.addr_country_code:
        return venue_event_is_held_at.addr_country_code == COUNTRY_CODE_ESTONIA

    # default is no VAT
    return False


def _create_venue_shop(organization, currency, created_by, event_venue=None):
    money_account = MoneyAccount()
    money_account.balance = Money(0, currency.code)
    money_account.save()

    # We'll take 0% plus banking fees from new venues
    margin = settings.DEFAULT_events_MARGIN
    shop = Shop.objects.create(owner=organization, money_account=money_account, default_currency=currency)
    ShopData.objects.create(shop=shop, margin=margin, enable_vat=shop_should_ask_vat(shop, event_venue))
    ShopCategory.objects.create(shop=shop, name=unicode(_('Tickets')), category_type=ShopCategory.TYPE_TICKET, created_by=created_by)
    return shop


def ensure_user_has_organization(func):
    """ Decorator to ensure that the user has an organization.

    If the user is not employee of any organization, a new organization is created for him.
    """
    @wraps(func)
    def wrapped_f(request, *args, **kwargs):
        user = request.user
        if user.is_authenticated() and not user.is_staff and not UserPermission.objects.filter(user=user).exists():
            return redirect('create_org')

        return func(request, *args, **kwargs)

    return wrapped_f


def redirect_venueadmin_to_app(request):
    """ Returns 301 redirect from /venueadmin/* to either PRO_HOSTNAME/* or /app/*
    """
    scheme = request.is_secure() and 'https' or 'http'
    subpath = request.get_full_path()[len("/venueadmin"):]

    pro_hostname = settings.HOSTS['pro'].get('hostname')
    if pro_hostname:
        return redirect(scheme + '://' + pro_hostname + subpath, permanent=False)
    else:
        return redirect('/app' + subpath, permanent=False)

def root_redirect(request):
    """ Redirects requests from root Pro path (e.g. pro.events.com or somedomain/pro/) to dashboard
    """
    return redirect('venueadmin_index')


@login_required(login_url=ADMIN_LOGIN_URL)
@ensure_user_has_organization
def starting_page(request):

    org = request.user_selected_org
    if not org:
        return root_redirect(request)

    steps = org.starting_progress()['steps']
    starting_page_actions = [
        {
            "name": _("Fill company details"),
            "text": _("Help us to pay you! If you enter your company details we are able to send you your payments when you have sold tickets or tables."),
            "icon": "suitcase",
            "action": {"url": reverse("org_legal_info", args=(org.id,)), "label": _("Fill Details")},
            "completed": steps['legal_info'],
            },
        {
            "name": _("Add your employees"),
            "text": _("Choose people who can have an access to events and select their permissions. Delegate, it makes your work easier."),
            "icon": "people",
            "action": {"url": reverse("organization_users", args=(org.id,)), "label": _("Add People")},
            "completed": steps['users'],
            },
        {
            "name": _("Start creating events"),
            "text": _("events is based on events. Create as many upcoming events as you can, then the chances of selling them out are higher."),
            "icon": "event",
            "action": {"url": reverse("create_event_wrapper"), "label": _("Create Event")},
            "completed": steps['create_event'],
            },
        {
            "name": _("Invite promoters"),
            "text": _("Let other people help you. Invite promoters to add guests to your lists. More people equals more revenues."),
            "icon": "promotors",
            "action": {"url": reverse("organization_promoters", args=(org.id,)), "label": _("Invite Promoters")},
            "completed": steps['promoters'],
            },
        {
            "name": _("Create floorplans"),
            "text": _("You need floorplans to sell your tables. It is really easy to create them, just drag and drop items to the room. Try it now!"),
            "icon": "floorplans",
            "action": {"url": reverse("venue_table_config", args=(org.venue.id,)) if org.venue else '', "label": _("Create Floorplans")},
            "completed": steps['floorplans'],
            },
        {
            "name": _("Use promotional tools"),
            "text": _("Add a shop to your Facebook page and allow your customers to buy tickets directly from Facebook. Share your public page."),
            "icon": "facebook",
            "action": {"url": reverse("venue_promo_tools", args=(org.venue.id,)) if org.venue else '', "label": _("Setup Tools")},
            "completed": steps['promo_tools'],
            },
        ]
    return render_to_response('venueadmin/starting_page.html', RequestContext(request, {
        'starting_page_actions': starting_page_actions,
        'ZOPIM_SHOW_POPUP': True,
        }))


@login_required(login_url=ADMIN_LOGIN_URL)
def create_org(request):
    user = request.user

    if user.is_staff or UserPermission.objects.filter(user=user).exists():
        # User has nothing to do here
        return redirect('venueadmin_index')

    company = None
    phone = None
    plan = Organization.PLAN_BASIC

    if 'plan' in request.GET and request.GET['plan'] == 'premium':
        plan = Organization.PLAN_PREMIUM

    if 'registration_fields' in request.session:
        company = request.session['registration_fields']['company']
        phone = request.session['registration_fields']['phone']
        plan = request.session['registration_fields']['plan']
        del request.session['registration_fields']

    organization_form = OrganizationForm(request.POST or None)
    if plan:
        organization_form.fields['plan'].initial = plan

    if organization_form.is_valid():
        company = organization_form.cleaned_data['company']
        phone = organization_form.cleaned_data['phone']
        plan = organization_form.cleaned_data['plan']

    if company:
        logging.info("Creating new organization for user (%d, %s)",
                     user.id, user.get_full_name())
        with transaction.commit_on_success():
            organization = Organization.objects.create(official_name=company, created_by=user, owner=user, phone=phone, plan=plan)
            perms = UserPermission.grant_all_permissions(user, by=user, org=organization)
            perms.save()
            statsd.incr('pro.orgs.created.auto_no_perms')
            metrics.record("PRO", "Organization created", user=organization.get_first_admin(), request=request)

        # Send out a welcome email.
        send_org_greeting_email(user, organization, request)

        is_premium = plan == Organization.PLAN_PREMIUM
        site_url = get_site_url() if request.subhost_name != 'pro' else ''

        org_url = "%s%s" % (site_url, reverse('org_show_by_id', kwargs={'orgid': organization.id}))

        org_loc = get_location_info_by_ip(request)
        org_name = organization.display_name()

        send_email(
            email='pro@events.com',
            template='emails/signup_to_pro.html',
            subject=('%s signed up for the PREMIUM plan' if is_premium else '%s signed up') % org_name,

            org_url=org_url,
            organization=organization,
            who_wanted=user,
            is_premium=is_premium,
            location=org_loc,
            )

        # Create new deal in Pipedrive
        pipedrive.create_new_deal(org_name,
                                  org_name + " deal (" + org_loc['country_code'] + ")",
                                  user.get_full_name(),
                                  user.email,
                                  organization.phone)

        # Always redirect to the starting page when a new org is created.
        return redirect('starting_page')

    return render_to_response('venueadmin/create_org.html', RequestContext(request, {
        'form': organization_form,
        }))


class DashboardView(TemplateView):
    template_name = 'venueadmin/dashboard/index.html'

    @method_decorator(login_required(login_url=ADMIN_LOGIN_URL))
    @method_decorator(ensure_user_has_organization)
    def dispatch(self, *args, **kwargs):
        return super(DashboardView, self).dispatch(*args, **kwargs)

    def get_events(self, request):
        if request.user_selected_org:
            # Organization view - events created by the org or in the org's venue
            org = request.user_selected_org
            events = Event.objects.filter(Q(owner_org=org) | Q(venue=org.venue))
        else:
            # Personal view - all actionable events
            events = UserPermission.get_actionable_events(request.user)

        # Are there any events.
        self.has_events = events.exists()

        # Limit events to those no more than 2 weeks old
        since = timezone.now() - timedelta(days=14)
        events = events.filter(end_time__gte=since).order_by('start_time').select_related('venue')

        Event.attach_tickets(events, sellable_only=False)
        for event in events:
            setattr(event, 'has_editable_lists', self.event_has_editable_lists(event, request.user))
            setattr(event, 'global_url', request.build_absolute_uri(event.get_absolute_url()))
            DashboardView.event_attach_counts(event)

        DashboardView.attach_promoter_permissions(events, request.user)

        return events

    @staticmethod
    def attach_promoter_permissions(events, user):
        # Check if the user is a promoter and can add table reservations for this event
        # Note that if promoter has a list limit, then can_book_tables is False
        event_ids = UserPermission.objects.filter(list__venuepromoterlist__promoter__email__iexact=user.email,
                                                  list_limit__isnull=True,
                                                  permission_list_edit=True,
                                                  user=user,
                                                  ).values_list("list__event", flat=True)

        for event in events:
            if event.id in event_ids:
                # Set event tickets, reservations and listed guests
                setattr(event, 'can_book_tables', True)

    @staticmethod
    def event_attach_counts(event):
        cache_key = 'event-dashboard-counts-%d-v2' % (event.id)

        # See if we have it cached
        data = cache.get(cache_key)
        if not data:
            # Calculate it since we don't have it cached.
            data = event.get_quickstats()
            cache.set(cache_key, data, 360*24*60*60)

        # Set event tickets, reservations and listed guests
        setattr(event, 'tickets', data['tickets'])
        setattr(event, 'reservations', data['reservations'])
        setattr(event, 'listed_guests', data['listed_guests'])

    @staticmethod
    def event_has_editable_lists(event, for_user):
        if event.has_user_permission(for_user, 'list_edit'):
            return True
        else:
            return UserPermission.objects.filter(list__event=event, user=for_user, permission_list_edit=True).exists()

    def group_events_by_time(self, events_all):
        # Sort all events into current and past bins
        events_past = []
        events_next = []
        for event in events_all:
            # Add it to the right list
            if event.is_archived():
                # Archived events are reversed, i.e. ordered most recent first
                events_past.insert(0, event)
            else:
                events_next.append(event)

        return events_past, events_next

    def get_fake_events(self):
        fake_org = Organization(
            official_name = _("My Organization Name"),
            )
        fake_evt = Event(
            id = 9999999,
            status = Event.STATUS_ACCEPTED,
            pageviews = 5,
            name = _("My Sample Event"),
            baseTicketPrice = "10.00",
            start_time = datetime.today() + timedelta(days=5),
            owner_org = fake_org,
            venue = Venue(
                name = _("My Venue Name"),
                owner_org = fake_org
            ),
            created_by = self.request.user,
            end_time = datetime.today() + timedelta(days=5) + timedelta(hours=5),
            created_timestamp = datetime.today() - timedelta(hours=5),
            description = _("My event description")
        )
        return [fake_evt, fake_evt]

    def get(self, request, venueid=None):
        events_all = self.get_events(request)

        events_past, events_next = self.group_events_by_time(events_all)

        no_upcoming_events = False
        # If no upoming events, create 2 fake events
        if not events_next:
            no_upcoming_events = True
            events_next = self.get_fake_events()

        # Active organizations and venues. Those are either the selected organization/venue, or all venues/orgs for
        #  which the user has some permissions (in the personal view).
        active_venues = UserPermission.get_user_venues(request.user)
        active_organizations = request.all_organizations

        # Show venue names for events unless we have a selected organization that's attached to a venue
        event_show_venue = not request.user_selected_org or not request.user_selected_org.venue

        notifications = Notification.objects.new_for_user(request.user)

        # Tutorial
        tutorial_text = ''
        tutorial_step = request.user.profile.venueadmin_tutorial_progress
        if tutorial_step >= 0:
            if tutorial_step < len(TUTORIAL_STEPS):
                tutorial_text = TUTORIAL_STEPS[tutorial_step]

        # At most how many current/upcoming events to show.
        upcoming_events_limit = 3
        past_events_limit = 4
        events_past = events_past[:past_events_limit]

        account_balance = None
        if request.user_selected_org:
            cache_key = 'org-account-balance-%d' % request.user_selected_org.id
            account_balance = cache.get(cache_key)
            if not account_balance:
                billing_view = BillingWebView()
                billing_view.initialize_data(request.user_selected_org)
                events_data_ongoing, events_data_not_withdrawn, events_data_withdrawn = billing_view.sort_events()
                account_balance = billing_view.get_account_balance(events_data_not_withdrawn, request.user_selected_org)
                # We save the balance to cache in BillingWebView.get_account_balance()

        return self.render_to_response({
            'all_organizations': request.all_organizations,
            'active_organizations': active_organizations,
            'active_venues': active_venues,

            'events_past': events_past,
            'events_past_columns': list(izip_longest(events_past[::2], events_past[1::2])),
            'events_upcoming': events_next[:upcoming_events_limit],
            'no_upcoming_events': no_upcoming_events,
            'have_more_upcoming_events': len(events_next) > upcoming_events_limit,
            'event_show_venue': event_show_venue,

            'nav_link_all': 'venueadmin_index',
            'nav_link_venue': 'venueadmin_index_venue',

            'notifications': notifications,

            'tutorial_step': tutorial_step + 1,
            'tutorial_steps_total': len(TUTORIAL_STEPS),
            'tutorial_text': tutorial_text,
            'FACEBOOK_APP_ID': getattr(settings, 'FACEBOOK_APP_ID', None),

            'force_desktop': self.request.COOKIES.get("force_desktop") == '1',
            'SITE_URL': get_site_url(None),

            'has_events': self.has_events,

            'show_upgrade': request.user_selected_org and request.user_selected_org.plan == Organization.PLAN_BASIC and self.request.COOKIES.get('ipmc_ignore') != '1',

            'account_balance': account_balance,
            'MINIMUM_WITHDRAW_AMOUNT': BillingWebView.MINIMUM_WITHDRAW_AMOUNT,
            })

    def post(self, request, venueid=None):
        venue = request.user_selected_org and request.user_selected_org.venue
        if not venue or not venue.has_user_permission(request.user, 'venue_edit'):
            return HttpResponseBadRequest()

        if 'facebookUrl' not in request.POST:
            return HttpResponseBadRequest()

        url = request.POST.get('facebookUrl')

        facebook_page_id = ''
        facebook_page_link = ''
        try:
            clean_fb_id = extract_id_token_from_str(url)
            obj = FacebookObjectByIdFetcher(clean_fb_id).get()
            facebook_page_id = obj['id']
            facebook_page_link = obj['link']
        except:
            pass
        else:
            venue.facebook_page_id = facebook_page_id
            venue.facebookUrl = facebook_page_link
            venue.save()

        return HttpResponseRedirect(reverse("venueadmin_index"))


class DashboardAllUpcomingEventsView(DashboardView):
    template_name = 'venueadmin/dashboard/all_upcoming_events.html'

    @method_decorator(ensure_user_has_organization)
    def dispatch(self, *args, **kwargs):
        return super(DashboardView, self).dispatch(*args, **kwargs)

    def get(self, request, venueid=None):
        if request.user.is_anonymous():
            raise Http404
        events_all = self.get_events(request)
        events_past, events_next = self.group_events_by_time(events_all)

        return self.render_to_response({
            'events_upcoming': events_next,
            })


@login_required(login_url=ADMIN_LOGIN_URL)
def org_all_events(request, organizationid=None):
    if organizationid:
        organization = get_object_or_404(Organization, id=organizationid)
        events = Event.objects.filter(owner_org=organization)
    else:
        # Personal view - all actionable events
        events = UserPermission.get_actionable_events(request.user)
    events = events.order_by('-start_time').select_related('venue__owner_org')

    # Paginate the events, 50 per page
    paginator = Paginator(events, 50)
    try:
        events_page = paginator.page(request.GET.get('page', 1))
    except:
        # If page is invalid, deliver first page.
        events_page = paginator.page(1)
    # Force eval of the paginated events query. If we don't do this, attach_tickets may fail since the events list will
    #  be evaluated twice with possibly different results.
    events = list(events_page.object_list)

    Event.attach_tickets(events, sellable_only=False)
    for event in events:
        setattr(event, 'has_editable_lists', DashboardView.event_has_editable_lists(event, request.user))
        DashboardView.event_attach_counts(event)

    # Group events by month
    def event_local_year_and_month(event):
        start_time_local = event.start_time_local
        return start_time_local.year, start_time_local.month

    periods = []
    for key, group in groupby(events, event_local_year_and_month):
        group_list = list(group)
        # Show only month if the event is less than 6 months old, otherwise month and year.
        start_time_local = group_list[0].start_time_local
        show_year = (timezone.now() - start_time_local) < timedelta(days=180)
        title = DateFormat(start_time_local).format('F' if show_year else 'F Y')

        periods.append((title, group_list))

    return render_to_response('venueadmin/dashboard/all_events.html', RequestContext(request, {
        'periods': periods,
        'events_page': events_page,
        }))


@login_required(login_url=ADMIN_LOGIN_URL)
def org_all_venues(request):
    venues = UserPermission.get_user_venues(request.user, with_permission=None)
    show_placeholder = False

    if not venues:
        show_placeholder = True
        venue = Venue(
            name = _("My Venue"),
            addr_country_code = _("US"),
            addr_city = _("Miami Beach"),
            addr_street = _("1399 Collins Ave"),
            )
        venues = [venue]
    else:
        for venue in venues:
            setattr(venue, 'public_url', request.build_absolute_uri(venue.get_absolute_url()))
            setattr(venue, 'avatar_css_url', avatar_css_url(venue, 116, 116, True))

    return render_to_response('venueadmin/all_venues.html', RequestContext(request, {
        'venues': sorted(venues, key=lambda venue: venue.name),
        'show_placeholder': show_placeholder,
        }))


class BillingViewBase(TemplateView):
    """ Shows the billing page for the current venue and PromoterEmployee combo.
        The data structure given to the template is:
        [
            {   # Data associated with a single event
                'event': {
                    'id': event.id,
                    'name': event.name,
                    'start_time': event.start_time,
                    'end_time': event.end_time,
                    'venue': Venue(),
                    'instance': Event(),
                    'purchases_overview_processed': event.purchases_overview_processed,
                 },
                'purchases': [
                    {
                        'purchase': Purchase(),
                        'event': Event(),
                        'purchase_timestamp': datetime(),
                        'purchased_items': [
                            {
                                'item_data': ShopItemData(),
                                'item': PurchasedItem(),
                                'price': Money(),
                                'event': Event(),
                            },
                            ...  # Next item
                        ],
                        'invisible_items': [...],   # same as purchased_items, but sold by events
                        'shop_data': ShopData(),
                        'fees': {...},              # same structure as fees in event data
                        'club_sales_total': Money(),
                    }
                    ... # Next purchase
                ],
                'purchased_item_groups': [
                    {
                        'count': int,
                        'item_data': ShopItemData(),
                        'price': Money(),
                        'total_price': Money(),
                    },
                    ... # Next group
                ],
                'overview': PurchasesOverview(),    # might be None
                'withdrawal': Withdrawal(),         # might be None
                'fees': {                           # all fees are negative
                    'bank_fees': [Money(), ...],
                    'margins': {
                        '5': [Money(), ...],        # margin percent: amounts
                        ...
                    },
                    'VATs': {
                        '20': [Money(), ...],       # VAT percent: amounts
                        ...
                    },
                },
                'club_sales_total': [EUR 10.00],    # Amount that the club sold items for
                'we_owe_club': [EUR  9.08],         # How much we owe the club (club_sales_total - fees)
            },
        ]
    """

    withdrawal_start_date = date(2013, 9, 1)

    @method_decorator(login_required(login_url=ADMIN_LOGIN_URL))
    @method_decorator(utils.permission_required("billing"))
    def dispatch(self, *args, **kwargs):
        return super(BillingViewBase, self).dispatch(*args, **kwargs)

    def initialize_data(self, organization, event=None):
        """
            Calls "filters" in a specific order which modify the internal data in
            this object and finally returns the correct and complete billing data
            structure that can be given to a template.
        """
        self.organization = organization

        if not self.organization.has_shop():
            # We show the empty billing page for organizations without shop (or without any sold items)
            self.event_datas = []
            self.events = []
            return

        # Build the final data structure by linearly applying
        # "filters" on the self data.
        self.fetch_data(event)
        self.group_purchaseditems_by_purchase()
        self.group_by_event()
        self.calculate_purchase_fees()
        self.calculate_event_fees()
        self.build_item_groups()
        #import pprint
        #pprint.pprint(self.event_datas, indent=4)

    def _round_money(self, money):
        """ Rounds moneys to 0.00 precision
        """
        money.amount = money.amount.quantize(Decimal('0.00'), rounding=ROUND_HALF_UP)
        return money

    def fetch_data(self, event=None):
        """
        Fetches all relevant objects from database.
        This includes events (or a single event, if we're creating report for a single event).
        """

        # Get events
        if event:
            self.events = [event]
        else:
            self.events = Event.objects.filter(owner_org=self.organization, end_time__gte=self.withdrawal_start_date)
            # TODO: should add paging
            self.events = self.events.order_by('-start_time').select_related('withdrawal', 'venue')

        # Get all organization's <ShopItemDatas>. We don't filter those by event since in case of menu items of table
        #  bookings, the ShopItemData is not event-specific but the PurchasedItem is.
        all_organization_shopitemdatas = ShopItemData.objects.filter(item__shop__owner=self.organization)
        # Get all acceptable purchaseditems that belong to this organization. This also filters our purchases made
        #  before the switchover date, which were included in the monthly overviews.
        self.purchaseditems = PurchasedItem.objects.paid_for(item_data__in=all_organization_shopitemdatas) \
            .filter(purchase__purchase_timestamp__gte=self.withdrawal_start_date) \
            .filter(event__in=self.events) \
            .select_related('item_data__item', 'event__venue', 'purchase__shop_data', 'purchase__bank_transaction')

        # Fetch purchase overviews
        self.overviews = PurchasesOverview.objects.filter(organization=self.organization, event__in=self.events)

    def group_purchaseditems_by_purchase(self):
        """
        Group items together as purchases.
        """
        get_purchase_id_from_item = lambda item: item.purchase.id

        self.purchases = []
        purchaseditems = sorted(self.purchaseditems, key=get_purchase_id_from_item)
        for key, group in groupby(purchaseditems, get_purchase_id_from_item):
            items_list = list(group)

            purchase = items_list[0].purchase
            event = items_list[0].event
            # Filter items into visible and invisible (= sold by events) items
            purchased_items = filter(lambda pi: pi.item_data.item.visible_in_billing, items_list)
            invisible_items = filter(lambda pi: not pi.item_data.item.visible_in_billing, items_list)
            if not purchased_items:
                # Don't consider purchases which contain only invisible items
                continue

            self.purchases.append({
                'purchase': purchase,
                'event': event,
                'purchase_timestamp': purchase.purchase_timestamp,
                'purchased_items': purchased_items,
                'invisible_items': invisible_items,
                'shop_data': purchase.shop_data,
                'price': purchase.price,
                })

    def group_by_event(self):
        """
        Creates self.event_datas which contains data associated with every event.
        For each event, we add event data (id, name, etc), list of purchases for this event, purchases overview object
        and withdrawal object (if they exist).
        Later we'll add purchased item groups and various totals for every event.
        """

        self.event_datas = []
        for event in self.events:
            event_overviews = filter(lambda po: po.bill_type == PurchasesOverview.TYPE_OVERVIEW and po.event.id == event.id, self.overviews)
            assert len(event_overviews) <= 1, "Event %d has %d PurchasesOverview objects" % (event.id, len(event_overviews))

            event_purchases = filter(lambda p: p['event'].id == event.id, self.purchases)

            event_data = {
                'event': {
                    'id': event.id,
                    'name': event.name,
                    'start_time': event.start_time,
                    'end_time': event.end_time,
                    'venue': event.venue,
                    'instance': event,
                    'purchases_overview_processed': event.purchases_overview_processed,
                    },
                'purchases': event_purchases,
                'overview': event_overviews[0] if event_overviews else None,
                'withdrawal': event.withdrawal,
                }

            self.event_datas.append(event_data)

    def calculate_purchase_fees(self):
        """
        Calculates per-purchase fees (banking fees, margins and VATs) for each purchase.
        Also calculates total amount of club sales. This is usually same as the purchase price but may differ if some
        of the sold items were sold by events.
        """

        for purchase in self.purchases:
            purchase['fees'] = {}
            purchase['fees']['bank_fees'] = None
            purchase['fees']['margins'] = defaultdict(list)
            purchase['fees']['VATs'] = defaultdict(list)
            # We start with the price that the user paid (Purchase.price).
            # This includes various fees and possibly also some items which are sold by events (invisible items).
            # There is a chance that a single purchase contains both items which should be visible in
            # billing (= sold by the organizer) as well as those that shouldn't (= sold by events). In this
            # case the purchase price does not equal the total price of items sold by the club. So here we
            # take the total price of items sold by events and subtract it from the purchase price.
            purchase['club_sales_total'] = purchase['price']
            if purchase['invisible_items']:
                purchase['club_sales_total'] -= sum([pi.item_data.price for pi in purchase['invisible_items']])

            if purchase['shop_data'].enable_margin:
                # Calculate margin fee
                margin_amount = -(purchase['club_sales_total'] * purchase['shop_data'].margin)
                margin_percent = (purchase['shop_data'].margin * 100).normalize().to_eng_string()
                purchase['fees']['margins'][margin_percent].append(margin_amount)

                if purchase['shop_data'].enable_vat:
                    # Calculate VAT
                    # margin_amount is already negative, no need to negate
                    vat_amount = (margin_amount * purchase['shop_data'].vat)
                    vat_percent = (purchase['shop_data'].vat * 100).normalize().to_eng_string()
                    purchase['fees']['VATs'][vat_percent].append(vat_amount)

            if purchase['shop_data'].enable_banking_fees:
                purchase['fees']['bank_fees'] = purchase['purchase'].get_banking_fees()

    def calculate_event_fees(self):
        """
        Sum fees of all purchases in an event.
        Also calculates the total amount of club sales for that event and the amount that we owe to the club (club sales
        minus various fees).
        """
        for event_data in self.event_datas:
            event_purchases = event_data['purchases']

            event_data['club_sales_total'] = merge_moneys([p['club_sales_total'] for p in event_purchases])

            event_data['fees'] = {}
            event_data['fees']['margins'] = defaultdict(list)
            event_data['fees']['VATs'] = defaultdict(list)
            event_data['fees']['bank_fees'] = []

            # Add up all the fees
            for purchase in event_purchases:
                for percent, amounts in purchase['fees']['margins'].items():
                    event_data['fees']['margins'][percent] += amounts
                for percent, amounts in purchase['fees']['VATs'].items():
                    event_data['fees']['VATs'][percent] += amounts
                if purchase['fees']['bank_fees']:
                    event_data['fees']['bank_fees'] += purchase['fees']['bank_fees']

            # Merge fees
            we_owe_club = [m for m in event_data['club_sales_total']]  # Make copy of the list
            for percent, amounts in event_data['fees']['margins'].iteritems():
                event_data['fees']['margins'][percent] = merge_moneys(amounts)
                we_owe_club += event_data['fees']['margins'][percent]
            for percent, amounts in event_data['fees']['VATs'].iteritems():
                event_data['fees']['VATs'][percent] = merge_moneys(amounts)
                we_owe_club += event_data['fees']['VATs'][percent]
            event_data['fees']['bank_fees'] = merge_moneys(event_data['fees']['bank_fees'])
            # Bank fees are positive, so negate them.
            event_data['fees']['bank_fees'] = [-fee for fee in event_data['fees']['bank_fees']]
            we_owe_club += event_data['fees']['bank_fees']

            # Django templates have problem with defaultdict.items, so convert them to ordinary dicts here.
            event_data['fees']['margins'] = dict(event_data['fees']['margins'])
            event_data['fees']['VATs'] = dict(event_data['fees']['VATs'])

            event_data['we_owe_club'] = [self._round_money(m) for m in merge_moneys(we_owe_club)]

    def build_item_groups(self):
        """
        Groups purchased items by price and name.
        We don't want to group by ShopItemData, since that could give us several lines with same content (items with
        same name/price but different ShopItemData, e.g. when sale times are changed).

        Adds 'purchased_item_groups' dict to each event in self.event_datas
        """
        def keyfunc_characteristics(item):
            """ Group purchaseditems by event, price and name. """
            return item.event, item.item_data.price, item.item_data.name

        def get_item_group(items):
            item_groups = []
            items = sorted(items, key=keyfunc_characteristics)
            for key, group in groupby(items, keyfunc_characteristics):
                group_list = list(group)

                item_group = dict()
                item_group['event'] = group_list[0].event
                item_group['item_data'] = group_list[0].item_data
                item_group['price'] = group_list[0].item_data.price
                item_group['count'] = len(group_list)
                item_group['total_price'] = item_group['count'] * item_group['price']

                item_groups.append(item_group)

            return item_groups

        for event_data in self.event_datas:
            purchased_items = sum([p['purchased_items'] for p in event_data['purchases']], [])

            event_data['purchased_item_groups'] = get_item_group(purchased_items)


class BillingWebView(BillingViewBase):
    template_name = "venueadmin/billing.html"
    MINIMUM_WITHDRAW_AMOUNT = 50


    def sort_events(self):
        # Sort events into three groups - not yet ended, ended but not withdrawn and withdrawn
        events_data_ongoing = []
        events_data_not_withdrawn = []
        events_data_withdrawn = []
        for event_data in self.event_datas:
            if event_data['withdrawal']:
                events_data_withdrawn.append(event_data)
            elif event_data['event']['end_time'] < timezone.now():
                events_data_not_withdrawn.append(event_data)
            else:
                events_data_ongoing.append(event_data)

        return events_data_ongoing, events_data_not_withdrawn, events_data_withdrawn

    @staticmethod
    def get_account_balance(events_data_not_withdrawn, organization):
        from venueadmin.cron import CreatePurchasesOverviews
        # Calculate total balance
        account_balance = []
        for event_data in events_data_not_withdrawn:
            # Generate purchase overview if it is not in the database already.
            # We need this to send to book keeping with the withdraw request and there might be ended events,
            # which overviews are not generated yet by the CreatePurchaseOverviews cron.
            if not event_data['overview'] and not event_data['event']['purchases_overview_processed']:
                logging.info("Creating purchase overview for event #%d,"
                             "because it has not been created by the CreatePurchaseOverviews cron yet.",
                             event_data['event']['id'])
                overview = CreatePurchasesOverviews.process_event(event_data['event']['instance'])
                if overview:
                    event_data['overview'] = overview
                Event.objects.filter(id=event_data['event']['id']).update(purchases_overview_processed=True)

            account_balance += event_data['we_owe_club']
        account_balance = merge_moneys(account_balance)

        assert len(account_balance) <= 1
        account_balance = account_balance[0] if account_balance else Money(0, organization.currency)

        cache.set('org-account-balance-%d' % organization.id, account_balance, 5*60)

        return account_balance

    def get(self, request, organizationid):
        organization = get_object_or_404(Organization, id=organizationid)

        self.initialize_data(organization)

        # Withdrawals
        previous_withdrawals = organization.withdrawals.all().order_by('-created_timestamp')[:5]

        events_data_ongoing, events_data_not_withdrawn, events_data_withdrawn = self.sort_events()
        account_balance = self.get_account_balance(events_data_not_withdrawn, organization)

        withdraw_form = WithdrawForm(user=request.user, organization=organization, requested_amount=account_balance)

        can_withdraw = account_balance.amount >= self.MINIMUM_WITHDRAW_AMOUNT or request.user.is_staff
        if request.POST and can_withdraw:
            withdraw_form = WithdrawForm(
                request.POST,
                user=request.user,
                organization=organization,
                requested_amount=account_balance
            )
            if withdraw_form.is_valid():
                withdrawal = withdraw_form.save()
                Event.objects.filter(id__in=[e['event']['id'] for e in events_data_not_withdrawn]).update(withdrawal=withdrawal)

                attachments = []
                for event_data in events_data_not_withdrawn:
                    if event_data['overview']:
                        attachments.append({'type': 'application/pdf',
                                            'name': slugify(event_data['event']['name']) + '-purchases.pdf',
                                            'content': event_data['overview'].pdf_content,
                                            })

                if len(settings.events_PURCHASES_OVERVIEW_RECEIVERS):
                    send_mandrill_email(
                        request=request,
                        emails=settings.events_PURCHASES_OVERVIEW_RECEIVERS,
                        subject="Withdraw request from %s" % organization.display_name(),
                        from_addr=request.user.email,
                        from_name=request.user.get_full_name(),
                        attachments=attachments,
                        template='emails/pro/withdraw_request.html',
                        tags=['withdraw_request'],
                        confirm_url=request.build_absolute_uri(withdrawal.get_absolute_url()),
                        withdrawal=withdrawal,
                        )

                return redirect('venueadmin_billing', organizationid=organizationid)

        # Check if we have any older purchase overviews (not event-specific)
        has_old_overviews = PurchasesOverview.objects.filter(organization=self.organization, event=None).exists()

        org_timezone = 'UTC'
        if organization.venue:
            org_timezone = organization.venue.timezone
        elif self.events:
            org_timezone = self.events[0].venue.timezone

        return render_to_response('venueadmin/billing.html', RequestContext(request, {
            'data': self.event_datas,
            'events_data_ongoing': events_data_ongoing,
            'events_data_not_withdrawn': events_data_not_withdrawn,
            'events_data_withdrawn': events_data_withdrawn,
            'organization': organization,
            'org_timezone': org_timezone,
            'current_date': date.today(),
            'account_balance': account_balance,
            'withdraw_form': withdraw_form,
            'previous_withdrawals': previous_withdrawals,
            'has_old_overviews': has_old_overviews,
            'can_withdraw': can_withdraw,
            'MINIMUM_WITHDRAW_AMOUNT': self.MINIMUM_WITHDRAW_AMOUNT,
            }))

    def post(self, request, organizationid):
        return self.get(request, organizationid)


class BillingPDFView(BillingViewBase):
    template_name = "venueadmin/billing_pdf.html"

    payment_days = settings.PAYMENT_DAYS
    type_choices = dict()
    for choice, name in PurchasesOverview.TYPE_CHOICES:
        type_choices[name] = choice


    def get(self, request, organizationid):
        organization = get_object_or_404(Organization, id=organizationid)

        if 'event' in request.GET:
            # Event-based purchases overview
            event = get_object_or_404(Event, id=request.GET.get('event'))
            pdf_overview = get_object_or_404(PurchasesOverview, organization=organization, event=event)

        else:
            # Period-based purchases overview (for backwards compatibility)
            try:
                start_date = datetime.strptime(request.GET.get('period_start'),"%d-%m-%Y").date()
                end_date = datetime.strptime(request.GET.get('period_end'),"%d-%m-%Y").date()
                bill_type = request.GET.get('bill_type')
                assert bill_type in self.type_choices
            except Exception as e:
                return HttpResponseBadRequest('Some arguments are missing')

            try:
                pdf_overview = PurchasesOverview.objects.get(organization=organization,
                                                             period_start=start_date, period_end=end_date, bill_type=self.type_choices[bill_type])
            except (PurchasesOverview.MultipleObjectsReturned, PurchasesOverview.DoesNotExist) as e:
                raise Http404

        resp = HttpResponse(content_type = 'application/pdf')
        resp['Content-Disposition'] = 'inline; filename=%s' % pdf_overview.file_name

        resp.write(pdf_overview.pdf_content.decode('base64'))

        return resp

    @classmethod
    def get_PDF_file(cls, event):
        """ Generates PDF overview of the event's purchases

        This is only used by the cronjob that generates PurchasesOverview reports and is here to make use of the logic
        shared with the html view.
        """

        view = cls()
        organization = event.owner_org
        view.initialize_data(organization, event)

        assert len(view.event_datas) == 1
        event_data = view.event_datas[0]

        # Don't generate PDF if the event had no relevant purchases
        if not event_data['purchases']:
            return None

        result = generate_pdf('venueadmin/billing_pdf.html', context={
            'event_data': event_data,
            'organization': organization,
            })
        return result


@login_required(login_url=ADMIN_LOGIN_URL)
@utils.permission_required("billing")
def purchase_overviews_old(request, organizationid):
    organization = get_object_or_404(Organization, id=organizationid)

    overviews = PurchasesOverview.objects.filter(organization=organization, event=None).order_by('-period_start')

    return render_to_response('venueadmin/billing_old.html', RequestContext(request, {
        'organization': organization,
        'overviews': overviews,
        }))


@login_required(login_url=ADMIN_LOGIN_URL)
@utils.permission_required("event_show_guests")
def show_event(request, eventid):
    event = get_object_or_404(Event, id=eventid)

    # Get category groups of event's organization. Don't get generic (gender) groups because those are hardcoded in the
    #  html template.
    cat_groups = VisitorCategoryGroup.objects.filter(organization=event.owner_org).order_by('id')
    for group in cat_groups:
        setattr(group, 'sorted_categories', group.categories.filter(closed_by=None).order_by('id'))

    areas_data = VenueArea.get_event_areas_data(event)
    promoters = [promoter.name for promoter in Promoter.objects.filter(organization=event.owner_org)]

    # Get all the employees for the internal chat
    users = UserPermission.objects.filter(organization=event.owner_org)

    # Add some info for social sharing
    setattr(event, 'global_url', request.build_absolute_uri(event.get_absolute_url()))
    setattr(event, 'avatar_url', request.build_absolute_uri(avatar_image_path(event, 110, 155, user_site=True)))

    # Guestlist will be loaded separately by a JSON query, so nothing more to do here.
    return render_to_response('venueadmin/event.html', RequestContext(request, {
        'event': event,
        'event_readonly': event.is_archived() or not event.has_user_permission(request.user, 'event_checkin_guests'),
        'cat_groups': cat_groups,
        'TIME_FORMAT': get_format('TIME_FORMAT'),
        'NOTE_TIME_FORMAT': get_format('DATETIME_FORMAT_JS'),
        'GUESTLIST_FORMAT_VERSION': GUESTLIST_FORMAT_VERSION,

        'areas': json.dumps(areas_data, indent=2),
        'has_areas': len(areas_data) is not 0,
        'promoters': json.dumps(promoters, indent=1),
        'party_size': range(0, 41),

        'users': users,
        }))


def get_lists_data(event, seperate_types=False, ignore_tables=False):
    guests, tags, lists, list_infos = event.get_guestlist()
    # Group guests by lists
    guests_by_list = defaultdict(list)
    for guest in guests:
        guests_by_list[guest['list']].append(guest)

    def get_list_info(lName):
        for tList in list_infos:
            if tList["name"].lower() == lName.lower():
                return tList["type"], \
                       tList.get('description', None), \
                       tList.get('sellable', False), \
                       tList.get('update_until', False), \
                       tList.get('last_update', False),
        return None, None, None, None, None

    # For each list, we want to separate tally and ordinary guests.
    lists_data = []
    tickets_data = []

    for list_name, guests in sorted(guests_by_list.items(), key=lambda t: t[0].lower()):
        # Group tally guests by comment (which also contains categories)
        tally_guests = defaultdict(int)
        for tally_guest in filter(lambda g: g['type'] == 'tally', guests):
            key = "%s - %s" % (tally_guest['name'], tally_guest['comment']) if tally_guest['comment'] else tally_guest['name']
            tally_guests[key] += tally_guest['quantity']

        list_type, list_desc, sellable, update_until, last_update = get_list_info(list_name)

        if ignore_tables and list_type == 'tables':
            continue

        if seperate_types:
            arr = tickets_data if list_type == 'ticket' else lists_data
        else:
            arr = lists_data

        arr.append({
            'name': list_name,
            'guests': filter(lambda g: g['type'] != 'tally', guests),
            'tally': sorted(tally_guests.items(), key=lambda item: item[0].lower()),
            'description': list_desc,
            'sellable': sellable,
            'update_until': update_until if update_until else None,
            'last_update': last_update if last_update else None,
            })

    return lists_data, guests, tickets_data

def get_table_data(event):
    # Get table bookings
    bookings = VenueTableBooking.objects.filter(Q(purchase=None) | Q(purchase__status=PurchasedItem.STATUS_BILLED),
                                                list_member__list__event=event)

    res = defaultdict(list)
    for v in bookings:
        res[v.table.area.name if v.table else ugettext('Booking List')].append(v)

    groups = [{
                  'name': k,
                  'bookings': [x.serialize() for x in sorted(v, key=lambda item: item.list_member.full_name)]
              } for k, v in res.items()]

    return groups


class EventPrintView(TemplateView):
    template_name = "venueadmin/event_print.html"

    class ColumnBuilder(object):
        CONFIG = {
            'renderArea': {
                'height': 1314,
                'width': 929
            },

            'pageMargin': [25, 25, 25, 25], # t, r, b, l

            'page_break': {
                'height': 113,
                'paddingTop': 7,
                'paddingBottom': 20
            },

            'list_name': {
                'lineHeight': 20,
                'paddingTop': 5,
                'paddingBottom': 5,
                'marginBottom': 10,

                'maxLineLength': 24,

                'renderFunc': '_render_list_name'
            },

            'tally_row': {
                'lineHeight': 20,
                'marginBottom': 7,

                'maxLineLength': 26,
                'renderFunc': '_render_tally_row'
            },

            'guest_row': {
                'lineHeight': 20,
                'marginBottom': 7,

                'maxLineLength': 26,
                'renderFunc': '_render_guest_row'
            },
            'table_row': {
                'lineHeight': 20,
                'marginBottom': 15,

                'maxLineLength': 25,
                'renderFunc': '_render_table_row'
            },

            'empty_row': {
                'lineHeight': 30
            }

        }

        MODE_LIST = '0'
        MODE_TALLY = '1'
        MODE_TABLES = '2'

        def __init__(self, lists, mode=MODE_LIST):
            self.colums = 3
            self.lists = lists
            self.ret = []

            self.cPage = 0
            self.cCol = 0
            self.oldList = None

            self.mode = mode

            # status coords
            self.renderAreaH = self.CONFIG['renderArea']['height'] - self.CONFIG['pageMargin'][0] - self.CONFIG['pageMargin'][3] - \
                               self.elem_height({'type': 'page_break', 'page': self.cPage})

            self.curX = 0


        def _render_list_name(self, col, cfg):
            assert cfg['maxLineLength'] != 0
            lines = math.ceil(float(len(col['list']['name'])) / float(cfg['maxLineLength']))
            if 'description' in col['list'] and col['list']['description']:
                lines += math.ceil(float(len(col['list']['description'])) / float(cfg['maxLineLength']))

            if col['list'].get('sellable', False):
                lines += 2
            if col['list'].get('update_until', False):
                lines += 1
            if col['list'].get('last_update', False):
                lines += 1

            return lines * cfg['lineHeight']

        def _render_guest_row(self, col, cfg):
            assert cfg['maxLineLength'] != 0

            name = "%(name)s%(plus)s" % {
                'name': col['guest']['name'],
                'plus': '+%d  (%d/%d)' % (col['guest']['quantity'] - 1,
                                          col['guest']['entered_count'],
                                          col['guest']['quantity']) if col['guest']['quantity'] > 0 else ''
            }
            lines = math.ceil(float(len(name)) / float(cfg['maxLineLength']))

            comment = col['guest'].get('comment')
            if col['guest'].get('ticket_numbers'):
                comment = map(lambda tn: tn['number'], col['guest'].get('ticket_numbers'))
            if comment:
                lines += math.ceil(len(comment) / float(cfg['maxLineLength']))

            return lines * cfg['lineHeight']

        def _render_tally_row(self, col, cfg):
            assert cfg['maxLineLength'] != 0

            name = "%d x %s" % (col['guest']['quantity'], col['guest']['comment'])
            return math.ceil(float(len(name)) / float(cfg['maxLineLength']))

        def _render_table_row(self, col, cfg):
            assert cfg['maxLineLength'] != 0
            booking = col['guest']
            height = 0

            name = '%(name)s%(count)s (%(party_m)d%(label_m)s / %(party_f)d%(label_f)s' % {
                'name': booking['name'],
                'count': ' +%d' % (booking['ticket_count']-1) if booking['ticket_count'] > 0 else '',
                'party_m': booking['party_m'],
                'label_m': pgettext("Males with one letter", 'M'),
                'party_f': booking['party_f'],
                'label_f': pgettext("Females with one letter", 'F')
            }
            lines = math.ceil(float(len(name)) / float(cfg['maxLineLength']))

            if 'table_name' in booking and booking['table_name']:
                str = '%s %s' % (ugettext('Table'), booking['table_name'])
                lines += math.ceil(float(len(str)) / float(cfg['maxLineLength']))

            if 'comment' in booking and booking['comment']:
                height += 7 # Bottom margin of comment
                lines += math.ceil(float(len(booking['comment'])) / float(cfg['maxLineLength']))

            if 'purchased_items' in booking and booking['purchased_items']:
                height += 10
                for item in booking['purchased_items']:
                    lines += math.ceil(float(len(item)) / float(cfg['maxLineLength'] - 1))

            others = [('email', ''), ('phone_number', ''), ('occasion', 'Occasion'), ('booked_by', 'Booked By'), ('arrival_time', 'Approx Arrival')]
            for field, label in others:
                if field in booking and booking[field]:
                    str = '%s%s' % ('%s: ' % ugettext(label) if label else '', booking[field])

                    lines += math.ceil(float(len(str)) / float(cfg['maxLineLength']))

            if booking['arrived_timestamp'] is not None or booking['left_timestamp'] is not None:
                return max(110, height + float(lines * cfg['lineHeight']))
            else:
                return height + float(lines * cfg['lineHeight'])

        def elem_height(self, elem):
            if elem['type'] not in self.CONFIG:
                raise NotImplementedError("ColumnBuilder::elem_height(): Element %s height is not configured." % elem['type'])

            allowed_modifiers = map(lambda x: 'margin%s' % x, ['Top', 'Bottom', 'Left', 'Right'])
            allowed_modifiers += map(lambda x: 'padding%s' % x, ['Top', 'Bottom', 'Left', 'Right'])

            elem_cfg = self.CONFIG[elem['type']]

            if 'height' in elem_cfg:

                return elem_cfg['height'] + sum(map(lambda x: elem_cfg[x] if x in elem_cfg else 0, allowed_modifiers))

            elif 'lineHeight' in elem_cfg:
                lH = elem_cfg['lineHeight']

                if 'renderFunc' in elem_cfg:
                    method = getattr(self, elem_cfg['renderFunc'], None)
                    if method:
                        lH = method(elem, elem_cfg)

                return lH + sum(map(lambda x: elem_cfg[x] if x in elem_cfg else 0, allowed_modifiers))

            else:
                raise NotImplementedError("ColumnBuilder::elem_height(): %s height can not be calculated." % elem['type'])

        def column_start(self):
            self.curX = 0
            self.ret.append({'type': 'column_start'})

        def column_stop(self):
            self.ret.append({'type': 'column_stop'})
            self.curX = 0
            self.cCol += 1

            if self.cCol >= self.colums:
                self.page_break()

        def page_break(self):
            self.cCol = 0
            self.cPage += 1

            elem = {'type': 'page_break', 'page': self.cPage}
            self.ret.append(elem)

        def list_name(self, list):
            elem = {'type': 'list_name', 'list': list}

            self.ret.append(elem)
            self.curX += self.elem_height(elem)

        def list_break(self):
            if self.curX == 0:
                return

            elem = {'type': 'empty_row'}

            self.ret.append(elem)
            self.curX += self.elem_height(elem)

            # Could this be done as post processing? So we don't create un-needed orphans
            if self.curX > self.renderAreaH * 0.40:
                self.column_stop()
                self.column_start()

        def page_titles(self, list):
            if self.curX == 0:
                self.column_start()
                if self.cCol == 0:
                    self.list_name(list)

        def guest_row(self, guest, list, key='guest_row'):
            self.page_titles(list)

            if (self.oldList and self.oldList != list):
                self.oldList = list
                self.list_break()
                self.list_name(list)

            elem = {'type': key, 'guest': guest}
            h = self.elem_height(elem)

            if self.curX + h > self.renderAreaH:
                self.column_stop()

            self.page_titles(list)

            self.ret.append(elem)
            self.curX += h

        def get_inner_arr(self, list):
            if self.mode == self.MODE_TALLY:
                return list["tally"]
            elif self.mode == self.MODE_TABLES:
                return list["bookings"]
            else:
                return list["guests"]

        def build_columns(self):
            self.ret = []
            self.curX = 0
            empty=False

            # Add page break to start so header is added to first page.
            self.page_break()

            if not self.lists:
                self.column_start()
                empty=True

            for list in self.lists:
                arr = self.get_inner_arr(list)

                for guest in arr:
                    if self.mode == self.MODE_TALLY:
                        guest = {
                            'quantity': guest[1],
                            'comment': guest[0]
                        }
                        self.guest_row(guest, list, key='tally_row')
                    elif self.mode == self.MODE_TABLES:
                        self.guest_row(guest, list, key='table_row')
                    else:
                        self.guest_row(guest, list)

                self.oldList = list

            if self.ret[-1]["type"] != 'column_stop':
                # Make sure column is properly ended after last item in list
                self.column_stop()

            if self.ret[-1]["type"] == 'page_break':
                # Remove empty page from end
                self.cPage -= 1
                self.ret = self.ret[:-1]

            return self.ret, self.cPage, empty

    @method_decorator(login_required(login_url=ADMIN_LOGIN_URL))
    @method_decorator(utils.permission_required("event_show_guests"))
    def dispatch(self, *args, **kwargs):
        return super(EventPrintView, self).dispatch(*args, **kwargs)

    def get(self, request, eventid):
        event = get_object_or_404(Event, id=eventid)

        pdf_data = EventPrintView.get_PDF_file(event, request, skip=False)

        response = HttpResponse(content_type = 'application/pdf')
        response['Content-Disposition'] = 'inline; filename="%s.pdf"' % ugettext('events %(event_name)s' % {'event_name': slugify(event.name)})
        response.write(pdf_data)

        return response

    def initialize_data(self, event):
        lists_data, guests, tickets_data = get_lists_data(event, True, ignore_tables=True)
        lists_tally = filter(lambda x: len(x['tally']) > 0, lists_data)

        self.list_columns, self.total_list_pages, list_empty = self.ColumnBuilder(lists_data).build_columns()
        self.ticket_columns, self.total_ticket_pages, ticket_empty = self.ColumnBuilder(tickets_data).build_columns()
        self.tally_columns, self.total_tally_pages, tally_empty = self.ColumnBuilder(lists_tally, mode=self.ColumnBuilder.MODE_TALLY).build_columns()

        table_data = get_table_data(event)
        self.table_columns, self.total_table_pages, tables_empty = self.ColumnBuilder(table_data, mode=self.ColumnBuilder.MODE_TABLES).build_columns()

        self.empty = list_empty and ticket_empty and tally_empty and tables_empty

    @classmethod
    def get_PDF_file(cls, event, request=None, skip=True):
        view = cls()
        view.initialize_data(event)

        # Don't generate PDF if the event had no relevant data
        if skip and view.empty:
            return None

        data = render_to_response(view.template_name, RequestContext(request, {
            'event': event,
            'MEDIA_ROOT': settings.FILE_URL_PREFIX + settings.MEDIA_ROOT,

            'list_columns': view.list_columns,
            'total_list_pages': view.total_list_pages,

            'ticket_columns': view.ticket_columns,
            'total_ticket_pages': view.total_ticket_pages,

            'tally_columns': view.tally_columns,
            'total_tally_pages': view.total_tally_pages,

            'table_columns': view.table_columns,
            'total_table_pages': view.total_table_pages,

            'currency': event.owner_org.currency,

            'empty': view.empty,

            'print_time': timezone.now()
        }))

        return get_pdf_data_from_html(data.content)


@login_required(login_url=ADMIN_LOGIN_URL)
@utils.permission_required("event_show_guests")
def show_event_csv(request, eventid):
    event = get_object_or_404(Event, id=eventid)

    lists_data, guests, tickets_data = get_lists_data(event)

    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(mimetype='text/csv; header=present')
    response['Content-Disposition'] = 'attachment; filename="%s.csv"' % slugify(event.name)

    writer = csv.writer(response, delimiter=formats.get_format('CSV_DELIMITER'))
    headers = [ugettext('Guest Name'), ugettext('List'), ugettext('Listed Count'),
               ugettext('Entered Count'), ugettext('Email')]

    writer.writerow(headers)
    for list_data in lists_data:
        for guest in list_data['guests']:
            writer.writerow([
                guest['name'],
                list_data['name'],
                guest['quantity'],
                guest['entered_count'],
                guest.get('email', ''),
                ])
        for comment, quantity in list_data['tally']:
            writer.writerow([
                comment,
                list_data['name'],
                quantity,
                quantity,
                '',
                ])

    return response

def last_modified_guestlist(request, eventid):
    """ Given a request and eventid it returns datetime value for when
    the guestlist page was last modified.
    This takes into account several factors:
        * Last modified redeempackages related to the event
        * Last purchased ticket to the event
        * Last added/modified vip
    This is used in the Django @last_modified decorator to set a Last-Modified header.
    """
    cache_key = 'event-%s-data-last-modified' % eventid
    last_modified = cache.get(cache_key, None)
    if not last_modified:
        now = timezone.now()
        cache.set(cache_key, now, 360*24*60*60)
        return now
    return last_modified


@login_required(login_url=ADMIN_LOGIN_URL)
@statsd_record_http_response_code('pro.checkin_view.guestlist_return_code')
@last_modified(last_modified_guestlist)
def event_guestlist(request, eventid):
    event = get_object_or_404(Event, id=eventid)

    promoter = False
    if not event.has_user_permission(request.user, "event_show_guests"):
        promoter = True
        if not Promoter.can_book_tables(request.user, event):
            raise PermissionDenied("%s.%s" % (request.user, "event_show_guests"))

    # Backwards compatibility
    req_version = request.GET.get('v', 0)

    # Allow expired events to show all tickets...
    expiry_timestamp = event.end_time + Event.EVENT_STILL_EDITABLE_FOR
    extra_statuses = [PurchasedItem.STATUS_EXPIRED] if expiry_timestamp < timezone.now() else []

    guests, tags, lists, list_infos = event.get_guestlist(old_format=req_version==0, extra_statuses=extra_statuses)


    if req_version == 0:
        # Dump guests and tags into response and return it
        json_dump = json.dumps({
            'guests': guests,
            'tags': list(tags),
            'lists': list(lists),
            'lists_data': list_infos,
            'version': 0,
            })
    else:
        # Compile aggregated lists. TODO: move this to event.get_guestlist() once we don't need to support old version
        #  anymore.
        lists_data = {}
        for l in list_infos:
            l_name = l['name']
            if l_name not in lists:
                # This list isn't visible, skip it.
                continue

            if l_name not in lists_data:
                lists_data[l_name] = {
                    'name': l_name,
                    'has_tally': False,
                    'type': l['type'],
                    }

            if l['type'] != 'ticket':
                lists_data[l_name]['has_tally'] = True

        bookings = []
        # Get table bookings
        all_bookings = VenueTableBooking.objects.filter(list_member__list__event=event)
        if promoter:
            all_bookings = all_bookings.filter(booked_by__email__iexact=request.user.email)

        all_bookings = all_bookings.prefetch_related('list_member__visitor', 'purchase__items__item_data', 'table')

        for booking in all_bookings:
            bookings.append(booking.serialize())

        if promoter:
            # Lets return early and show only bookings to the promoter
            json_dump = json.dumps({
                'bookings': bookings,
                'version': GUESTLIST_FORMAT_VERSION,
                })
            return HttpResponse(json_dump, mimetype='application/json')

        notes = [ note.serialize() for note in EventNote.objects.filter(event=event) ]

        # Dump guests and tags into response and return it
        json_dump = json.dumps({
            'guests': guests,
            'lists': lists_data.values(),
            'bookings': bookings,
            'notes': notes,
            'version': GUESTLIST_FORMAT_VERSION,
            })
    return HttpResponse(json_dump, mimetype='application/json')

@csrf_exempt
@login_required(login_url=ADMIN_LOGIN_URL)
@utils.permission_required("list_edit")
def event_guest_update_comment(request, eventid):
    event = get_object_or_404(Event, id=eventid)
    new_comment = request.POST.get('value', '')

    rex = re.compile('(?P<type>\w+)\-(?P<list_id>\d+)\-(?P<listmember_id>\d+)')
    matches = rex.match(request.POST['id'])
    list_id = matches.group('list_id')
    listmember_id = matches.group('listmember_id')

    item_type = matches.group('type')
    assert item_type == "list"

    # NOTE: Could have used "update" here, but that doesn't fire the "post_save" signal :/
    item = VenueListMember.objects.get(list=list_id, id=listmember_id)
    item.comment = new_comment
    item.save()

    return HttpResponse(new_comment)


class EventCheckinHandler(View):
    uuid_re = re.compile(r'^[0-9a-f]{32}$')

    class BadRequestException(Exception):
        pass

    @method_decorator(csrf_exempt)
    @method_decorator(login_required(login_url=ADMIN_LOGIN_URL))
    def dispatch(self, *args, **kwargs):
        return super(EventCheckinHandler, self).dispatch(*args, **kwargs)

    def post(self, request, eventid):
        self.request = request
        self.event = get_object_or_404(Event, id=eventid)
        self.logger = logging.getLogger('pro.EventCheckinHandler.%d' % self.event.id)

        self.is_promoter = False
        if not self.event.has_user_permission(self.request.user, "event_show_guests"):
            self.is_promoter = True
            if not Promoter.can_book_tables(self.request.user, self.event):
                raise PermissionDenied("%s.%s" % (self.request.user, "event_show_guests"))

        # Compatibility with old clients
        # TODO: remove this once we don't get requests from old clients anymore (TODO: add approx date here)
        if 'version' not in self.request.POST:
            return event_guest_set_entered_count_old(request=self.request, eventid=eventid)
        elif self.request.POST['version'] != str(GUESTLIST_FORMAT_VERSION):
            # Obsolete version, force client to reload before and re-submit.
            return HttpResponse(json.dumps({
                'success': False,
                'version': GUESTLIST_FORMAT_VERSION,
                'reason': 'version mismatch',
                }), mimetype='application/json')

        try:
            request_timestamp = datetime.fromtimestamp(int(request.POST['current_timestamp'])/1000.0, timezone.utc)
        except:
            self.logger.warning("Got exception when parsing current_timestamp: %s", request.POST['current_timestamp'])
            return HttpResponseBadRequest()

        # Parse the actions data and do initial validation (ensures they all have id and action type).
        try:
            actions = self.preparse_actions(request.POST['actions'], request_timestamp)
        except Exception as e:
            # Probably bad JSON
            self.logger.warning("Got exception when parsing actions data: %s", e)
            return HttpResponseBadRequest()

        # Process all the actions. Returns set of affected guest uuids.
        affected_guests, affected_bookings, failed_bookings = self.process_actions(actions)

        # TODO: this is awfully inefficient ATM since we compile the entire list, then filter out only the guests we
        #  need. It should be possible to fetch only the ones that we need...
        guests, tags, lists, list_infos = self.event.get_guestlist()
        guests = filter(lambda g: g['id'] in affected_guests, guests)

        # Return all bookings that are involved in actions, so client-side can correct errors.
        bookings = []
        all_bookings = VenueTableBooking.objects.filter(list_member__list__event=self.event, uuid__in=affected_bookings.union(failed_bookings)).prefetch_related('list_member__visitor', 'purchase__items__item_data', 'table')
        for booking in all_bookings:
            bookings.append(booking.serialize())

        ret = {
            'success': True,
            'version': GUESTLIST_FORMAT_VERSION,
            'actions': [ { 'id': a['id'], 'action': a['action'] } for a in actions ],
            'guests': guests,
            'bookings': bookings,
            }
        return HttpResponse(json.dumps(ret), mimetype='application/json')

    def preparse_actions(self, actions_json, request_timestamp):
        client_time_difference = timezone.now() - request_timestamp
        actions = []
        for action in json.loads(actions_json):
            if not isinstance(action, dict) or 'id' not in action or 'action' not in action or 'timestamp' not in action:
                self.logger.warning("Invalid action data %s", repr(action))
                continue

            try:
                client_timestamp = datetime.fromtimestamp(action['timestamp']/1000.0, timezone.utc)
            except:
                continue
            else:
                action['timestamp'] = client_timestamp + client_time_difference

            # Promoters are only allowed to perform certain actions
            if self.is_promoter and action['action'] not in ('bookings.add', 'bookings.set', 'bookings.delete'):
                self.logger.warning("Promoter performed an illegal action %s", repr(action))
                continue

            actions.append(action)

        # Apply the add actions at first and then all the other actions ordered by timestamp
        # This resolves the problem, when actions are taken from localStorage and
        # the actions get mixed up (eg "checkin" before "add" for walk-ins)
        return sorted(actions, key=lambda a: ('.add' not in a['action'], a['timestamp']))

    def process_actions(self, actions):
        # Quick check for actions that have already been applied
        applied_action_uuids = set(EventAction.objects.filter(event=self.event, uuid__in=[ a['id'] for a in actions ]).values_list('uuid', flat=True))

        affected_guests = []
        affected_bookings = []
        failed_bookings = []
        for action in actions:
            if action['id'] in applied_action_uuids:
                self.logger.info("Action %s has already been applied", action['id'])
                if 'guest_id' in action:
                    affected_guests.append(action['guest_id'])
                elif 'booking_id' in action:
                    affected_bookings.append(action['booking_id'])
                continue

            # Try to apply the action, ignore most errors.
            try:
                guest_uuid, booking_uuid = self.process_action(action)
            except self.BadRequestException:
                self.logger.warning("Action %s-%s failed", action['id'], action['action'])
                if 'booking_id' in action:
                    failed_bookings.append(action['booking_id'])
                if 'booking_b' in action:
                    failed_bookings.append(action['booking_b'])
                continue

            if guest_uuid:
                affected_guests.append(guest_uuid)

            if booking_uuid:
                if isinstance(booking_uuid, types.ListType):
                    for aff_booking in booking_uuid:
                        affected_bookings.append(aff_booking)
                else:
                    affected_bookings.append(booking_uuid)

        # Return UUIDs of affected guests and bookings
        self.logger.debug("Affected guests: %s", ', '.join(set(affected_guests)))
        self.logger.debug("Affected bookings: %s", ', '.join(set(affected_bookings)))
        self.logger.debug("Failed bookings: %s", ', '.join(set(failed_bookings)))

        return set(affected_guests), set(affected_bookings), set(failed_bookings)

    def process_action(self, action):
        self.logger.debug("process_action(): %s %s", action['action'], action['id'])

        action_type = action['action']
        if action_type == 'guest.add':
            return self.process_guest_add(action), None
        elif action_type == 'guest.set':
            return self.process_guest_set(action), None
        elif action_type in ['guest.checkin', 'guest.checkin.all', 'guest.checkout.all']:
            return self.process_guest_checkin(action), None
        elif action_type == 'convert.v0':
            return self.process_convert_v0(action), None

        elif action_type == 'bookings.add':
            return None, self.process_bookings_add(action)
        elif action_type == 'bookings.set':
            return None, self.process_bookings_set(action)
        elif action_type == 'bookings.delete':
            return None, self.process_bookings_delete(action)
        elif action_type == 'bookings.arrived':
            return None, self.process_bookings_arrived(action)
        elif action_type == 'bookings.left':
            return None, self.process_bookings_left(action)
        elif action_type == 'bookings.set_table':
            return None, self.process_bookings_set_table(action)
        elif action_type == 'bookings.switch_tables':
            return None, self.process_bookings_switch_tables(action)

        elif action_type == 'notes.add':
            self.process_notes_add(action)
            return None, None

        else:
            self.logger.warning("process_action(): Action %s: unknown type '%s'", action['id'], action_type)
            raise self.BadRequestException()

    def process_guest_add(self, action):
        is_tally = True

        # Used for tally guests and the add guest dialog in checkin
        # Try to parse out the list and member ids
        try:
            # Validate data's structure
            assert 'attrs' in action
            guest_attrs = action['attrs']
            assert 'list' in guest_attrs and 'name' in guest_attrs and 'comment' in guest_attrs

            is_tally = 'not_tally' not in guest_attrs or not guest_attrs['not_tally']
        except AssertionError as e:
            self.logger.warning("process_guest_add(): Action %s: invalid data %s", action['id'], repr(action))
            raise self.BadRequestException()
        # Verify UUID format
        guest_uuid = action['id']
        self.validate_uuid(guest_uuid, "process_guest_add()")

        # Find the list, if any
        guestlist = None
        if guest_attrs['list']:
            try:
                guestlist = VenueList.objects.filter(event=self.event, name=guest_attrs['list']).order_by('id')[0]
            except IndexError:
                pass

        if not guestlist:
            if not is_tally:
                self.logger.warning("process_guest_add(): Action %s: Attempted to add a Non-tally guest added without list, data %s", action['id'], repr(action))
                raise self.BadRequestException()

            # There's a small chance of get_or_create() creating two lists, in case of parallel requests. Use
            #  locking to prevent that. We don't have anything good to lock, so we lock the entire event. This
            #  isn't exactly nice, but we're doing it for a very short amount of time, so it should be ok.
            with transaction.commit_on_success():
                # Lock the event
                list(Event.all_objects.filter(pk=self.event.pk).select_for_update())
                # Create a hidden 'default' list unless it already exists
                guestlist, created = VenueList.objects.get_or_create(event=self.event, list_type=VenueList.TYPE_TALLY_HIDDEN,
                                                                     defaults={ 'owner': self.event.owner_org, 'created_by': self.request.user })

        # Create the new guest if it doesn't already exist
        with transaction.commit_on_success():
            # Lock the guestlist. We don't (usually) have the VenueListMember yet, so we can't lock that, but we
            #  must prevent multiple VenueListMembers with same uuid from being created.
            list(VenueList.objects.filter(pk=guestlist.pk).select_for_update())

            if not self.action_applied(guest_uuid, "process_guest_add()"):

                if is_tally and "Tally " in guest_attrs['name']:
                    name, separator, guest_attrs['comment'] = guest_attrs['comment'].partition(', ')
                else:
                    name = guest_attrs['name']
                name = name[:75]

                ticket_count = 1
                if 'quantity' in guest_attrs and not is_tally:
                    ticket_count = min(2147483647, guest_attrs['quantity'])

                if not is_tally:
                    guests = VenueListMember.objects.filter(list=guestlist, full_name=name)

                    # At least one Guest with the same name exists, and one or more of them is equal to the provided comment.
                    if guests and filter(lambda g: g.comment == guest_attrs['comment'], guests):
                        free_merge = 0
                        found_name = False
                        new_comment = ""

                        while not found_name:
                            free_merge += 1
                            append = "(#%d)" % free_merge
                            new_comment = "%s%s%s" % (guest_attrs.get('comment', ''), '; ' if guest_attrs['comment'] else '', append)

                            # Find first available list comment.
                            for guest in filter(lambda g: g.comment == guest_attrs['comment'], guests):
                                if guest.comment != new_comment:
                                    found_name = True
                                    break
                            if found_name:
                                # Check that the comment found in previous loop isn't already in use somewhere else
                                for guest in guests:
                                    if guest.comment == new_comment:
                                        found_name = False

                        # Log the change
                        self.logger.debug("process_guest_add(): Making VenueListMember %s unique by it's comment from %s to %s.", guest_uuid, guest_attrs['comment'], new_comment)
                        guest_attrs['comment'] = new_comment

                # Create new VenueVisitor and VenueListMember objects
                self.logger.debug("process_guest_add(): Creating VenueListMember %s", guest_uuid)
                visitor = VenueVisitor.get_visitor(full_name=name, email='', is_tally=is_tally)
                VenueListMember.objects.create(list=guestlist, created_by=self.request.user, visitor=visitor,
                                               uuid=guest_uuid, full_name=name, ticket_count=ticket_count, comment=guest_attrs['comment'], is_tally=is_tally)

        return guest_uuid

    def process_guest_set(self, action_data):
        # Validate action_data's data
        try:
            assert 'guest_id' in action_data and 'changes' in action_data
            changes = action_data['changes']
            assert isinstance(changes, dict)
            # Make sure comment, if given, is string
            assert isinstance(changes.get('comment', ""), (str, unicode))
        except AssertionError:
            self.logger.info("process_guest_set(): Action %s: invalid data %s",
                             action_data['id'], repr(action_data))
            raise self.BadRequestException()

        action_id = action_data['id']
        guest_id = action_data['guest_id']

        self.validate_uuid(action_id, "process_guest_set()")
        self.validate_uuid(guest_id, "process_guest_set()")

        with transaction.commit_on_success():
            # Get the VenueListMember and lock it
            try:
                listmember = VenueListMember.objects.filter(uuid=guest_id, list__event=self.event).select_for_update()[0]
            except IndexError:
                self.logger.info("process_guest_set(): Action %s: VLM %s doesn't exist",
                                 action_id, guest_id)
                raise self.BadRequestException()

            if not self.action_applied(action_id, "process_guest_set()"):
                if 'comment' in changes:
                    listmember.comment = changes['comment'][:4000]
                    listmember.save()

        return guest_id

    def process_guest_checkin(self, action_data):
        # Validate action_data's data
        try:
            assert 'guest_id' in action_data
            categories = action_data.get('categories', [])
            assert isinstance(categories, list)
        except AssertionError:
            self.logger.info("process_guest_checkin(): Action %s: invalid data %s",
                             action_data['id'], repr(action_data))
            raise self.BadRequestException()

        cache_key = "metrics_event_checkin_%d" % self.event.owner_org.id
        if not cache.get(cache_key):
            metrics.record("PRO", "Check-in guest", user=self.event.owner_org.get_first_admin())
            cache.set(cache_key, True, 360*24*60*60)

        if action_data['guest_id'].startswith('purchase-'):
            return self.process_ticket_checkin(action_data)
        elif action_data['guest_id'].startswith('user-'):
            return self.process_ticket_checkin_old(action_data)

        action_id = action_data['id']
        guest_id = action_data['guest_id']
        timestamp = action_data['timestamp']

        self.validate_uuid(action_id, "process_guest_checkin()")
        self.validate_uuid(guest_id, "process_guest_checkin()")

        with transaction.commit_on_success():
            # Get the VenueListMember and lock it
            try:
                listmember = VenueListMember.objects.filter(uuid=guest_id, list__event=self.event).select_for_update()[0]
            except IndexError:
                # This might happen if the guest was just removed from the guestlist
                self.logger.info("process_guest_checkin(): Action %s: VLM %s doesn't exist",
                                 action_id, guest_id)
                raise self.BadRequestException()

            if not self.action_applied(action_id, "process_guest_checkin()"):
                # Get the total number of tickets this list member has
                total_tickets = listmember.ticket_count
                # Count the total number of checkins/declines and verify
                all_checkins = VenueVisit.objects.filter(list_member=listmember, event=self.event)
                total_checkins = len(filter(lambda visit: visit.status == VenueVisit.STATUS_ENTERED, all_checkins))
                total_left = len(filter(lambda visit: visit.status == VenueVisit.STATUS_LEFT, all_checkins))
                currently_in = total_checkins - total_left

                delta = self.get_guest_checkin_delta(action_data, currently_in, total_tickets)
                if not delta:
                    return guest_id

                #self.logger.info("process_guest_checkin(): Action %s: Creating %d checkins", action_id, delta)
                # Get categories (if any) to attach to VenueVisit
                if categories:
                    categories = list(VisitorCategory.objects.filter(Q(group__organization=self.event.owner_org) | Q(group__organization=None),
                                                                     id__in=categories))
                visit_status = VenueVisit.STATUS_ENTERED if delta >= 0 else VenueVisit.STATUS_LEFT

                for i in range(abs(delta)):
                    visit = VenueVisit.objects.create(list_member=listmember, event=self.event, status=visit_status,
                                                      entrance_by=self.request.user, entrance_timestamp=timestamp)

                    if categories:
                        visit.categories.add(*categories)

        return guest_id

    def process_ticket_checkin_old(self, action_data):
        """ Processes old-style ticket checkin.

        The old style ticket guests had id in the form "user-<user_id>-<shopitemdata_id>"
        """

        # Try to parse out the user and shopitem ids
        try:
            guest_tokens = action_data['guest_id'].split('-')
            assert guest_tokens[0] == 'user'
            user_id = int(guest_tokens[1])
            shopitemdata_id = int(guest_tokens[2])
        except:
            self.logger.info("process_ticket_checkin(): Action %s: invalid data %s",
                             action_data['id'], repr(action_data))
            raise self.BadRequestException()

        # Get purchased items of this user
        purchased_items = PurchasedItem.objects.filter(owner=user_id)

        # Note that this returns old-style id, so the returning of changed guests' data won't work properly, but it
        #  should be mostly fine. It's only for compatibility anyway.
        return self.process_ticket_checkin_internal(action_data, shopitemdata_id, purchased_items)

    def process_ticket_checkin(self, action_data):
        # Try to parse out the user and shopitem ids
        try:
            guest_tokens = action_data['guest_id'].split('-')
            assert guest_tokens[0] == 'purchase'
            purchase_id = int(guest_tokens[1])
            shopitemdata_id = int(guest_tokens[2])
            ticket_number = None
            if action_data['action'] == "guest.checkin" and 'ticket_number' in action_data:
                ticket_number = int(action_data['ticket_number'])
        except:
            self.logger.info("process_ticket_checkin(): Action %s: invalid data %s",
                             action_data['id'], repr(action_data))
            raise self.BadRequestException()

        # Get purchased items of this purchase
        purchased_items = PurchasedItem.objects.filter(purchase=purchase_id)
        if ticket_number is not None:
            purchased_items = purchased_items.filter(item_number=ticket_number)

        return self.process_ticket_checkin_internal(action_data, shopitemdata_id, purchased_items)

    def process_ticket_checkin_internal(self, action_data, shopitemdata_id, purchased_items):
        """ Internal method for processing common part of (old- and new-style) ticket checkins.
        """

        action_id = action_data['id']
        guest_id = action_data['guest_id']
        timestamp = action_data['timestamp']

        self.validate_uuid(action_id, "process_ticket_checkin()")

        # Ensure that the shopitem is tied to the given event
        try:
            shopitemdata = ShopItemData.objects.get(id=shopitemdata_id)
        except ShopItemData.DoesNotExist:
            raise self.BadRequestException()

        if (not shopitemdata.event) or shopitemdata.event.id != self.event.id:
            self.logger.warning("process_ticket_checkin(): Action %s: invalid ShopItemData id %d for event %d and user %d",
                                action_id, shopitemdata_id, self.event.id, user_id)
            raise self.BadRequestException()

        # Further filter the purchased items by shopitemdata and status and lock them
        purchased_items = purchased_items.filter(item_data=shopitemdata_id,
                                                 status__in=[ PurchasedItem.STATUS_BILLED, PurchasedItem.STATUS_REDEEMED ]).select_for_update()

        # Create action
        if not self.action_applied(action_id, "process_ticket_checkin()"):
            # Filter a combined list here since it's probably cheaper to make 1 db query instead of 2.
            unused_items = filter(lambda item: item.status == PurchasedItem.STATUS_BILLED, purchased_items)
            used_items = filter(lambda item: item.status == PurchasedItem.STATUS_REDEEMED, purchased_items)

            delta = self.get_guest_checkin_delta(action_data, len(used_items), len(purchased_items))

            if delta > 0:
                # Redeem 'em, making a separate redeem package for every item
                for i in range(delta):
                    item = unused_items[i]
                    RedeemPackage.redeem(item.owner, self.request.user, [ item ], timestamp)

            elif delta < 0:
                # Unredeem some tickets
                for i in range(-delta):
                    item = used_items[i]
                    # Find active redeem package for this item
                    pkgs = item.redeem_packages.filter(user=item.owner, cancelled_by=None)
                    # Sanity checks. Note that every ticket is currently assumed to have its own separate redeem package.
                    assert len(pkgs) == 1
                    pkg_items = pkgs[0].items.all()
                    assert len(pkg_items) == 1
                    assert pkg_items[0].id == item.id
                    # Unredeem it
                    pkgs[0].unredeem(self.request.user, timestamp)

        return guest_id

    def get_guest_checkin_delta(self, action_data, entered_count, total_count):
        if action_data['action'] == 'guest.checkin.all':
            delta = total_count - entered_count
        elif action_data['action'] == 'guest.checkout.all':
            delta = -entered_count
        else:
            # Validate that action_data contains 'delta' and it's an int.
            try:
                assert 'delta' in action_data
                assert isinstance(action_data['delta'], int)
            except AssertionError:
                self.logger.info("get_guest_checkin_delta(): Action %s %s: invalid data %s",
                                 action_data['id'], action_data['action'], repr(action_data))
                raise self.BadRequestException()

            delta = action_data['delta']

        # We can make at most  entered_count  checkouts and at most  total_count-entered_count  checkins.
        cleaned_delta = max(-entered_count, min(delta, total_count - entered_count))

        if cleaned_delta != delta:
            # Log a warning
            self.logger.info("get_guest_checkin_delta(): Action %s %s: Limited delta from %d to %d for guest %s (%d/%d currently in)",
                             action_data['id'], action_data['action'], delta, cleaned_delta, action_data['guest_id'],
                             entered_count, total_count)

        return cleaned_delta

    def validate_uuid(self, uuid, trace):
        if not self.uuid_re.match(uuid):
            self.logger.warning("%s: Invalid UUID '%s'", trace, uuid)
            raise self.BadRequestException()

    def validate_booking(self, action_data, trace, key='booking'):

        try:
            assert key in action_data
            assert 'booking_id' in action_data
            booking_attrs = action_data[key]
            booking_attrs['booking_id'] = action_data['booking_id']
            assert 'name' in booking_attrs and 'comment' in booking_attrs and 'booked_by' in booking_attrs and 'email' in booking_attrs
            assert 'phone_number' in booking_attrs and 'party_m' in booking_attrs and 'party_f' in booking_attrs and 'table_id' in booking_attrs
            #assert 'occasion' in booking_attrs
        except:
            self.logger.info("%s: Action %s: invalid data %s", trace, action_data['id'], repr(action_data))
            raise self.BadRequestException()

        try:
            line_parser = VenueListLineParser()
            name, additional_guests, name_comment = line_parser.parse_line(booking_attrs["name"])
            assert name
        except:
            self.logger.info("%s: Action %s: invalid booking.name format, data(%s)", trace, action_data['id'], repr(action_data))
            raise self.BadRequestException()

        total_guests = booking_attrs["party_m"] + booking_attrs["party_f"]
        if total_guests == 0:
            if additional_guests > 0:
                self.logger.info("%s: Action %s: Plus count in name field is deprecated for bookings. Attempting to merge. data(%s)", trace, action_data['id'], repr(action_data))
            total_guests = additional_guests + 1

        if total_guests <= 0:
            self.logger.info("%s: Action %s: Total guest count cannot be 0.", trace, action_data['id'])
            raise self.BadRequestException()

        try:
            assert len(name) <= 80
            if booking_attrs["comment"]:
                assert len(booking_attrs["comment"]) <= 4096
            assert len(booking_attrs["phone_number"]) <= 32 and len(booking_attrs["booked_by"]) <= 50
            assert len(booking_attrs["occasion"]) < 64
            assert len(booking_attrs["arrival_time"]) < 64
        except:
            self.logger.info("%s: Action %s: some booking values too long, data(%s)", trace, action_data['id'], repr(action_data))
            raise self.BadRequestException()

        if booking_attrs["email"]:
            try:
                assert len(booking_attrs["email"]) <= 128
                validate_email(booking_attrs["email"])
            except Exception, e:
                self.logger.info(e.args);
                self.logger.info("%s: Action %s: invalid booking.email format, data(%s)", trace, action_data['id'], booking_attrs["email"])
                raise self.BadRequestException()

        # find the table...
        table = None
        if booking_attrs["table_id"]:
            try:
                table = VenueAreaTable.objects.get(id=booking_attrs["table_id"], area__venue=self.event.venue)
            except VenueAreaTable.DoesNotExist:
                self.logger.info("%s: Action %s: invalid table, data(%s)", trace, action_data['id'], repr(action_data))
                raise self.BadRequestException()

        return booking_attrs, name, table, total_guests

    def booking_lock(self, action_id, booking_uuid, trace, table=None):
        try:
            booking = VenueTableBooking.all_objects.select_for_update().get(uuid=booking_uuid, list_member__list__event=self.event)
            if table and table != booking.table:
                raise VenueTableBooking.DoesNotExist()

            # Check if the booking is done by the promoter
            if self.is_promoter and self.request.user.email.lower() != booking.booked_by.email.lower():
                self.logger.warning("Promoter tried to change someone else's booking: %s Action: %s VTB: %s",
                                    trace, action_id, booking_uuid)
                raise self.BadRequestException()

            return booking
        except VenueTableBooking.DoesNotExist:
            statsd.incr('pro.tablebookings.obsolete_data')
            self.logger.info("%s: Action %s: VTB %s doesn't exist",
                             trace, action_id, booking_uuid)
            raise self.BadRequestException()

    def action_applied(self, action_id, trace):
        # Create the action if it doesn't exist
        action, created = EventAction.objects.get_or_create(event=self.event, uuid=action_id)
        if not created:
            self.logger.info("%s: Action %s already applied", trace, action_id)
            return True
        return False

    def process_bookings_add(self, action_data):
        """ Adds a new booking
        """

        booking_attrs, name, table, total_guests = self.validate_booking(action_data, "process_bookings_add()")

        # Verify UUID format
        booking_uuid = action_data['id']
        self.validate_uuid(booking_uuid, "process_bookings_add()")

        if table and VenueTableBooking.objects.filter(table__id=table.id, list_member__list__event=self.event, left_timestamp=None).exists():
            table = None
            statsd.incr('pro.tablebookings.forced_to_bookinglist')
            self.logger.info("%s: Action process_bookings_add: table already booked %s, forcing booking to bookinglist.", booking_uuid, table)

        # Manage booked by field.
        promoter = None
        if self.is_promoter:
            promoter = Promoter.objects.get(email__iexact=self.request.user.email, organization=self.event.owner_org)
        elif booking_attrs["booked_by"]:
            with transaction.commit_on_success():
                # Lock org while we manage promoters
                list(Organization.all_objects.filter(pk=self.event.owner_org.pk).select_for_update())

                promoter, created = Promoter.objects.get_or_create(name=booking_attrs["booked_by"], organization=self.event.owner_org, defaults={
                    'created_by': self.request.user,
                    })

        # Find the tables_list, if any
        tables_list = None
        try:
            tables_list = VenueList.objects.filter(event=self.event, list_type=VenueList.TYPE_TABLES).order_by('id')[0]
        except IndexError:
            pass

        if not tables_list or promoter:
            # Lock the event just in case, since parallel requests can provide hassle.
            with transaction.commit_on_success():
                # Lock the event
                list(Event.all_objects.filter(pk=self.event.pk).select_for_update())

                if not tables_list:
                    # Create a tables list
                    tables_list, created = VenueList.objects.get_or_create(event=self.event, list_type=VenueList.TYPE_TABLES,
                                                                           defaults={ 'owner': self.event.owner_org, 'created_by': self.request.user })

                if promoter:
                    # If promoter is not active on the event, make him active - create a list
                    promoter_list, created = VenuePromoterList.objects.get_or_create(event=self.event, promoter=promoter, defaults={
                        'list_type': VenueList.TYPE_PROMOTER,
                        'owner': self.event.owner_org,
                        'name': promoter.name,
                        'created_by': self.request.user,
                        })

        # Create the new booking if it doesn't already exist
        with transaction.commit_on_success():
            # Lock the table bookings for this event
            list(VenueList.objects.filter(pk=tables_list.pk).select_for_update())

            # Create the action if it doesn't exist
            if not self.action_applied(booking_uuid, "process_bookings_add()"):
                self.logger.debug("process_bookings_add(): Creating VenueTableBooking %s", booking_uuid)
                # Create new list member
                visitor = VenueVisitor.get_visitor(name, email='')
                list_member = VenueListMember.objects.create(list=tables_list,
                                                             created_by=self.request.user,
                                                             visitor=visitor,
                                                             full_name=name,
                                                             ticket_count=total_guests,
                                                             comment=booking_attrs["comment"])

                # Create new VenueTableBooking object
                booking = VenueTableBooking.objects.create(table=table,
                                                           list_member=list_member,
                                                           created_by=self.request.user,
                                                           booked_by=promoter,
                                                           email=booking_attrs["email"],
                                                           phone_number=booking_attrs["phone_number"],
                                                           party_m=booking_attrs["party_m"],
                                                           party_f=booking_attrs["party_f"],
                                                           occasion=booking_attrs['occasion'],
                                                           arrival_time=booking_attrs['arrival_time'],
                                                           uuid=booking_uuid)

                if self.is_promoter:
                    metrics.record("PRO", "Guest Added By Promoter", {'with_table_booking': True}, request=self.request)
                metrics.record("PRO", "Table Booking Created", user=self.event.owner_org.get_first_admin(), request=self.request)

                if not self.is_promoter:
                    # Sync purchase with people DB
                    sync_person(self.event.owner_org,
                                list_member.full_name,
                                booking_attrs["email"],
                                user=self.request.user,
                                # Ensure we won't get DB error for long garbage phone nums (valid ones should be <20 chars)
                                phone=booking_attrs["phone_number"][:20])

                statsd.incr('pro.tablebookings.created.total')
                statsd.incr('pro.tablebookings.created.' + ('withtable' if table else 'notable'))

        return booking_uuid

    def process_bookings_set(self, action_data):
        """ Edits a booking
        """

        booking_attrs, name, table_id, total_guests = self.validate_booking(action_data, "process_bookings_set()", "booking")

        action_id = action_data['id']
        booking_id = booking_attrs['booking_id']

        self.validate_uuid(action_id, "process_bookings_set()")
        self.validate_uuid(booking_id, "process_bookings_set()")

        # Manage booked by field.
        promoter = None
        if booking_attrs["booked_by"]:
            with transaction.commit_on_success():
                # Lock org while we manage promoters
                list(Organization.all_objects.filter(pk=self.event.owner_org.pk).select_for_update())

                promoter, created = Promoter.objects.get_or_create(name=booking_attrs["booked_by"], organization=self.event.owner_org, defaults={
                    'created_by': self.request.user,
                    })

        # Find the tables_list, if any
        tables_list = None
        try:
            tables_list = VenueList.objects.filter(event=self.event, list_type=VenueList.TYPE_TABLES).order_by('id')[0]
        except IndexError:
            pass

        if not tables_list or promoter:
            # Lock the event just in case, since parallel requests can provide hassle.
            with transaction.commit_on_success():
                # Lock the event
                list(Event.all_objects.filter(pk=self.event.pk).select_for_update())

                if not tables_list:
                    # Create a tables list
                    tables_list, created = VenueList.objects.get_or_create(event=self.event, list_type=VenueList.TYPE_TABLES,
                                                                           defaults={ 'owner': self.event.owner_org, 'created_by': self.request.user })

                if promoter:
                    # If promoter is not active on the event, make him active - create a list
                    promoter_list, created = VenuePromoterList.objects.get_or_create(event=self.event, promoter=promoter, defaults={
                        'list_type': VenueList.TYPE_PROMOTER,
                        'owner': self.event.owner_org,
                        'name': promoter.name,
                        'created_by': self.request.user,
                        })

        with transaction.commit_on_success():
            # Get the VenueTableBooking and lock it
            booking = self.booking_lock(action_id, booking_id, "process_bookings_set()", table_id)

            if booking.left_timestamp:
                try:
                    assert booking.list_member.full_name == name
                    assert booking.email == booking_attrs["email"]
                    assert booking.phone_number == booking_attrs["phone_number"]
                    assert booking.party_m == booking_attrs["party_m"] and booking.party_f == booking_attrs["party_f"]
                    assert booking.occasion == booking_attrs['occasion']
                    assert booking.arrival_time == booking_attrs['arrival_time']
                except:
                    self.logger.info("process_bookings_set(): Action %s: Tried to edit departed guest uneditable values (%s).", action_id, booking)
                    raise self.BadRequestException()

            # Lock the table bookings for this event
            list(VenueList.objects.filter(pk=tables_list.pk).select_for_update())

            # Create the action if it doesn't exist
            if not self.action_applied(action_id, "process_bookings_set()"):
                self.logger.debug("process_bookings_set(): Editing VenueTableBooking %s", booking_id)
                list_member = booking.list_member

                if list_member and list_member.full_name != name:
                    list_member = None

                if not list_member:
                    # Create new list member
                    visitor = VenueVisitor.get_visitor(name, email='')
                    list_member = VenueListMember.objects.create(list=tables_list, created_by=self.request.user, visitor=visitor,
                                                                 full_name=name, ticket_count=total_guests, comment=booking_attrs["comment"])
                elif list_member.ticket_count != total_guests or list_member.comment != booking_attrs["comment"]:
                    list_member.ticket_count = total_guests
                    list_member.comment = booking_attrs["comment"]
                    list_member.save()

                changed = False
                old_list_member_pk = None
                should_sync = False

                if booking.list_member.pk != list_member.pk:
                    old_list_member_pk = booking.list_member.pk
                    booking.list_member = list_member
                    changed = True

                if booking.booked_by != promoter:
                    booking.booked_by = promoter
                    changed = True

                if booking.email != booking_attrs["email"]:
                    if not booking.email:
                        should_sync = True
                    booking.email = booking_attrs["email"]
                    changed = True

                if booking.phone_number != booking_attrs["phone_number"]:
                    booking.phone_number = booking_attrs["phone_number"]
                    changed = True

                if booking.occasion != booking_attrs["occasion"]:
                    booking.occasion = booking_attrs["occasion"]
                    changed = True

                if booking.arrival_time != booking_attrs["arrival_time"]:
                    booking.arrival_time = booking_attrs["arrival_time"]
                    changed = True

                if booking.party_m != booking_attrs["party_m"]:
                    booking.party_m = booking_attrs["party_m"]
                    changed = True

                if booking.party_f != booking_attrs["party_f"]:
                    booking.party_f = booking_attrs["party_f"]
                    changed = True

                if changed:
                    booking.save()
                    statsd.incr('pro.tablebookings.data_modified')
                    if old_list_member_pk:
                        VenueListMember.objects.filter(pk=old_list_member_pk).delete()

                if should_sync and not self.is_promoter:
                    # Sync purchase with people DB
                    sync_person(self.event.owner_org,
                                list_member.full_name,
                                booking.email,
                                user=self.request.user,
                                phone=booking.phone_number if booking.phone_number else '')

        return booking_id

    def process_bookings_delete(self, action_data):
        try:
            assert 'booking_id' in action_data
        except:
            self.logger.info("%s: Action process_bookings_delete: invalid data %s", action_data['id'], repr(action_data))
            raise self.BadRequestException()

        list_member = None
        action_id = action_data['id']
        booking_id = action_data['booking_id']

        with transaction.commit_on_success():
            booking = self.booking_lock(action_id, booking_id, "process_bookings_delete()")

            try:
                list_member = VenueListMember.objects.filter(pk=booking.list_member.pk).select_for_update()[0]
            except:
                statsd.incr('pro.tablebookings.obsolete_data')
                self.logger.info("process_bookings_delete(): Action %s: VLM %s doesn't exist",
                                 action_id, booking.list_member.pk)
                raise self.BadRequestException()

            if booking.left_timestamp:
                statsd.incr('pro.tablebookings.obsolete_data')
                self.logger.info("process_bookings_delete(): Action %s: VTB %s is marked as departed, cant delete!",
                                 action_id, booking_id)
                raise self.BadRequestException()


            if not self.action_applied(action_id, "process_bookings_delete()"):
                # More logging if the booking is purchased booking
                if booking.purchase_id:
                    self.logger.info("Will delete table reservation, purchase id %d", booking.purchase_id)

                self.logger.debug("Deleting booking list reservation of event %d (%s), for '%s'",
                                  self.event.id, self.event.name, booking.list_member.full_name)

                # Note that this will also delete the VenueTableBooking object
                list_member.delete()
                statsd.incr('pro.tablebookings.deleted')

        return booking_id

    def process_bookings_set_table(self, action_data):
        table = None
        try:
            assert 'booking_id' in action_data
            if 'table' in action_data:
                table = action_data['table']
        except:
            self.logger.info("%s: Action process_bookings_set_table: invalid data %s", action_data['id'], repr(action_data))
            raise self.BadRequestException()

        action_id = action_data['id']
        booking_id = action_data['booking_id']

        if table:
            try:
                table = VenueAreaTable.objects.get(id=table, area__venue=self.event.venue)
            except:
                self.logger.info("%s: Action process_bookings_set_table: VenueAreaTable %s doesn't exist", action_data['id'], action_data['table'])
                raise self.BadRequestException()

            if VenueTableBooking.objects.filter(table__id=table.id, list_member__list__event=self.event, left_timestamp=None).exists():
                statsd.incr('pro.tablebookings.obsolete_data')
                self.logger.info("%s: Action process_bookings_set_table: table already booked %s", action_id, table)
                raise self.BadRequestException()

        with transaction.commit_on_success():
            # Get the VenueTableBooking and lock it
            booking = self.booking_lock(action_id, booking_id, "process_bookings_set_table()")

            if booking.left_timestamp:
                statsd.incr('pro.tablebookings.obsolete_data')
                self.logger.info("process_bookings_set_table(): Action %s: VTB %s is marked departed, can't remove table!",
                                 action_id, booking_id)
                raise self.BadRequestException()

            # Create the action if it doesn't exist
            if not self.action_applied(action_id, "process_bookings_set_table()"):
                if not table:
                    self.logger.info("Moving table reservation of event %d (%s), table %s (%s) for '%s' to booking list",
                                     self.event.id, self.event.name, booking.table.id if booking.table else "None", booking.table.number if booking.table else "None", booking.list_member.full_name)
                    statsd.incr('pro.tablebookings.table_cleared')
                else:
                    logging.info("Moving booking list member of event %d (%s) for '%s' to table %d (%s)",
                                 self.event.id, self.event.name, booking.list_member.full_name, table.id, table.number)
                    if booking.table_id:
                        statsd.incr('pro.tablebookings.table_modified')
                    else:
                        statsd.incr('pro.tablebookings.table_assigned')

                booking.table = table
                booking.save()

        return booking_id

    def process_bookings_switch_tables(self, action_data):
        try:
            assert 'booking_id' in action_data
            assert 'booking_b' in action_data

            assert 'table_a' in action_data
            assert 'table_b' in action_data
        except:
            self.logger.info("%s: Action process_bookings_switch_tables: invalid data %s", action_data['id'], repr(action_data))
            raise self.BadRequestException()

        action_id = action_data['id']

        booking_a_id = action_data['booking_id']
        booking_b_id = action_data['booking_b']

        table_a_id = action_data['table_a']
        table_b_id = action_data['table_b']

        table_a_obj = None
        table_b_obj = None

        if table_a_id:
            table_a_obj = VenueAreaTable.objects.get(id=table_a_id, area__venue=self.event.venue)
        if table_b_id:
            table_b_obj = VenueAreaTable.objects.get(id=table_b_id, area__venue=self.event.venue)

        # Check that both bookings currently have the same table ids in clientside, this removes possibility of unneeded overwrites.
        if not VenueTableBooking.objects.filter(table__id=table_a_id, uuid=booking_a_id, left_timestamp=None).exists():
            statsd.incr('pro.tablebookings.obsolete_data')
            self.logger.info("%s: Action process_bookings_switch_tables: booking_a(%s) is not in table_a(%s)", action_data['id'], booking_a_id, table_a_obj)
            raise self.BadRequestException()
        if not VenueTableBooking.objects.filter(table__id=table_b_id, uuid=booking_b_id, left_timestamp=None).exists():
            statsd.incr('pro.tablebookings.obsolete_data')
            self.logger.info("%s: Action process_bookings_switch_tables: booking_b(%s) is not in table_b(%s)", action_data['id'], booking_b_id, table_b_obj)
            raise self.BadRequestException()

        # Do the switch
        with transaction.commit_on_success():
            booking_a = self.booking_lock(action_id, booking_a_id, "process_bookings_switch_tables()")
            booking_b = self.booking_lock(action_id, booking_b_id, "process_bookings_switch_tables()")

            if booking_a.left_timestamp or booking_b.left_timestamp:
                statsd.incr('pro.tablebookings.obsolete_data')
                self.logger.info("process_bookings_switch_tables(): Action %s: VTB %s or %s is marked departed, can't switch tables!",
                                 action_id, booking_a_id, booking_b_id)
                raise self.BadRequestException()

            if not self.action_applied(action_id, "process_bookings_switch_tables()"):
                self.logger.debug("Switching tables between bookings %s and %s of event %d (%s)",
                                  booking_a, booking_b, self.event.id, self.event.name)
                booking_a.table_id = table_b_id
                booking_b.table_id = table_a_id
                booking_a.save()
                booking_b.save()
                statsd.incr('pro.tablebookings.switched_tables')

        return [booking_a_id, booking_b_id]

    def process_bookings_arrived(self, action_data):
        try:
            assert 'booking_id' in action_data
            assert 'timestamp' in action_data
        except:
            self.logger.info("%s: Action process_bookings_arrived: invalid data %s", action_data['id'], repr(action_data))
            raise self.BadRequestException()

        action_id = action_data['id']
        booking_id = action_data['booking_id']
        timestamp = action_data['timestamp']

        with transaction.commit_on_success():
            # Get the VenueTableBooking and lock it
            booking = self.booking_lock(action_id, booking_id, "process_bookings_arrived()")

            if booking.left_timestamp:
                statsd.incr('pro.tablebookings.obsolete_data')
                self.logger.info("process_bookings_arrived(): Action %s: VTB %s is marked departed, can't mark as arrived!",
                                 action_id, booking_id)
                raise self.BadRequestException()

            # Create the action if it doesn't exist
            if not self.action_applied(action_id, "process_bookings_arrived()"):
                self.logger.debug("Marking booking reservation of event %d (%s), for '%s' as arrived in %s",
                                  self.event.id, self.event.name, booking.list_member.full_name, timestamp)
                booking.arrived_timestamp = timestamp
                booking.save()
                statsd.incr('pro.tablebookings.marked_arrived')
        return booking_id

    def process_bookings_left(self, action_data):
        spending = 0

        try:
            assert 'booking_id' in action_data
            assert 'spending' in action_data
            assert 'timestamp' in action_data
        except:
            self.logger.info("%s: Action process_bookings_left: invalid data %s", action_data['id'], repr(action_data))
            raise self.BadRequestException()

        action_id = action_data['id']
        booking_id = action_data['booking_id']
        timestamp = action_data['timestamp']
        spending = action_data['spending']

        try:
            spending = float(spending)
            assert spending >= 0
        except ValueError:
            self.logger.info("process_bookings_left(): Action %s: Bad spending, data %s",
                             action_id, repr(action_data))
            raise self.BadRequestException()

        with transaction.commit_on_success():
            # Get the VenueTableBooking and lock it
            booking = self.booking_lock(action_id, booking_id, "process_bookings_left()")

            if not booking.arrived_timestamp:
                statsd.incr('pro.tablebookings.obsolete_data')
                self.logger.info("process_bookings_left(): Action %s: VTB %s is not arrived, can't mark as departed!",
                                 action_id, booking_id)
                raise self.BadRequestException()

            # Create the action if it doesn't exist
            if not self.action_applied(action_id, "process_bookings_left()"):
                if not booking.left_timestamp:
                    statsd.incr('pro.tablebookings.marked_left')
                    booking.left_timestamp = timestamp
                    self.logger.info("Marking booking reservation of event %d (%s), for '%s' as departed in %s with spending %d",
                                     self.event.id, self.event.name, booking.list_member.full_name, timestamp, spending)
                else:
                    statsd.incr('pro.tablebookings.edit_spending')
                    self.logger.info("Changing booking reservation of event %d (%s), for '%s' spending to %d",
                                     self.event.id, self.event.name, booking.list_member.full_name, spending)

                booking.spending = spending
                booking.save()

        return booking_id

    def process_notes_add(self, action_data):
        try:
            assert 'attrs' in action_data
            note_attrs = action_data['attrs']
            assert 'text' in note_attrs and 'id' in note_attrs
        except:
            self.logger.info("%s: Action process_notes_add: invalid data %s", action_data['id'], repr(action_data))
            raise self.BadRequestException()

        action_id = action_data['id']
        note_uuid = note_attrs['id']
        timestamp = action_data['timestamp']
        text = note_attrs['text']

        # Action id and note uuid should always be equal for now
        if action_id != note_uuid:
            # Just log the error and continue
            self.logger.error("process_notes_add(): Action id '' differs from note uuid '' (event %d)",
                              action_id, note_uuid, self.event.id)

        with transaction.commit_on_success():
            # Lock the event
            list(Event.all_objects.filter(pk=self.event.pk).select_for_update())

            if not self.action_applied(action_id, "process_notes_add()"):
                EventNote.objects.create(uuid=note_uuid, event=self.event, created_by=self.request.user,
                                         created_timestamp=timestamp, text=text)

    def process_convert_v0(self, action_data):
        """ Converts old stored actions to new ones, then executes them
        """
        try:
            assert 'key' in action_data
            guest_tokens = action_data['key'].split('-')
            assert len(guest_tokens) == 4 or (len(guest_tokens) == 3 and guest_tokens[1] == 'tally')
            assert int(guest_tokens[0]) == self.event.id
        except:
            self.logger.info("process_convert_v0(): Action %s: invalid generic data %s", action_data['id'], repr(action_data))
            raise self.BadRequestException()

        # We want to know how many convert actions we get
        self.logger.info("process_convert_v0(): Convert %s: data %s", action_data['key'], repr(action_data))

        self.logger.info("guest_tokens[1]: %s", guest_tokens[1])
        if guest_tokens[1] == 'user':
            try:
                user_id = int(guest_tokens[2])
                shopitemdata_id = int(guest_tokens[3])
                checkins = int(action_data['value'])
            except:
                self.logger.info("process_convert_v0(): Action %s (%s): invalid data %s",
                                 action_data['id'], action_data['key'], repr(action_data))
                raise self.BadRequestException()

            # Figure out number of checkins we need to make. This is not entirely safe as we don't use locking but old
            #  checkin style was hit-and-miss anyway.
            used_items = PurchasedItem.objects.filter(item_data__event=self.event.id, item_data=shopitemdata_id,
                                                      owner=user_id, status=PurchasedItem.STATUS_REDEEMED)
            delta = checkins - len(used_items)

            # Create new action and apply it
            new_action = {
                'action': 'guest.checkin',
                'id': action_data['id'],
                'timestamp': action_data['timestamp'],
                'guest_id': '-'.join(guest_tokens[1:]),
                'delta': checkins - len(used_items),
                }
            return self.process_guest_checkin(new_action)

        elif guest_tokens[1] == 'list':
            try:
                checkins = int(action_data['value'])
                list_id = int(guest_tokens[2])
                listmember_id = int(guest_tokens[3])
            except:
                self.logger.info("process_convert_v0(): Action %s (%s): invalid data %s",
                                 action_data['id'], action_data['key'], repr(action_data))
                raise self.BadRequestException()

            try:
                listmember = VenueListMember.objects.get(id=listmember_id, list__event=self.event)
            except VenueListMember.DoesNotExist:
                # This might happen if the guest was just removed from the guestlist
                self.logger.info("process_convert_v0(): Action %s (%s): VLM %d doesn't exist",
                                 action_data['id'], action_data['key'], listmember_id)
                raise self.BadRequestException()

            # Again, we need to find out the delta - how many checkins to make.
            all_checkins = VenueVisit.objects.filter(list_member=listmember, event=self.event)
            total_checkins = len(filter(lambda visit: visit.status == VenueVisit.STATUS_ENTERED, all_checkins))
            total_left = len(filter(lambda visit: visit.status == VenueVisit.STATUS_LEFT, all_checkins))
            currently_in = total_checkins - total_left

            # Create new action and apply it
            new_action = {
                'action': 'guest.checkin',
                'id': action_data['id'],
                'timestamp': action_data['timestamp'],
                'guest_id': listmember.uuid,
                'delta': checkins - currently_in,
                }
            return self.process_guest_checkin(new_action)

        elif guest_tokens[1] == 'tally':
            try:
                assert isinstance(action_data.get('categories', None), list)
                assert 'comment' in action_data and 'list' in action_data and 'uuid' in action_data
            except:
                self.logger.info("process_convert_v0(): Action %s (%s): invalid data %s",
                                 action_data['id'], action_data['key'], repr(action_data))
                raise self.BadRequestException()

            if action_data.get('deleted', False):
                return

            tally_name, separator, tally_comment = action_data['comment'].partition(', ')

            # First create new guest
            new_action = {
                'action': 'guest.add',
                'id': action_data['uuid'],
                'timestamp': action_data['timestamp'],
                'attrs': {
                    'list': action_data['list'],
                    'name': tally_name,
                    'comment': tally_comment,
                    },
                }
            self.process_guest_add(new_action)

            # Then do a checkin
            new_action = {
                'action': 'guest.checkin',
                'id': action_data['id'],
                'timestamp': action_data['timestamp'],
                'guest_id': action_data['uuid'],
                'delta': 1,
                }
            return self.process_guest_checkin(new_action)

        else:
            self.logger.info("process_convert_v0(): Action %s (%s): unknown type, data: %s",
                             action_data['id'], action_data['key'], repr(action_data))
            raise self.BadRequestException()


@csrf_exempt
@login_required(login_url=ADMIN_LOGIN_URL)
@utils.permission_required("event_checkin_guests")
def event_guest_set_entered_count_old(request, eventid):
    event = get_object_or_404(Event, id=eventid)
    # Convert event id to int, so that we can later use it for comparing with event.id
    eventid = int(eventid)

    try:
        tasks = json.loads(request.POST['pending_changes'])
    except Exception:
        return HttpResponseBadRequest()

    statsd.incr('pro.checkin_view.checkins.requests')
    # We'll return a json-encoded object
    ret = {}
    for key, val in tasks:
        try:
            guest_tokens = key.split('-')[1:]  # Cut off the event ID from the beginning
            assert len(guest_tokens) == 3 or (len(guest_tokens) == 4 and guest_tokens[3] == 'declined') or \
                   (len(guest_tokens) == 2 and guest_tokens[0] == 'tally')
        except Exception:
            return HttpResponseBadRequest()

        # Are we declining or checking in?
        declining = len(guest_tokens) == 4 and guest_tokens[3] == 'declined'
        # If declining, make sure it's enabled
        if declining:
            return HttpResponseForbidden('Declining not enabled for this event')

        # Do every checkin/out in a separate transaction. This minimizes the time that rows are locked for in the
        #  database, since we use row locking to avoid conflicts.
        with transaction.commit_on_success():
            if guest_tokens[0] == 'user':
                # Try to parse out the user and shopitem ids
                try:
                    checkins = int(val)
                    user_id = int(guest_tokens[1])
                    shopitemdata_id = int(guest_tokens[2])
                except:
                    return HttpResponseBadRequest()

                # Ensure that the shopitem is tied to the given event
                shopitemdata = get_object_or_404(ShopItemData, id=shopitemdata_id)
                if (not shopitemdata.event) or shopitemdata.event.id != int(eventid):
                    logging.warning("event_guest_set_entered_count(): Got invalid ShopItemData id %d for event %d and user %d",
                                    shopitemdata_id, eventid, user_id)
                    return HttpResponseBadRequest()

                # Get all purchased items of that type of that user
                purchased_items = PurchasedItem.objects.filter(item_data=shopitemdata_id, owner=user_id, status__in=[ PurchasedItem.STATUS_BILLED, PurchasedItem.STATUS_REDEEMED ])
                # Lock all fetched items (blocking request)
                purchased_items = utils.get_obj_and_locks(PurchasedItem, purchased_items.values_list('id', flat=True))
                # If we got the lock, refetch the items since something may have
                # changed since the requesting of the lock.
                purchased_items = PurchasedItem.objects.filter(item_data=shopitemdata_id, owner=user_id, status__in=[ PurchasedItem.STATUS_BILLED, PurchasedItem.STATUS_REDEEMED ])
                # Filter a combined list here since it's probably cheaper to make 1 db query instead of 2.
                unused_items = filter(lambda item: item.status == PurchasedItem.STATUS_BILLED, purchased_items)
                used_items = filter(lambda item: item.status == PurchasedItem.STATUS_REDEEMED, purchased_items)

                if declining:
                    # Here checkins indicates how many tickets should be declined in total
                    declined_items_count = PurchasedItem.objects.filter(item_data=shopitemdata_id, owner=user_id, status=PurchasedItem.STATUS_DECLINED).count()
                    logging.info("  already have %d / %d declines", declined_items_count, checkins)
                    extra_declines = checkins - declined_items_count
                    if extra_declines < 0:
                        logging.warning("event_guest_set_entered_count(): trying to reduce declined count for %s (%d to %d)",
                                        key, declined_items_count, checkins)
                        return HttpResponseBadRequest()
                    elif extra_declines > len(unused_items):
                        logging.warning("event_guest_set_entered_count(): not enough unused tickets to decline %d more of %s", extra_declines, key)
                        return HttpResponseBadRequest()
                    elif extra_declines > 0:
                        # Decline more tickets
                        logging.info("  declining %d more", extra_declines)
                        for i in range(extra_declines):
                            item = unused_items[i]
                            item.declined(request.user)

                elif len(used_items) < checkins:
                    # Have to mark more tickets as redeemed
                    to_redeem_count = checkins - len(used_items)
                    # Check if user has enough available tickets
                    if len(unused_items) < to_redeem_count:
                        return HttpResponseBadRequest()
                    # Redeem 'em, making a separate redeem package for every item
                    for i in range(to_redeem_count):
                        item = unused_items[i]
                        RedeemPackage.redeem(item.owner, request.user, [ item ])
                        statsd.incr('pro.checkin_view.checkins.ticket.checkin')

                elif len(used_items) > checkins:
                    # Have to unredeem some tickets
                    to_unredeem_count = len(used_items) - checkins
                    # Check if user has enough used tickets
                    if len(used_items) < to_unredeem_count:
                        return HttpResponseBadRequest()
                    # Unredeem 'em
                    for i in range(to_unredeem_count):
                        item = used_items[i]
                        # Find active redeem package for this item
                        pkgs = item.redeem_packages.filter(user=item.owner, cancelled_by=None)
                        # Sanity checks. Note that every ticket is currently assumed to have its own separate redeem package.
                        assert len(pkgs) == 1
                        pkg_items = pkgs[0].items.all()
                        assert len(pkg_items) == 1
                        assert pkg_items[0].id == item.id
                        # Unredeem it
                        pkgs[0].unredeem(request.user)
                        statsd.incr('pro.checkin_view.checkins.ticket.checkout')

            elif guest_tokens[0] == 'list':
                # Try to parse out the list and member ids
                try:
                    checkins = int(val)
                    list_id = int(guest_tokens[1])
                    listmember_id = int(guest_tokens[2])
                except:
                    return HttpResponseBadRequest()

                # Find the List and ListMember objects. Lock the ListMember
                try:
                    listmember = VenueListMember.objects.filter(id=listmember_id).select_for_update()[0]
                except IndexError:
                    raise Http404
                venuelist = listmember.list

                # Verify given list id
                if venuelist.id != list_id:
                    return HttpResponseBadRequest()
                # Make sure the current event uses this list
                if venuelist.event.id != eventid:
                    return HttpResponseBadRequest()

                # Get the total number of tickets this list member has
                total_tickets = listmember.ticket_count
                # Count the total number of checkins/declines and verify
                all_checkins = VenueVisit.objects.filter(list_member=listmember, event=event)
                total_checkins = len(filter(lambda visit: visit.status == VenueVisit.STATUS_ENTERED, all_checkins))
                total_declined = len(filter(lambda visit: visit.status == VenueVisit.STATUS_DECLINED, all_checkins))
                total_left = len(filter(lambda visit: visit.status == VenueVisit.STATUS_LEFT, all_checkins))
                currently_in = total_checkins - total_left

                if declining:
                    # Here checkins indicates how many tickets should be declined in total
                    logging.info("  already have %d / %d declines", total_declined, checkins)
                    extra_declines = checkins - total_declined

                    if extra_declines < 0:
                        logging.warning("event_guest_set_entered_count(): trying to reduce declined count for %s (%d to %d)",
                                        key, total_declined, checkins)
                        return HttpResponseBadRequest()
                    elif extra_declines > total_tickets - currently_in - total_declined:
                        logging.warning("event_guest_set_entered_count(): not enough unused tickets to decline %d more of %s", extra_declines, key)
                        return HttpResponseBadRequest()
                    elif extra_declines > 0:
                        # Do it!
                        for i in range(extra_declines):
                            VenueVisit.objects.create(list_member=listmember, event=event, entrance_by=request.user, status=VenueVisit.STATUS_DECLINED)

                else:
                    # Normal checkins
                    if checkins > (total_tickets - total_declined) or checkins < 0:
                        return HttpResponseBadRequest()
                    extra_checkins = checkins - currently_in

                    # Do it!
                    visit_status = VenueVisit.STATUS_ENTERED if extra_checkins >= 0 else VenueVisit.STATUS_LEFT
                    for i in range(abs(extra_checkins)):
                        VenueVisit.objects.create(status=visit_status, list_member=listmember, event=event, entrance_by=request.user)
                    statsd.incr('pro.checkin_view.checkins.list.' + ('checkin' if extra_checkins >= 0 else 'checkout'), abs(extra_checkins))

            elif guest_tokens[0] == 'tally':
                # Tally is special - the only included token is the client's local id of the tally guest. All the other
                #  data about the guest is json-encoded into the value.
                # Try to parse out the list and member ids
                try:
                    local_id = int(guest_tokens[1])
                    data = json.loads(val)
                    # Validate data's structure
                    assert 'list' in data and 'comment' in data and 'categories' in data and isinstance(data['categories'], list)
                except Exception as e:
                    return HttpResponseBadRequest()

                # Find the list, if any
                guestlist = None
                if data['list']:
                    try:
                        guestlist = VenueList.objects.filter(event=event, name=data['list']).order_by('id')[0]
                    except IndexError:
                        pass
                if not guestlist:
                    # There's a small chance of get_or_create() creating two lists, in case of parallel requests. Use
                    #  locking to prevent that. We don't have anything good to lock, so we lock the entire event. This
                    #  isn't exactly nice, but we're doing it for a very short amount of time, so it should be ok.
                    with transaction.commit_on_success():
                        # Lock the event
                        list(Event.all_objects.filter(pk=event.pk).select_for_update())
                        # Create a hidden 'default' list unless it already exists
                        guestlist, created = VenueList.objects.get_or_create(event=event, list_type=VenueList.TYPE_TALLY_HIDDEN,
                                                                             defaults={ 'owner': event.owner_org, 'created_by': request.user })

                # Lock the guestlist. We don't (usually) have the VenueListMember yet, so we can't lock that, but we
                #  must prevent multiple VenueListMembers with same uuid from being created.
                list(VenueList.objects.filter(pk=guestlist.pk).select_for_update())
                # Check if guest with this UUID already exists (if UUID is given)
                listmember = None
                uuid = unicode(data.get('uuid', '')).lower()
                if uuid:
                    # Verify UUID format
                    if not re.match(r'^[0-9a-f]{32}$', uuid):
                        logging.warning("Invalid UUID '%s' for tally guest '%s' of event %d (%s)",
                                        uuid, local_id, event.id, event.name)
                        return HttpResponseBadRequest()
                    # See if we already have VenueListMember with that UUID
                    try:
                        listmember = VenueListMember.objects.get(uuid=uuid, list=guestlist)
                    except VenueListMember.DoesNotExist:
                        pass

                if listmember:
                    # Do nothing.
                    logging.info("Skipping already-checked-in guest with uuid %s", uuid)
                else:
                    # Old tally guests' comments might be in the format "general comment; Male, comp."
                    split_comment = data['comment_full'].partition('; ')
                    if split_comment[2]:
                        tally_name, separator, tally_comment = split_comment[2].partition(', ')
                        tally_comment = "%s; %s" % (split_comment[0], tally_comment)
                    else:
                        tally_name, separator, tally_comment = split_comment[0].partition(', ')

                    # Create new VenueVisitor, VenueListMember and VenueVisit objects
                    visitor = VenueVisitor.get_visitor(full_name=tally_name, email='', is_tally=True)
                    visitor_metadata = visitor.metadata(event.owner_org, force_save=True)
                    listmember = VenueListMember.objects.create(list=guestlist, created_by=request.user, visitor=visitor,
                                                                uuid=uuid, full_name=tally_name, ticket_count=1, comment=tally_comment, is_tally=True)

                    visit = VenueVisit.objects.create(list_member=listmember, event=event, entrance_by=request.user)
                    statsd.incr('pro.checkin_view.checkins.tally.checkin')

                    # Attach categories to VenueVisitor and VenueVisit
                    for category in VisitorCategory.objects.filter(id__in=data['categories']):
                        visitor_metadata.categories.add(category)
                        visit.categories.add(category)
                        statsd.incr('pro.checkin_view.checkins.tally.category_added')

                # Add  local_id -> server_id  mapping to the return data
                if 'tally' not in ret:
                    ret['tally'] = {}
                ret['tally'][key] = 'list-%d-%d' % (guestlist.id, listmember.id)

            else:
                return HttpResponseBadRequest()

    return HttpResponse(json.dumps(ret), mimetype='application/json')


@login_required(login_url=ADMIN_LOGIN_URL)
@cache_control(no_cache=True, no_store=True, must_revalidate=True)
def org_lists_manager(request):
    list_permissions = UserPermission.objects.select_related('list').exclude(list=None).filter(list__event=None,
                                                                                               user=request.user,
                                                                                               permission_list_edit=True)
    if not list_permissions:
        raise PermissionDenied("%s.list_edit" % request.user)

    return org_lists(request, list_permissions=list_permissions)


@login_required(login_url=ADMIN_LOGIN_URL)
@cache_control(no_cache=True, no_store=True, must_revalidate=True)
def org_lists(request, organizationid=None, list_permissions=None):
    assert organizationid or list_permissions

    organization = None
    if organizationid:
        organization = get_object_or_404(Organization, id=organizationid)

        if not organization.has_user_permission(request.user, 'list_edit') and not list_permissions:
            raise PermissionDenied("%s.list_edit" % request.user)

    # Get lists
    if organization:
        lists = organization.lists.filter(event=None, list_type=VenueList.TYPE_NORMAL)
        lists = [l for l in lists]
    else:
        lists = [perm.list for perm in list_permissions]

    lists = sorted(lists, key=lambda l: (l.list_type, bool(l.sync_from), l.name))
    guest_lists = []
    for list in lists:
        if not list_permissions:
            managers = UserPermission.objects.filter(list=list).select_related('user')
            setattr(list, 'managers', managers)

        list_members = get_list_members(list, None)

        mass_editor_content = ""
        for member in list_members:
            mass_editor_content += member["name"]
            if member["quantity"] > 1:
                mass_editor_content += ' +%d' % (member["quantity"] - 1)
            if member["comment"]:
                mass_editor_content += member["comment"]
            mass_editor_content += '\n'
        setattr(list, 'mass_editor_content', mass_editor_content)

        setattr(list, 'members_count', sum([m['quantity'] for m in list_members]))
        if list_permissions:
            setattr(list, 'limit', filter(lambda p: p.list_id == list.id, list_permissions)[0].list_limit)
            setattr(list, 'limit_count', sum([m['quantity'] for m in filter(lambda m: m['added_by'] == request.user.id, list_members)]))

        guest_lists.append((list, list_members))

    if not guest_lists and organization:
        viplist1 = VenueList(list_type=1, owner=organization, name=_("Sample permanent list 1"))
        viplist2 = VenueList(list_type=1, owner=organization, name=_("Sample permanent list 2"))
        setattr(viplist1, 'is_placeholder', True)
        setattr(viplist2, 'is_placeholder', True)
        guest_lists.append((viplist1, []))
        guest_lists.append((viplist2, []))

    return render_to_response('venueadmin/org_lists.html', RequestContext(request, {
        'guest_lists': guest_lists,
        'organization_id': organization.id if organization else "null",
        'is_manager': bool(list_permissions),
        'list_manager_form': ListUserForm(request=request),
        'list_member_form': VenueListMemberForm(request=request),
        'is_org_lists_page': True,
        }))


def no_venues(request):
    return render_to_response('venueadmin/no_venues.html', RequestContext(request, {
    }))


@login_required(login_url=ADMIN_LOGIN_URL)
@utils.permission_required("venue_edit")
def edit_venue(request, venueid):
    venue = get_object_or_404(Venue, id=venueid)

    form = VenueForm(request.POST or None, request.FILES or None, instance=venue)
    if form.is_valid():
        form.fields['avatar'].save_avatar(form, owner=venue, delete=form.cleaned_data['delete_avatar'])

        form.save()

        # The venues country field can be changed, that effects the VAT logic.
        if venue.has_shop():
            venue.shop.data.enable_vat = shop_should_ask_vat(venue.shop, venue)
            venue.shop.data.save()

        return HttpResponseRedirect(reverse("venueadmin_index"))

    venue_public_url = request.build_absolute_uri(venue.get_absolute_url())

    return render_to_response('venueadmin/venue_edit.html', RequestContext(request, {
        'venue_form': form,
        'venue': venue,
        'cover': avatar_image_path(venue, 144, 144),
        'venue_public_url': venue_public_url,
        }))


@login_required(login_url=ADMIN_LOGIN_URL)
def venue_claim(request, venueid):
    venue = get_object_or_404(Venue, id=venueid)
    org = request.user_selected_org
    if org and (org.owned_venue == venue or org.is_venue_being_claimed):
        return redirect('event_add', venueid=venue.id)

    venue_claim_form = VenueClaimForm(request.POST or None, selected_org=request.user_selected_org, user=request.user,
                                      initial={'email_address': request.user.email})

    if venue_claim_form.is_valid():

        cleaned_org = venue_claim_form.cleaned_data['organization']
        if cleaned_org == 'add_new':
            # Create new organization, make the user its admin
            organization = Organization.objects.create(official_name=venue_claim_form.cleaned_data['new_org_name'], created_by=request.user, owner=request.user, venue=venue)
            perms = UserPermission.grant_all_permissions(request.user, by=request.user, org=organization)
            perms.save()
        elif isinstance(cleaned_org, Organization):
            organization = cleaned_org

            # Set organization's venue to point to the new venue
            assert not organization.venue
            organization.venue = venue
            organization.save()
        else:
            assert False, "Invalid organization %s" % repr(cleaned_org)

        VenueClaimRequest.create(organization, venue, request.user, venue_claim_form.cleaned_data['email_address'])

        return redirect('create_event', venueid=venue.id)

    return render_to_response('venueadmin/venue_claim.html', RequestContext(request, {
        'venue': venue,
        'claim_venue_form': venue_claim_form
    }))


@login_required(login_url=ADMIN_LOGIN_URL)
@utils.permission_required("floorplans_edit")
def venue_table_config(request, venueid):
    venue = get_object_or_404(Venue, id=venueid)

    object_choices = [[{
                           'name': choice[1],
                           'class': 'object-%s' % choice[0].partition('-')[0],
                           'subclass': 'object-%s' % choice[0],
                           'type': choice[0]
                       } for choice in group] for group in AREA_OBJECT_CHOICES]

    return render_to_response('venueadmin/venue_table_config.html', RequestContext(request, {
        'venue': venue,
        'areas': json.dumps(VenueArea.get_venue_areas_data(venue), indent=2),
        'object_choices': object_choices,
        }))


@login_required(login_url=ADMIN_LOGIN_URL)
@utils.permission_required("floorplans_edit")
def venue_menu(request, venueid):
    venue = get_object_or_404(Venue, id=venueid)
    org = venue.owner_org
    if not org:
        logging.error("venue_menu() called for venue %d which doesn't have confirmed owner", venue.id)
        raise Http404

    if not org.has_shop():
        currency_form = CurrencySelectionForm(request.POST or None)

        if currency_form.is_valid():
            currency = currency_form.cleaned_data['currency']
            logging.info("venue_menu(): Org %d (%s) doesn't have shop yet, creating it (%s)",
                         org.id, org.display_name(), currency.code)
            _create_venue_shop(org, currency, request.user)

            return redirect('venue_menu', venueid=venueid)

        return render_to_response('venueadmin/venue_menu.html', RequestContext(request, {
            'organization': org,
            'venue': venue,
            'currency_form': currency_form,
            }))

    menu_items = ShopItem.objects.menu_items(org.shop)

    return render_to_response('venueadmin/venue_menu.html', RequestContext(request, {
        'organization': org,
        'venue': venue,
        'currency': org.currency,
        'menu_items': menu_items,
        }))


def format_subscription_interval(interval, count):
    PLAN_INTERVAL_YEARLY = 'year'
    PLAN_INTERVAL_MONTHLY = 'month'
    PLAN_INTERVAL_WEEKLY = 'week'
    second_forms = {
        PLAN_INTERVAL_YEARLY: ungettext('yearly', 'once every %(count)d years', count) % {count: interval},
        PLAN_INTERVAL_MONTHLY:  ungettext('monthly', 'once every %(count)d months', count) % {count: interval},
        PLAN_INTERVAL_WEEKLY:  ungettext('weekly', 'once every %(count)d weeks', count) % {count: interval},
    }
    return second_forms[interval]


@login_required(login_url=ADMIN_LOGIN_URL)
@utils.permission_required("venue_edit")
def org_legal_info(request, organizationid):
    organization = get_object_or_404(Organization, id=organizationid)

    # Create the form and see if it's valid
    form = OrganizationLegalInfoForm(request.POST or None, instance=organization)
    if form.is_valid():
        org = form.save()

        if not org.has_shop() and 'currency' in form.cleaned_data:
            try:
                currency = Currency.objects.get(id=form.cleaned_data['currency'])
            except Currency.DoesNotExist:
                pass
            else:
                _create_venue_shop(org, currency, request.user)

        else:
            # Country may change -> VAT (tax) settings must be revised.
            org.shop.data.enable_vat = shop_should_ask_vat(org.shop)
            org.shop.data.save()

        messages.info(request, _("Organization saved\nOrganization info was saved."))
        return redirect('org_legal_info', organizationid=org.id)

    org_public_url = request.build_absolute_uri(organization.get_absolute_url())
    subscribe_form = OrganizationSubscriptionForm(initial={'action': OrganizationSubscriptionForm.ACTION_CREATE})
    plan_id = organization.subscription_plan_id
    try:
        plan_data = get_stripe_plan(plan_id)
    except StripeError:
        logging.exception('Error getting Stripe plan from Stripe - possible configuration error')
        plan_data = None

    org_shop = organization.shop.data if organization.has_shop() else None

    required_form_fields = OrganizationLegalInfoForm.required_form_fields
    instance_data = {field_name: getattr(organization, field_name, '') for field_name in required_form_fields}
    instance_data['currency'] = organization.currency.id if organization.currency else None

    check_subscribe_allowed_form = OrganizationLegalInfoForm(instance_data, instance=organization)
    if plan_data:
        allow_subscribing = check_subscribe_allowed_form.is_valid()
    else:
        allow_subscribing = False

    subscription_interval = getattr(plan_data, 'interval', 'month')
    subscription_interval_count = getattr(plan_data, 'interval_count', 1)

    return render_to_response('venueadmin/org_legal_info.html', RequestContext(request, {
        'payment_days': settings.PAYMENT_DAYS,
        'organization': organization,
        'has_subscription': organization.subscription_data and 'subscription_id' in organization.subscription_data,
        'org_public_url': org_public_url,
        'form': form,
        'stripe_key': settings.STRIPE_PUBLISHABLE_KEY,
        'subscribe_form': subscribe_form,
        'subscription_configuration_error': not bool(plan_data),
        'subscription_monthly_fee': "%.2f" % (getattr(plan_data, 'amount', 9900)/100.0),
        'subscription_monthly_fee_cents': getattr(plan_data, 'amount', 9900),
        'subscription_currency': getattr(plan_data, 'currency', 'eur').upper(),
        'subscription_interval':subscription_interval,
        'subscription_interval_verbose': format_subscription_interval(
            subscription_interval, subscription_interval_count,
        ),
        'margin_enabled': org_shop.enable_margin if org_shop else False,
        'margin': (org_shop.margin * Decimal(100)).quantize(Decimal('0.01')) if org_shop else 0,
        'organization_url': request.build_absolute_uri(organization.get_absolute_url()),
        'allow_subscribing': allow_subscribing,
    }))


@login_required(login_url=ADMIN_LOGIN_URL)
@utils.permission_required("billing")
@require_POST
def org_subscription(request, organizationid):
    organization = get_object_or_404(Organization, id=organizationid)
    user = request.user

    subscribe_form = OrganizationSubscriptionForm(request.POST)
    if not subscribe_form.is_valid():
        return HttpResponse(json.dumps({
            'error': 'Form is invalid',
            'errorDetail': subscribe_form.errors,
        }), content_type='application/json')

    action = subscribe_form.cleaned_data['action']
    if action in [subscribe_form.ACTION_CREATE, subscribe_form.ACTION_CREATE_CONFIRM]:
        error_message = unicode(_("Subscribing failed\n"
                                  "Please check your credit card details. Contact our customer support if problems persist"))

        def get_error_response():
            return HttpResponse(json.dumps({
                'error': error_message,
            }), content_type='application/json')

        # Handle initial setup step
        if action == subscribe_form.ACTION_CREATE:
            token = subscribe_form.cleaned_data['stripe_token']

            logging.info("Creating subscription for organization %d (%s), by user %d (%s; %s)",
                         organization.id, organization.display_name(), user.id, user.email, user.get_full_name())
            try:
                customer = stripe.Customer.create(
                    description="Customer for org %s" % organization.display_name(),
                    email=user.email,
                    source=token,
                )
            except StripeError:
                logging.exception("org_subscription(): StripeError while creating customer")
                return get_error_response()

            # If the organization's trial_expiry is in the future, notify Stripe to use the same trial end timestamp.
            if organization.trial_expiry_timestamp > timezone.now():
                trial_end = calendar.timegm(organization.trial_expiry_timestamp.utctimetuple())
            else:
                trial_end = 'now'
            try:
                subscription = stripe.Subscription.create(
                    customer=customer,
                    trial_end=trial_end,
                    items=[
                        {
                            'plan': organization.subscription_plan_id,
                        },
                    ],
                    expand=[
                        'latest_invoice.payment_intent',
                        'pending_setup_intent',
                    ],
                    # For SCA payments, this is allowed to gracefully fail first invoice so we can have payment intent
                    # and request SCA in frontend
                    payment_behavior='allow_incomplete',
                )
            except StripeError:
                logging.exception("org_subscription(): StripeError while creating subscription for customer %s",
                                  customer.id)
                return get_error_response()

            if subscription.status == 'incomplete':
                if any([
                    not subscription.latest_invoice,
                    not subscription.latest_invoice.payment_intent,
                    subscription.latest_invoice.payment_intent.status not in ['requires_action', 'requires_source_action']
                ]):
                    logging.exception(
                        "org_subscription(): Stripe payment couldn't be completed when"
                        " creating subscription for customer %s",
                        customer.id,
                    )
                    return get_error_response()
                else:
                    # Requires user action on frontend for confirming the purchase
                    organization.subscription_data = {
                        'pending_subscription_id': subscription.id,
                        'pending_customer_id': customer.id,
                    }
                    organization.save()

                    return HttpResponse(json.dumps({
                        'paymentIntentSecret': subscription.latest_invoice.payment_intent.client_secret,
                        'paymentIntentId': subscription.latest_invoice.payment_intent.id,
                    }), content_type='application/json')
            elif subscription.status == 'trialing':
                # For trialing subscriptions, the subscription may succeed straight away, or may require setup intent
                # to be processed if SCA is active for the card
                if subscription.pending_setup_intent:
                    organization.subscription_data = {
                        'pending_subscription_id': subscription.id,
                        'pending_customer_id': customer.id,
                    }
                    organization.save()

                    return HttpResponse(json.dumps({
                        'setupIntentSecret': subscription.pending_setup_intent.client_secret,
                        'setupIntentId': subscription.pending_setup_intent.id,
                    }), content_type='application/json')

                # If no setup intent necessary, continue with activating the subscription below
            elif subscription.status != 'active':
                logging.exception(
                    "org_subscription(): Unknown subscription status returned by stripe %s",
                    customer.id,
                )
                return get_error_response()
        else:
            # Action == ACTION_CREATE_CONFIRM
            try:
                customer = stripe.Customer.retrieve(organization.subscription_data.get('pending_customer_id'))
                subscription = stripe.Subscription.retrieve(
                    organization.subscription_data.get('pending_subscription_id'),
                    expand=[
                        'latest_invoice.payment_intent',
                        'pending_setup_intent',
                    ],
                )
            except StripeError:
                logging.exception(
                    "org_subscription(): Couldn't load stripe customer",
                )
                return get_error_response()
            if subscription.status not in ['active', 'trialing']:
                logging.exception(
                    "org_subscription(): Stripe subscription failed after SCA",
                )
                return get_error_response()
            if subscription.status == 'trialing' and subscription.pending_setup_intent:
                logging.exception(
                    "org_subscription(): Stripe subscription setup was not completed",
                )
                return get_error_response()

        # Subscription is active if execution reached this point
        logging.info("Subscription created with id %s", subscription.id)

        # Save subscription info
        try:
            plandata = get_stripe_plan(organization.subscription_plan_id)
        except StripeError:
            logging.exception(
                "org_subscription(): Failed to fetch Stripe plan from Stripe - possible configuration error.",
            )
            return get_error_response()
        currency = Currency.objects.get(code=plandata.currency.upper())

        organization.monthly_fee_in_eur = Decimal(Decimal(plandata.amount/100.0)/currency.rate) \
            .quantize(Decimal('0.01'), rounding=ROUND_HALF_UP)

        # Get data for card used
        if hasattr(customer, 'cards') and customer.cards.data:
            card_data = customer.cards.data[0]
            card_id = card_data.id
            card_last4 = card_data.last4
            card_type = card_data.type
        elif hasattr(customer, 'sources') and customer.sources.data:
            card_data = customer.sources.data[0]
            card_id = card_data.id
            card_last4 = card_data.last4
            card_type = card_data.brand
        else:
            # Fallback to still allow the purchase if something unexpectedly changes in Stripe API
            # (shouldn't happen)
            # This is relatively safe, since this data is only kept for displaying the subscription info to
            # the client - unsubscription only requires customer id and subscription id
            card_id = None
            card_last4 = 'XXXX'
            card_type = ''

        organization.subscription_data = {
            'subscription_id': subscription.id,
            'customer_id': customer.id,
            'card_id': card_id,
            'last4': card_last4,
            'type': card_type,
        }

        organization.plan = Organization.PLAN_PREMIUM
        organization.save()

        send_mandrill_email(
            email='pro@events.com',
            template='emails/staff/subscription_created.html',
            subject='%s subscribed to events Pro' % organization.display_name(),
            organization=organization,
            user=user,
            subscription=subscription,
            )

        metrics.record("PRO", "Subscription completed", user=organization.get_first_admin(), request=request)
        messages.info(request, _("Subscription created\nThanks for your business!"))
        return HttpResponse(json.dumps({
            'success': True,
        }), content_type='application/json')

    elif action == subscribe_form.ACTION_CANCEL:
        # Cancel the active subscription

        subscription_id = organization.subscription_data and organization.subscription_data.get('subscription_id')

        logging.info("org_subscription(): Deleting subscription %s of organization %d (%s) by user %d (%s; %s)",
                     subscription_id, organization.id, organization.display_name(),
                     user.id, user.email, user.get_full_name())
        try:
            organization.cancel_subscription()
        except Organization.NoSubscriptionException:
            return HttpResponse(json.dumps({
                'success': True,
            }), content_type='application/json')
        except StripeError:
            logging.error("org_subscription(): Deleting subscription %s of organization %d (%s) by"
                          " user %d (%s; %s) failed",
                         subscription_id, organization.id, organization.display_name(),
                         user.id, user.email, user.get_full_name())
            return HttpResponse(json.dumps({
                'error': unicode(_("Error occurred\nThere was a problem while cancelling your subscription. "
                                    "We're investigating and will contact you."))
            }), content_type='application/json')

        send_mandrill_email(
            email='pro@events.com',
            template='emails/staff/subscription_cancelled.html',
            subject='%s cancelled events PRO subscription!' % organization.display_name(),
            organization=organization,
            user=user,
            )

        logging.info("org_subscription(): Subscription deleted")
        metrics.record("PRO", "Subscription cancelled", user=organization.get_first_admin(), request=request)
        messages.info(request, _("Subscription cancelled\nWe're sorry to see you go :-("))
        return HttpResponse(json.dumps({
            'success': True,
        }), content_type='application/json')

    else:
        # This should not happen since OrganizationSubscriptionForm validates the action.
        logging.error("org_subscription(): Unexpected action '%s'", action)
        return HttpResponse(json.dumps({
            'success': False,
        }), content_type='application/json')


def venue_fb_shop(request, venueid):
    return redirect('venue_promo_tools', venueid=venueid)


@login_required(login_url=ADMIN_LOGIN_URL)
@utils.permission_required("venue_edit")
def venue_promo_tools(request, venueid):
    """
        This view serves three purposes:
            1.  Present the view where the venueadmin can add his venue URL and
            thus the Facebook page ID.
            2. The user makes a post request to save the fetched page ID.
            3. If we got the page ID we present the user the app install link,
            the link goes to Facebook, and is redirect back to this page with some
            GET arguments, we parse them and redirect the user back to his venue page
            on FB.

    """
    venue = get_object_or_404(Venue, id=venueid)
    app_id = settings.FACEBOOK_SHOP_APP_ID

    # This is the final url, when clicked prompts the page owner to install our app
    # The next URI *this* page(FB will append a GET argument)
    app_install_url = "https://www.facebook.com/dialog/pagetab?app_id=%s&next=%s" % (app_id, request.build_absolute_uri())

    # This handles the redirect back to Facebook after install
    if request.GET:
        page_id = None
        # Regex for parsing the page id out of the GET arguments
        rex = re.compile('.*?\[(\d+)\]$')
        for key in request.GET.keys():
            page_id = rex.match(key).groups(1)[0]
            if page_id:
                redirect_url = "http://facebook.com/%s" % page_id
                return HttpResponseRedirect(redirect_url)

    venue_public_url = request.build_absolute_uri(venue.get_absolute_url())
    widget_url = request.build_absolute_uri(reverse("widget"))

    return render_to_response('venueadmin/venue_promo_tools.html', RequestContext(request, {
        'venue': venue,
        'app_install_url': app_install_url,
        'venue_public_url': venue_public_url,
        'widget_url': widget_url,
        }))


@login_required(login_url=ADMIN_LOGIN_URL)
def simple_venue_add(request):
    form = VenueForm(request.POST or None, initial={ 'name': request.GET.get('name', '') })

    if form.is_valid():
        # Get Venue (not saved yet) from the form
        venue = form.save(commit=False)

        # Create Venue
        venue.created_by = request.user  # Just for reference
        venue.active = True
        venue.save()
        statsd.incr('pro.venues.created.total')

        # Give creator user all permissions
        venue_permission = UserPermission.grant_all_permissions(request.user, by=request.user, venue=venue)
        venue_permission.save()

        form.fields['avatar'].save_avatar(form, owner=venue)

        organization = None
        if form.cleaned_data['i_own_this']:
            # User claims to own this venue
            if request.user_selected_org and not request.user_selected_org.venue and \
                    request.user_selected_org.has_user_permission(request.user, 'org_set_venue'):
                # Use the existing org, it's not attached to any venues yet and user has permissions.
                organization = request.user_selected_org
                organization.venue = venue
                organization.save()
            else:
                # Create an org with the same name as venue, make the user its admin
                organization = Organization.objects.create(official_name=form.cleaned_data['name'], created_by=request.user, owner=request.user, venue=venue)
                perms = UserPermission.grant_all_permissions(request.user, by=request.user, org=organization)
                perms.save()
                statsd.incr('pro.venues.created.with_claim')

            # Immediately give ownership for the created venues.
            venue.owner_org = organization
            venue.save()
            # Notify the staff as well
            message = "New venue '%s' was created and claimed with '%s' by '%s'" % (
                venue.name, organization.official_name, request.user.get_full_name()
            )
            NotificationMessage.create(message, request.user, from_organization=organization, to_staff=True,
                                       data={'venue': venue.id, 'organization': organization.id})

        # Users can get to the venue creation page only from the event_add page, so redirect back there.
        response = redirect('create_event', venueid=venue.id)
        if organization:
            response.set_cookie('active_organization_id', organization.id, max_age=5*365*86400)
        return response

    return render_to_response('venueadmin/venue_add_simple.html', RequestContext(request, {
        'venue_form': form,
        }))


@login_required(login_url=ADMIN_LOGIN_URL)
def create_event_wrapper(request):
    venue_id = None
    if 'venue' in request.POST:
        return create_event(request, request.POST['venue'])

    org = request.user_selected_org
    if org and org.venue_id:
        venue_id = org.venue_id

    return create_event(request, venue_id)

def collect_ticket_fields(post_data):
    ret = {}
    field_finder = re.compile("^(new-ticket-)(\d+)-(.+)")
    all_tickets = [ match for match in map(field_finder.match, post_data.iterkeys()) if match ]
    all_tickets = map(lambda x: {
        "name": x.group(0),
        "prefix": "%s%s" % (x.group(1), x.group(2)),
        "value": post_data[x.group(0)] or 0
    },
                      all_tickets)

    for field in all_tickets:
        if field["prefix"] not in ret:
            ret[field["prefix"]] = []
        ret[field["prefix"]].append(field["name"])
    return ret

@login_required(login_url=ADMIN_LOGIN_URL)
@cache_control(no_cache=True, no_store=True, must_revalidate=True)
def create_event(request, venueid=None):
    if 'venue' in request.POST:
        venueid = request.POST['venue']

    try:
        venue = Venue.objects.get(id=venueid)
    except:
        venue = None
        user_can_add_event = False
    else:
        user_can_add_event = venue.has_user_permission(request.user, 'event_add')

    organization = request.user_selected_org
    # If there's no org selected or user doesn't have event creation permission redirect to organization creation page
    if not organization or not organization.has_user_permission(request.user, 'event_add'):
        return redirect("venueadmin_index")

    if not request.POST:
        metrics.record("PRO", "Started create event", user=organization.get_first_admin(), request=request)

    ticket_forms = []
    sell_until = EventForm.default_end_time(venue or organization.venue) if venue or organization.venue else None

    try:
        shop = organization.shop
    except Shop.DoesNotExist:
        shop = None

    if request.POST:
        ticket_fields = collect_ticket_fields(request.POST)
        for key, field in ticket_fields.items():
            ticket_forms.append(EventTicketForm2(request.POST, event=None, user=request.user, prefix=key, shop=shop, sell_until=sell_until))

    series_form = EventSeriesForm(request.POST or None, request=request) if organization.feature_recurring_events else None
    form = EventForm2(request.POST or None, request.FILES or None, organization=organization, venue=venue, user=request.user)

    if organization.has_shop():
        form.fields['baseTicketPrice'].widget.attrs['appended_text'] = organization.shop.default_currency
    if 'facebook_id' in request.GET:
        form.fields['facebook_id'].initial = request.GET['facebook_id']

    with timezone.override(venue.timezone if venue else 'UTC'):
        if form.is_valid() and (not series_form or series_form.is_valid()):
            for ticket in ticket_forms:
                ticket.end_time = form.cleaned_data['end_time']
                if not organization.has_shop():
                    ticket.currency = form.cleaned_data['currency']

            if all(map(lambda x: x.is_valid(), ticket_forms)):
                # If the org doesn't have a shop yet, then create it now.
                if not organization.has_shop():
                    currency = form.cleaned_data['currency']
                    logging.info("create_event(): Org %d (%s) doesn't have shop yet, creating it (%s)",
                                 organization.id, organization.display_name(), currency.code)
                    _create_venue_shop(organization, currency, request.user, venue)

                # Save the new event
                event = form.save(commit=False)
                event.status = Event.STATUS_ACCEPTED if user_can_add_event else Event.STATUS_PENDING
                event.venue_id = form.cleaned_data['venue']
                event.owner_org = organization
                event.created_by = request.user

                if series_form:
                    series = series_form.save()
                    event.series = series

                event.save()
                form.image_helper.post_save(event)
                form.pull_fb_image(event)

                # Add tickets to event
                for frm in ticket_forms:
                    frm.event = event
                    frm.shop = organization.shop
                    frm.save()

                # Add special events tickets to event
                special_tickets = SpecialTicket.objects.filter(organization=organization)
                for special_ticket in special_tickets:
                    if str(special_ticket.price.currency) == str(organization.currency):
                        form = EventTicketForm(data={
                            'name': special_ticket.name,
                            'description': special_ticket.description,
                            'price': special_ticket.price.amount,
                            'in_stock': special_ticket.in_stock,
                            'sell_until_0': event.end_time.strftime("%Y-%m-%d"),
                            'sell_until_1': event.end_time.strftime("%H:%M:%S"),
                            }, event=event, user=request.user)

                        if form.is_valid():
                            ticket = form.save()
                            # In this point either we or Django has a problem.
                            # ShopItem here has correct data, but data_id is None,
                            # resulting in NULL value in the DB after next change.
                            # Instead, lets fetch the item again and update it.
                            ShopItem.objects.filter(id=ticket.id).update(visible_in_billing=False)
                        else:
                            logging.info("Error while validating the special ticket form: %s", form.errors)
                    else:
                        logging.info("Special ticket's '%s' currency (%s) doesn't match organization's currency (%s)",
                                     special_ticket, special_ticket.price.currency, organization.currency)

                # Add event's tags
                event.tags = form.cleaned_data['tags']


                if series_form:
                    series.create_events()

                # Send Metrics
                metrics.record("PRO", "Completed create event", user=organization.get_first_admin(), request=request)

                # Send an EventCreationRequest if user didn't have event creation permissions
                if not user_can_add_event:
                    EventCreationRequest.create(event)

                # Copy all permanent lists to this event
                permanent_lists = VenueList.objects.filter(event=None, list_type=VenueList.TYPE_NORMAL, owner=organization)
                for permalist in permanent_lists:
                    # Copy list
                    event_list = permalist.copy({'created_by': request.user, 'event': event, 'sync_from': permalist})
                    # Copy explicit list permissions
                    for list_permission in UserPermission.objects.filter(list=permalist):
                        # Trick to easily copy most of the fields of the permission object
                        list_permission.id = None
                        list_permission.list = event_list
                        list_permission.created_by = request.user
                        list_permission.save()

                # Activate facebook list
                if event.facebook_id:
                    try:
                        clean_fb_id = extract_id_token_from_str(event.facebook_id)
                        fb_obj = FacebookObjectByIdFetcher(clean_fb_id).get()
                    except (Exception, AttributeError):
                        pass
                    else:
                        fb_list, created = VenueFacebookList.objects.get_or_create(
                            list_type=VenueList.TYPE_FACEBOOK,
                            event=event,
                            owner=event.owner_org,
                            defaults={
                                'created_by': request.user,
                                'update_until': event.start_time,
                                'name': 'Facebook'
                            }
                        )
                        logging.info("Turned FB list on for event %s", event)

                # Activate all floorplans
                event.table_booking_areas.add(*list(VenueArea.objects.filter(venue=event.venue)))

                send_event_creation_email(event=event, request=request)

                if 'finish' in request.POST:
                    response = redirect("venueadmin_index")
                else:
                    # Append  ?create  to the url
                    redir_url = reverse('event_edit_tickets', kwargs={'eventid': event.id}) + '?create'
                    response = HttpResponseRedirect(redir_url)
                return response

        available_venues = []
        for _venue in Venue.objects.filter(active=True):
            available_venues.append({
                'id': _venue.id,
                'name': escape(_venue.name),
                'address': escape(_venue.addr_city + ', ' +_venue.addr_country_code),
                'has_permission': _venue.has_user_permission(request.user, 'event_add'),
                })

        add_ticket_form = EventTicketForm2(event=None, shop=shop, user=request.user, prefix="NEW", sell_until=sell_until)

        return render_to_response('venueadmin/create_event2.html', RequestContext(request, {
            'available_venues': json.dumps(available_venues, indent=1),
            'has_permission': user_can_add_event,
            'form': form,
            'series_form': series_form,
            'venue': venue,
            'organization': organization,
            'event_create_mode': True,
            'event_subpage': 'details',
            'add_ticket_form': add_ticket_form,
            'ticket_forms': ticket_forms
        }))


@login_required(login_url=ADMIN_LOGIN_URL)
@utils.permission_required("event_edit")
@cache_control(no_cache=True, no_store=True, must_revalidate=True)
def edit_event(request, eventid):
    event = get_object_or_404(Event, id=eventid)

    old_end_time = event.end_time

    # Convert datetimes to correct TZ's, this is to prevent incorrect changed_data
    initial = {}
    initial['start_time'] = event.start_time_local
    initial['end_time'] = event.end_time_local

    form = EventForm(request.POST or None, request.FILES or None, instance=event, initial=initial,
                     organization=event.owner_org, venue=event.venue, user=request.user)

    series_form = None
    if event.owner_org.feature_recurring_events:
        series_form = EventSeriesForm(request.POST or None, instance=event.series, request=request)
    form.fields['baseTicketPrice'].widget.attrs['appended_text'] = event.owner_org.shop.default_currency

    with timezone.override(event.venue.timezone):
        if form.is_valid() and (not series_form or series_form.is_valid()):
            event = form.save(commit=False)
            if series_form:
                series = series_form.save()
                event.series = series
            else:
                series = None
            event.save()
            # Since we didn't commit with form.save(), we need to call form.save_m2m() to save e.g. tags.
            form.save_m2m()
            form.image_helper.post_save(event)
            form.pull_fb_image(event)

            # Update tickets which have same sellable_until as the old event.end_time OR events that have
            # their end time after the modified event.end_time. This ensures better usability for users
            # and no errors for us.
            query = Q(data__sellable_until__gt=event.end_time)
            if old_end_time != event.end_time:
                query |= Q(data__sellable_until=old_end_time)
            tickets = ShopItem.objects.filter(query, data__event=event)
            if tickets:
                messages.info(request, "%s\n%s" % (ngettext("%d ticket modified!", "%d tickets modified!", len(tickets)) % len(tickets),
                                                   ugettext("Adjusted ticket sale end times, since event end time was modified.")))
                for ticket in tickets:
                    ticket.data.sellable_until = event.end_time
                    ticket.save()

            if series_form and series:
                series.create_events()

            # This check is needed because the user might create an event and click Continue and then go back to the
            #  first subpage (event details). If she then clicks Continue again, we need to redirect to the tickets
            #  subpage again (same as in create event view).
            if 'continue' in request.POST:
                # Append  ?create  to the url
                redir_url = reverse('event_edit_tickets', kwargs={'eventid': event.id}) + '?create'
                response = HttpResponseRedirect(redir_url)
            else:
                response = redirect("venueadmin_index")
            return response

        if event.end_time < timezone.now():
            form.fields['start_time'].widget.attrs['readonly'] = "readonly"
            form.fields['end_time'].widget.attrs['readonly'] = "readonly"

        return render_to_response('venueadmin/event_edit.html', RequestContext(request, {
            'form': form,
            'series_form': series_form,
            'event': event,
            'mobile_avatar': mobile_avatar(event, 640, 600) if request.user.is_staff else None,
            'event_create_mode': 'create' in request.GET,
            'event_subpage': 'details',
            }))


@login_required(login_url=ADMIN_LOGIN_URL)
@utils.permission_required("list_edit")
@cache_control(no_cache=True, no_store=True, must_revalidate=True)
def event_edit_promoters(request, eventid):
    event = get_object_or_404(Event, id=eventid)

    promoters = Promoter.objects.filter(organization=event.owner_org).order_by('name')
    promoter_lists = VenuePromoterList.objects.filter(event=event).order_by('promoter__name')
    promoters_with_existing_lists = set(promoter_lists.values_list('promoter', flat=True))

    last_state = None
    all_same_state = True
    state_for_all = False

    initial_data = []
    for promoter in promoters:
        this_state = promoter.id in promoters_with_existing_lists

        if last_state is not None and this_state != last_state:
            all_same_state = False

        last_state = this_state
        state_for_all = last_state

        if this_state:
            # A list already exists (is active) for this promoter.
            continue

        initial_data.append({
            'id': '',
            'active': False,
            'promoter': promoter,
            'list_limit': promoter.list_limit,
            'commission_male': promoter.commission_male,
            'commission_female': promoter.commission_female,
            })

    promoter_formset = EventPromoterFormSet(request.POST or None,
                                            queryset=promoter_lists, promoters=promoters,
                                            initial=initial_data, extra=len(initial_data), request=request)

    event_create_mode = 'create' in request.GET


    if event_create_mode:
        # Create: Default: toggle all on
        show_toggle_on = not (all_same_state and state_for_all)
    else:
        # Edit: Default: toggle all off
        show_toggle_on = all_same_state and not state_for_all

    return render_to_response('venueadmin/event_promoters.html', RequestContext(request, {
        'event': event,
        'promoter_formset': promoter_formset,
        'add_form': OrganizationAddPromoterForm(request=request, organization=event.owner_org),
        'event_create_mode': event_create_mode,
        'event_subpage': 'promoters',

        'show_toggle_on': show_toggle_on,
        }))


@login_required(login_url=ADMIN_LOGIN_URL)
@utils.permission_required("event_edit")
@cache_control(no_cache=True, no_store=True, must_revalidate=True)
def event_edit_tickets(request, eventid):
    event = get_object_or_404(Event, id=eventid)

    tickets = ShopItem.objects.filter(data__event=event, visible=True,
                                      data__categories__category_type=ShopCategory.TYPE_TICKET)

    ticket_forms = [
        EventTicketForm(instance=ticket, event=event, user=request.user, prefix='ticket-%d' % ticket.id)
        for ticket in tickets
    ]
    with timezone.override(event.venue.timezone):
        return render_to_response('venueadmin/event_tickets.html', RequestContext(request, {
            'event': event,
            'tables_form': EventTableSellingForm(instance=event),
            'ticket_forms': ticket_forms,
            'add_ticket_form': EventTicketForm(event=event, user=request.user, prefix="NEW"),
            'event_create_mode': 'create' in request.GET,
            'event_subpage': 'tickets',
            }))


def get_list_members(list, event):
    member_sort_key = lambda m: m['name'].lower()

    return sorted(filter(lambda m: m['type'] == 'normal', list.get_members(event)), key=member_sort_key)


@login_required(login_url=ADMIN_LOGIN_URL)
@cache_control(no_cache=True, no_store=True, must_revalidate=True)
def event_edit_lists(request, eventid):
    event = get_object_or_404(Event, id=eventid)

    # Allow expired events to show all tickets...
    expiry_timestamp = event.end_time + Event.EVENT_STILL_EDITABLE_FOR
    extra_statuses = [PurchasedItem.STATUS_EXPIRED] if expiry_timestamp < timezone.now() else []

    # Permission check
    permissions = None
    if not event.has_user_permission(request.user, 'list_edit'):
        # Does the user have explicit permissions to some of the lists?
        permissions = UserPermission.objects.filter(list__event=event, user=request.user, permission_list_edit=True)
        if not permissions:
            raise PermissionDenied("%s.list_edit" % request.user)

    list_types = dict()
    for type in VenueList.TYPE_CHOICES:
        list_types[type[1]] = type[0]

    # Get lists
    lists = event.lists.all()
    if permissions:
        lists = lists.filter(id__in=[p.list_id for p in permissions])
    lists = [l for l in lists]

    rsvp_form = None
    ticket_lists = dict()
    if not permissions:
        # Get attached permalists
        synced_ids = map(lambda x: x.sync_from.id, filter(lambda item: item.sync_from, lists))
        # Get all permalists excluding attached ones
        permalists = VenueList.objects.exclude(id__in=synced_ids).filter(owner=event.owner_org, event=None)

        # Add not added permalists to lists
        lists.extend(permalists)

        rsvp_form = VenueFreeListForm(event=event, user=request.user, prefix='rsvp')

        # If free guestlist is not in the array, append the default one
        if not len(filter(lambda l: l.list_type == VenueList.TYPE_FREEGUESTLIST, lists)):
            lists.append(rsvp_form.instance)

        # Get ticket lists
        shop_items = ShopItemData.objects.filter(event=event, categories__category_type=ShopCategory.TYPE_TICKET)

        for item in shop_items:
            list = VenueTicketBuyersList(item)
            list_members = list.get_members(event, extra_statuses=extra_statuses)
            if item.name in ticket_lists:
                list_members.extend(ticket_lists[item.name][1])

            members_count = sum([m['quantity'] for m in list_members])

            # Hide lists with zero members if the ticket data is obsolete (there's newer version of the ticket) or the
            #  ticket has been deleted.
            if members_count == 0 and (not item.item.visible or item.obsolete):
                continue

            left_items = max(item.item.total - members_count, 0)
            ticket_lists[item.name] = (members_count, sorted(list_members, key=lambda m: m['name'].lower()), item.item.id, left_items)

    lists = sorted(lists, key=lambda l: (l.list_type, bool(l.sync_from or l.event != event), l.name.lower()))
    guest_lists = []
    for list in lists:
        list_members = get_list_members(list, event)
        if list.list_type in (VenueList.TYPE_NORMAL, VenueList.TYPE_PROMOTER):
            setattr(list, 'promote_url', request.build_absolute_uri(list.get_promote_url()))

            mass_editor_content = ""
            for member in list_members:
                mass_editor_content += member["name"]
                if member["quantity"] > 1:
                    mass_editor_content += ' +%d' % (member["quantity"] - 1)
                if member["comment"]:
                    mass_editor_content += member["comment"]
                mass_editor_content += '\n'
            setattr(list, 'mass_editor_content', mass_editor_content)

        setattr(list, 'members_count', sum([m['quantity'] for m in list_members]))

        guest_lists.append((list, list_members))

        if permissions:
            setattr(list, 'limit', filter(lambda p: p.list_id == list.id, permissions)[0].list_limit)
            setattr(list, 'limit_count', sum([m['quantity'] for m in filter(lambda m: m['added_by'] == request.user.id, list_members)]))
        else:
            managers = UserPermission.objects.filter(list=list).select_related('user')
            setattr(list, 'managers', managers)

    site_url = get_site_url(None)

    with timezone.override(event.venue.timezone):

        return render_to_response('venueadmin/event_lists.html', RequestContext(request, {
            'event': event,
            'guest_lists': guest_lists,
            'rsvp_form': rsvp_form,
            'ticket_lists': ticket_lists,
            'list_types': list_types,
            'list_manager_form': ListUserForm(request=request),
            'list_member_form': VenueListMemberForm(event=event, request=request),
            'event_create_mode': 'create' in request.GET,
            'event_subpage': 'lists',
            'is_manager': permissions is not None,
            'FACEBOOK_APP_ID': getattr(settings, 'FACEBOOK_APP_ID', None),
            'SITE_URL': site_url,
            }))


@login_required(login_url=ADMIN_LOGIN_URL)
def event_share(request, eventid):
    event = get_object_or_404(Event, id=eventid)

    setattr(event, 'global_url', request.build_absolute_uri(event.get_absolute_url()))
    setattr(event, 'avatar_url', request.build_absolute_uri(avatar_image_path(event, 110, 155, user_site=True)))

    promo_emails = EventPromotionEmail.objects.filter(event=event).order_by('sent_timestamp')
    EventPromotionEmail.update_emails(promo_emails)

    generic_tags = PersonTag.objects.filter(generic=True)
    generic_tag_names = map(lambda tag: {'id': tag.id, 'text': unicode(tag.name_i18n)}, generic_tags)
    org_tag_names = map(lambda tag: {'id': tag['id'], 'text': tag['name']}, PersonTag.objects.filter(person__organization=event.owner_org_id, generic=False).distinct('name').values('id', 'name'))
    tag_options = generic_tag_names+org_tag_names

    with timezone.override(event.venue.timezone):
        return render_to_response('venueadmin/event_share.html', RequestContext(request, {
            'event': event,

            'FACEBOOK_APP_ID': getattr(settings, 'FACEBOOK_APP_ID', None),
            'event_create_mode': 'create' in request.GET,
            'event_subpage': 'sharing',
            'recipient_count': Person.objects.exclude(email='').filter(organization_id=event.owner_org_id).count(),
            'form': EventPromotionForm(event=event, created_by=request.user),
            'promo_emails': promo_emails,
            'tag_options': json.dumps(sorted(tag_options, key=lambda t: t['text'])),
            }))


@login_required(login_url=ADMIN_LOGIN_URL)
@utils.permission_required("event_edit")
def event_promotion_email(request, eventid):
    event = get_object_or_404(Event, id=eventid)
    if 'title' not in request.GET or 'content' not in request.GET:
        raise Http404

    form = EventPromotionForm(request.GET or None, event=event, created_by=request.user)

    if form.is_valid():
        cleaned_data = form.cleaned_data
    else:
        cleaned_data = {
            'title': event.name if 'title' in form.errors else request.GET['title'],
            'content': '' if 'content' in form.errors else request.GET['content'],
            }

    return render_to_response('emails/user/event_promotion.html', RequestContext(request, {
        'title': cleaned_data['title'],
        'content': cleaned_data['content'],
        'event_url': request.build_absolute_uri(event.get_absolute_url()),
        'avatar_url': request.build_absolute_uri(avatar_image_path(event, 173, 248, user_site=True)),
        'SITE_URL': get_site_url(request),
        'client_name': ' %s' % request.user.get_full_name(),
        }))


@utils.permission_required("basic")
def org_chat(request, organizationid):
    organization = get_object_or_404(Organization, id=organizationid)

    if not organization.feature_chat:
        return redirect('venueadmin_index')

    presentation_mode = 'presentation' in request.GET

    form = ChatAdminForm()

    template_name = 'venueadmin/org_chat.html'
    if presentation_mode:
        template_name = 'venueadmin/org_chat_big.html'

    # We have special hostnames for some organizers. We have hardcoded Organization ids mapped to them here.
    chat_urls_map = {
        13: 'atlantis.events.com',
        15: 'studio.events.com',
        }

    return render_to_response(template_name, RequestContext(request, {
        'organization': organization,
        'chat_admin_mode': 'false' if presentation_mode else 'true',
        'post_form': form,
        'chat_url': chat_urls_map.get(organization.id, 'chat.events.com'),
        }))


@login_required(login_url=ADMIN_LOGIN_URL)
@cache_control(no_cache=True, no_store=True, must_revalidate=True)
def event_edit_lists_mobile(request, eventid):
    event = get_object_or_404(Event, id=eventid)

    # Permission check
    permissions = None
    if not event.has_user_permission(request.user, 'list_edit'):
        # Does the user have explicit permissions to some of the lists?
        permissions = UserPermission.objects.filter(list__event=event, user=request.user, permission_list_edit=True)
        if not permissions:
            raise PermissionDenied("%s.list_edit" % request.user)

    # Get lists
    lists = event.lists.filter(list_type__in=(VenueList.TYPE_NORMAL, VenueList.TYPE_PROMOTER))
    if permissions:
        lists = lists.filter(id__in=[p.list_id for p in permissions])
    lists = [l for l in lists]

    lists = sorted(lists, key=lambda l: l.name.lower())
    guest_lists = []
    for list in lists:
        if list.list_type in (VenueList.TYPE_NORMAL, VenueList.TYPE_PROMOTER):
            setattr(list, 'promote_url', request.build_absolute_uri(list.get_promote_url()))

        list_members = get_list_members(list, event)
        setattr(list, 'members_count', sum([m['quantity'] for m in list_members]))
        guest_lists.append((list, list_members))

        if permissions:
            setattr(list, 'limit', filter(lambda p: p.list_id == list.id, permissions)[0].list_limit)
            setattr(list, 'limit_count', sum([m['quantity'] for m in filter(lambda m: m['added_by'] == request.user.id, list_members)]))


    with timezone.override(event.venue.timezone):

        return render_to_response('venueadmin/event_lists_mobile.html', RequestContext(request, {
            'event': event,
            'guest_lists': guest_lists,
            'list_member_form': VenueListMemberForm(event=event, request=request),
            'is_manager': permissions is not None,
            'FACEBOOK_APP_ID': getattr(settings, 'FACEBOOK_APP_ID', None),
            'SITE_URL': get_site_url(None),
            }))


@login_required(login_url=ADMIN_LOGIN_URL)
@cache_control(no_cache=True, no_store=True, must_revalidate=True)
def event_edit_tables_mobile(request, eventid):
    event = get_object_or_404(Event, id=eventid)

    is_promoter = False
    if not event.has_user_permission(request.user, "event_show_guests"):
        is_promoter = True
        if not Promoter.can_book_tables(request.user, event):
            raise PermissionDenied("%s.%s" % (request.user, "event_show_guests"))

    with timezone.override(event.venue.timezone):

        return render_to_response('venueadmin/event_tables_mobile.html', RequestContext(request, {
            'GUESTLIST_FORMAT_VERSION': GUESTLIST_FORMAT_VERSION,
            'event': event,
            'booking_form': TableBookingMobileForm(),
            'is_promoter': is_promoter,
            }))


class StatisticsView(TemplateView):
    # List of choices for days selection, shown to the user
    days_choices = [(1, _('Yesterday')), (7, _('1 week')), (14, _('2 weeks')),
                    (30, _('30 days')), (60, _('60 days')), (90, _('90 days')), (180, _('180 days'))]

    class TallyTable(tables.Table):
        list_name = tables.Column(verbose_name = _("List Name"))
        payout = tables.TemplateColumn(verbose_name = _("Payout"), template_code = '{{ value|floatformat:2  }}')
        males = tables.Column(verbose_name = _("Male"))
        females = tables.Column(verbose_name = _("Female"))
        total = tables.Column(verbose_name = _("Guests"), attrs={"th": {"class": "tally-fixed-column"}})

        orderable = 0

        class Meta:
            attrs = {'class': "table table-tally-summary"}


    def __init__(self, *args, **kwargs):
        super(StatisticsView, self).__init__(*args, **kwargs)

        self.list_filter = None
        self.weekday_filter = None
        self.list_names = {}

    @method_decorator(login_required(login_url=ADMIN_LOGIN_URL))
    @method_decorator(utils.permission_required("statistics"))
    def dispatch(self, *args, **kwargs):
        return super(StatisticsView, self).dispatch(*args, **kwargs)

    def init_stats(self, venue):
        self.visits_by_event = defaultdict(int)
        self.visits_by_date = defaultdict(int)
        self.listed_visits_by_date = defaultdict(int)
        self.listed_by_date = defaultdict(int)
        self.visits_by_time = defaultdict(int)
        # Number of visits and set of visited events per VenueVisitors (from lists) and per Users (ticket purchasers)
        self.visits_by_visitor = defaultdict(int)
        self.events_by_visitor = defaultdict(set)
        self.time_by_visitor = defaultdict(timedelta)
        self.visits_by_user = defaultdict(int)
        self.events_by_user = defaultdict(set)
        self.time_by_user = defaultdict(timedelta)
        self.tally_stats = {}

        self.expand_stats_by_time = False

        try:
            self.venue_shop = venue.shop
        except Shop.DoesNotExist:
            self.venue_shop = None
        self.ticket_revenues = []

        self.bookings = []
        self.bookings_by_promoter = dict()

    def collect_event(self, event, is_venue_stats=False):
        self.expand_stats_by_time = event.owner_org.feature_expand_stats_by_time

        # Force expand to false for venue statistics view
        if is_venue_stats:
            self.expand_stats_by_time = False

        # Check cache
        cache_version = 6
        cache_key = 'statistics-event-%d-%d-%d' % (event.id, cache_version, int(self.expand_stats_by_time))
        if self.list_filter:
            # Django cache raises CacheKeyWarning with white spaces, let's just replace them for now
            cache_key = 'statistics-event-%d-%s-%d-%d' % (event.id, md5(self.list_filter.lower().encode('utf-8')).hexdigest(),
                                                          cache_version,
                                                          int(self.expand_stats_by_time))
        stats = cache.get(cache_key)

        event_last_modified_key = 'event-%d-data-last-modified' % (event.id)
        event_last_modified = cache.get(event_last_modified_key)

        if not event_last_modified:
            cache.set(event_last_modified_key, timezone.now(), 360*24*60*60)

        elif stats and event_last_modified < stats['statistics-cached']:
            self.listed_by_date[event.start_time_local.date()] += stats['listed-by-date']
            self.listed_visits_by_date[event.start_time_local.date()] += stats['listed-visits-by-date']
            self.visits_by_date[event.start_time_local.date()] += stats['visits-by-date']
            self.visits_by_event[event] += stats['visits-by-event']

            self.ticket_revenues.extend(stats['ticket-revenues'])
            self.ticket_revenues = merge_moneys(self.ticket_revenues)

            for key, value in stats['visits-by-time'].iteritems():
                self.visits_by_time[key] += value

            for key, value in stats['visits-by-user'].iteritems():
                self.visits_by_user[key] += value
            for key, value in stats['events-by-user'].iteritems():
                self.events_by_user[key].update(value)
            for key, value in stats['time-by-user'].iteritems():
                self.time_by_user[key] += value

            for key, value in stats['visits-by-visitor'].iteritems():
                self.visits_by_visitor[key] += value
            for key, value in stats['events-by-visitor'].iteritems():
                self.events_by_visitor[key].update(value)
            for key, value in stats['time-by-visitor'].iteritems():
                self.time_by_visitor[key] += value

            if 'tally' in stats:
                self.add_tally_stats(stats['tally'])

            self.list_names.update(stats['list_names'])

            if stats['bookings']:
                self.bookings.extend(stats['bookings'])
            for promoter_name, data in stats['bookings-by-promoter'].items():
                if promoter_name not in self.bookings_by_promoter:
                    self.bookings_by_promoter[promoter_name] = defaultdict(int)
                self.bookings_by_promoter[promoter_name]['checked_in_count'] += data['checked_in_count']
                self.bookings_by_promoter[promoter_name]['spending'] += data['spending']

            statsd.incr('pro.stats_event.cache.hit')
            return
        statsd.incr('pro.stats_event.cache.miss')

        # Create event_stats dict, so we don't need to do anything later on to prevent exceptions (eg KeyError)
        self.event_stats = dict()
        self.event_stats['listed-by-date'] = 0
        self.event_stats['listed-visits-by-date'] = 0
        self.event_stats['visits-by-date'] = 0
        self.event_stats['visits-by-event'] = 0
        self.event_stats['ticket-revenues'] = []

        self.event_stats['visits-by-time'] = defaultdict(int)
        self.event_stats['visits-by-user'] = defaultdict(int)
        self.event_stats['events-by-user'] = defaultdict(set)
        self.event_stats['time-by-user'] = defaultdict(timedelta)
        self.event_stats['visits-by-visitor'] = defaultdict(int)
        self.event_stats['events-by-visitor'] = defaultdict(set)
        self.event_stats['time-by-visitor'] = defaultdict(timedelta)

        self.event_stats['promoter-tally-categories'] = dict()

        self.event_stats['bookings'] = []
        self.event_stats['bookings-by-promoter'] = dict()

        event_items = ShopItemData.objects.filter(event=event, categories__category_type=ShopCategory.TYPE_TICKET)
        event_filtered_items = event_items.filter(name__iexact=self.list_filter) if self.list_filter else event_items

        event_lists = event.lists.all()
        event_filtered_lists = event_lists.filter(name__iexact=self.list_filter) if self.list_filter else event_lists

        # Our list name filtering is case insensitive, but we want to show nicer names to users
        self.event_stats['list_names'] = dict()
        for list in event_lists:
            if list.name.lower() not in self.list_names:
                self.list_names[list.name.lower()] = list.name
                self.event_stats['list_names'][list.name.lower()] = list.name
        for item_data in event_items:
            if item_data.item.visible == True or PurchasedItem.objects.filter(item_data=item_data).count():
                self.list_names[item_data.name.lower()] = item_data.name
                self.event_stats['list_names'][item_data.name.lower()] = item_data.name
        if len(event_filtered_items):
            self.collect_event_tickets(event, event_filtered_items)
            # Merge all found purchases together, to form an array
            # of sums of different currencies.
            self.ticket_revenues = merge_moneys(self.ticket_revenues)

        if len(event_filtered_lists):
            self.collect_event_guests(event, event_filtered_lists)

        self.collect_event_bookings(event)

        if event.is_archived():
            # Cache for 360 days
            self.event_stats['statistics-cached'] = timezone.now()
            cache.set(cache_key, self.event_stats, 360*24*60*60)

    def collect_event_tickets(self, event, event_items):
        # Get all purchase items, including redeemed and expired ones
        interesting_statuses = [ PurchasedItem.STATUS_BILLED, PurchasedItem.STATUS_REDEEMED, PurchasedItem.STATUS_DECLINED, PurchasedItem.STATUS_EXPIRED ]
        tickets = list(PurchasedItem.objects.filter(item_data__in=event_items, status__in=interesting_statuses) \
                       .select_related('item_data'))
        redeemed_tickets = filter(lambda item: item.status == PurchasedItem.STATUS_REDEEMED, tickets)

        self.listed_by_date[event.start_time_local.date()] += len(tickets)
        self.event_stats['listed-by-date'] = len(tickets)
        self.visits_by_date[event.start_time_local.date()] += len(redeemed_tickets)
        self.event_stats['visits-by-date'] = len(redeemed_tickets)
        self.visits_by_event[event] += len(redeemed_tickets)
        self.event_stats['visits-by-event'] = len(redeemed_tickets)
        for ticket in tickets:
            self.event_stats['ticket-revenues'].append(ticket.item_data.price)
        self.event_stats['ticket-revenues'] = merge_moneys(self.event_stats['ticket-revenues'])
        self.ticket_revenues.extend(self.event_stats['ticket-revenues'])
        redeem_packages = RedeemPackage.objects.filter(items__in=tickets)

        for pkg in redeem_packages:
            time_by_user = timedelta()
            if pkg.cancelled_by:
                time_by_user = pkg.cancelled_timestamp - pkg.confirmed_timestamp
            else:
                self.visits_by_time[self.datetimeToTimeInterval(event.localize_dt(pkg.confirmed_timestamp))] += 1
                self.event_stats['visits-by-time'][self.datetimeToTimeInterval(event.localize_dt(pkg.confirmed_timestamp))] += 1

                self.visits_by_user[pkg.user_id] += 1
                self.event_stats['visits-by-user'][pkg.user_id] += 1

                self.events_by_user[pkg.user_id].add(event)
                self.event_stats['events-by-user'][pkg.user_id].add(event)

                if not event.owner_org.feature_top_spenders_with_checkout_only:
                    time_by_user = event.end_time - pkg.confirmed_timestamp if event.end_time > pkg.confirmed_timestamp else timedelta()
            self.time_by_user[pkg.user_id] += time_by_user
            self.event_stats['time-by-user'][pkg.user_id] += time_by_user

    def collect_event_guests(self, event, event_lists_query):
        # Number of listed guests
        event_guests_listed = VenueListMember.objects.filter(list__in=event_lists_query, is_tally=False).aggregate(total=Sum('ticket_count'))['total'] or 0
        self.listed_by_date[event.start_time_local.date()] += event_guests_listed
        self.event_stats['listed-by-date'] += event_guests_listed

        # All checkins to this event
        event_visits = VenueVisit.objects.filter(event=event, status=VenueVisit.STATUS_ENTERED).select_related('list_member').order_by('entrance_timestamp').prefetch_related('categories__group')
        event_left_visits = VenueVisit.objects.filter(event=event, status=VenueVisit.STATUS_LEFT).select_related('list_member').order_by('entrance_timestamp')

        if self.list_filter:
            event_visits = event_visits.filter(list_member__list__in=event_lists_query)
            event_left_visits = event_left_visits.filter(list_member__list__in=event_lists_query)

        self.visits_by_event[event] += len(event_visits) - len(event_left_visits)
        self.event_stats['visits-by-event'] += len(event_visits) - len(event_left_visits)

        self.event_stats['listed-visits-by-date'] = len(filter(lambda item: not item.list_member.is_tally, event_visits)) - len(filter(lambda item: not item.list_member.is_tally, event_left_visits))
        self.listed_visits_by_date[event.start_time_local.date()] = self.event_stats['listed-visits-by-date']

        self.event_stats['tally'] = self.collect_event_tally_guests(event, event_lists_query, event_visits, event_left_visits)
        self.add_tally_stats(self.event_stats['tally'])

        event_left_visits_copy = list(event_left_visits)
        # Group checkins by various parameters
        for visit in event_visits:
            self.event_stats['visits-by-date'] += 1
            if visit.entrance_timestamp:
                self.visits_by_time[self.datetimeToTimeInterval(event.localize_dt(visit.entrance_timestamp))] += 1
                self.event_stats['visits-by-time'][self.datetimeToTimeInterval(event.localize_dt(visit.entrance_timestamp))] += 1
            self.visits_by_visitor[visit.list_member.visitor_id] += 1
            self.event_stats['visits-by-visitor'][visit.list_member.visitor_id] += 1
            self.events_by_visitor[visit.list_member.visitor_id].add(event)
            self.event_stats['events-by-visitor'][visit.list_member.visitor_id].add(event)

            left_visits = filter(lambda left_visit: left_visit.list_member == visit.list_member and left_visit.entrance_timestamp > visit.entrance_timestamp, event_left_visits_copy)
            time_by_visitor = timedelta()
            if not visit.list_member.is_tally:
                if len(left_visits):
                    time_by_visitor = left_visits[0].entrance_timestamp - visit.entrance_timestamp
                    event_left_visits_copy.remove(left_visits[0])
                elif not event.owner_org.feature_top_spenders_with_checkout_only:
                    time_by_visitor = event.end_time - visit.entrance_timestamp if event.end_time > visit.entrance_timestamp else timedelta()
                self.time_by_visitor[visit.list_member.visitor_id] += time_by_visitor
                self.event_stats['time-by-visitor'][visit.list_member.visitor_id] += time_by_visitor

        used_event_visit_ids = set()
        for left_visit in event_left_visits:
            self.event_stats['visits-by-date'] -= 1
            list_member_visits = filter(lambda v: v.list_member == left_visit.list_member and v.id not in used_event_visit_ids \
                and v.entrance_timestamp < left_visit.entrance_timestamp, event_visits)
            list_member_visits = sorted(list_member_visits, key=lambda v: v.entrance_timestamp, reverse=True)
            if list_member_visits and list_member_visits[0].entrance_timestamp:
                used_event_visit_ids.add(list_member_visits[0].id)
                self.visits_by_time[self.datetimeToTimeInterval(event.localize_dt(list_member_visits[0].entrance_timestamp))] -= 1
                self.event_stats['visits-by-time'][self.datetimeToTimeInterval(event.localize_dt(list_member_visits[0].entrance_timestamp))] -= 1
            self.visits_by_visitor[left_visit.list_member.visitor_id] -= 1
            self.event_stats['visits-by-visitor'][left_visit.list_member.visitor_id] -= 1
            if event in self.events_by_visitor[left_visit.list_member.visitor_id] and not self.visits_by_visitor[left_visit.list_member.visitor_id]:
                # If the event is in events-by-visitor list and there's no visits by this visitor any more, remove it!
                self.events_by_visitor[left_visit.list_member.visitor_id].remove(event)
                self.event_stats['events-by-visitor'][left_visit.list_member.visitor_id].remove(event)

        # Use the start date of the event to group visits by date. This is easier for both us as well as venues.
        self.visits_by_date[event.start_time_local.date()] += self.event_stats['visits-by-date']

    def collect_event_bookings(self, event):
        all_bookings = VenueTableBooking.objects.filter(arrived_timestamp__isnull=False, list_member__list__event=event) \
            .select_related('booked_by', 'list_member', 'table') \
            .prefetch_related('list_member__visits')
        promoter_key = lambda booking: booking.booked_by.name if booking.booked_by else ''
        data = sorted(all_bookings, key=promoter_key)
        for promoter_name, group in groupby(data, promoter_key):

            self.event_stats['bookings-by-promoter'][promoter_name] = defaultdict(int)
            self.bookings_by_promoter[promoter_name] = defaultdict(int)

            for booking in group:
                checked_in_count = len(filter(lambda visit: visit.status==VenueVisit.STATUS_ENTERED, booking.list_member.visits.all()))
                spending = booking.spending

                self.event_stats['bookings-by-promoter'][promoter_name]['checked_in_count'] += checked_in_count
                self.bookings_by_promoter[promoter_name]['checked_in_count'] += checked_in_count
                self.event_stats['bookings-by-promoter'][promoter_name]['spending'] += spending
                self.bookings_by_promoter[promoter_name]['spending'] += spending

                # We might not have any table data with old bookings
                bookings_data = {
                    'checked_in_count': checked_in_count,
                    'spending': spending,
                    'table_nr': booking.table.number if booking.table else '',
                    'min_spending': booking.table.minimum_spend if booking.table else 0,
                    'arrival_time': booking.arrived_timestamp,
                    'booked_by': booking.booked_by.name if booking.booked_by else '',
                    }
                self.event_stats['bookings'].append(bookings_data)
                self.bookings.append(bookings_data)


    def collect_event_tally_guests(self, event, event_lists_query, event_visits, event_left_visits):
        """ Collects all the data about tally visitors.

        All the data about guest counts and payouts in different lists and categories is collected here. The returned
        structure is:
        {
            'list_name': {
                'payout': 17.50,
                'males': 21,        # Note that this can be bigger than sum of males in every category (since some
                'females': 26,      #  guests are only in male/female category).
                'categories': {     # Note that categories here do NOT contain builtin categories (male/female).
                    'category_name': {
                        'sort_key': (1, 2),     # Used for sorting the categories when showing them to the user.
                        'males': 17,
                        'females': 21,
                    },
                },
            },
            ...
        }
        """

        # Get some info about builtin categories
        default_categories = list(VisitorCategory.objects.filter(group__organization=None))
        assert len(default_categories) == 2
        default_category_ids = set([ c.id for c in default_categories ])
        male_category_id = filter(lambda c: c.name == 'Male', default_categories)[0].id

        tally_data = {}

        # Iterate through all lists of an event
        for venue_list in event_lists_query.select_related('venuepromoterlist'):
            list_name = venue_list.name

            if list_name not in tally_data:
                tally_data[list_name] = {
                    'payout': Decimal(),
                    'males': 0,
                    'females': 0,
                    'categories': {},
                    }

            # Get male/female commissions
            commission_male = 0
            commission_female = 0
            try:
                promoter_list = venue_list.venuepromoterlist
            except VenuePromoterList.DoesNotExist:
                promoter_list = None
            if promoter_list:
                commission_male = promoter_list.commission_male or 0
                commission_female = promoter_list.commission_female or 0

            # Get all visits of guests in this list
            list_visits = filter(lambda v: v.list_member.list_id == venue_list.id, list(event_visits)+list(event_left_visits))
            for visit in list_visits:
                count_change = 1
                # Visits with STATUS_LEFT don't have categories, let's change the visit to get the categories.
                if visit.status == VenueVisit.STATUS_LEFT:
                    visit = filter(lambda v: v.list_member == visit.list_member and not getattr(v, 'has_left_visit', False), event_visits)[0]
                    setattr(visit, 'has_left_visit', True)
                    count_change = -1

                visit_categories = visit.categories.all()
                # Find the base category (male/female)
                base_categories = filter(lambda c_id: c_id in [ v.id for v in visit_categories ], default_category_ids)
                if len(base_categories) == 1:
                    is_male = base_categories[0] == male_category_id
                else:
                    # If the visit has no male/female category, we're not interested in it.
                    continue

                commission = commission_male if is_male else commission_female
                if count_change < 0:
                    commission = -commission

                gender_idx = 'males' if is_male else 'females'

                # Go through all categories attached to the visit
                for cat in visit_categories:
                    if cat.id in default_category_ids:
                        # We don't record default categories separately.
                        continue

                    if cat.name not in tally_data[list_name]['categories']:
                        tally_data[list_name]['categories'][cat.name] = {
                            'sort_key': (cat.group.id, cat.id),
                            'males': 0,
                            'females': 0,
                            }
                    tally_data[list_name]['categories'][cat.name][gender_idx] += count_change

                # Update lists statistics.
                tally_data[list_name][gender_idx] += count_change
                tally_data[list_name]['payout'] += commission


            # Cleanup if there were no tally guests in this list
            if not tally_data[list_name]['males'] and not tally_data[list_name]['females']:
                del tally_data[list_name]

        return tally_data

    def add_tally_stats(self, tally_stats):
        """ Adds specified tally stats (usually of a single event) to self.tally_stats
        """
        for list_name, list_stats in tally_stats.iteritems():
            if list_name not in self.tally_stats:
                self.tally_stats[list_name] = list_stats
            else:
                # Add to existing stats
                self.tally_stats[list_name]['payout'] += list_stats['payout']
                self.tally_stats[list_name]['males'] += list_stats['males']
                self.tally_stats[list_name]['females'] += list_stats['females']

                for cat_name, cat_stats in self.tally_stats[list_name]['categories'].iteritems():
                    if cat_name not in self.tally_stats[list_name]['categories']:
                        self.tally_stats[list_name]['categories'][cat_name] = cat_stats
                    else:
                        # Add to category stats
                        self.tally_stats[list_name]['categories'][cat_name]['males'] += cat_stats['males']
                        self.tally_stats[list_name]['categories'][cat_name]['females'] += cat_stats['females']

    def calc_stats_by_date(self, days):
        # Limit and expand visits_by_date
        stats_by_date = []
        for days_ago in range(days):
            d = date.today() - timedelta(days=days_ago)
            if not self.weekday_filter or d.isoweekday() == self.weekday_filter:
                stats_by_date.append((d, self.visits_by_date.get(d, 0), self.listed_by_date.get(d, 0), self.listed_visits_by_date.get(d, 0)))
        stats_by_date.sort()
        guests_visits = sum([ data[1] for data in stats_by_date ])
        guests_listed = sum([ data[2] for data in stats_by_date ])
        guest_listed_visits = sum([ data[3] for data in stats_by_date ])
        active_days = sum([ min(1, visits) for day, visits, listed, listed_visits in stats_by_date ])

        return {
            'stats_by_date': stats_by_date,
            'guests_listed': guests_listed,
            'guests_visits': guests_visits,
            'guests_listed_visits': guest_listed_visits,
            'active_days': active_days,
            }

    def _calc_stats_by_time_expanded(self, context):
        stats_by_time = []

        date_base = self.event.start_time

        for d in range(context['active_days']):
            date_base_d = (date_base + timedelta(days=d)).date()

            for minutes in range(0, 24 * 60, 15):
                t = dtime(minutes / 60, minutes % 60)

                visits = self.visits_by_time.get('%s|%s' % (date_base_d, t), 0)
                visits_per_min = visits/15.0 / context['active_days']
                visits_pct = visits / float(context['guests_visits'])

                # We use datetime object with bogus time to be able to format the time in Django template
                dt = datetime.combine(date_base_d, t)

                # Add custom tooltip
                tooltip = "%d visits\\n%.1f%% of total" % (visits, 100*visits_pct)
                stats_by_time.append((dt, visits, visits_pct, visits_per_min, tooltip))

            stats_by_time = sorted(stats_by_time, key=lambda data: data[0])

            # Remove zero-count visit times from the beginning and end
            while stats_by_time and stats_by_time[0][1] == 0:
                del stats_by_time[0]
            while stats_by_time and stats_by_time[-1][1] == 0:
                del stats_by_time[-1]

        return stats_by_time

    def _calc_stats_by_time_grouped(self, context):
        stats_by_time = []
        for minutes in range(0, 24*60, 15):
            t = dtime(minutes/60, minutes%60)
            visits = self.visits_by_time.get(t, 0)
            visits_per_min = visits/15.0 / context['active_days']
            visits_pct = visits / float(context['guests_visits'])
            # We use datetime object with bogus time to be able to format the time in Django template
            dt = datetime.combine(date.today(), t)
            # Add custom tooltip
            tooltip = "%d visits\\n%.1f%% of total" % (visits, 100*visits_pct)
            stats_by_time.append((dt, visits, visits_pct, visits_per_min, tooltip))
        stats_by_time = sorted(stats_by_time, key=lambda data: (data[0] - timedelta(seconds=10*3600)).time())
        # Remove zero-count visit times from the beginning and end
        while stats_by_time and stats_by_time[0][1] == 0:
            del stats_by_time[0]
        while stats_by_time and stats_by_time[-1][1] == 0:
            del stats_by_time[-1]

        return stats_by_time

    def calc_stats_by_time(self, context):
        if context.get('expand_stats_by_time', False):
            stats_by_time = self._calc_stats_by_time_expanded(context)

        else:
            stats_by_time = self._calc_stats_by_time_grouped(context)

        return {
            'visits_by_time': stats_by_time,
        }

    def generate_tally_tables(self, tally_stats):
        """ Generates tables based of tally statistics.

        Basically this uses the aggregated tally statistics data to create tables to be used in HTML.
        """

        # Sort the stats by list name
        sorted_tally_stats = sorted(tally_stats.items(), key=lambda stats: stats[0].lower())

        # Generate summary table. This contains payout and male/female/total counts for every list.
        summary_table = []
        totals = {
            'payout': Decimal(),
            'males': 0,
            'females': 0,
            'visitors': 0,
            }
        for list_name, list_stats in sorted_tally_stats:
            totals['payout'] += list_stats['payout']
            totals['males'] += list_stats['males']
            totals['females'] += list_stats['females']
            totals['visitors'] += list_stats['males'] + list_stats['females']

            summary_table.append({
                'list_name': list_name or ugettext('Untitled list'),
                'payout': list_stats['payout'],
                'males': list_stats['males'],
                'females': list_stats['females'],
                'total': list_stats['males'] + list_stats['females'],
                })

        # Short totals shown under the summary table
        summary_totals = [
            {'label': _('Male'),    'value': totals['males'] },
            {'label': _('Female'),  'value': totals['females'] },
            {'label': _('Total'),   'value': totals['visitors'] },
            {'label': _('Payout'),  'value': totals['payout'], 'format': 1},
            ]

        # Create per-gender tables. There are two of those - for males and for females. Each table contains one row per
        #  every list that had any tally visits of that gender. In columns we have tally categories as well as total
        #  number of tally guests of that gender in that list..
        gender_tables = []
        for gender_id, gender_label in [ ('males', _('Males')), ('females', _('Females')) ]:
            if not totals[gender_id]:
                # No visits for this gender, skip it.
                continue

            table = []
            # Get all categories for which this gender has checkins
            categories = {}
            for list_name, list_stats in sorted_tally_stats:
                for cat_name, cat_stats in list_stats['categories'].iteritems():
                    categories[cat_name] = cat_stats['sort_key']
            # Sort categories by sort_key
            sorted_categories = [ cat_name for cat_name, cat_key in sorted(categories.items(), key=lambda item: item[1]) ]

            # Add Header Line
            header_line = []
            header_line.append(_("List Name"))
            for column in sorted_categories:
                header_line.append(column)
            header_line.append(_("Guests"))
            table.append(header_line)

            # Add Data Lines
            for list_name, list_stats in sorted_tally_stats:
                if not list_stats[gender_id]:
                    # This list didn't have any visits of this gender. Don't show it.
                    continue

                item_line = []
                item_line.append(list_name or ugettext('Untitled list'))
                for cat in sorted_categories:
                    val = 0
                    if cat in list_stats['categories']:
                        val = list_stats['categories'][cat][gender_id]
                    item_line.append(val)
                item_line.append(list_stats[gender_id])
                table.append(item_line)

            gender_tables.append((gender_label, table))

        return {
            'summary_table': summary_table,
            'summary_totals': summary_totals,
            'gender_tables': gender_tables,
            }

    def calc_context(self, request, days = 0, stats_top_rows = 10):
        context = {
            'expand_stats_by_time': self.expand_stats_by_time,
        }

        if days:
            context.update(self.calc_stats_by_date(days))
        else:
            # Getting stats for a single event only
            assert len(self.listed_by_date) <= 1 and len(self.visits_by_date) <= 1
            context['guests_visits'] = sum(self.visits_by_date.values())
            context['guests_listed'] = sum(self.listed_by_date.values())
            context['guests_listed_visits'] = sum(self.listed_visits_by_date.values())
            context['active_days'] = 1

            if self.expand_stats_by_time:
                context['active_days'] = max((self.event.end_time - self.event.start_time).days, 0) + 1

        context['guests_visited_listed_pct'] = 100*context['guests_listed_visits']/float(context['guests_listed']) if context['guests_listed'] > 0 else 0

        if context['guests_visits']:
            context.update(self.calc_stats_by_time(context))

        def aggregate_visits(users, visitors, limit=10, min_limit=1):
            """ Aggregates and sorts visitors (from lists) and users (ticker buyers) by number of visits

            users and visitors parameters are lists of (id, visits) tuples where id is id of either VenueVisitor or User
            object.
            Return list of (name, visits) tuples.
            """
            data = []
            # Add users and visitors with more than 1 visit or have spent more than 1h in the venue
            data += [ (x[1], True,  x[0]) for x in filter(lambda x: x[1] > min_limit, users) ]
            data += [ (x[1], False, x[0]) for x in filter(lambda x: x[1] > min_limit, visitors) ]
            # Sort
            data.sort(reverse=True)
            # Get names of visitors and return
            ret = []
            for visits, is_user, id in data[:limit]:
                if is_user:
                    ret.append((UserProfile.objects.get(user__id=id).full_name, visits))
                else:
                    ret.append((VenueVisitor.objects.get(id=id).full_name, visits))
            return ret

        visits_by_visitor = aggregate_visits(self.visits_by_user.iteritems(), self.visits_by_visitor.iteritems(),
                                             limit=stats_top_rows)
        visitor_events_count = aggregate_visits([ (id, len(events)) for id, events in self.events_by_user.iteritems() ],
                                                [ (id, len(events)) for id, events in self.events_by_visitor.iteritems() ], limit=stats_top_rows)
        time_by_visitor = aggregate_visits([ (id, time) for id, time in self.time_by_user.iteritems() ],
                                           [ (id, time) for id, time in self.time_by_visitor.iteritems() ],
                                           limit=stats_top_rows, min_limit=timedelta(hours=1))

        time_text_by_visitor = []
        for visitor, time in time_by_visitor:
            hours_in_days = time.days*24
            hours = time.seconds / (60*60)
            minutes = (time.seconds - hours*60*60) / 60
            time_text = '%dh %02dm' % (hours_in_days + hours, minutes)
            time_text_by_visitor.append((visitor, time, time_text))

        visits_by_event = filter(lambda x: x[1] > 0, self.visits_by_event.items())
        visits_by_event.sort(key=lambda x: x[1], reverse=True)
        visits_by_event = visits_by_event[:stats_top_rows]

        self.list_names = self.postprocess_list_names()

        # Generate tables from tally data
        tally_tables = self.generate_tally_tables(self.tally_stats)
        if tally_tables['summary_table']:
            tally_summary_table = self.TallyTable(tally_tables['summary_table'])
        else:
            tally_summary_table = None

        self.bookings = sorted(self.bookings, key=lambda b: b['arrival_time'])
        natural_sort(self.bookings, key=lambda b: b['table_nr'])

        context.update({
            'days': days,
            'time_text_by_visitor': time_text_by_visitor,
            'visits_by_visitor': visits_by_visitor,
            'visits_by_event': visits_by_event,
            'visitor_events_count': visitor_events_count,
            'venue_shop': self.venue_shop,
            'ticket_revenues': self.ticket_revenues,
            'list_names': self.list_names,
            'list_filter': self.list_filter,
            'weekday_filter': self.weekday_filter,
            'tally_summary_table': tally_summary_table,
            'tally_summary_totals': tally_tables['summary_totals'],
            'tally_gender_tables': tally_tables['gender_tables'],
            'bookings': self.bookings,
            'bookings_by_promoter': self.bookings_by_promoter,
            })

        return context

    # Helper function to quantize datetime into fixed points of 15-min interval
    def datetimeToTimeInterval(self, d):
        interval = 15

        if self.expand_stats_by_time:
            return '%s|%s' % (d.date(), dtime(d.hour, d.minute/interval*interval))

        else:
            return dtime(d.hour, d.minute/interval*interval)

    def postprocess_list_names(self):
        # Remove empty list if it's present
        self.list_names.pop('', '')
        return sorted(self.list_names.values(), key=lambda n: n.lower())

    def render_empty(self, **kwargs):
        self.list_names = self.postprocess_list_names()

        context = {
            'events': None,
            'days_choices': self.days_choices,
            'list_names': self.list_names,
            'list_filter': self.list_filter,
            }
        context.update(kwargs)
        return self.render_to_response(context)

class VenueStatisticsView(StatisticsView):
    template_name = 'venueadmin/stats_venue.html'

    def get(self, request, venueid, days=None):
        venue = get_object_or_404(Venue, id=venueid)

        days = min(180, int(days or 30))  # default to 30 days, limit to 180 days
        start_time = timezone.now() - timedelta(days=days)
        self.list_filter = request.GET.get('list_filter', None)
        try:
            self.weekday_filter = int(request.GET.get('weekday_filter'))
        except:
            pass
        _profiling_start = time.time()

        # Get all events
        _profiling_events_start = time.time()
        events = list(Event.objects.filter(owner_org=venue.owner_org, start_time__gte=start_time, start_time__lte=timezone.now()).order_by('-start_time').select_related('venue'))
        if self.weekday_filter:
            events = filter(lambda e: e.start_time_local.isoweekday() == self.weekday_filter, events)
        _profiling_events_total = time.time() - _profiling_events_start

        # Immediately return if there are no events for the selected period
        if not events:
            return self.render_empty(venue=venue, days=days, weekday_filter=self.weekday_filter, weekday_choices=WEEKDAYS_ABBR.values())

        # Init counters
        self.init_stats(venue)

        # Collect all visits data
        _profiling_get_visits_start = time.time()
        for event in events:
            self.collect_event(event, is_venue_stats=True)
        _profiling_get_visits_total = time.time() - _profiling_get_visits_start

        if not self.listed_by_date:
            return self.render_empty(venue=venue, days=days, weekday_filter=self.weekday_filter, weekday_choices=WEEKDAYS_ABBR.values())

        if venue.owner_org and venue.owner_org.feature_top_spenders_with_checkout_only:
            top_spenders_text = _('If the person is not checked out the time spent in the venue is not taken into account.')
        else:
            top_spenders_text = _("It is assumed that everyone stays in the venue until event's end.")

        # Get value of  rows  argument (how many rows to display in summary boxes, e.g. top visitors)
        stats_top_rows = 10
        try:
            stats_top_rows = int(request.GET.get('rows', stats_top_rows ))
        except ValueError:
            pass

        # Aggregate and process it
        _profiling_aggregate_start = time.time()
        context = self.calc_context(request, days, stats_top_rows = stats_top_rows)
        context.update({
            'top_spenders_text': top_spenders_text,
            'venue': venue,
            'events': events,
            'weekday_choices': WEEKDAYS_ABBR.values(),
            'days_choices': self.days_choices,
            'guests_visits_per_event': context['guests_visits']/float(len(events)),
            })
        _profiling_aggregate_total = time.time() - _profiling_aggregate_start

        _profiling_total = time.time() - _profiling_start
        if _profiling_total > 0.3:
            logging.info("venue_statistics(): %d days for %d (%s) took %.3f secs (%.3f + %.3f + %.3f)",
                         days, int(venueid), venue.name, _profiling_total, _profiling_events_total, _profiling_get_visits_total, _profiling_aggregate_total)

        return self.render_to_response(context)


class EventStatisticsView(StatisticsView):
    template_name = 'venueadmin/stats_event.html'

    def get(self, request, eventid):
        self.event = get_object_or_404(Event, id=eventid)
        venue = self.event.venue

        self.list_filter = request.GET.get('list_filter', None)

        self.init_stats(venue)

        self.collect_event(self.event)

        if self.event.owner_org.feature_top_spenders_with_checkout_only:
            top_spenders_text = _('If the person is not checked out the time spent in the venue is not taken into account.')
        else:
            top_spenders_text = _("It is assumed that everyone stays in the venue until event's end.")

        # Nightly report
        event_report_form = EventAddNoteForm()

        context = self.calc_context(request)
        context.update({
            'top_spenders_text': top_spenders_text,
            'venue': venue,
            'event': self.event,
            'days_choices': self.days_choices,
            'guests_visits_per_event': context['guests_visits'],
            'notes': self.event.notes.order_by('created_timestamp'),
            'event_add_note_form': event_report_form,
            'MEDIA_ROOT': settings.FILE_URL_PREFIX + settings.MEDIA_ROOT,
            })
        return self.render_to_response(context)

    def post(self, request, eventid):
        event = get_object_or_404(Event, id=eventid)

        if request.POST and 'emails' in request.POST:
            # Send nightly pdf report to emails
            requested_emails = re.sub(r"[\r\n;]", ",", request.POST['emails'])
            # First remove blank emails and then strip the filtered ones
            emails = [email.strip() for email in filter(lambda email: email.strip(), requested_emails.split(','))]
            if emails:
                pdf_data = EventStatisticsPDFView.generate_pdf(request, eventid)

                try:
                    send_email(emails=emails, subject=_("Report - %(event_name)s") % {'event_name': event.name},
                               template="emails/nightly_report.html", event=event,
                               attachments=[(slugify(event.name) + '.pdf', pdf_data, 'application/pdf')]
                    )
                    messages.success(request, ngettext('E-mail sent!\nReport was successfully sent.',
                                                       'E-mails sent!\nReports were successfully sent.', len(emails)))
                    statsd.incr('pro.stats_event.nightly_reports.sent')
                    statsd.incr('pro.stats_event.nightly_reports.recipients', len(emails))

                    return HttpResponseRedirect(request.get_full_path())
                except:
                    messages.error(request, _('E-mails were not sent!\nPlease check the e-mail addresses.'))
                    statsd.incr('pro.stats_event.nightly_reports.send_errors')
                    return HttpResponseRedirect(request.get_full_path())

        else:
            # Check if notes has been changed
            event_add_note_form = EventAddNoteForm(request.POST or None, event=event, user=self.request.user)
            if event_add_note_form.is_valid():
                event_add_note_form.save()

                return HttpResponseRedirect(request.get_full_path())

        return self.get(request, eventid)


class EventStatisticsPDFView(EventStatisticsView):
    template_name = 'venueadmin/stats_event_pdf.html'

    @classmethod
    def generate_pdf(cls, request, eventid):
        response = cls(request=request).get(request, eventid)
        return response.content

    def get(self, request, eventid):
        html_response = super(EventStatisticsPDFView, self).get(request, eventid)
        if not sum(self.visits_by_date.values()):
            raise Http404("No guests checked in yet - %s (%d)" % (self.event.name, self.event.id))

        html_response.render()
        pdf_data = get_pdf_data_from_html(html_response.content)

        response = HttpResponse(content_type = 'application/pdf')
        response['Content-Disposition'] = 'attachment; filename="%s.pdf"' % slugify(self.event.name)
        response.write(pdf_data)

        statsd.incr('pro.stats_event.nightly_reports.views')
        return response


@login_required(login_url=ADMIN_LOGIN_URL)
@utils.permission_required("statistics")
def people_database(request, organizationid):
    organization = get_object_or_404(Organization, id=organizationid)

    persons = Person.objects.filter(organization=organization)

    persons_exist = persons.exists()

    if request.POST and 'delete-persons' in request.POST:
        person_ids = request.POST['delete-persons'].split()
        persons.filter(id__in=person_ids).update(closed_by=request.user, closed_timestamp=timezone.now())
        statsd.incr('pro.people_database.deleted', len(person_ids))

        return HttpResponseRedirect(request.get_full_path())

    person = None
    if 'id' in request.POST and request.POST['id']:
        try:
            person = get_object_or_404(Person, id=int(request.POST['id']), organization=organization)
        except:
            return HttpResponseBadRequest()

    person_form = PersonForm(request.POST or None, instance=person, request=request, organization=organization)
    if person_form.is_valid():
        person_form.save()

        success_message = _('Person added\n%(name)s has been added to the database!')
        extra_tags = 'added'
        if person:
            success_message = _('Person updated\n%(name)s has been updated in the database!')
            extra_tags = 'updated'
        messages.success(request, success_message % {
            'name': person_form.cleaned_data['name'] or person_form.cleaned_data['email'],
            }, extra_tags=extra_tags)

        return HttpResponseRedirect(request.get_full_path())

    persons_total_count = persons.count()

    # Period filter
    period_from = None
    period_to = None

    filter_form = PersonPeriodFilterForm(request.GET or None)
    if filter_form.is_valid():
        if 'period_from' in filter_form.cleaned_data and filter_form.cleaned_data['period_from']:
            period_from = filter_form.cleaned_data['period_from']
            persons = persons.filter(created_timestamp__gte=period_from)
        if 'period_to' in filter_form.cleaned_data and filter_form.cleaned_data['period_to']:
            period_to = filter_form.cleaned_data['period_to']
            persons = persons.filter(created_timestamp__lte=period_to+timedelta(days=1))

    # Create tabs according to the first letters of names
    person_names = persons.values_list('name', flat=True)
    tab_letters = set()
    requested_tab = request.GET.get('tab', None)
    selected_tab = 'all'
    # Let's create letters for the tabs
    # This might get slow with more than 10-20 000 persons
    for name in person_names:
        if name == '':
            letter = 'other'
        else:
            if name[0].upper() in tab_letters:
                continue
            if Person.get_sort_key(name) == Person.SORTING_LETTER:
                letter = name[0].upper()
            else:
                letter = 'other'

        tab_letters.add(letter)
        if requested_tab == letter:
            selected_tab = letter

    tab_letters = sorted(tab_letters, key=lambda letter: (letter != 'other', letter))

    if selected_tab and selected_tab not in ('other', 'all'):
        # istartswith causes problems with sqlite. Should be fine elsewhere
        # https://docs.djangoproject.com/en/dev/ref/databases/#sqlite-string-matching
        persons = persons.filter(Q(name__startswith=selected_tab.lower()) | Q(name__startswith=selected_tab.upper()))
    elif selected_tab == 'other':
        persons = persons.exclude(sort_key=Person.SORTING_LETTER)

    order_by = request.GET.get('order_by', 'name')
    order_by = order_by if order_by in ('name', 'email', 'birthday', 'added') else 'name'
    order = request.GET.get('order', 'asc').lower()
    order = 'asc' if order == 'asc' else 'desc'

    if order_by == 'name':
        persons = persons.order_by('sort_key', 'name', 'email')
    elif order_by == 'email':
        persons = persons.order_by('email')
    elif order_by == 'birthday':
        # We want to show the persons without a birthday in the end (with ASC ordering)
        # We order only by month because we don't store the month without a date
        persons = persons.extra(select={'month': "COALESCE(birthday_month, 13)"})
        persons = persons.extra(order_by = ['month', 'birthday_day', '-birthday_year', 'sort_key', 'name', 'email'])
    elif order_by == 'added':
        persons = persons.order_by('created_timestamp', 'sort_key', 'name', 'email')

    if order == 'desc':
        persons = persons.reverse()

    if organization.venue:
        user_timezone = pytz.timezone(organization.venue.timezone)
    else:
        user_timezone = pytz.timezone(get_timezone_by_ip(request))

    if 'export' in request.GET and request.GET['export'].lower() == 'csv':
        # Create the HttpResponse object with the appropriate CSV header.
        response = HttpResponse(mimetype='text/csv; header=present')
        response['Content-Disposition'] = 'attachment; filename="%s.csv"' % slugify(organization.display_name())

        writer = csv.writer(response, delimiter=formats.get_format('CSV_DELIMITER'))
        headers = [ugettext('Name'), ugettext('Email'), ugettext('Birth year'), ugettext('Birth month'),
                   ugettext('Birth day'), ugettext('Phone'), ugettext('Added')]

        writer.writerow(headers)
        for person in persons:
            birthday = person.birthday()
            writer.writerow([
                person.name,
                person.email,
                birthday.year if birthday and birthday.year != 4 else None,
                birthday.month if birthday else None,
                birthday.day if birthday else None,
                person.phone,
                user_timezone.normalize(person.created_timestamp.astimezone(user_timezone)).strftime('%Y-%m-%d %H:%M:%S'),
            ])

        statsd.incr('pro.people_database.exports')
        return response

    period_date_format = formats.get_format('DATE_INPUT_FORMATS')[0]

    open_modal = False
    for msg in messages.get_messages(request):
        if 'added' in msg.tags:
            open_modal = True
            break

    generic_tags = PersonTag.objects.filter(generic=True)
    generic_tag_names = map(lambda tag: unicode(tag.name_i18n), generic_tags)
    org_tag_names = list(PersonTag.objects.filter(person__organization=organization, generic=False).distinct('name').values_list('name', flat=True))
    tag_options = set(generic_tag_names+org_tag_names)

    persons_filtered_count = persons.count()
    persons_count_text = ungettext('%(persons_count)s person in database',
                                   '%(persons_count)s persons in database',
                                   persons_total_count) % {'persons_count': persons_total_count}
    if persons_filtered_count != persons_total_count:
        persons_count_text = ungettext('Filtered %(filtered_count)s of %(persons_count)s person in database',
                                       'Filtered %(filtered_count)s of %(persons_count)s persons in database',
                                       persons_total_count,
                                       ) % {
                                 'filtered_count': persons_filtered_count,
                                 'persons_count': persons_total_count,
                                 }

    with timezone.override(user_timezone):
        return render_to_response('venueadmin/people_database.html', RequestContext(request, {
            'organization': organization,
            'persons': persons,
            'persons_exist': persons_exist,
            'selected_tab': selected_tab,
            'tab_letters': tab_letters,
            'order_by': order_by,
            'order_direction': order,
            'opposite_direction': 'desc' if order == 'asc' else 'asc',
            'filter_form': filter_form,
            'filter_append_period_from': '&period_from=%s' % period_from.strftime(period_date_format) if period_from else '',
            'filter_append_period_to': '&period_to=%s' % period_to.strftime(period_date_format) if period_to else '',
            'period_from': period_from,
            'period_to': period_to,
            'person_form': person_form,
            'tabs_max_width': len(tab_letters)*38+400,
            'open_modal': open_modal,
            'tag_options': json.dumps(sorted(tag_options)),
            'persons_count_text': persons_count_text,
            }))


@login_required(login_url=ADMIN_LOGIN_URL)
@utils.permission_required("permissions_edit")
def organization_users(request, organizationid):
    organization = get_object_or_404(Organization, id=organizationid)

    request_user = None
    staff_forms = []
    # Get users who have permissions to the organization. For now, we do not expose UI for editing venue permissions
    #  directly (although the backend has this functionality).
    users = UserPermission.objects.filter(organization=organization)

    # Sort by user names, keeping current user at the beginning of the list.
    for permission in sorted(users, key=lambda p: (p.user != request.user, p.user.profile.full_name.lower())):
        form = OrganizationUserForm(request=request, organization=organization, instance=permission, prefix='user-%d' % permission.id)
        staff_forms.append(form)

    # Assert that we found the current user (the one that's making the request) among the employees
    assert (staff_forms and staff_forms[0].instance.user == request.user) or request.user.is_staff

    return render_to_response('venueadmin/org_users.html', RequestContext(request, {
        'staff_forms': staff_forms,
        'organization': organization,
        'user_roles': UserPermission.EMPLOYEE_ROLES,
        'add_form': OrganizationAddUserForm(request=request, organization=organization),
        }))


@login_required(login_url=ADMIN_LOGIN_URL)
@utils.permission_required("permissions_edit")
def organization_promoters(request, organizationid):
    organization = get_object_or_404(Organization, id=organizationid)

    forms_without_email = []
    forms_with_email = []

    promoters = Promoter.objects.filter(organization=organization).order_by('name', 'email')

    for promoter in promoters:
        form = OrganizationPromoterForm(request=request, organization=organization, instance=promoter, prefix='promoter-%d' % promoter.id)
        if promoter.email:
            forms_with_email.append(form)
        else:
            forms_without_email.append(form)

    show_promoter_placeholder = False
    if not forms_with_email and not forms_without_email:
        show_promoter_placeholder = True

        promoter1 = Promoter(email=pgettext('Sample e-mail', 'john@events.com'), organization=organization,
                             name=pgettext('Sample name', 'John Smith'), list_limit=100)
        form1 = OrganizationPromoterForm(request=request, organization=organization, instance=promoter1)
        forms_without_email.append(form1)

        promoter2 = Promoter(email=pgettext('Sample e-mail', 'david@events.com'), organization=organization,
                             name=pgettext('Sample name', 'David Johnson'), list_limit=50)
        form2 = OrganizationPromoterForm(request=request, organization=organization, instance=promoter2)
        forms_without_email.append(form2)

    return render_to_response('venueadmin/org_promoters.html', RequestContext(request, {
        'organization': organization,
        'forms_without_email': forms_without_email,
        'forms_with_email': forms_with_email,
        'add_form': OrganizationAddPromoterForm(request=request, organization=organization),
        'show_promoter_placeholder': show_promoter_placeholder,
        }))


@login_required(login_url=ADMIN_LOGIN_URL)
def notifications(request):
    notifications = Notification.objects.for_user(request.user, show_all=True)

    # Divide the notifications into new and seen/actioned ones
    new_notifications = filter(lambda n: n.status == Notification.STATUS_NEW, notifications)
    actioned_notifications = filter(lambda n: n.status != Notification.STATUS_NEW, notifications)[:20]

    user_timezone = pytz.timezone(get_timezone_by_ip(request))
    with timezone.override(user_timezone):
        return render_to_response('venueadmin/notifications.html', RequestContext(request, {
            'new_notifications': new_notifications,
            'actioned_notifications': actioned_notifications,
            }))


def get_fuzzy_facebook_object(request):
    """ Returns all basic information about an FB object

        Args: request, <any string that is parsable to FB object ID>

        Returns: HttpResponse(str(json))
            or HttpResponseBadRequest()

        This is mostly used in event_add and venue_facebook_shop
    """
    fb_token = request.GET.get('token', None)
    if not fb_token:
        raise Http404

    timezone = 'UTC'
    venue_id = request.GET.get('venue', None)
    if venue_id:
        venue = get_object_or_404(Venue, id=venue_id)
        timezone = venue.timezone

    fb_info = None
    try:
        clean_fb_id = extract_id_token_from_str(fb_token)

        fields = request.GET.get('fields', None)
        if fields:
            fb_obj = FacebookObjectByIdFetcher(clean_fb_id, timezone=timezone, fields=fields).get()
        else:
            fb_obj = FacebookObjectByIdFetcher(clean_fb_id, timezone=timezone).get()

        if request.GET.get('load_cover_image', False):
            if fb_obj.get('cover', None) and fb_obj['cover'].get('cover_id', None):
                # Note: We use the current user's access-token if available
                # to request this resource from facebook. Since we need the full quality image
                # and the fb graph api needs an user token to request it. :)
                try:
                    all_tokens = UserSocialAuth.get_social_auth_for_user(request.user).get()
                    if all_tokens.tokens and all_tokens.tokens.get('access_token', None):
                        fb_obj['real_images'] = FacebookObjectByIdFetcher(
                            fb_obj['cover']['cover_id'],
                            user_token=all_tokens.tokens.get('access_token')
                        ).get()
                except:
                    pass

        fb_info = json.dumps(fb_obj, cls=DjangoJSONEncoder)
    except (Exception, AttributeError):
        pass

    if fb_info and fb_info != "null":
        return HttpResponse(fb_info, "application/json")
    else:
        return HttpResponseBadRequest()


@login_required(login_url=ADMIN_LOGIN_URL)
def venueadmin_settings(request):
    form = EditUserForm(user=request.user, is_pro=True, data=request.POST or None, prefix="user")
    form2 = ProEditProfileForm(data=request.POST or None, instance=request.user.profile, prefix="profile")

    if form.is_valid() and form2.is_valid():
        if form.cleaned_data.get('password', None):
            statsd.incr('account.password.changed.total')
            statsd.incr('account.password.changed.pro')

        form.save()
        form2.save()

        # Show dashboard
        return redirect('venueadmin_index')

    return render_to_response('venueadmin/my_settings.html', RequestContext(request, {
        'settings_form': form,
        'profile_form': form2,
        }))

@login_required(login_url=ADMIN_LOGIN_URL)
def distributor_overview(request):
    no_info = True
    # Get data for last 30 days
    date_from = date.today() - timedelta(days=settings.DISTRIBUTOR_HISTORY_DAYS)

    # Get all distributors regions
    regions = DistributorRegion.objects.filter(user=request.user)

    if not regions:
        return HttpResponseRedirect(reverse("venueadmin_index"))

    venues = []
    organizations = []
    for region in regions:
        region_venues = Venue.objects.filter(addr_country_code=region.country)
        region_organizations = Organization.objects.filter(addr_country_code=region.country)
        region_venue_organizations = Organization.objects.filter(addr_country_code='', venue__addr_country_code=region.country)
        if region.city:
            region_venues = region_venues.filter(addr_city__iexact=region.city)
            region_organizations = region_organizations.filter(addr_city__iexact=region.city)
            region_venue_organizations = region_venue_organizations.filter(venue__addr_city__iexact=region.city)
        venues.extend(region_venues)
        organizations.extend(region_organizations)
        organizations.extend(region_venue_organizations)

    venues = sorted(venues, key=lambda x: x.created_timestamp, reverse=True)
    organization_list = sorted(organizations, key=lambda x: (not x.has_checkins(), x.display_name()))
    organizations = sorted(organizations, key=lambda x: x.created_timestamp, reverse=True)
    active_organizations = filter(lambda org: org.has_checkins(), organization_list)
    inactive_organizations = filter(lambda org: not org.has_checkins(), organization_list)

    events = Event.objects.filter(venue__in=venues, active=True, created_timestamp__gte=date_from).order_by('-created_timestamp')

    purchased_items = PurchasedItem.objects.filter(event__in=events)

    weekly_stats = []
    start_date = date.today() - timedelta(days=date.today().weekday())
    while start_date > date_from:
        end_date = start_date + timedelta(days=6)
        events_count = events.filter(start_time__gte=start_date, start_time__lte=end_date).count()
        item_count = purchased_items.filter(purchase__purchase_timestamp__gte=start_date, purchase__purchase_timestamp__lte=end_date).count()

        weekly_stats.append({
            'start_date': start_date,
            'end_date': end_date,
            'events': events_count,
            'purchased_items': item_count,
            })
        start_date -= timedelta(days=7)

    if len(venues) + len(organizations):
        no_info = False

    user_timezone = pytz.timezone(get_timezone_by_ip(request))
    with timezone.override(user_timezone):
        return render_to_response('venueadmin/distributor_all.html', RequestContext(request, {
            'organization': None,
            'active_organizations': active_organizations,
            'inactive_organizations': inactive_organizations,
            'no_info': no_info,
            'weeks': weekly_stats,
            'venues': venues[:10],
            'organizations': organizations[:10],
            }))

@login_required(login_url=ADMIN_LOGIN_URL)
def distributor_view_org(request, organizationid):
    organization = get_object_or_404(Organization, id=organizationid)
    no_info = True
    # Get data for last 30 days
    date_from = date.today() - timedelta(days=settings.DISTRIBUTOR_HISTORY_DAYS)

    # Get all distributors regions
    regions = DistributorRegion.objects.filter(user=request.user)

    if not regions:
        return HttpResponseRedirect(reverse("venueadmin_index"))

    organizations = []
    for region in regions:
        region_organizations = Organization.objects.filter(addr_country_code=region.country)
        region_venue_organizations = Organization.objects.filter(addr_country_code='', venue__addr_country_code=region.country)
        if region.city:
            region_organizations = region_organizations.filter(addr_city__iexact=region.city)
            region_venue_organizations = region_venue_organizations.filter(venue__addr_city__iexact=region.city)
        organizations.extend(region_organizations)
        organizations.extend(region_venue_organizations)

    if organization not in organizations:
        return HttpResponseRedirect(reverse("distributor_overview"))

    organization_list = sorted(organizations, key=lambda x: (not x.has_checkins(), x.display_name()))
    active_organizations = filter(lambda org: org.has_checkins(), organization_list)
    inactive_organizations = filter(lambda org: not org.has_checkins(), organization_list)

    events = Event.objects.filter(owner_org__id=organization.id, active=True, created_timestamp__gte=date_from).order_by('-created_timestamp')

    listed_dict = defaultdict(int)
    visited_dict = defaultdict(int)
    for event in events:
        # Number of listed guests
        lists_query = VenueList.objects.filter(event=event)
        guests_listed = VenueListMember.objects.filter(list__in=lists_query).aggregate(total=Sum('ticket_count'))['total'] or 0
        listed_dict[event.start_time.date()] += guests_listed

        # All checkins to this event
        guests_visits = float(len(list(VenueVisit.objects.filter(event=event, status=VenueVisit.STATUS_ENTERED))))
        visited_dict[event.start_time.date()] += guests_visits

    event_items = ShopItemData.objects.filter(event__in=events, categories__category_type=ShopCategory.TYPE_TICKET)
    # Get all purchase items, including redeemed and expired ones
    interesting_statuses = [ PurchasedItem.STATUS_BILLED, PurchasedItem.STATUS_REDEEMED, PurchasedItem.STATUS_DECLINED, PurchasedItem.STATUS_EXPIRED ]
    tickets = PurchasedItem.objects.filter(item_data__in=event_items, status__in=interesting_statuses).select_related('item_data', 'event')

    tickets = sorted(tickets)
    for key, group in groupby(tickets, lambda x: x.event.start_time.date()):
        group = list(group)
        redeemed_tickets = filter(lambda item: item.status == PurchasedItem.STATUS_REDEEMED, group)
        listed_dict[key] += len(group)
        visited_dict[key] += len(redeemed_tickets)

    # Sort tickets and listed guests by date
    stats_by_date = []
    for days_ago in range(settings.DISTRIBUTOR_HISTORY_DAYS):
        d = date.today() - timedelta(days=days_ago)
        row = (d, visited_dict.get(d, 0), listed_dict.get(d, 0))
        stats_by_date.append(row)
    stats_by_date.sort()
    # Totals
    guests_visits = float(sum([ stats[1] for stats in stats_by_date ]))
    guests_listed = sum([ stats[2] for stats in stats_by_date ])

    guests_visits_per_event = 0
    guests_visited_listed_pct = 0
    if len(events):
        guests_visits_per_event = guests_visits/len(events)
    if guests_listed:
        guests_visited_listed_pct = guests_visits/guests_listed*100

    if len(events):
        no_info = False

    user_timezone = pytz.timezone(get_timezone_by_ip(request))
    with timezone.override(user_timezone):
        return render_to_response('venueadmin/distributor_org.html', RequestContext(request, {
            'organization': organization,
            'active_organizations': active_organizations,
            'inactive_organizations': inactive_organizations,
            'no_org_info': no_info,
            'stats_by_date': stats_by_date,
            'events': events,
            'guests_visits_per_event': guests_visits_per_event,
            'guests_visits': guests_visits,
            'guests_listed': guests_listed,
            'guests_visited_listed_pct': guests_visited_listed_pct,

            }))

@login_required(login_url=ADMIN_LOGIN_URL)
@utils.permission_required("tally_config")
def tally_configuration(request, organizationid):
    organization = get_object_or_404(Organization, id=organizationid)

    cat_groups = VisitorCategoryGroup.objects.filter(
        Q(organization=organization) | Q(organization=None)
    ).order_by('id')
    for group in cat_groups:
        sorted_categories = group.categories.filter(closed_by=None).order_by('position')
        setattr(group, 'sorted_categories', sorted_categories)

    return render_to_response('venueadmin/tally_config.html', RequestContext(request, {
        'organization': organization,
        'cat_groups': cat_groups,
        'category_form': VisitorCategoryForm(),
        'group_form': VisitorCategoryGroupForm(),
        }))


@login_required(login_url=ADMIN_LOGIN_URL)
@utils.permission_required("venue_edit")
def org_promo_tools(request, organizationid):
    organization = get_object_or_404(Organization, id=organizationid)
    app_id = settings.FACEBOOK_SHOP_APP_ID

    # This is the final url, when clicked prompts the page owner to install our app
    # The next URI *this* page(FB will append a GET argument)
    app_install_url = "https://www.facebook.com/dialog/pagetab?app_id=%s&next=%s" % (app_id, request.build_absolute_uri())

    # This handles the redirect back to Facebook after install
    if request.GET:
        page_id = None
        # Regex for parsing the page id out of the GET arguments
        rex = re.compile('.*?\[(\d+)\]$')
        for key in request.GET.keys():
            page_id = rex.match(key).groups(1)[0]
            if page_id:
                redirect_url = "http://facebook.com/%s" % page_id
                return HttpResponseRedirect(redirect_url)

    return render_to_response('venueadmin/org_promo.html', RequestContext(request, {
        'organization': organization,
    }))


def pay_withdrawal(request, withdrawalid):
    withdrawal = get_object_or_404(Withdrawal, uuid=withdrawalid)
    withdrawal_form = None

    if not withdrawal.paid_by:
        withdrawal_form = WithdrawalPayForm(initial={
            'paid_by': request.user.get_full_name() if request.user.is_authenticated() else '',
            })

        if request.POST:
            withdrawal_form = WithdrawalPayForm(request.POST, instance=withdrawal)
            if withdrawal_form.is_valid():
                withdrawal = withdrawal_form.save()
                send_mandrill_email(
                    request=request,
                    emails=[u.email for u in withdrawal.organization.get_org_admins()],
                    subject="Your withdrawal request from events has been processed",
                    template='emails/pro/withdraw_processed.html',
                    tags=['withdraw_processed'],
                    )
                return redirect("pay_withdrawal", withdrawalid=withdrawalid)

    return render_to_response("venueadmin/pay_withdrawal.html", RequestContext(request, {
        'withdrawal': withdrawal,
        'withdrawal_form': withdrawal_form,
        }))


def org_set_active(request, organizationid):
    # Note that this doesn't have @login_required because then we want to first set the cookie, then forward to login.
    organization = get_object_or_404(Organization, id=organizationid)
    response = redirect('venueadmin_index')
    response.set_cookie('active_organization_id', organization.id, max_age=5*365*86400)
    return response


def window_close_callback(request):
    return render_to_response('venueadmin/close_dialog.html', RequestContext(request, {}))


def get_stripe_plan(plan_id):
    cache_key = 'stripe-plan-1-%s' % plan_id
    plan = cache.get(cache_key)
    if plan is None:
        plan = stripe.Plan.retrieve(plan_id)
        cache.set(cache_key, plan, 3600)
    return plan
