import logging
from django.conf import settings
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response
from django.utils import timezone
from django.utils.translation import ugettext as _
from django.utils.http import urlquote

from events.search import Searcher
from utils.utils import send_mandrill_email, get_site_url, get_pdf_data_from_html, slugify
from venueadmin.forms import VenueListLineParser


def reverse_to_pro(name, kwargs={}):
    """
    This function ensures that links retrieved with reverse are always usable for main domain. Since get_site_url
    always returns events.com regardless of the domain user is currently on. E.g: Adds
    /app/ to start of the link if its not there yet.
    """
    url = reverse(name, kwargs=kwargs)
    if not url.startswith("/app/"):
        url = "/app/%s" % url
    return url

def send_org_greeting_email(user, organization, request):
    message_template = "emails/mandrill/base.html"
    message_subject = _("%s, welcome to events Pro!") % user.first_name
    message_title = _('Welcome to events Pro!')
    message_desc = _("Thanks for signing up for events Pro, we really believe events "
                     "Pro will help you become even more efficient and successful. It's "
                     "really easy to get yourself up and running.")
    site_url = get_site_url(request)
    media_url = site_url + settings.MEDIA_URL
    landing_url = request.build_absolute_uri(reverse('pro_landing'))

    tags = ['org_greet']

    tips = [
        {
            'img_url': media_url + 'images/emails/email-tip-login.png',
            'title': _('Getting started'),
            'message':
                _('You can log in with the e-mail %(email)s and with the password you set. If you happen to forget '
                  'it, <a href="%(site_url)s%(password_reset_url)s" target="_blank">click here</a>.') % ({
                    'email': user.email,
                    'password_reset_url': reverse_to_pro('venueadmin_password_reset'),
                    'site_url': site_url,
                }),
            'action': {
                'label': _('Log in'),
                'url': '%s%s' % (site_url, reverse_to_pro('venueadmin_login')),
            },
        },
        {
            'img_url': media_url + 'images/emails/email-tip-event.png',
            'title': _('Create your first event'),
            'message': _("It's easy and takes only 10 seconds. You are able to copy events from Facebook by "
                         "pasting in the event link and we will fill out the rest or just create a new one "
                         "manually, it's up to you!"),
            'action': {
                'label': _('Create event'),
                'url': '%s%s' % (site_url, reverse_to_pro('create_event_wrapper')),
            },
        },
        {
            'img_url': media_url + 'images/emails/email-tip-fill_details.png',
            'title': _('Fill in company details'),
            'message': _('If you would like to start selling tickets through our app (which we thoroughly '
                         'recommend by the way) you must fill out your bank details on the company details '
                         'section. This is because we process the payments and then are able to transfer your earnings.'),
            'action': {
                'label': _('Fill in Details'),
                'url': '%s%s' % (site_url, reverse_to_pro('org_legal_info', kwargs={'organizationid': organization.id})),
            },
        },
        {
            'img_url': media_url + 'images/emails/email-tip-add_users.png',
            'title': _('Invite colleagues'),
            'message': _('Easily add colleagues who are also working on the event by clicking on users and using '
                         'their email address to send them an invite. You are able to choose and change their '
                         'access level at any time.'),
            'action': {
                'label': _('Invite colleagues'),
                'url': '%s%s' % (site_url, reverse_to_pro('organization_users', kwargs={'organizationid': organization.id})),
            },
        },
    ]
    send_mandrill_email(user=user, subject=message_subject, title=message_title, template=message_template, tips=tips,
                        message=message_desc, landing_url=landing_url, request=request, tags=tags)

def send_event_creation_email(event, request):
    emails = map(lambda x: x.email,
                 filter(lambda user: user.email and user.profile.send_email_with_tag('pro_event_create'),
                        event.owner_org.get_org_admins()))
    if not emails:
        return

    message_template = "emails/mandrill/base.html"
    site_url = get_site_url(request)
    media_url = site_url + settings.MEDIA_URL

    landing_url = request.build_absolute_uri(reverse('pro_landing'))
    tags = ['event_create']

    event_link = request.build_absolute_uri(event.get_absolute_url())

    message_title = _("Awesome news - your event is alive!")
    message_subject = _("Your event %s has been published!") % event.name
    message_desc = _("Check out your event <a href='%(event_link)s' target='_blank'>here</a>. "
                     "Promoting %(event_name)s is really easy using events.") % {
        'event_name': event.name,
        'event_link': event_link,
    }

    fb_share_url = 'https://www.facebook.com/sharer/sharer.php?u=%s' % urlquote(event_link)
    twitter_share_url = 'https://twitter.com/share?url=%(event_url)s&amp;text=%(share_text)s&amp;' \
                        'via=events&amp;related=events' % {
        'site_url': urlquote(site_url),
        'event_url': urlquote(event_link),
        'share_text': urlquote(('Get on the List %s - ') % (event.name)),
    }

    tips = [
        {
            'img_url': media_url + 'images/emails/email-tip-tickets.png',
            'title': _("Add tickets"),
            'message':
                _("Create different types of ticket, choose the quantity "
                  "available and the period in which you wish to sell them."),
            'action': {
                'label': _('Create tickets'),
                'url': '%s%s' % (site_url, reverse_to_pro('event_edit_tickets', kwargs={'eventid': event.id})),
            },
        },
        {
            'img_url': media_url + 'images/emails/email-tip-promote.png',
            'title': _("Promote your event"),
            'message':
                _('<a href="%(fb_link)s" target="_blank">Share your event via Facebook</a> or '
                  '<a href="%(twitter_link)s" target="_blank">spread the word on Twitter</a>, '
                  'straight from the events Pro in just two clicks by hitting "Promote event".') % {
                    'fb_link': fb_share_url,
                    'twitter_link': twitter_share_url,
                },
            'action': {
                'label': _('Promote event'),
                'url': '%s%s' % (site_url, reverse_to_pro('event_share', kwargs={'eventid': event.id})),
            },
        },
        {
            'img_url': media_url + 'images/emails/email-tip-add_users.png',
            'title': _("Add guests"),
            'message':
                _('Simply create guest lists and add your guests, then add promoters who will '
                  'add their own guests and just watch the attendees grow and grow. Just click on "Add guests".'),
            'action': {
                'label': _('Add guests'),
                'url': '%s%s' % (site_url, reverse_to_pro('event_edit_lists', kwargs={'eventid': event.id})),
            },
        },
    ]

    OPT_OUT = reverse_to_pro('venueadmin_settings')
    send_mandrill_email(emails=emails, subject=message_subject, title=message_title, template=message_template,
                        tips=tips, message=message_desc, landing_url=landing_url, request=request, tags=tags, OPT_OUT=OPT_OUT)

def get_nearby_events(user, org):
    s = Searcher()
    s.query_types = 'event'
    s.max_results = 3

    if org and org.owned_venue:
        s.coords = (float(org.owned_venue.location_lat), float(org.owned_venue.location_lon))

    s.search()
    nearby_events = list(s.results_events)
    return nearby_events

def send_we_miss_you_email(user, organization):
    if not user.email or not user.profile.send_email_with_tag('pro_we_miss_you'):
        return

    tags = ['we_miss_you']
    message_template = "emails/mandrill/base.html"
    site_url = get_site_url(None, force_https=True)

    media_url = site_url + settings.MEDIA_URL

    message_subject = _("%s, we miss you!") % user.first_name
    message_desc = _("We noticed that you haven't created any events since you signed "
                     "up and we just wanted to check everything is ok? ")

    nearby_events = get_nearby_events(user, organization)

    landing_url = "%s%s" % (site_url, reverse('pro_landing'))

    tips = [
        {
            'img_url': media_url + 'images/emails/email-tip-event.png',
            'title': _("Add your upcoming events"),
            'message':
                _("It's really easy and quick. Just copy events from Facebook by pasting in the "
                  "event link and we will fill out the rest or just create a new one manually, it's up to "
                  "you! We have loads of users creating new events everyday and we wouldn't want you to be missing out!"),
            'action': {
                'label': _('Create event'),
                'url': '%s%s' % (site_url, reverse_to_pro('create_event_wrapper')),
            },
        },
    ]

    OPT_OUT = reverse_to_pro('venueadmin_settings')
    send_mandrill_email(user=user, subject=message_subject, template=message_template, tips=tips, message=message_desc,
                        nearby_events=nearby_events, landing_url=landing_url, tags=tags, OPT_OUT=OPT_OUT)


def send_free_ticket_email(event, list, list_member):
    email = VenueListLineParser().parse_email_from_comment(list_member.comment)
    if not email:
        logging.error("send_free_ticket_email(): %s does not have email in comment (%s)??",
                      list_member, list_member.comment)
        return

    message_template = "emails/user/free_tickets.html"
    message_title = _("events complimentary tickets")

    with timezone.override(event.venue.timezone):
        html_response = render_to_response('user/free_tickets_pdf.html', {
            'MEDIA_ROOT': settings.FILE_URL_PREFIX + settings.MEDIA_ROOT,
            'event': event,
            'list': list,
            'list_member': list_member,
            'free_tickets': list_member.free_tickets.all(),
        })
        pdf_html = html_response.content

    pdf_data = get_pdf_data_from_html(pdf_html)

    attachments = [{
        'type': 'application/pdf',
        'name': _('events tickets - %(event_name)s.pdf' % {'event_name': slugify(event.name)}),
        'content': pdf_data.encode('base64'),
    }]

    with timezone.override(event.venue.timezone):
        send_mandrill_email(email=email, subject=message_title, template=message_template,
                            attachments=attachments, tags=['free_ticket'],
                            event=event, list_member=list_member)
