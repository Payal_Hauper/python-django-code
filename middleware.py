from venue.models import Event, UserPermission


class SelectedOrganizationMiddleware(object):
    @staticmethod
    def process_request(request):
        # Default to no organizations
        request.all_organizations = []
        request.user_selected_org = None
        request.is_pro_host = request.path.startswith('/app/') or request.path.startswith('/pro/') or request.subhost_name == 'pro'

        if request.user.is_anonymous():
            return None
        if not request.is_pro_host:
            return None

        SelectedOrganizationMiddleware.get_organizations(request, request.user)

        return None

    @staticmethod
    def get_organizations(request, user):
        # Get all user's organizations
        request.all_organizations = UserPermission.get_user_organizations(user)
        if not request.all_organizations:
            # Will show personal view and no org selection menu
            return None

        # Sort organizations by display_name
        request.all_organizations = sorted(request.all_organizations, key=lambda o: o.display_name())

        # Add organization last activity timestamp to the organization if the user is staff
        if user.is_staff:
            events = Event.objects.filter(owner_org__in=request.all_organizations)\
                .order_by('owner_org', '-created_timestamp')\
                .distinct('owner_org')

            event_dict = {e.owner_org_id: e for e in events}

            for org in request.all_organizations:
                org_event = event_dict.get(org.id)
                org.last_activity = org.created_timestamp if org_event is None else org_event.created_timestamp

        # Get selected org. The logic is as follows: we get the value of the 'active_organization_id' cookie.
        # 1) if it's int and id of an organization for which the user has some permissions, then this is the active org.
        # 2) if it's int and 0, then show personal view.
        # 3) else (not int or not id of user's org) we default to the first org that the user has.
        # This has the benefit of defaulting to an organization as most activity happens there.
        selected_org = request.COOKIES.get('active_organization_id', None)
        try:
            selected_org_id = int(selected_org)
            if selected_org_id == 0:
                # Case 2: personal view
                request.user_selected_org = None
            else:
                # Case 1 or 3: check if user has org with this id
                request.user_selected_org = filter(lambda o: o.id == selected_org_id, request.all_organizations)[0]
        except (TypeError, ValueError, IndexError):
            # Case 3: default to user's first org
            request.user_selected_org = request.all_organizations[0]

        # Check if the user has been given any personal permissions, currently only possible for lists
        request.user_has_personal_permissions = request.user_selected_org == None or \
            UserPermission.objects.exclude(list=None).filter(user=user, permission_list_edit=True)

        return None
