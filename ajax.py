from time import time as unix_time

from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.core.serializers.json import DjangoJSONEncoder
from django.db.models.query_utils import Q
from django.http import HttpResponseBadRequest, Http404
from django.shortcuts import get_object_or_404
from django.template import RequestContext
from django.template.loader import render_to_string
from django.utils.formats import date_format
from django.utils.translation import ugettext as _, ngettext
from utils.utils import slugify, send_mandrill_email, get_site_url

from location.utils import get_location_info_by_ip
from venue.models import VisitorCategory, VisitorCategoryGroup, VenueAreaTable, VenueListMember, VenueTableBooking, VenueArea, VenueAreaObject, VenueList
from venueadmin.email import send_free_ticket_email
from venueadmin.forms import *
from venueadmin.views import get_list_members, EventStatisticsPDFView
from venueadmin.tutorial import TUTORIAL_STEPS
from utils import utils
from utils.avatars import avatar_image_path
from utils.facebook_utils import FacebookConnectionFetcher, FacebookObjectByIdFetcher, extract_id_token_from_str
from utils.statsd import statsd, statsd_timing_and_incr

from dajaxice.decorators import dajaxice_register
import json
from smtplib import SMTPRecipientsRefused


@dajaxice_register
@utils.permission_required("permissions_edit")
def save_organization_user(request, organizationid, form_data):
    organization = get_object_or_404(Organization, id=organizationid)

    # Try and get the permission object from DB
    prefix = form_data['prefix']
    permission = get_object_or_404(UserPermission, organization=organization, id=form_data[prefix + '-id'])
    form = OrganizationUserForm(form_data, request=request, organization=organization, instance=permission, prefix=prefix)

    if form.is_valid():
        # Do custom validation
        # Make sure at least 1 admin remains
        if not UserPermission.objects.filter(organization=organization, permission_permissions_edit=True).exclude(pk=form.instance.pk).exists():
            # Fake unbound form to get initial values for all the fields and send them back
            form.is_bound = False
            fields = [ (field.auto_id, field.value()) for field in form ]
            return json.dumps({
                'success': False,
                'error': _('There must be at least one admin'),
                'fields': fields,
            })

        # Looks good, save and return
        form.save()
        return json.dumps({
            'success': True,
            'deleted': form.cleaned_data['delete'],
            'role': force_unicode(form.instance.get_role_name()),
        })

    else:
        # Fake unbound form to get initial values for all the fields and send them back
        form.is_bound = False
        fields = [ (field.auto_id, field.value()) for field in form ]
        return json.dumps({
            'success': False,
            'form_errors': form.errors,
            'fields': fields,
        })


@dajaxice_register
@utils.permission_required("permissions_edit")
def add_organization_user(request, organizationid, invitee_email, role):
    organization = get_object_or_404(Organization, id=organizationid)

    form_data = {
        'email': invitee_email,
        'role': role,
    }
    # Create form
    form = OrganizationUserForm(form_data, request=request, organization=organization)
    if not form.is_valid():
        return json.dumps({
            'success': False,
            'form_errors': form.errors,  # errors as json dict
        })

    try:
        # We only want to create the objects if everything succeeds, so use manual transaction mode here
        with transaction.commit_on_success():
            permission = form.save()

    except SMTPRecipientsRefused:
        return json.dumps({
            'success': False,
            'form_errors': { 'user_email': [_("Couldn't send invitation, make sure the email address is valid")] },
        })

    except Exception as e:
        logging.exception("Unexpected exception while trying to grant permissions to '%s'", invitee_email)
        return json.dumps({
            'success': False,
        })

    else:
        # Create new form for the added permission and return it
        form = OrganizationUserForm(request=request, organization=organization, instance=permission, prefix='user-%d' % permission.id)

        return json.dumps({
            'success': True,
            'form_html': render_to_string('venueadmin/org_users_row.html',
                                          context_instance=RequestContext(request, {'form': form})),
        })


@dajaxice_register
@utils.permission_required("permissions_edit")
def save_organization_promoter(request, organizationid, form_data):
    organization = get_object_or_404(Organization, id=organizationid)

    # Try and get the promoter from DB
    prefix = form_data['prefix']
    promoter = get_object_or_404(Promoter, organization=organization, id=form_data[prefix + '-id'])
    form = OrganizationPromoterForm(form_data, request=request, organization=organization, instance=promoter, prefix=prefix)

    if form.is_valid():

        form.save()
        return json.dumps({
            'success': True,
            'deleted': form.cleaned_data['delete'],
        })

    else:
        # Fake unbound form to get initial values for all the fields and send them back
        form.is_bound = False
        fields = [ (field.auto_id, unicode(field.value()) if field.value() else '') for field in form ]
        return json.dumps({
            'success': False,
            'form_errors': form.errors,
            'fields': fields
        })


def add_organization_promoter_generic(request, organization, name, email, list_limit, commission_male, commission_female):
    form_data = {
        'name': name,
        'email': email,
        'list_limit': list_limit if email else 0,
        'commission_male': commission_male,
        'commission_female': commission_female,
    }
    # Create form
    form = OrganizationPromoterForm(form_data, request=request, organization=organization)
    if not form.is_valid():
        return None, {
            'success': False,
            'form_errors': form.errors,
        }

    try:
        # We only want to create the objects if everything succeeds
        with transaction.commit_on_success():
            promoter = form.save()

    except Exception as e:
        logging.exception("Unexpected exception while trying to add promoter with email '%s' to organization %d (%s)",
                email, organization.id, organization.display_name())
        return None, {
            'success': False,
        }
    else:
        return promoter, {}


@dajaxice_register
@utils.permission_required("permissions_edit")
def add_organization_promoter(request, organizationid, name, email, list_limit, commission_male, commission_female):
    organization = get_object_or_404(Organization, id=organizationid)

    promoter, ret = add_organization_promoter_generic(request, organization, name, email, list_limit, commission_male, commission_female)

    if not promoter:
        return json.dumps(ret)
    else:
        # Create new form for the added permission and return it
        form = OrganizationPromoterForm(request=request, organization=organization, instance=promoter, prefix='promoter-%d' % promoter.id)

        return json.dumps({
            'success': True,
            'form_html': render_to_string('venueadmin/org_promoters_row.html',
                                          context_instance=RequestContext(request, {'form': form})),
            'has_email': bool(email),
        })


@dajaxice_register
@utils.permission_required("permissions_edit")
def add_organization_promoter_from_event(request, organizationid, event_id, form_prefix, name, email, list_limit, commission_male, commission_female):
    organization = get_object_or_404(Organization, id=organizationid)
    event = get_object_or_404(Event, id=event_id, owner_org=organization)

    promoter, ret = add_organization_promoter_generic(request, organization, name, email, list_limit, commission_male, commission_female)

    if not promoter:
        return json.dumps(ret)

    # We want to also add the new promoter to this event.
    # Create form data for EventPromoterForm
    form_data = {
        'id': '',
        'active': True,
        'promoter': promoter.id,
        'list_limit': promoter.list_limit,
        'commission_male': promoter.commission_male,
        'commission_female': promoter.commission_female,
    }
    # Add prefix
    form_data = { (form_prefix + '-' + k): v for k,v in form_data.iteritems() }

    # Create form
    promoters = Promoter.objects.filter(organization=organization)
    # The initial argument is to make the form understand that the list_limit field should be removed (hack).
    form = EventPromoterForm(data=form_data, request=request, event=event, promoters=promoters, prefix=form_prefix,
                             initial={'promoter': promoter})
    # Validate (this should always succeed) and save
    success = form.is_valid()
    if success:
        form.save()
    else:
        logging.error("add_organization_promoter_from_event(): create EventPromoterForm is invalid. "
                      "Errors: %s; form_data: %s; promoter: %d; event: %d",
                      form.errors, form_data, promoter.id, event_id)

    # This is needed to display the correct name
    form.promoter_obj = promoter
    return json.dumps({
        'success': True,
        'form_html': render_to_string('venueadmin/event_promoters_form.html',
                                      context_instance=RequestContext(request, {'form': form})),
    })

@dajaxice_register
@utils.permission_required("list_edit")
def toggle_event_promoters(request, eventid, organizationid, forms_data, state):
    event = get_object_or_404(Event, id=eventid)
    if event.owner_org_id != organizationid:
        return HttpResponseBadRequest()

    # Get all promoters for this org.
    promoters = Promoter.objects.filter(organization=event.owner_org).order_by('name')
    res = {
        'success': True,
        'result': {},
    }

    # foreach promoter
    for form_data in forms_data:
        prefix = form_data['prefix']
        form_data[prefix + '-active'] = state
        res["result"][prefix] = save_event_promoter_generic(request, event, promoters, form_data)

    return json.dumps(res)

@dajaxice_register
@utils.permission_required("list_edit")
def save_event_promoter(request, eventid, organizationid, form_data):
    event = get_object_or_404(Event, id=eventid)
    if event.owner_org_id != organizationid or "prefix" not in form_data:
        return HttpResponseBadRequest()

    promoters = Promoter.objects.filter(organization=event.owner_org).order_by('name')

    return json.dumps(save_event_promoter_generic(request, event, promoters, form_data))

def save_event_promoter_generic(request, event, promoters, form_data):
    prefix = form_data['prefix']
    instance = None
    instance_id = form_data[prefix + '-venuelist_ptr']
    if instance_id:
        try:
            instance = VenuePromoterList.objects.get(owner=event.owner_org, id=instance_id)
        except VenuePromoterList.DoesNotExist:
            # Already deleted, return success
            statsd.incr('pro.lists.promoters.already_deleted')
            return {
                'success': True,
                'status': False,
            }
    form = EventPromoterForm(data=form_data, event=event, request=request, promoters=promoters, instance=instance, prefix=prefix)
    success = form.is_valid()
    if success:
        instance = form.save()

    return {
        'success': success,
        'errors': form.errors,
        'list_id': success and instance and instance.id,
        'list_limit': form.list_limit_label(),
        'status': instance is not None,
    }

@dajaxice_register
def tutorial_set_step(request, step):
    # We get one-based step, convert it to zero-indexed
    try:
        step = int(step) - 1
    except:
        return json.dumps({
            'success': False,
        })

    if step < -1 or step >= len(TUTORIAL_STEPS):
        step = -1
        statsd.incr('pro.tutorial.completed_tutorials')

    request.user.profile.venueadmin_tutorial_progress = step
    request.user.profile.save()
    statsd.incr('pro.tutorial.completed_steps')

    return json.dumps({
        'success': True,
        'step': step + 1,
        'steps_total': len(TUTORIAL_STEPS),
        'message': None if step < 0 else force_unicode(TUTORIAL_STEPS[step]),
    })


@dajaxice_register
@utils.permission_required("tally_config")
def tally_update_category_order(request, organizationid, group_id, category_ids):
    organization = get_object_or_404(Organization, id=organizationid)
    group = get_object_or_404(VisitorCategoryGroup, id=group_id, organization=organization)

    for pos, id in enumerate(category_ids):
        VisitorCategory.objects.filter(id=id, group=group).update(position=pos)

    return json.dumps({
        'success': True,
        })


@dajaxice_register
@utils.permission_required("tally_config")
def change_tally_mode(request, organizationid, checkin_tally):
    organization = get_object_or_404(Organization, id=organizationid)

    organization.listed_guests_tally_mode = bool(checkin_tally)
    organization.save()

    return json.dumps({
        'success': True,
        })


@dajaxice_register
@utils.permission_required("tally_config")
def tally_category_save(request, organizationid, group_id, name):
    organization = get_object_or_404(Organization, id=organizationid)
    group = get_object_or_404(VisitorCategoryGroup, id=group_id, organization=organization)

    category_form = VisitorCategoryForm(data={'name': name}, group=group, user=request.user)
    if category_form.is_valid():
        category = category_form.save()

        return json.dumps({
            'success': True,
            'category_html': render_to_string('venueadmin/tally_config_category.html', {'category': category, 'group': category.group}),
            })

    return json.dumps({
        'success': False,
        'errors': category_form.errors,
        })


@dajaxice_register
@utils.permission_required("tally_config")
def tally_category_delete(request, organizationid, category_id):
    organization = get_object_or_404(Organization, id=organizationid)

    try:
        category_id = int(category_id)
    except ValueError:
        category_id = None
    updated = VisitorCategory.objects.filter(id=category_id, group__organization=organization, closed_by=None)\
                                     .update(closed_by=request.user, closed_timestamp=timezone.now())
    if updated:
        return json.dumps({
            'success': True,
        })

    return json.dumps({
        'success': False,
        'message': _('Category could not be deleted'),
    })


@dajaxice_register
@utils.permission_required("tally_config")
def tally_group_save(request, organizationid, name):
    organization = get_object_or_404(Organization, id=organizationid)

    group_form = VisitorCategoryGroupForm(data={'name': name}, organization=organization, user=request.user)

    if group_form.is_valid():
        group = group_form.save()
        return json.dumps({
            'success': True,
            'group_html': render_to_string('venueadmin/tally_config_group.html', {'group': group}),
            })

    return json.dumps({
        'success': False,
        'errors': group_form.errors,
        })


@dajaxice_register
@utils.permission_required("tally_config")
def tally_group_delete(request, organizationid, group_id):
    organization = get_object_or_404(Organization, id=organizationid)

    try:
        group_id = int(group_id)
        group = VisitorCategoryGroup.objects.filter(id=group_id, organization=organization, closed_by=None)
        if group.update(closed_by=request.user, closed_timestamp=timezone.now()):
            VisitorCategory.objects.filter(group__id=group_id, closed_by=None).update(closed_by=request.user, closed_timestamp=timezone.now())
            return json.dumps({
                        'success': True,
                    })
    except ValueError, VisitorCategoryGroup.DoesNotExist:
        pass

    return json.dumps({
                       'success': False,
                       'message': _('Group could not be deleted'),
                       })

@dajaxice_register
@utils.permission_required("floorplans_edit")
def save_tables(request, venueid, data):

    try:
        venue = Venue.objects.filter(id=venueid).select_related('owner_org')[0]
    except IndexError:
        return HttpResponseBadRequest()

    try:
        data = json.loads(data)
        if not isinstance(data, dict) or 'floorplans' not in data or 'timestamps' not in data:
            return HttpResponseBadRequest()
    except:
        return HttpResponseBadRequest()

    def delete_objects(objects_to_delete):
        tables_to_delete = filter(lambda obj: hasattr(obj, 'venueareatable'), objects_to_delete)
        tables_to_delete_ids = [obj.venueareatable.id for obj in tables_to_delete]

        # VenueTableBookings and VenueListMembers for future events will be deleted as well
        table_bookings = VenueTableBooking.objects.filter(table__id__in=tables_to_delete_ids, list_member__list__event__start_time__gt=timezone.now())
        for booking in table_bookings:
            VenueListMember.objects.filter(pk=booking.list_member.pk).delete()

        objects_to_delete.update(closed_by=request.user, closed_timestamp=timezone.now())

    seen_area_ids = []
    seen_area_data = []
    created_areas = []
    for area_data in data['floorplans']:
        area_id = area_data['id']
        if area_id:
            area = get_object_or_404(VenueArea, id=area_id, venue=venue)
        else:
            area = VenueArea(venue=venue, created_by=request.user, last_modified_by=request.user)
            created_areas.append(area)


        if area_id:
            last_modified_timestamp = data['timestamps'][area_id]
            if area.last_modified_timestamp.isoformat() != last_modified_timestamp:
                return json.dumps({
                    'success': False,
                    'error_msg': _("Table data has been modified by %(name)s. Your changes will be lost until you refresh the page.") % {
                        'name': area.last_modified_by.profile.full_name,
                    }
                })

        area.name = area_data['name']
        area.save(request.user)
        seen_area_ids.append(area.id)
        seen_area_data.append(area.serialize(None))

        seen_obj_ids = []
        for obj_data in area_data['objects']:
            obj_id = obj_data['id']
            obj_type = obj_data['type']
            obj_is_table = obj_type.split('-')[0] == 'table'

            obj_class = VenueAreaTable if obj_is_table else VenueAreaObject
            if obj_id:
                obj = get_object_or_404(obj_class, id=obj_id, area=area)
            else:
                obj = obj_class(area=area, created_by=request.user)

            obj.type = obj_data['type']
            obj.data = obj_data['data']
            if obj_is_table:
                obj.capacity = obj_data['capacity']
                obj.number = obj_data['number']
                obj.minimum_spend = obj_data['minimum_spend'] or None
                if venue.owner_org and venue.owner_org.feature_table_description and 'description' in obj_data:
                    obj.description = obj_data['description']

            obj.save()
            seen_obj_ids.append(obj.id)

        # Delete objects in this area that we didn't see (those were deleted on client side)
        objects_to_delete = VenueAreaObject.objects.filter(area=area).exclude(id__in=seen_obj_ids)
        delete_objects(objects_to_delete)

    # Delete areas that we didn't see (those were also deleted on client side)
    areas_to_delete = VenueArea.objects.filter(venue=venue).exclude(id__in=seen_area_ids)
    for area in areas_to_delete:
        objects_to_delete = VenueAreaObject.objects.filter(area=area)
        delete_objects(objects_to_delete)

    if areas_to_delete.exists() or created_areas:
        # Update non-obsolete events in this venue
        logging.info("ajax.save_tables(): Updating associated floorplans of events of %s (%d): add %d, remove %d",
                     venue.name, venue.id, len(created_areas), areas_to_delete.count())
        for event in venue.events.filter(end_time__gte=timezone.now()):
            logging.info("ajax.save_tables():   Event %d (%s)...", event.id, event.name)
            event.table_booking_areas.remove(*list(areas_to_delete))
            event.table_booking_areas.add(*created_areas)

    areas_to_delete.update(closed_by=request.user, closed_timestamp=timezone.now(), last_modified_timestamp=timezone.now(), last_modified_by=request.user)

    return json.dumps({
        'success': True,
        'data': seen_area_data,
    })


@dajaxice_register
@utils.permission_required("floorplans_edit")
def menu_item_save(request, venueid, item_id, item_name, item_price):
    venue = get_object_or_404(Venue, id=venueid)

    org = venue.owner_org
    if not org or not org.has_shop():
        return HttpResponseBadRequest()

    form = VenueMenuItemForm(data={'name': item_name, 'price': item_price})
    if not form.is_valid():
        return json.dumps({
            'success': False,
            'form_errors': form.errors,
        })

    # Add currency to the price
    item_price = Money(form.cleaned_data['price'], org.currency)

    if item_id:
        # We're modifying an existing item.
        try:
            item_id = int(item_id)
        except ValueError:
            return HttpResponseBadRequest()

        shop_item = get_object_or_404(ShopItem, id=item_id, shop=org.shop)
        shop_item.data.name = form.cleaned_data['name']
        shop_item.data.price = item_price
        shop_item.save()

    else:
        # We're creating a new item
        shop_item = ShopItem(shop=org.shop)
        shop_item.init_data(
            name=form.cleaned_data['name'],
            created_by=request.user,
            event=None,
            description='',
            price=item_price,
            sellable_from=None,
            sellable_until=None
        )
        shop_item.save()

        # Attach category, creating it if necessary.
        category, created = ShopCategory.objects.get_or_create(shop=org.shop, name='',
                                                               category_type=ShopCategory.TYPE_MENU_ITEM,
                                                               defaults={'created_by': request.user})
        shop_item.data.categories.add(category)

        # Add to existing events
        for event in venue.events.filter(table_booking_enabled=True, table_booking_until__gt=timezone.now()):
            event.table_booking_menu_items.add(shop_item)

    return json.dumps({
        'success': True,
        'item_html': render_to_string('venueadmin/venue_menu_item.html', {'item': shop_item, 'currency': org.currency}),
    })


@dajaxice_register
@utils.permission_required("floorplans_edit")
def menu_item_delete(request, venueid, item_id):
    venue = get_object_or_404(Venue, id=venueid)

    org = venue.owner_org
    if not org or not org.has_shop():
        return HttpResponseBadRequest()

    try:
        item_id = int(item_id)
    except ValueError:
        return HttpResponseBadRequest()

    shop_item = get_object_or_404(ShopItem, id=item_id, shop=org.shop)
    shop_item.visible = False
    shop_item.save()

    # Remove from existing events
    for event in venue.events.filter(table_booking_enabled=True, table_booking_until__gt=timezone.now()):
        event.table_booking_menu_items.remove(shop_item)

    return json.dumps({
        'success': True,
    })


@dajaxice_register
def get_fb_events(request, orgid):
    org = get_object_or_404(Organization, id=orgid)

    if not org.venue and not org.facebook_page_id:
        return HttpResponseBadRequest()

    if (not org.venue or not org.venue.facebook_page_id) and not org.facebook_page_id:
        return json.dumps({
            'success': False,
        })

    fb_id = org.venue.facebook_page_id if (org.venue and org.venue.facebook_page_id) else org.facebook_page_id
    tz = org.venue.timezone if org.venue else None

    if not tz:
        tz = 'UTC'
        if org.events.exists():
            tz = org.events.all()[0].venue.timezone

    facebook_events = FacebookConnectionFetcher(fb_id, 'events',  since=str(int(unix_time()))).get() or []
    facebook_events_ids = []

    for event in facebook_events:
        facebook_events_ids.append(int(event['id']))

    fb_events = []
    if facebook_events:
        added_ids = Event.objects.filter(facebook_id__in=facebook_events_ids).values_list('facebook_id', flat=True)
        facebook_events = filter(lambda e: e['id'] not in added_ids, facebook_events)
        facebook_events = sorted(facebook_events, key=lambda e: e['start_time'])

        for event in facebook_events:
            fb_events.append({
                              'id': event['id'],
                              'name': event['name'],
                              'start_time': event['start_time'].strftime(formats.get_format('DATE_INPUT_FORMATS')[0]),
                              })

    return json.dumps({
        'success': True,
        'fb_events': fb_events,
        })

@dajaxice_register
@utils.permission_required("event_add")
def hide_venue_creation_notification(request, organizationid):
    org = get_object_or_404(Organization, id=organizationid)

    org.show_venue_creation_notification = False
    org.save()

    return json.dumps({
        'success': True,
        })

@dajaxice_register
@utils.permission_required("event_edit")
def save_event_table_selling_form(request, eventid, table_booking_enabled, table_booking_until_0, table_booking_until_1, table_booking_help_text, table_booking_areas):
    event = get_object_or_404(Event, id=eventid)

    form = EventTableSellingForm(data={
        'table_booking_enabled': table_booking_enabled,
        'table_booking_until_0': table_booking_until_0,
        'table_booking_until_1': table_booking_until_1,
        'table_booking_help_text': table_booking_help_text,
        'table_booking_areas': table_booking_areas,
    }, instance=event)

    with timezone.override(event.venue.timezone):
        success = form.is_valid()
        if success:
            form.save()

        return json.dumps({
            'success': success,
            'errors': form.errors,
            })


@dajaxice_register
@utils.permission_required("list_edit")
def add_event_list(request, eventid, name):
    event = get_object_or_404(Event, id=eventid)

    return add_list_generic(event.owner_org, event, name, request.user)


@dajaxice_register
@utils.permission_required("list_edit")
def add_event_free_ticket_list(request, eventid, name):
    event = get_object_or_404(Event, id=eventid)

    return add_list_generic(event.owner_org, event, name, request.user, list_type=VenueList.TYPE_FREE_TICKET)


@dajaxice_register
@utils.permission_required("list_edit")
def save_event_list(request, eventid, listid, name):
    list_types = [VenueList.TYPE_NORMAL, VenueList.TYPE_FREE_TICKET]
    list = get_object_or_404(VenueList, id=listid, event__id=eventid, list_type__in=list_types)

    return save_list_generic(list, name)


@dajaxice_register
@utils.permission_required("list_edit")
def delete_event_list(request, eventid, listid):
    list_types = [VenueList.TYPE_NORMAL, VenueList.TYPE_FREE_TICKET]
    list = get_object_or_404(VenueList, id=listid, event__id=eventid, list_type__in=list_types)

    return delete_list_generic(list)


@dajaxice_register
@utils.permission_required("list_edit")
def add_permanent_list(request, organizationid, name):
    organization = get_object_or_404(Organization, id=organizationid)

    # Check if name is not used in events
    try:
        assert VenueList.objects.filter(list_type=VenueList.TYPE_NORMAL, owner__id=organizationid, name__iexact=name, event__end_time__gt=timezone.now()).count() == 0
    except:
        return json.dumps({
            'success': False,
            'error_msg': _("List with the name '%(name)s' already exists.") % ({
                    'name': name,
                }),
            })

    return add_list_generic(organization, None, name, request.user, is_org_lists_page=True)


@dajaxice_register
@utils.permission_required("list_edit")
def save_permanent_list(request, organizationid, listid, name):
    list = get_object_or_404(VenueList, id=listid, owner__id=organizationid, list_type=VenueList.TYPE_NORMAL)

    # Check if name is not used in events
    try:
        assert VenueList.objects.filter(list_type=VenueList.TYPE_NORMAL, owner__id=organizationid, name__iexact=name).count() == 0
    except:
        return json.dumps({
            'success': False,
            'error_msg': _("List with the name '%(name)s' already exists.") % ({
                    'name': name,
                }),
            })

    return save_list_generic(list, name)


@dajaxice_register
@utils.permission_required("list_edit")
def delete_permanent_list(request, organizationid, listid):
    list = get_object_or_404(VenueList, id=listid, owner__id=organizationid, list_type=VenueList.TYPE_NORMAL)

    return  delete_list_generic(list)


def add_list_generic(organization, event, name, created_by, is_org_lists_page=False, list_type=VenueList.TYPE_NORMAL):
    # Name check
    if len(name) > 50:
        return json.dumps({
            'success': False,
            'error_msg': _("List name has a 50 character limit."),
        })

    try:
        if event:
            list_types = [VenueList.TYPE_NORMAL, VenueList.TYPE_FREE_TICKET]
            assert VenueList.objects.filter(Q(event=event) | Q(event=None), list_type__in=list_types, owner=organization, name__iexact=name).count() == 0
        else:
            assert VenueList.objects.filter(list_type=VenueList.TYPE_NORMAL, owner=organization, name__iexact=name, event=None).count() == 0
    except:
        return json.dumps({
            'success': False,
            'error_msg': _("List with the name '%(name)s' already exists.") % ({
                'name': name,
            }),
        })

    # Add list
    with transaction.commit_manually():
        list = VenueList.objects.create(list_type=list_type, owner=organization, event=event, created_by=created_by, name=name)
        transaction.commit()

    if event:
        logging.info("Creating list ('%s') for event ('%s') in org %s", name, event, organization)
    else:
        logging.info("Creating permalist ('%s') in org %s", name, organization)

        for event in Event.objects.filter(owner_org=organization, end_time__gt=timezone.now()):

            # Check if this permalist isn't added to this event
            if VenueList.objects.filter(event=event, owner=organization, sync_from=list).count() > 0:
                continue

            # Make a copy of the permanent list and attach it to the event
            logging.info("    %s: copying new permanent list '%s' (%d) to event %s (%d)",
                         organization.display_name(), list.name, list.pk, event.name, event.pk)
            list.copy({'created_by': created_by, 'event': event, 'sync_from': list})

    template_name = 'venueadmin/event_lists_normal.html'
    if list.list_type == VenueList.TYPE_FREE_TICKET:
        template_name = 'venueadmin/event_lists_free_ticket.html'

    return json.dumps({
        'success': True,
        'item_html': render_to_string(template_name, {'list': list, 'guests': [], 'event': event, 'is_org_lists_page': is_org_lists_page}),
        })


def save_list_generic(list, name):

    # Name check
    if len(name) > 50:
        return json.dumps({
            'success': False,
            'error_msg': _("List name has a 50 character limit."),
        })
    try:
        if list.event:
            list_types = [VenueList.TYPE_NORMAL, VenueList.TYPE_FREE_TICKET]
            assert VenueList.objects.filter(Q(event=list.event) | Q(event=None), list_type__in=list_types, owner=list.owner, name__iexact=name).count() == 0
        else:
            assert VenueList.objects.filter(list_type=VenueList.TYPE_NORMAL, owner=list.owner, name__iexact=name, event=None).count() == 0
    except:
        return json.dumps({
            'success': False,
            'error_msg': _("List with the name '%(name)s' already exists.") % ({
                'name': name,
            }),
        })

    if list.event:
        logging.info("Setting list %d ('%s') for event ('%s') in org %s name to ('%s')", list.id, list.name, list.event, list.owner, name)
    else:
        logging.info("Setting permalist %d ('%s') in org %s name to ('%s')", list.id, list.name, list.owner, name)

    if not list.event:
        logging.info("Syncing permalist %d ('%s') in org %s name to copies", list.id, list.name, list.owner)
        VenueList.objects.filter(~Q(event=None), list_type=VenueList.TYPE_NORMAL, owner=list.owner, sync_from=list).update(name=name)

    list.name = name
    list.save()
    return json.dumps({
        'success': True,
        })

def delete_list_generic(list):
    if list.event:
        logging.info("Deleting list %d ('%s') for event ('%s') in org %s", list.id, list.name, list.event, list.owner)
    else:
        logging.info("Deleting permalist %d ('%s') in org %s", list.id, list.name, list.owner)

    list.delete()
    return json.dumps({
        'success': True,
        })

# TODO: Verify that this permission check is correct.
@dajaxice_register
@utils.permission_required("event_edit")
def toggle_event_permalist(request, eventid, listid, permalistid, toggle):
    event = get_object_or_404(Event, id=eventid)

    list = None
    permalist = None
    new_list = None
    list_members = []

    if listid == permalistid:
        # Case 1: Permalist was turned off clientside, and client wanted to turn it on.
        try:
            permalist = VenueList.objects.get(id=permalistid)
        except VenueList.DoesNotExist:
            # Exception 1.1: The permalist has been deleted from org lists.
            return json.dumps({
                'success': False,
                'deleted': True,
                'error_msg': _("Permalist %(permalistid)s has been deleted from your organization.") % ({
                    'permalistid': permalistid,
                }),
            })
    else:
        # Case 2: Permalist was turned on clientside, and client wants to turn it off.
        try:
            list = VenueList.objects.get(id=listid, list_type=VenueList.TYPE_NORMAL)
        except VenueList.DoesNotExist:
            # Exception 2.1: This list was already turned off somehow, and this listid is currently invalid.
            pass

        if permalistid:
            try:
                permalist = VenueList.objects.get(id=permalistid)
            except VenueList.DoesNotExist:
                # Exception 2.2: The permalist has been deleted from org lists.
                return json.dumps({
                    'success': False,
                    'deleted': True,
                    'error_msg': _("Permalist %(permalistid)s has been deleted from your organization.") % ({
                        'permalistid': permalistid,
                    }),
                })

    if toggle and permalist:
        # provided list is a permalist, lets turn the permalist on for this event by copying it to a new list item.

        # Check if a copy exists already.
        try:
            list = VenueList.objects.filter(event=event, owner=permalist.owner, sync_from=permalist)
            assert len(list) == 0
        except:
            list = list[0]
            list_members = get_list_members(list, event)
            list.members_count = len(list_members)
            return json.dumps({
                'success': False,
                'error_msg': _("This list was already turned on."),
                'real_state': {
                    'enabled': True,
                    'item_html': render_to_string('venueadmin/event_lists_normal.html', {'list': list, 'guests': list_members, 'event': event}),
                }
            })

        logging.info("Enabling permalist %d ('%s') for event %d ('%s')", permalist.id, permalist.name, event.pk, event.name)
        list = permalist.copy({'created_by': request.user, 'event': event, 'sync_from': permalist})
        new_list = list

    elif not toggle:
        if not list:
            list_members = get_list_members(permalist, event)
            permalist.members_count = len(list_members)
            return json.dumps({
                'success': False,
                'error_msg': _("This list was already turned off."),
                'real_state': {
                    'enabled': False,
                    'item_html': render_to_string('venueadmin/event_lists_normal.html', {'list': permalist, 'guests': list_members, 'event': event}),
                }
            })

        logging.info("Disabling permalist %d ('%s') for event %d ('%s')", permalist.id, permalist.name, event.pk, event.name)
        new_list = permalist
        list.delete()

    if not new_list:
        return json.dumps({
           'success': False,
        })

    if new_list.sync_from:
        list_members = get_list_members(new_list, event)
    else:
        list_members = get_list_members(new_list, None)

    new_list.members_count = sum([m['quantity'] for m in list_members])

    return json.dumps({
        'success': True,
        'item_html': render_to_string('venueadmin/event_lists_normal.html', {'list': new_list, 'guests': list_members, 'event': event}),
    })

@dajaxice_register
@utils.permission_required("event_edit")
def save_event_ticket(request, eventid, organizationid, form_data):
    event = get_object_or_404(Event, id=eventid)
    if event.owner_org_id != organizationid or "prefix" not in form_data:
        return HttpResponseBadRequest()

    with timezone.override(event.venue.timezone):
        form = EventTicketForm(data=form_data, event=event, user=request.user, prefix=form_data['prefix'])

        success = form.is_valid()
        if success:
            shop_item = form.save()

        return json.dumps({
            'success': success,
            'ticket_id': shop_item.id if success and shop_item else None,
            'errors': form.errors,
            })


@dajaxice_register
def save_list_member(request, eventid, data, is_mobile=False):
    event = get_object_or_404(Event, id=eventid)

    instance = None
    if 'uuid' in data and data['uuid']:
        try:
            instance = VenueListMember.objects.get(uuid=data['uuid'], list_id=int(data['list']))
        except:
            return HttpResponseBadRequest()

    form = VenueListMemberForm(data=data, instance=instance, event=event, request=request)
    return add_guest_to_list_generic(request, form, is_mobile=is_mobile, event=event)

@dajaxice_register
def save_permalist_member(request, data, is_mobile=False):
    instance = None
    if 'uuid' in data and data['uuid']:
        try:
            instance = VenueListMember.objects.get(uuid=data['uuid'], list_id=int(data['list']))
        except:
            return HttpResponseBadRequest()

    form = VenueListMemberForm(data=data, instance=instance, request=request)
    return add_guest_to_list_generic(request, form, is_mobile=is_mobile)


def add_guest_to_list_generic(request, form, is_mobile, event=None):
    guest_data = None

    success = form.is_valid()
    if success:
        list_member = form.save()
        organization = list_member.list.owner

        added_timestamp = ''
        if event:
            added_timestamp = date_format(event.localize_dt(list_member.created_timestamp), 'SHORT_DATETIME_FORMAT')
        elif organization.venue:
            added_timestamp = date_format(organization.venue.localize_dt(list_member.created_timestamp), 'SHORT_DATETIME_FORMAT')

        template = 'venueadmin/event_lists_row.html'
        if is_mobile:
            template = 'venueadmin/event_lists_row_mobile.html'

        guest_data = render_to_string(template, context_instance=RequestContext(request, {
            'guest': {
                'id': list_member.uuid,
                'name': list_member.full_name,
                'quantity': list_member.ticket_count,
                'comment': list_member.comment,
                'added_timestamp': added_timestamp,
                },
            'is_org_lists_page': event is None,
            'list': list_member.list,
            'is_email_sent': list_member.is_email_sent,
        }))

    return json.dumps({
        'success': success,
        'guest_html': guest_data,
        'errors': form.errors,
        })


@dajaxice_register
def delete_guest_from_list(request, eventid, guest_id):
    event = get_object_or_404(Event, id=eventid)

    try:
        list_member = VenueListMember.objects.get(uuid=guest_id, list__event=event)
    except:
        return json.dumps({
            'success': False,
            })
    else:
        if not event.has_user_permission(request.user, 'list_edit'):
            # Does the user have explicit permissions to some of the lists?
            if not (UserPermission.objects.filter(list_id=list_member.list, user=request.user, permission_list_edit=True).exists() and \
                            list_member.created_by == request.user):
                raise PermissionDenied("%s.list_edit" % request.user)

        list_member.delete()
        statsd.incr('pro.lists.regular.removed')
        return json.dumps({
            'success': True,
            })

@dajaxice_register
def delete_guest_from_permalist(request, guest_id):
    try:
        guest = VenueListMember.objects.select_related('list__owner').get(uuid=guest_id, list__event=None)
    except:
        return json.dumps({
            'success': False,
        })
    else:
        # Check permissions
        if not guest.list.owner.has_user_permission(request.user, 'list_edit'):
            # We need to check if the guest was added by the user anyway, so skip the permission check
            if guest.created_by != request.user:
                raise PermissionDenied("%s can't delete someone else's guests" % request.user)

        member_data = VenueList.MemberData(
            guest.full_name,
            guest.ticket_count,
            guest.comment,
            guest.id)
        for list in VenueListForm.venue_active_event_lists(guest.list.owner).filter(sync_from=guest.list):
            members = list.current_members_dict()
            if member_data.key in members:
                list.apply_diff([],
                    [member_data.key],
                    [],
                    members,
                    [],
                    request.user)
        guest.delete()
        return json.dumps({
            'success': True,
        })


@dajaxice_register
def mass_editor_save(request, listid, data, include_rich_html=True):

    list = get_object_or_404(VenueList, id=listid)

    permissions = None
    # For permalists the object is an Organization, otherwise Event
    object = list.event if list.event else list.owner
    if not object.has_user_permission(request.user, 'list_edit'):
        # Does the user have explicit permissions to some of the lists?
        permissions = UserPermission.objects.filter(list=list, user=request.user, permission_list_edit=True)
        if not permissions:
            raise PermissionDenied("%s.list_edit" % request.user)

    form = VenueListForm(data=data, request=request, organization=list.owner, instance=list)

    if form.is_valid() and form.save() and not form.save_conflicts:

        list_members = get_list_members(list, list.event)
        rich_html = ""
        if include_rich_html:
            rich_html = "".join(
                [
                    render_to_string('venueadmin/event_lists_row.html',
                        context_instance=RequestContext(request,
                            {
                                'guest': guest,
                                'is_org_lists_page': list.event is None,
                                'list': list,
                                'is_manager': bool(permissions),
                            })
                    ) for guest in list_members
                ])

        return_data = {
            'success': True,
            'members_count': sum([m['quantity'] for m in list_members]),
            'rich_html': rich_html
        }

        if permissions:
            return_data['limit_count'] = sum([m['quantity'] for m in filter(lambda m: m['added_by'] == request.user.id, list_members)])
        return json.dumps(return_data)

    return json.dumps({
        'success': False,
        'errors': form.errors,
    })

@dajaxice_register
def save_manager(request, eventid, listid, data):
    # manager list type checks
    qs = VenueList.objects.exclude(list_type=VenueList.TYPE_FREE_TICKET)
    list = get_object_or_404(qs, id=listid, event_id=eventid)

    if list.event:
        if not list.event.has_user_permission(request.user, 'list_edit'):
            raise PermissionDenied("%s.%s" % (request.user, 'list_edit'))
    elif not list.owner.has_user_permission(request.user, 'list_edit'):
        raise PermissionDenied("%s.%s" % (request.user, 'list_edit'))

    instance = None
    if 'id' in data and data['id']:
        instance = get_object_or_404(UserPermission, id=data.pop('id'), list=list)

    form = ListUserForm(data=data, instance=instance, list=list, request=request)
    manager_html = None

    success = form.is_valid()
    if success:
        try:
            manager = form.save()
        except SMTPRecipientsRefused:
            return json.dumps({
                'success': False,
                'errors': { 'email': [ugettext("Couldn't send invitation to the manager, make sure the email address is valid")] },
                })

        if not manager.closed_by:
            manager_html = render_to_string('venueadmin/event_lists_manager.html', context_instance=RequestContext(request, {
                'manager': manager,
                }))

    return json.dumps({
        'success': success,
        'manager_html': manager_html,
        'errors': form.errors,
        })


@dajaxice_register
@utils.permission_required("list_edit")
def save_rsvp_list(request, eventid, data):
    event = get_object_or_404(Event, id=eventid)

    form = VenueFreeListForm(data, event=event, user=request.user, prefix='rsvp')

    with timezone.override(event.venue.timezone):
        success = form.is_valid()
        status = bool(form.instance.pk)

        if success:
            rsvp_list = form.save()
            status = rsvp_list is not None

        return json.dumps({
            'success': success,
            'status': status,
            'errors': form.errors,
            })


@dajaxice_register
@utils.permission_required("list_edit")
def save_facebook_list(request, eventid, data):
    event = get_object_or_404(Event, id=eventid)

    tags_html = ''
    fb_id_changed = False

    if not event.facebook_id:
        wrong_fb_id = json.dumps({
            'success': False,
            'status': False,
            'errors': {'facebook_id': ugettext("Please insert your event's Facebook page ID")},
            })

        if 'facebook_id' not in data or not data['facebook_id']:
            return wrong_fb_id

        fb_info = None
        try:
            clean_fb_id = extract_id_token_from_str(data['facebook_id'])
            fb_obj = FacebookObjectByIdFetcher(clean_fb_id).get()
            fb_info = json.dumps(fb_obj, cls=DjangoJSONEncoder)
        except (Exception, AttributeError):
            pass

        if not fb_info or fb_info == "null" or 'start_time' not in fb_info:
            return wrong_fb_id
        else:
            event.facebook_id = json.loads(fb_info)['id']
            fb_id_changed = True
            # We save it after we have validated the form

    form = VenueFacebookListForm(data, event=event, user=request.user, prefix='facebook')

    with timezone.override(event.venue.timezone):
        success = form.is_valid()
        status = bool(form.instance.pk)

        if success:

            if fb_id_changed:
                event.save()

            fb_list = form.save()
            status = fb_list is not None
            tags_html = render_to_string('venueadmin/event_lists_facebook_tags.html', context_instance=RequestContext(request, {
                'tags_formset': VenueFacebookListForm(event=event, user=request.user, prefix='facebook').tags_formset,
                }))

    return_data = {'success': success,
                   'status': status,
                   'errors': form.errors,
                   'tag_errors': form.tags_formset.errors,
                   'tags_html': tags_html,
                   }

    if 'facebook_id' in data and data['facebook_id'] and success:
        list_members = get_list_members(fb_list, event)
        return_data['members_count'] = sum([m['quantity'] for m in list_members])
        return_data['members_html'] = render_to_string('venueadmin/event_lists_facebook_members.html', context_instance=RequestContext(request, {
            'guests': list_members,
            }))

    return json.dumps(return_data)


@dajaxice_register
@utils.permission_required("event_checkin_guests")
def send_nightly_report(request, eventid, data):
    event = get_object_or_404(Event, id=eventid)

    emails = []
    # TODO: shouldn't we check for event users? Anyway, this is ATM same as the users for whom we show checkboxes in
    #  nightly report sending modal in event info view.
    for permission in UserPermission.objects.filter(organization=event.owner_org).select_related('user'):
        if data.get('user_%d' % permission.user.id) == 'on':
            emails.append(permission.user.email)

    try:
        pdf_data = EventStatisticsPDFView.generate_pdf(request, eventid)
    except Http404:
        return json.dumps({
            'success': False,
            'message': {
                'title': _('No check-ins for the event!'),
                'text': _('Please check someone in before sending out nightly reports.'),
                'class': 'error',
            },
        })

    try:
        # Email sending is copied from EventStatisticsView
        send_email(emails=emails, subject=_("Report - %(event_name)s") % {'event_name': event.name},
                   template="emails/nightly_report.html", event=event,
                   attachments=[(slugify(event.name) + '.pdf', pdf_data, 'application/pdf')]
        )
        statsd.incr('pro.stats_event.nightly_reports.sent')
        statsd.incr('pro.stats_event.nightly_reports.recipients', len(emails))

        return json.dumps({
            'success': True,
            'message': {
                'title': ngettext('E-mail sent!', 'E-mails sent!', len(emails)),
                'text': ngettext('Report was successfully sent.', 'Reports were successfully sent.', len(emails)),
                'class': 'success',
            },
        })

    except:
        statsd.incr('pro.stats_event.nightly_reports.send_errors')

        return json.dumps({
            'success': False,
            'message': {
                'title': _('E-mails were not sent!'),
                'text': _('Please check the e-mail addresses.'),
                'class': 'error',
            },
        })


@dajaxice_register
def save_list_description(request, eventid, listid, desc):
    event = get_object_or_404(Event, id=eventid)
    list = get_object_or_404(VenueList, id=listid)

    permissions = None
    if not event.has_user_permission(request.user, 'list_edit'):
        # Does the user have explicit permissions to some of the lists?
        try:
            permissions = UserPermission.objects.filter(list_id=listid, user=request.user, permission_list_edit=True)
        except:
            return HttpResponseBadRequest()
        else:
            if not permissions:
                raise PermissionDenied("%s.list_edit" % request.user)

    list.description = desc
    list.save()

    return json.dumps({
        'success': True,
    })


@dajaxice_register
@utils.permission_required("list_edit")
def email_list_guests(request, eventid, listid):
    event = get_object_or_404(Event, id=eventid)
    list = get_object_or_404(VenueList, id=listid, event=event)

    members = list.members.filter(is_email_sent=False)
    for member in members:
        send_free_ticket_email(event, list, member)
    VenueListMember.objects.filter(id__in=[m.id for m in members]).update(is_email_sent=True)

    template_name = 'venueadmin/event_lists_free_ticket.html'
    list_html = render_to_string(template_name, {
        'list': list,
        'guests': get_list_members(list, event),
        'event': event,
        'is_org_lists_page': False,
    })
    assert all([m.is_email_sent for m in list.members.all()])
    return json.dumps({
        'success': True,
        'message': _("Email sent to %(guest_count)d guests") % {'guest_count': len(members)},
        'list_html': list_html,
    })


@dajaxice_register
@utils.permission_required("list_edit")
def email_single_list_guest(request, eventid, listid, guestid):
    event = get_object_or_404(Event, id=eventid)
    list = get_object_or_404(VenueList, id=listid, event=event)
    guest = get_object_or_404(VenueListMember, uuid=guestid, list=list)

    send_free_ticket_email(event, list, guest)
    guest.is_email_sent = True
    guest.save()

    template = 'venueadmin/event_lists_row.html'
    guest_html = render_to_string(template, {
        'guest': {
            'id': guest.uuid,
            'name': guest.full_name,
            'quantity': guest.ticket_count,
            'comment': guest.comment,
            'added_timestamp': date_format(event.localize_dt(guest.created_timestamp), 'SHORT_DATETIME_FORMAT'),
            'is_email_sent': guest.is_email_sent,
        },
        'is_org_lists_page': False,
        'list': list,
    })

    return json.dumps({
        'success': True,
        'message': _("Email sent"),
        'guest_html': guest_html,
    })


@dajaxice_register
def send_event_promotion_email(request, eventid, data):
    event = get_object_or_404(Event, id=eventid)

    if not event.has_user_permission(request.user, 'statistics'):
        raise PermissionDenied("%s.%s" % (request.user, 'statistics'))

    form = EventPromotionForm(data, event=event, created_by=request.user)

    if not form.is_valid():
        return json.dumps({
            'success': False,
            'errors': form.errors,
        })

    promotion = form.save(commit=False)
    recipients = form.get_recipients()
    if not recipients:
        return json.dumps({
            'success': False,
        })
    promotion.recipients = len(recipients)
    promotion.save()

    merge_vars = []
    for recipient in recipients:
        name = (' %s' % recipient[0]) if recipient[0] else ''
        merge_vars.append({'rcpt': recipient[1],
                           'vars': [{'content': name, 'name': 'name'}]
        })

    logging.info("Sending promotional emails for event '%s' (%d) to %d recipients (campaign id %d)",
                 event.name, event.id, promotion.recipients, promotion.id)
    with statsd_timing_and_incr("misc.mandrill.send_event_promotion"):
        send_mandrill_email(request=request,
                            emails=[r[1] for r in recipients],
                            subject=promotion.title,
                            from_addr=promotion.sender,
                            from_name='',
                            template='emails/user/event_promotion.html',
                            merge_vars=merge_vars,
                            metadata={'uuid': promotion.uuid, 'organization': event.owner_org_id},
                            tags=['promo_event'],
                            title=promotion.title,
                            content=promotion.content,
                            event_url=request.build_absolute_uri(event.get_absolute_url()),
                            avatar_url=request.build_absolute_uri(avatar_image_path(event, 173, 248, user_site=True)),
                            SITE_URL=get_site_url(request),
                            client_name='*|NAME|*',
                            OPT_OUT='*|UNSUB:%s|*' % (request.build_absolute_uri(reverse("unsubscribed")))
                            )

    promo_emails = EventPromotionEmail.objects.filter(event=event).order_by('sent_timestamp')
    EventPromotionEmail.update_emails(promo_emails)

    return json.dumps({
        'success': True,
        'recipient_count': len(recipients),
        'campaigns_html': render_to_string('venueadmin/event_campaigns.html',
                                           context_instance=RequestContext(request, {'event': event,
                                                                                     'promo_emails': promo_emails}))
    })

@dajaxice_register
def get_recipient_count(request, eventid, data):
    event = get_object_or_404(Event, id=eventid)

    if not event.has_user_permission(request.user, 'statistics'):
        raise PermissionDenied("%s.%s" % (request.user, 'statistics'))

    form = EventPromotionRecipientsForm(data, event=event)

    if not form.is_valid():
        return json.dumps({
            'success': False,
            'errors': form.errors,
        })

    return json.dumps({
        'success': True,
        'recipient_count': len(form.get_recipients()),
    })


@dajaxice_register
@utils.permission_required("permissions_edit")
def upgrade_package(request, organizationid):
    organization = get_object_or_404(Organization, id=organizationid)

    if organization.plan != Organization.PLAN_PREMIUM:
        site_url = get_site_url(request)
        send_email(
            email='',
            template='emails/request_premium.html',
            subject='%s requested PREMIUM plan' % organization.display_name(),

            site_url=site_url,
            organization=organization,
            who_wanted=request.user,
            location=get_location_info_by_ip(request),
        )
        return json.dumps({
            'success': True,
        })

    return json.dumps({
        'success': False,
    })


@dajaxice_register
@utils.permission_required("event_edit")
def delete_event(request, eventid):

    event = get_object_or_404(Event, id=eventid)
    success = event.can_delete()
    if success:
        event.closed_by = request.user
        event.closed_timestamp = timezone.now()
        event.save()
        messages.success(request, _("Event deleted\nIf this was an accident, contact us."))

    return json.dumps({
        'success': success,
        'message': _("The event already has some sold tickets or booked tables.") if not success else ""
    })

