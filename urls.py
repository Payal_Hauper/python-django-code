# -*- coding: utf-8 -*-

from django.conf.urls import *
from venueadmin.views import EventStatisticsView, EventStatisticsPDFView, DashboardView, VenueStatisticsView, BillingPDFView, BillingWebView, EventCheckinHandler, DashboardAllUpcomingEventsView, EventPrintView

from account.forms import ProSetPasswordForm

# URLs common to subdomain and subdir, i.e. available both as pro.some.domain/xxx as well as some.domain/pro/xxx
common_urlpatterns = patterns('venueadmin.views',
    url(r'^$', 'root_redirect', name='venueadmin_root'),

    url(r'^dashboard/$', DashboardView.as_view(), name='venueadmin_index'),
    url(r'^dashboard/all_upcoming/$', DashboardAllUpcomingEventsView.as_view(), name='dashboard_all_upcoming_events'),
    url(r'^events/$', 'org_all_events', name='venueadmin_all_events'),
    url(r'^venues/$', 'org_all_venues', name='org_all_venues'),

    url(r'^event/create/$', 'create_event_wrapper', name='create_event_wrapper'),
    url(r'^event/(?P<eventid>\d+)/$', 'show_event', name='venueadmin_event'),
    url(r'^event/(?P<eventid>\d+)/guestlist.json$', 'event_guestlist', name='venueadmin_event_guestlist'),
    url(r'^event/(?P<eventid>\d+)/change_guest_comment/$', 'event_guest_update_comment', name='venueadmin_event_guest_update_comment'),
    url(r'^event/(?P<eventid>\d+)/do_checkins/$', EventCheckinHandler.as_view(), name='venueadmin_event_do_checkins'),
    url(r'^event/(?P<eventid>\d+)/set_checkedin_count/$', 'event_guest_set_entered_count_old', name='venueadmin_event_guest_set_entered_count_old'),
    url(r'^event/(?P<eventid>\d+)/lists/$', 'event_edit_lists', name='event_edit_lists'),
    url(r'^event/(?P<eventid>\d+)/edit/$', 'edit_event', name='event_edit'),
    url(r'^event/(?P<eventid>\d+)/promoters/$', 'event_edit_promoters', name='event_edit_promoters'),
    url(r'^event/(?P<eventid>\d+)/tickets/$', 'event_edit_tickets', name='event_edit_tickets'),
    url(r'^event/(?P<eventid>\d+)/share/$', 'event_share', name='event_share'),
    url(r'^event/(?P<eventid>\d+)/share/promotion_email/$', 'event_promotion_email', name='event_promotion_email'),
    url(r'^event/(?P<eventid>\d+)/statistics/$', EventStatisticsView.as_view(), name='event_statistics'),
    url(r'^event/(?P<eventid>\d+)/statistics/pdf/$', EventStatisticsPDFView.as_view(), name='event_statistics_pdf'),
    url(r'^event/(?P<eventid>\d+)/print/$', EventPrintView.as_view(), name='venueadmin_event_print'),
    url(r'^event/(?P<eventid>\d+)/csv/$', 'show_event_csv', name='venueadmin_event_csv'),
    url(r'^event/(?P<eventid>\d+)/tables/$', 'event_edit_tables_mobile', name='event_edit_tables'),

    url(r'^event/(?P<eventid>\d+)/lists/mobile/$', 'event_edit_lists_mobile', name='event_edit_lists_mobile'),
    url(r'^event/(?P<eventid>\d+)/tables/mobile/$', 'event_edit_tables_mobile', name='event_edit_tables_mobile'),

    url(r'^venue/create/$', 'simple_venue_add', name='venue_add_simple'),
    url(r'^venue/(?P<venueid>\d+)/createevent/$', 'create_event', name='create_event'),
    url(r'^venue/(?P<venueid>\d+)/edit/$', 'edit_venue', name='venue_edit'),
    url(r'^venue/(?P<venueid>\d+)/statistics/$', VenueStatisticsView.as_view(), name='venue_statistics'),
    url(r'^venue/(?P<venueid>\d+)/statistics/(?P<days>\d+)/$', VenueStatisticsView.as_view(), name='venue_statistics_days'),
    url(r'^venue/(?P<venueid>\d+)/facebook_shop/$', 'venue_fb_shop', name='venue_facebook_shop'),  # DEPRECATED
    url(r'^venue/(?P<venueid>\d+)/promotion/$', 'venue_promo_tools', name='venue_promo_tools'),
    url(r'^venue/(?P<venueid>\d+)/claim/$', 'venue_claim', name='venue_claim'),
    url(r'^venue/(?P<venueid>\d+)/tables/$', 'venue_table_config', name='venue_table_config'),
    url(r'^venue/(?P<venueid>\d+)/menu/$', 'venue_menu', name='venue_menu'),

    url(r'^organization/$', 'create_org', name='create_org'),
    url(r'^organization/(?P<organizationid>\d+)/events/$', 'org_all_events', name='org_all_events'),
    url(r'^organization/(?P<organizationid>\d+)/lists/$', 'org_lists', name='org_lists'),
    url(r'^organization/lists/$', 'org_lists_manager', name='org_lists_manager'),
    url(r'^organization/(?P<organizationid>\d+)/users/$', 'organization_users', name='organization_users'),
    url(r'^organization/(?P<organizationid>\d+)/promoters/$', 'organization_promoters', name='organization_promoters'),
    url(r'^organization/(?P<organizationid>\d+)/database/$', 'people_database', name='people_database'),
    url(r'^organization/(?P<organizationid>\d+)/billing/$', BillingWebView.as_view(), name='venueadmin_billing'),
    url(r'^organization/(?P<organizationid>\d+)/billing/pdf/$', BillingPDFView.as_view(), name='venueadmin_billing_pdf'),
    url(r'^organization/(?P<organizationid>\d+)/billing/old/$', 'purchase_overviews_old', name='venueadmin_billing_old'),
    url(r'^organization/(?P<organizationid>\d+)/legal_info/$', 'org_legal_info', name='org_legal_info'),
    url(r'^organization/(?P<organizationid>\d+)/legal_info/subscription/$', 'org_subscription', name='org_subscription'),
    url(r'^organization/(?P<organizationid>\d+)/tally/$', 'tally_configuration', name='tally_configuration'),

    url(r'^organization/(?P<organizationid>\d+)/set_active/$', 'org_set_active', name='org_set_active'),

    url(r'^organization/(?P<organizationid>\d+)/chat/$', 'org_chat', name='org_chat'),

    url(r'^withdrawal/(?P<withdrawalid>\w+)/$', 'pay_withdrawal', name='pay_withdrawal'),

    url(r'^notifications/$', 'notifications', name='notifications'),
    url(r'^my_settings/$', 'venueadmin_settings', name='venueadmin_settings'),
    url(r'^distributor/$', 'distributor_overview', name='distributor_overview'),
    url(r'^distributor/organization-(?P<organizationid>\d+)/$', 'distributor_view_org', name='distributor_view_org'),

    url(r'^no_venues/$', 'no_venues', name='venueadmin_no_venues'),

    url(r'^window_close_callback/$', 'window_close_callback', name='window_close_callback'),

    url(r'^starting_page/$', 'starting_page', name='starting_page'),

    (r'^sky/', include('skyplus.urls')),
)


# Subdir urls
urlpatterns = common_urlpatterns

urlpatterns += patterns('account.views',
    url(r'^login/$', 'login_or_register', {
        'template_name': 'venueadmin/login.html',
        'redirect_default': 'venueadmin_index',
        'is_pro_site': True,
    }, name='venueadmin_login'),
    url(r'^signup/$', 'login_or_register', {
        'template_name': 'venueadmin/login.html',
        'redirect_default': 'venueadmin_index',
        'is_pro_site': True,
    }, name='venueadmin_signup'),
)

urlpatterns += patterns('account.views',

    url(r'^logout/$', 'logout', name = 'venueadmin_logout'),
    url(r'^account/my_settings/$', 'show_my_settings', name='pro_settings_page'),

    url(r'^account/reset/$', 'password_reset_form', {
            'is_pro_site': True,
            'template_name': 'venueadmin/password_reset_form.html',
            'email_template_name': 'emails/password_reset_pro.html',
            'redirect_default': 'venueadmin_password_reset_done'
        }, name='venueadmin_password_reset'),
    url(r'^account/reset_done/$', 'password_reset_done_form', {
            'is_pro_site': True,
            'template_name': 'venueadmin/password_reset_message.html',
        }, name='venueadmin_password_reset_done'),

    url(r'^account/reset/(?P<uidb36>[0-9A-Za-z]{1,13})-(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', 'password_reset_confirm_proxy', {
            'set_password_form': ProSetPasswordForm,
            'template_name': 'venueadmin/password_reset_confirm.html',
            'redirect_url': "venueadmin_password_reset_complete",
        }, name='pro_password_reset_confirm'),
)

urlpatterns += patterns('django.contrib.auth.views',
    url(r'^account/reset_complete/$', 'password_reset_complete', {
            'template_name': 'venueadmin/password_reset_message.html',
        }, name='venueadmin_password_reset_complete'),
)

# Subdomain urls are in urls_root.py
