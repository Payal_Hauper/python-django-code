# -*- coding: utf-8 -*-

from django import template
from django.core.urlresolvers import reverse
from venue.models import UserPermission

register = template.Library()


class PermissionCheckNode(template.Node):
    def __init__(self, venue, permission_name, nodelist_true, nodelist_false):
        self.venue_var = template.Variable(venue)
        self.permission_name = permission_name
        self.nodelist_true = nodelist_true
        self.nodelist_false = nodelist_false

    def render(self, context):
        try:
            # Get venue and user
            venue = self.venue_var.resolve(context)
            user = context.get('user')

            # Does the user have the necessary permission?
            has_permission = venue.has_user_permission(user, self.permission_name)

            if not has_permission:
                if self.nodelist_false:
                    return self.nodelist_false.render(context)
                else:
                    return ''
            else:
                return self.nodelist_true.render(context)
        except template.VariableDoesNotExist:
            return ''

@register.tag
def if_venueadmin_permission(parser, token):
    """ Checks if current user has given venueadmin permission in given venue

    {% if_venueadmin_permission venue "edit_viplist" %}
        You can edit VIP lists!
    {% else_venueadmin_permission %}
        Else branch is optional.
    {% endif_venueadmin_permission %}
    """

    try:
        # split_contents() knows not to split quoted strings.
        tag_name, venue, permission_name = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError("%r tag requires two arguments" % token.contents.split()[0])
    if not (permission_name[0] == permission_name[-1] and permission_name[0] in ('"', "'")):
        raise template.TemplateSyntaxError("%r: permission name should be in quotes" % tag_name)

    nodelist_true = parser.parse(('else_venueadmin_permission', 'endif_venueadmin_permission'))
    token = parser.next_token()
    if token.contents == 'else_venueadmin_permission':
        nodelist_false = parser.parse(('endif_venueadmin_permission',))
        parser.delete_first_token()
    else:
        nodelist_false = None

    return PermissionCheckNode(venue, permission_name[1:-1], nodelist_true, nodelist_false)


class EventDefaultUrlNode(template.Node):
    def __init__(self, event_var, var_name):
        self.event_var = template.Variable(event_var)
        self.var_name = var_name
    def render(self, context):
        user = context.get('user')
        event = self.event_var.resolve(context)

        if event.has_user_permission(user, 'event_show_guests'):
            context[self.var_name] = reverse('venueadmin_event', kwargs={ 'eventid': event.id })
        elif event.has_user_permission(user, 'list_edit'):
            context[self.var_name] = reverse('event_edit_lists', kwargs={ 'eventid': event.id })
        else:
            context[self.var_name] = ''
        return ''

@register.tag
def event_default_url(parser, token):
    """ Sets given variable to contain default url for an event (if any)

    Usage:  {% event_default_url event as event_url_variable %}
    """
    # This version uses a regular expression to parse tag contents.
    try:
        # Splitting by None == splitting by spaces.
        tag_name, event, as_text, var_name = token.split_contents()
        assert as_text == 'as'
    except (ValueError, AssertionError):
        raise template.TemplateSyntaxError("Usage: %r event as event_url_var" % token.contents.split()[0])

    return EventDefaultUrlNode(event, var_name)


@register.assignment_tag
def is_permalist_manager(user):
    return UserPermission.objects.select_related('list').exclude(list=None).filter(list__event=None,
                                                                                   user=user,
                                                                                   permission_list_edit=True).exists()
