# -*- coding: utf-8 -*-

from django import template

import pytz
import json

register = template.Library()


@register.simple_tag
def timezone_dict():
    """ returns Dictionary 'country-iso-code' -> 'timezone'  as json string
    """
    timezones = dict(pytz.country_timezones)
    # Add empty array (no choices) when no country is selected, so that we can always expect an array in JS.
    timezones[''] = []
    return json.dumps(timezones, sort_keys=True)
