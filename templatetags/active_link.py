from django import template
from django.core.urlresolvers import reverse

register = template.Library()

@register.simple_tag(takes_context=True)
def active(context, url):
    request = context.get('request')
    if request.path.strip() == url.strip():
        return 'active'
    return ''
