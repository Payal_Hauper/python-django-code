from django import template

from venue.models import UserPermission

register = template.Library()


@register.tag
def if_starting_progress(parser, token):
    try:
        tag_name, org = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError("%r tag requires one argument" % token.contents.split()[0])

    nodelist = parser.parse(('endif_starting_progress',))
    parser.delete_first_token()
    return StartingProgressNode(nodelist, org)

class StartingProgressNode(template.Node):
    def __init__(self, nodelist, org):
        self.nodelist = nodelist
        self.org_var = template.Variable(org)

    def render(self, context):
        org = self.org_var.resolve(context)
        if not org:
            return ''

        progress = org.starting_progress()['progress']
        if progress >= 100:
            return ''

        # Check if the user has enough permissions to manage this venue
        user = context.get('user')
        user_perms = UserPermission.user_permissions_for(user, organization=org)
        if 'permissions_edit' not in user_perms or 'venue_edit' not in user_perms:
            return ''

        context['starting_progress'] = progress
        output = self.nodelist.render(context)
        return output
