from django.conf import settings
from django.contrib.auth.models import AnonymousUser
from django.db import transaction
from django.db.models import Count, Q
from django.http import Http404, HttpRequest
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _, ugettext

from account.models import UserProfile
from django_cron import CronJobBase, Schedule
from venue.models import (UserPermission, PurchasesOverview, Organization, VenueFreeList, Event, EventSeries,
                          VenueTicketBuyersList, TemporaryFileWrapper)
from venueadmin.email import send_we_miss_you_email
from venueadmin.views import BillingPDFView, EventStatisticsPDFView, EventPrintView
from shop.models import ShopData, ShopItem, ShopItemData, ShopCategory
from utils.statsd import statsd
from utils.utils import send_email, slugify, send_mandrill_email

from datetime import date, timedelta, datetime, time
from itertools import groupby
import logging
from smtplib import SMTPRecipientsRefused


class CreatePurchasesOverviews(CronJobBase):

    schedule = Schedule(run_every_mins=5)
    code = 'events:venueadmin:CreatePurchasesOverviews'
    MIN_NUM_FAILURES = 1

    created_overviews_count = 0

    def do(self):
        events = Event.objects.filter(end_time__gte=BillingPDFView.withdrawal_start_date, end_time__lt=timezone.now())
        events = events.filter(purchases_overview_processed=False)
        events = events.select_related('owner_org', 'venue')

        for event in events:
            statsd.incr('cron.jobs.events.venueadmin.CreatePurchasesOverviews.events_checked')

            overview = self.process_event(event)
            if overview:
                self.created_overviews_count += 1

        if self.created_overviews_count:
            logging.info('CreatePurchasesOverviews.job(): Created %d overviews', self.created_overviews_count)

    @classmethod
    @transaction.commit_on_success
    def process_event(cls, event):
        pdf = BillingPDFView.get_PDF_file(event)
        if not pdf:
            # Event had no purchases, do nothing
            statsd.incr('cron.jobs.events.venueadmin.CreatePurchasesOverviews.skipped.no_purchases')
            return None

        overview = PurchasesOverview()
        overview.event = event
        overview.organization = event.owner_org
        overview.bill_type = PurchasesOverview.TYPE_OVERVIEW
        overview.pdf_content = pdf.getvalue().encode("base64")
        overview.save()

        statsd.incr('cron.jobs.events.venueadmin.CreatePurchasesOverviews.overviews_created')

        cls.send_event_overview_email(overview)
        Event.objects.filter(id=event.id).update(purchases_overview_processed=True)

        return overview

    @classmethod
    def send_event_overview_email(cls, overview):
        event = overview.event

        # Immediately send to accountants
        attachments = [{
            'type': 'application/pdf',
            'name': slugify(event.name) + '-purchases.pdf',
            'content': overview.pdf_content,
        }]
        send_mandrill_email(
            emails=settings.events_ACCOUNTING_RECEIVERS,
            subject="Purchases overview for %s @%s" % (event.name, event.venue.name),
            attachments=attachments,
            template='emails/pro/accounting_purchase_overview.html',
            tags=['accounting_purchase_overview'],
            event=event,
        )


class SendAutomaticNightlyReports(CronJobBase):
    """ Emails out both nightly reports and purchases overviews of ended events.

    Note that this job depends on CreatePurchasesOverviews having already processed the events.
    """

    schedule = Schedule(run_every_mins=5)
    code = 'events:venueadmin:SendAutomaticNightlyReports'
    MIN_NUM_FAILURES = 1

    sent_emails_count = 0

    def do(self):
        # Don't fetch events that ended more than half an hour ago.
        checkpoint = timezone.now() - timedelta(minutes=30)
        events = Event.objects.filter(end_time__gt=checkpoint, end_time__lt=timezone.now(),
                                      automatic_report_sent=False) \
            .select_related('owner_org', 'purchases_overview')

        for event in events:
            email_addresses = self.get_email_addresses(event.owner_org)

            if self.send_email_to_managers(event, email_addresses):
                event.automatic_report_sent = True
                event.save()

        logging.info('SendAutomaticNightlyReports.job(): Fetched %d events (%d emails sent)',
                     len(events),
                     self.sent_emails_count)

    def get_email_addresses(self, organization):
        """
            Get the emails of admins and managers who have not disabled it
        """
        return map(lambda x: x.email,
                   filter(lambda x: x.email and x.profile.send_email_with_tag('pro_nightly_report'), organization.get_org_admins(staff=True)))

    def send_email_to_managers(self, event, email_addresses):
        try:
            request = HttpRequest()
            setattr(request, 'user', AnonymousUser())
            setattr(request, 'subhost_name', 'default')
            pdf_data = EventStatisticsPDFView.generate_pdf(request, eventid=event.id)
        except Http404:
            # No checked in guests, skip it
            return False

        attachments = [(slugify(event.name) + '.pdf', pdf_data, 'application/pdf')]
        if event.purchases_overview:
            pdf_data = event.purchases_overview.pdf_content.decode('base64')
            attachments.append((slugify(event.name) + '-purchases.pdf', pdf_data, 'application/pdf'))
        elif not event.purchases_overview_processed:
            logging.error("Event %d (%s: %s) has not been processed by CreatePurchasesOverviews yet",
                          event.id, event.venue.name, event.name)

        try:
            send_email(emails=email_addresses, subject=_("Report - %(event_name)s") % {'event_name': event.name},
                       template="emails/nightly_report.html", event=event, attachments=attachments
            )
            self.sent_emails_count += 1
        except SMTPRecipientsRefused:
            logging.warning("SendAutomaticNightlyReports.send_email_to_managers(): e-mail could not be sent to %s for event #%d - %s", email_addresses, event.id, event.name)
            return False

        return True


class SendRSVPListToVenue(CronJobBase):

    schedule = Schedule(run_every_mins=5)
    code = 'events:venueadmin:SendRSVPListToVenue'
    MIN_NUM_FAILURES = 1

    def do(self):
        # Get all the lists that we'll need to send:
        # - event owner must have  feature_email_rsvp_list  turned on
        # - list must be closed
        # - no emails must have been sent yet
        lists = list(VenueFreeList.objects.filter(event__owner_org__feature_email_rsvp_list=True,
                open_until__lt=timezone.now(), sent_to_emails='').select_related('event'))
        logging.info("SendRSVPListToVenue: Processing %d lists", len(lists))
        for free_list in lists:
            emails = [ free_list.event.venue.email ]
            subject = "RSVP guests for %s" % free_list.event.name
            template = "emails/venueadmin_rsvp_guestlist.html"

            guest_lines = self.format_members_text(free_list.current_members_dict())
            # Special treatment for empty lists. This is somewhat ugly, but better than adding another field to model,
            #  I think.
            if not guest_lines:
                VenueFreeList.objects.filter(pk=free_list.pk).update(sent_to_emails='-')
                continue

            logging.info("SendRSVPListToVenue: Sending list of event '%s' (%d) containing %d members",
                free_list.event.name, free_list.event.id, len(guest_lines))
            with transaction.commit_on_success():
                send_email(emails=emails, subject=subject, template=template,
                        event=free_list.event, guest_lines=guest_lines)
                VenueFreeList.objects.filter(pk=free_list.pk).update(sent_to_emails=','.join(emails))
                statsd.incr('cron.jobs.events.venueadmin.SendRSVPListToVenue.emails_sent')

    # Copied from VenueListForm, with minor changes
    def format_members_text(self, members):
        members_lines = [ self.format_member_line(member) for member in members.itervalues() ]
        return sorted(members_lines, key=lambda m: m.lower())

    def format_member_line(self, member):
        """ Formats a member into textual line form. Does not append newline.
        """
        line = member.name
        if member.count > 1:
            line += u" +%d" % (member.count - 1)
        if member.comment:
            line += u", %s" % member.comment

        return line


class SendPrintableListsToVenue(CronJobBase):
    schedule = Schedule(run_every_mins=5)
    code = 'events:venueadmin:SendPrintableListsToVenue'
    MIN_NUM_FAILURES = 1

    def do(self):
        # Get all Events that we haven't processed yet
        events = Event.objects.filter(sent_lists_to_emails='').select_related('owner_org')
        logging.info("SendPrintableListsToVenue: Found %d events", events.count())

        # Here we'll store the events for which we won't send emails but still want to consider as processed.
        noop_event_ids = []
        skipped_event_ids = []

        for event in events:
            statsd.incr('cron.jobs.events.venueadmin.SendPrintableListsToVenue.events_checked')

            if not event.owner_org.feature_email_printed_lists_to_emails:
                if event.start_time <= timezone.now():
                    noop_event_ids.append(event.id)
                else:
                    skipped_event_ids.append(event.id)
                    statsd.incr('cron.jobs.events.venueadmin.SendPrintableListsToVenue.skipped.no_feature')
                continue

            time_until = event.start_time - timezone.now()

            if time_until > timedelta(hours=event.owner_org.feature_email_printed_lists_hours):
                skipped_event_ids.append(event.id)
                statsd.incr('cron.jobs.events.venueadmin.SendPrintableListsToVenue.skipped.too_early')
                continue

            pdf_data = EventPrintView.get_PDF_file(event)

            if pdf_data:
                attachments = [{
                    'type': 'application/pdf',
                    'name': ugettext('events lists - %(event_name)s.pdf' % {'event_name': slugify(event.name)}),
                    'content': pdf_data.encode('base64'),
                }]

                emails = [e.strip() for e in event.owner_org.feature_email_printed_lists_to_emails.split(',')]
                subject = "Printed Lists for %s" % event.name
                template = "emails/pro/printed_lists.html"

                logging.info("SendPrintableListsToVenue: Sending lists of event '%s' (%d)", event.name, event.id)

                with transaction.commit_on_success():
                    send_mandrill_email(emails=emails, subject=subject, template=template, tags=['test', 'event-printablelists'],
                                        event=event, attachments=attachments)

                    Event.objects.filter(pk=event.pk).update(sent_lists_to_emails=','.join(emails))
                    statsd.incr('cron.jobs.events.venueadmin.SendPrintableListsToVenue.emails_sent')

        logging.info("SendPrintableListsToVenue: Skipped %d events", len(skipped_event_ids))

        # Mark the noop events as having been processed.
        Event.objects.filter(id__in=noop_event_ids).update(sent_lists_to_emails='-')
        logging.info("SendPrintableListsToVenue: Marked %d events as proccessed", len(noop_event_ids))




class SendTicketBuyersListToVenue(CronJobBase):
    schedule = Schedule(run_every_mins=5)
    code = 'events:venueadmin:SendTicketBuyersListToVenue'
    MIN_NUM_FAILURES = 1

    def do(self):
        # Get all Events that we haven't processed yet
        events = Event.objects.filter(sent_tickets_to_emails='').select_related('owner_org')
        # Here we'll store the events for which we won't send emails but still want to consider as processed.
        noop_event_ids = []
        for event in events:
            statsd.incr('cron.jobs.events.venueadmin.SendTicketBuyersListToVenue.events_checked')

            event_tickets = ShopItem.objects.visible().filter(data__event=event).select_related('data')
            sellable_tickets = filter(lambda t: t.is_sellable(), event_tickets)
            if sellable_tickets or (not event_tickets and event.end_time < timezone.now()):
                # Don't process events that either still have sellable tickets or have no tickets at all and haven't
                # ended yet (in this case there's a chance that someone will add tickets).
                continue

            if not event.owner_org.feature_email_tickets_to_emails:
                noop_event_ids.append(event.id)
                continue

            guests = self.get_event_guests(event)
            if not guests:
                statsd.incr('cron.jobs.events.venueadmin.SendTicketBuyersListToVenue.skipped.no_guests')
                noop_event_ids.append(event.id)
                continue

            emails = [e.strip() for e in event.owner_org.feature_email_tickets_to_emails.split(',')]
            subject = "Ticket buyers for %s" % event.name
            template = "emails/pro/ticket_buyers_list.html"

            logging.info("SendTicketBuyersListToVenue: Sending list of event '%s' (%d) containing %d members",
                         event.name, event.id, len(guests))
            with transaction.commit_on_success():
                send_mandrill_email(emails=emails, subject=subject, template=template, tags=['event-ticketbuyers'],
                                    event=event, guests=guests)
                Event.objects.filter(pk=event.pk).update(sent_tickets_to_emails=','.join(emails))
                statsd.incr('cron.jobs.events.venueadmin.SendTicketBuyersListToVenue.emails_sent')

        # Mark the noop events as having been processed.
        Event.objects.filter(id__in=noop_event_ids).update(sent_tickets_to_emails='-')

    def get_event_guests(self, event):
        event_items = ShopItemData.objects.filter(event=event, categories__category_type=ShopCategory.TYPE_TICKET)
        # Gather all lists for this event
        lists = [VenueTicketBuyersList(shop_item) for shop_item in event_items]

        guests = []
        for list in lists:
            for guest in list.get_members(event):
                guests.append({
                    'name': guest['name'],
                    'quantity': guest['quantity'],
                    'comment': guest['comment'],
                    'list': guest['list'],
                })

        return sorted(guests, key=lambda guest: guest['name'].lower())


class SendWeMissYouEmail(CronJobBase):

    schedule = Schedule(run_at_times=["11:15"])
    code = 'events:venueadmin:SendWeMissYouEmail'
    MIN_NUM_FAILURES = 1

    def get_inactive_orgs(self):
        now = timezone.now()

        # Get event count grouped by organizations(1 query)
        events = Event.objects.values('owner_org').annotate(org_count=Count('owner_org'))

        # Get organizations that don't have events. Prefetch venue and owner user(3 queries).
        # Sort out the users that have received we-miss-you emails in the last 7 days.
        # Sort out the users that are younger than 5 days.
        organizations = Organization.objects.prefetch_related("venue", "owner", "owner__userprofile").filter(~Q(id__in=map(lambda x: x['owner_org'], events)),
                                                                                       Q(owner__userprofile__last_mia_email=None) | Q(owner__userprofile__last_mia_email__lt=now - timedelta(days=7)),
                                                                                       owner__date_joined__lt=now - timedelta(days=5))
        # Filter unique users.
        ret = []
        for k, g in groupby(sorted(organizations, key=lambda x: x.owner.id), key=lambda x: x.owner.id):
            ret.append(list(g)[0])

        return ret

    def do(self):
        now = timezone.now()
        # Optimized query:
        organizations = self.get_inactive_orgs()
        logging.info("SendWeMissYouEmail: Processing %d users", len(organizations))

        sent = 0

        if organizations:
            for organization in organizations:
                logging.info("SendWeMissYouEmail: Sending WeMissYouEmail to %s" % organization.owner)

                send_we_miss_you_email(organization.owner, organization)
                sent = sent + 1
                statsd.incr('cron.jobs.events.venueadmin.SendWeMissYouEmail.email_sent')

            UserProfile.objects.filter(user__in=map(lambda x: x.owner, organizations)).update(last_mia_email=now)

        # Total queries should now be 7(excluding send_we_miss_you_email inner queries).
        logging.info("SendWeMissYouEmail: Sent %d emails", sent)


class CreateRecurringEvents(CronJobBase):

    schedule = Schedule(run_at_times=['12:00'])
    code = 'events:venue:CreateRecurringEvents'

    def do(self):
        event_count = 0
        ticket_count = 0
        series_query = EventSeries.objects.all().prefetch_related('events')

        for series in series_query:
            created_events, created_tickets = series.create_events()
            event_count += created_events
            ticket_count += created_tickets

        logging.info('Created %d new events with %d new tickets for %d series', event_count, ticket_count, len(series_query))

class ClearTemporaryFileUploads(CronJobBase):

    schedule = Schedule(run_every_mins=24*60)  # once a day
    code = 'events:venueadmin:ClearTemporaryFileUploads'
    MIN_NUM_FAILURES = 1

    def do(self):
        now = timezone.now()
        temps = TemporaryFileWrapper.objects.filter(timestamp__lt=now - timedelta(hours=6))
        count = temps.count()
        temps.delete()
        logging.info('Deleted %d temporary file uploads.', count)

