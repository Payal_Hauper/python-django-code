from django.core.cache import cache
from django.db.models.signals import pre_delete, post_save, m2m_changed, pre_save
from django.dispatch import receiver
from django.utils import timezone

from invite_member.signals import invite_accepted
from shop.models import RedeemPackage, PurchasedItem
from utils.facebook_utils import FetchFacebookListForNewEventThread
from utils.utils import metrics, logger
from venue.models import VenueList, VenueFacebookList,VenueBabashList, VenueListMember, VenueVisit, VenueTableBooking, UserPermission,\
    Promoter, Event, EventNote, Organization


def modified_redeempackage_callback(sender, instance, **kwargs):
    purchased_items = instance.items.all()[:1]
    if not purchased_items:
        return
    event = purchased_items[0].item_data.event
    set_event_modified_time(event)

def modified_purchaseditem_callback(sender, instance, **kwargs):
    event = instance.item_data.event
    set_event_modified_time(event)

def modified_venuelist_callback(sender, instance, **kwargs):
    set_event_modified_time(instance.event)

def modified_venuelistmember_callback(sender, instance, **kwargs):
    set_event_modified_time(instance.list.event)

def modified_venuevisit_callback(sender, instance, **kwargs):
    set_event_modified_time(instance.event)

@receiver(post_save, sender=Event)
def modified_event_callback(sender, instance, created, **kwargs):
    set_event_modified_time(instance)
    if instance.babash_event_id:
        babash_list, created = VenueBabashList.get_or_create_list_for_event(instance, by_user=instance.owner_org.owner)
        # Force refresh of cache from babash. A little heavy if they're updating many rows,
        # but is best to see results right after save if they changed something on babash side.
        logger.info("Event postsave signal modified_event_callback. Forceing update of Babashguestlist")
        babash_list.update_attenders_in_thread(force=True)
    else:
        VenueBabashList.delete_list_for_event(event=instance)

def modified_tables_callback(sender, instance, **kwargs):
    set_event_modified_time(instance.list_member.list.event)

@receiver(post_save, sender=EventNote)
def modified_eventnote_callback(sender, instance, **kwargs):
    set_event_modified_time(instance.event)

def clear_dashboard_counts(event):
    # Invalidate dashboard counts cache
    cache.delete('event-dashboard-counts-%d-v2' % (event.id))

def set_event_modified_time(event):
    if not event:
        return
    cache.set('event-%d-data-last-modified' % event.id, timezone.now(), 360*24*60*60)

    clear_dashboard_counts(event)


def modified_venuefacebooklist_callback(sender, instance, created, **kwargs):
    # Pull the attendees when FB list is added to an event (either exiting event or a new one)
    if created:
        # Re-pull from DB to have all datetimes converted to server timezone
        fb_list = VenueFacebookList.objects.get(pk=instance.pk)
        FetchFacebookListForNewEventThread(fb_list).start()

def modified_organization_callback(sender, instance, **kwargs):
    # Send billed event to Mixpanel if we start billing the organization
    # Send cancelled event to Mixpanel if we stop billing the organization
    # NOTE that the monthly fee can be also 0 if we're billing. That means that the org
    # is not in trial any more - they are using us for free
    prev_fee = None
    if instance.pk:
        # instance is the organization with the new data, that is being saved
        # "org" here is the organization with the old data
        org = sender.objects.get(pk=instance.pk)
        prev_fee = org.monthly_fee_in_eur
    if prev_fee is None and instance.monthly_fee_in_eur is not None:
        metrics.record("PRO", "Org Billed", user=instance.get_first_admin())
    elif prev_fee is not None and instance.monthly_fee_in_eur is None:
        metrics.record("PRO", "Org Cancelled", user=instance.get_first_admin())


post_save.connect(modified_redeempackage_callback, sender=RedeemPackage)
m2m_changed.connect(modified_redeempackage_callback, sender=RedeemPackage)
post_save.connect(modified_purchaseditem_callback, sender=PurchasedItem)

post_save.connect(modified_venuelist_callback, sender=VenueList)
m2m_changed.connect(modified_venuelist_callback, sender=VenueList)
pre_delete.connect(modified_venuelist_callback, sender=VenueList)
post_save.connect(modified_venuelistmember_callback, sender=VenueListMember)
pre_delete.connect(modified_venuelistmember_callback, sender=VenueListMember)
post_save.connect(modified_venuevisit_callback, sender=VenueVisit)
pre_delete.connect(modified_venuevisit_callback, sender=VenueVisit)

post_save.connect(modified_venuefacebooklist_callback, sender=VenueFacebookList)

post_save.connect(modified_tables_callback, sender=VenueTableBooking)
pre_delete.connect(modified_tables_callback, sender=VenueTableBooking)

pre_save.connect(modified_organization_callback, sender=Organization)

def venueadmin_invite_accepted(sender, **kwargs):
    key = sender

    try:
        permission = UserPermission.objects.get(id=key.data['userpermission'])
    except UserPermission.DoesNotExist():
        return

    # See if the invite type is known and get the UserPermission object
    if key.key_type.startswith('organization-user-'):
        assert permission.organization_id == key.data['organization']

        metrics.record("PRO", "Invitation Accepted", {'user_type': permission.get_role_id()}, user=key.invitee_user)

    elif key.key_type.startswith('list-user-'):
        assert permission.list_id == key.data['list']

        list_type = 'promoter' if permission.list.list_type == VenueList.TYPE_PROMOTER else 'list manager'
        metrics.record("PRO", "Invitation Accepted", {'user_type': list_type}, user=key.invitee_user)

    else:
        # We don't handle this type of invites
        return

    permission.user = key.invitee_user
    permission.save()

    # Force e-mail sync even if the e-mails match case insensitively
    if key.invitee_email != permission.user.email:
        if hasattr(permission.list, 'venuepromoterlist'):
            Promoter.objects.filter(email__iexact=key.invitee_email).update(email=permission.user.email)

    # TODO: remove obsolete inactive User objects

invite_accepted.connect(venueadmin_invite_accepted)
