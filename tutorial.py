# -*- coding: utf-8 -*-

from django.utils.translation import ugettext_lazy as _


TUTORIAL_STEPS = [
_(u"""
<p><strong>Welcome to events!</strong></p>
<p>This tutorial introduces you to the main features of events so you won’t get in trouble using them.
If you have any questions not answered by this tutorial, please look at the FAQ in the footer.</p>
<p>You rock! Click next to continue!</p>
"""),

_(u"""
<p>events has been designed for entertainment industry professionals to make every-day guest list
and event management easier.</p>
<p>You are currently on the main dashboard of events.
Below you will see events that you have either created or have access to, divided into current, upcoming,
and past events.</p>
<p>On the right you can see the main toolbar. Press next to go over the main workflows of events .</p>
"""),

_(u"""
<p>Using events is extremely easy:
<ol>
    <li><strong>Create events</strong></li>
    <li><strong>Edit guestlists</strong></li>
    <li><strong>Check people in</strong></li>
</ol>
Try it yourself now!
</p>
"""),

_(u"""
<p>If you have administrative rights then you can also:
<ol>
    <li><strong>Create permanent lists</strong> &ndash; lists that you use regularly,
        like “Family list” or “VIP” list or “Private members” or “my friends”.<br />
        All permanent lists are automatically copied and attached to all the events you have created ;)</li>
    <li>Check event <strong>reports</strong>.</li>
    <li><strong>Invite more users</strong> to manage your events &ndash; manage users button.</li>
"""),

_(u"""
<p>Ok, these were the basics of events.<br />
If you need any more help, feel free to check the HELP section, where the most frequent questions and problems
are discussed.</p>
<p>We are always happy to receive feedback on how to improve the product further. Feel free to contact us via
<a href="mailto:info@events.com">info@events.com</a>
"""),
]
