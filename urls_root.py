# Urls here are used when pro sub-host is used.
# We include here the usual venueadmin urls but also helper urls such as account mgmt, media, etc.

from django.conf.urls import *
from django.conf import settings
from django.views.generic.simple import redirect_to

from dajaxice.core import dajaxice_autodiscover
import notification

from account.forms import ProSetPasswordForm
from venueadmin.urls import common_urlpatterns

dajaxice_autodiscover()
notification.autodiscover()


handler500 = 'ui.views.pro_server_error'
handler404 = 'ui.views.pro_not_found'

urlpatterns = patterns('',
    (r'^account/invites/', include('invite_member.urls')),

    (r'^api/v1/', include('api.v1.urls')),
    (r'^api/timing/', include('tracker.urls')),

    (r'^chat/', include('chat.urls')),

    (r'^%s/' % settings.DAJAXICE_MEDIA_PREFIX, include('dajaxice.urls')),
)

urlpatterns += patterns('account.views',
    url(r'^login/$', redirect_to, {'url': '/app/login/'}),
    url(r'^signup/$', redirect_to, {'url': '/app/signup/'}),
    url(r'^account/social_auth/complete/(?P<backend>[^/]+)/$', 'social_auth_complete', name='socialauth_complete'),

    url(r'^logout/$', 'logout', name = 'venueadmin_logout'),
    url(r'^account/my_settings/$', 'show_my_settings', name='pro_settings_page'),

    url(r'^account/reset/$', 'password_reset_form', {
            'is_pro_site': True,
            'template_name': 'venueadmin/password_reset_form.html',
            'email_template_name': 'emails/password_reset_pro.html',
            'redirect_default': 'venueadmin_password_reset_done'
        }, name='venueadmin_password_reset'),
    url(r'^account/reset_done/$', 'password_reset_done_form', {
            'is_pro_site': True,
            'template_name': 'venueadmin/password_reset_message.html',
        }, name='venueadmin_password_reset_done'),
    url(r'^app/login/$', 'login_or_register', {
        'template_name': 'venueadmin/login.html',
        'redirect_default': 'venueadmin_index',
        'is_pro_site': True,
    }, name='venueadmin_login'),

    url(r'^app/signup/$', 'login_or_register', {
        'template_name': 'venueadmin/login.html',
        'redirect_default': 'venueadmin_index',
        'is_pro_site': True,
    }, name='venueadmin_signup'),

    url(r'^account/reset/(?P<uidb36>[0-9A-Za-z]{1,13})-(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', 'password_reset_confirm_proxy', {
            'set_password_form': ProSetPasswordForm,
            'template_name': 'venueadmin/password_reset_confirm.html',
            'redirect_url': "venueadmin_password_reset_complete",
        }, name='pro_password_reset_confirm'),
)

urlpatterns += patterns('venueadmin.views',
    (r'^account/social_auth/', include('social_auth.urls')),
)

urlpatterns += patterns('django.contrib.auth.views',

    url(r'^account/reset_complete/$', 'password_reset_complete', {
            'template_name': 'venueadmin/password_reset_message.html',
        }, name='venueadmin_password_reset_complete'),
)


if settings.DEBUG:
    urlpatterns += patterns('',
        # This is for the CSS and static files:
        (r'^media/(?P<path>.*)$', 'django.views.static.serve',
         {'document_root': settings.MEDIA_ROOT}),
    )

# Forward everything else to venueadmin.urls
urlpatterns += common_urlpatterns
