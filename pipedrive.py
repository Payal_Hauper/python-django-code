from django.conf import settings
import requests
import logging
import threading


api_token = settings.PIPEDRIVE_API_TOKEN
deal_stage_id = settings.PIPEDRIVE_STAGE_ID
api_url = "http://api.pipedrive.com/v1/"

# Fixed default value: 12 * 99 EUR
deal_value = 1188
deal_currency = "EUR"

# Pipedrive requires each org and person to have owner. Currently we don't give the user id, so the default user,
# whose api_token is used, is attached as the owner for orgs and persons in Pipedrive
user_id = "267465"


def create_new_deal(org_name, deal_title, person_name, person_email, person_phone):
    """
    This function sends a request to Pipedrive API to create a new organization, person and a deal to events pipeline
    """
    if not api_token:
        return
    thread = threading.Thread(target=do_create_new_deal, args=(org_name, deal_title, person_name, person_email, person_phone))
    thread.start()


def do_create_new_deal(org_name, deal_title, person_name, person_email, person_phone):
    # Create organization with this org name
    post_data = {'name': org_name}
    r = requests.post(api_url + "organizations?api_token=" + api_token, data=post_data)
    resp = r.json()
    if not resp['success']:
        logging.error('Could not create org in Pipedrive')
        return
    org_id = resp['data']['id']

    # Create contact person for this org
    person_id = create_new_person(person_name, person_email, person_phone, org_id)

    if not person_id:
        logging.error('Could not create person in Pipedrive')
        return

    # Create deal for this organization
    post_data = {'title': deal_title,
                 'value': deal_value,
                 'currency': deal_currency,
                 'user_id': user_id,
                 'person_id': person_id,
                 'org_id': org_id,
                 'stage_id': deal_stage_id,
    }
    r = requests.post(api_url + "deals?api_token=" + api_token, data=post_data)
    if not r.json()['success']:
        logging.error('Could not create the deal in Pipedrive')
        return
    logging.info('Deal in Pipedrive created')


def create_new_person(person_name, person_email, person_phone, org_id):
    """
    This function sends a request to Pipedrive API to create a new person
    """

    # Create deal for this organization
    post_data = {'name': person_name,
                 'email': person_email,
                 'phone': person_phone,
                 'org_id': org_id,
    }
    r = requests.post(api_url + "persons?api_token=" + api_token, data=post_data)
    resp = r.json()
    if not resp['success']:
        return False
    return resp['data']['id']
